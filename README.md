# Modules for Perl5 Pheix CMS

## Overview

**Pheix CMS** is a MIT-licence compliant, Perl5 driven Content Management System. It is extremely lightweight, simple and customizable.

This version is on **v2.11 branch** with:

* Feedbck::Casual
* Fileupld::Generic
* Gallery::System
* Lightshop::Sales
* News::Archive
* Remote::FS
* Rss::Yandex
* Shopcat::V3
* Weblog::Static
* Xaoslab::EEG

## Links & credits

Check out [Perl5 Pheix site](http://perl.pheix.org).

**LICENCE** — a common form of the [MIT License](https://opensource.org/licenses/MIT);

**README.md** — you're looking at.

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
