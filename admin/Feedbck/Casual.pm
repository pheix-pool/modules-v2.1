package Feedbck::Casual;

use strict;
use warnings;

##############################################################################
#
# File   :  Casual.pm
#
##############################################################################
#
# Главный пакет управления обратной связью в административной части CMS Pheix
#
##############################################################################

BEGIN { }

use POSIX;

#------------------------------------------------------------------------------

my $MODULE_NAME       = "Feedbck";
my $MODULE_FOLDER     = "admin/libs/modules/$MODULE_NAME/";
my $ADMIN_SKIN_PATH   = 'admin/skins/classic_.txt';
my $MODULE_DB_FILE    = "conf/system/feedback.tnk";

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_feedback_startpage ()
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода сообщений обратной связи из CMS.
#
#------------------------------------------------------------------------------

sub show_feedback_startpage {
    open( FCFH, "<$ADMIN_SKIN_PATH" );
    my @scheme = <FCFH>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $content = &_inner_feedback_startpage(@_);
            $scheme[$cnt] =~ s/\%content\%/$content/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close FCFH;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_feedback_startpage ()
#------------------------------------------------------------------------------
#
# Нативная функция вывода главной страницы.
#
#------------------------------------------------------------------------------

sub _inner_feedback_startpage {

    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr = Config::ModuleMngr::getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $feedback_content_variable =
      qq~<div class=admin_header>$module_descr</div>~.&showUpdateHint($_[4],$_[3],$_[2]);

    my $curr_page = 0;

    if ( -e "$MODULE_DB_FILE" ) {

        open( TDATFILE, "<$MODULE_DB_FILE" );
        my @feedback_on_curr_page = sort { $b cmp $a } <TDATFILE>;
        close TDATFILE;

        # Вывод раздела в список

        if ( $#feedback_on_curr_page > -1 ) {
            my $feedback_counter = 0;
            $feedback_content_variable .= qq~
                <table cellspacing="0" cellpadding="10" width="100%" class="tabselect">
                ~;
            for (
                my $inxonpage = 0 ;
                $inxonpage <= $#feedback_on_curr_page ;
                $inxonpage++
              )
            {
                my $rand    = rand();
                my @tmp_arr = split( /\|/, $feedback_on_curr_page[$inxonpage] );
                my $bgcolor = 'class="tr01"';
                if ( $feedback_counter % 2 == 0 ) { $bgcolor = 'class="tr02"'; }
                $tmp_arr[$#tmp_arr] =~ s/[\n]+//g;

                my $updownlinks = '';
                if ( $#feedback_on_curr_page > 0 ) {
                    my $feedbacksortID = $tmp_arr[0];
                    $feedbacksortID =~ s/[\D]+//s;
                    $updownlinks = qq~
                        <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admfeedbackmoveup&amp;sgid=$feedbacksortID&amp;page=$curr_page"
                           class="fa-action-link">
                           <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>&nbsp;
                        <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admfeedbackmovedown&amp;sgid=$feedbacksortID&amp;page=$curr_page"
                           class="fa-action-link">
                           <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>
                        ~;
                    if ( $inxonpage == 0 ) {
                        $updownlinks = qq~
                            <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>&nbsp;
                            <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admfeedbackmovedown&amp;sgid=$feedbacksortID&amp;page=$curr_page"
                               class="fa-action-link">
                               <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>~;
                    }
                    if ( $inxonpage == $#feedback_on_curr_page ) {
                        $updownlinks = qq~
                            <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admfeedbackmoveup&amp;sgid=$feedbacksortID&amp;page=$curr_page"
                               class="fa-action-link">
                               <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>&nbsp;
                            <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
                    }
                }
                else {
                    $updownlinks = qq~
                        <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>&nbsp;
                        <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                    ~;
                }

                $feedback_content_variable .= qq~
                    <tr $bgcolor style="height:20px;">
                    <td width="10%" class="standart_text" align="center">$updownlinks</td>
                    <td width="80%" class="standart_text" style="font-size:11pt;"><b>
                        <span style="color:#C0C0C0">$tmp_arr[2],</span> $tmp_arr[4] 
                            <span style="color:#707070">&lt;<a href="mailto:$tmp_arr[5]" class="maillink" style="font-size:12pt">$tmp_arr[5]</a>&gt;</span></b>
                        <div class=cmstext style="padding-top:10px; font-size:10pt; color:#606060">$tmp_arr[7]</div>
                        <div class=cmstext style="font-size:10pt; color:#A0A0A0">$tmp_arr[8]</div></td>
                    <td width="10%" class="standart_text" align=right>
                        <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=delfeedback&amp;messid=$tmp_arr[1]','Вы уверены, что хотите удалить сообщение от пользователя $tmp_arr[4] безвозвратно?')" class="fa-action-link"><i class="fa fa-trash" title="Удалить сообщение"></i></td></tr>~;
                $feedback_counter++;
            }
            $feedback_content_variable .= qq~</table>~;
        }
        else {
            $feedback_content_variable .= qq~
                <p class=error>Список записей пуст -&nbsp;
                файл $MODULE_DB_FILE пуст&nbsp;
                (\$#feedback_on_curr_page = $#feedback_on_curr_page)!</p>
            ~;
        }

    }    # if (-e $MODULE_DB_FILE)
    else {
        $feedback_content_variable .= qq~
            <p class=error>Список записей пуст -&nbsp;
            файл $MODULE_DB_FILE не найден!</p>
        ~;
    }
    return $feedback_content_variable;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_feedback_date_format ()
#------------------------------------------------------------------------------
#
# Форматированная дата.
#
#------------------------------------------------------------------------------

sub _inner_feedback_date_format {
    my $date = localtime;
    $date =~ s/[\.\:\,]//g;
    $date =~ s/[\s]+/ /g;
    my @date_array = split( / /, $date );
    @date_array = ( $date_array[2], $date_array[1], $date_array[4] );
    if ( $date_array[1] eq 'Jan' ) { $date_array[1] = 'Январь' }
    if ( $date_array[1] eq 'Feb' ) { $date_array[1] = 'Февраль' }
    if ( $date_array[1] eq 'Mar' ) { $date_array[1] = 'Март' }
    if ( $date_array[1] eq 'Apr' ) { $date_array[1] = 'Апрель' }
    if ( $date_array[1] eq 'May' ) { $date_array[1] = 'Май' }
    if ( $date_array[1] eq 'Jun' ) { $date_array[1] = 'Июнь' }
    if ( $date_array[1] eq 'Jul' ) { $date_array[1] = 'Июль' }
    if ( $date_array[1] eq 'Aug' ) { $date_array[1] = 'Август' }
    if ( $date_array[1] eq 'Sep' ) { $date_array[1] = 'Сентябрь' }
    if ( $date_array[1] eq 'Oct' ) { $date_array[1] = 'Октябрь' }
    if ( $date_array[1] eq 'Nov' ) { $date_array[1] = 'Ноябрь' }
    if ( $date_array[1] eq 'Dec' ) { $date_array[1] = 'Декабрь' }
    return "$date_array[0] $date_array[1], $date_array[2]";
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_feedback_delete ()
#------------------------------------------------------------------------------
#
# Удаление сообщения из БД.
#
#------------------------------------------------------------------------------

sub _inner_feedback_delete {
    my $retcontent = '';
my $retcode = 0;
my $messid = $_[0];
    open( TDDAATFH, "<$MODULE_DB_FILE" );
    my @indexes = <TDDAATFH>;
    close(TDDAATFH);
    my $count = 0;
    foreach ( my $kinx = 0 ; $kinx <= $#indexes ; $kinx++ ) {
        my $check = $indexes[$kinx];
        $check =~ s/[\n]+//g;
        if ( $check ne '' ) { $count++; }
    }
    if ( $count >= 1 ) {
        for ( my $i = 0 ; $i <= $#indexes ; $i++ ) {
            my @temp_arr = split( /\|/, $indexes[$i] );
            if ( $messid eq $temp_arr[1] ) {
                my @ostatok   = @indexes[ $i + 1 .. $#indexes ];
                my @pre_array = @indexes[ 0 .. $i - 1 ];
                @indexes   = ( @pre_array, @ostatok );
                $retcode = 1;
                last;
            }
        }
        if ($retcode == 1) {
            if (@indexes) {
                open( TDDAATFH, ">$MODULE_DB_FILE" );
                seek( TDDAATFH, 0, 0 );
                flock( TDDAATFH, 2 );
                print TDDAATFH @indexes;
                truncate( TDDAATFH, tell(TDDAATFH) );
                flock( TDDAATFH, 8 ); 
                close(TDDAATFH);
            } else {
                if ( ( -e "$MODULE_DB_FILE" ) ) {
                    unlink("$MODULE_DB_FILE");
                }
            }
        } else {
            $retcontent = qq~
                <p class=hintcont>Сообщение <font class=hintspan>$messid</font> не найдено в базе данных!</p>
                ~;
        }
    }

    # $count > 1
    else {
        if ( ( -e "$MODULE_DB_FILE" ) ) {
            unlink("$MODULE_DB_FILE");
            $retcode = 1;
        } else {
             $retcontent = qq~
                <p class=hintcont>Ошибка при удалении <font class=hintspan>$MODULE_DB_FILE</font><br>
                Файл не найден!</p>
                ~;
        }
    }
    if ($retcode == 1) {
     $retcontent = qq~<p class=hintcont>База данных сообщений обратной связи успешно обновлена!</p>~;
    } else {
        ;
    }
    return($retcontent,$retcode);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_feedback_getdetails ()
#------------------------------------------------------------------------------
#
# Детализация информации сообщения из БД.
#
#------------------------------------------------------------------------------

sub _inner_feedback_getdetails {
    open( TIFH, "+<$MODULE_DB_FILE" );
    my @indexes  = <TIFH>;
    my $mod_time = time;
    my $date     = &_inner_feedback_date_format();
    for ( my $i = 0 ; $i <= $#indexes ; $i++ ) {
        my @temp_arr = split( /\|/, $indexes[$i] );
        if ( $_[0] eq $temp_arr[1] ) { return @temp_arr; }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_files_array ()
#------------------------------------------------------------------------------
#
# Получение списка файлов некоторого каталога.
#
#------------------------------------------------------------------------------

sub get_files_array {
    my $dir = $_[0];
    $dir =~ s/\\/\//g;
    opendir DIR, $dir;
    my @files = grep { ( !/^\.+$/ ) and !( -d "$dir/$_" ) } readdir DIR;
    closedir DIR;
    return @files;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_feedback_sgmoveup ()
#------------------------------------------------------------------------------
#
# Функция передвижения записи в блоге вверх по списку.
#
#------------------------------------------------------------------------------

sub _inner_feedback_sgmoveup {

    if ( -e "$MODULE_DB_FILE" ) {
        open( TIFH, "+<$MODULE_DB_FILE" );
        my @cats_page = sort { $b cmp $a } <TIFH>;

        for ( my $i = 0 ; $i <= $#cats_page ; $i++ ) {
            my @tmp = split( /\|/, $cats_page[$i] );

            #print "<h5>$tmp[0]:$i</h5>";
            if ( $tmp[0] == $_[0] ) {
                my @tmp2 = split( /\|/, $cats_page[ $i - 1 ] );
                my $buf = $tmp2[0];
                $tmp2[0] = $tmp[0];
                $tmp[0]  = $buf;
                $cats_page[ $i - 1 ] = join( "|", @tmp2 );
                $cats_page[$i] = join( "|", @tmp );
                last;
            }
        }

        seek( TIFH, 0, 0 );
        flock( TIFH, 2 );
        print TIFH @cats_page;
        truncate( TIFH, tell(TIFH) );
        flock( TIFH, 8 );
        close(TIFH);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_feedback_sgmovedown ()
#------------------------------------------------------------------------------
#
# Функция передвижения записи в блоге вниз по списку.
#
#------------------------------------------------------------------------------

sub _inner_feedback_sgmovedown {

    if ( -e "$MODULE_DB_FILE" ) {
        open( TIFH, "+<$MODULE_DB_FILE" );
        my @cats_page = sort { $b cmp $a } <TIFH>;

        for ( my $i = 0 ; $i <= $#cats_page ; $i++ ) {
            my @tmp = split( /\|/, $cats_page[$i] );

            #print "<h4>$tmp[0]</h4>";
            if ( $tmp[0] == $_[0] ) {
                my @tmp2 = split( /\|/, $cats_page[ $i + 1 ] );
                my $buf = $tmp2[0];
                $tmp2[0] = $tmp[0];
                $tmp[0]  = $buf;
                $cats_page[ $i + 1 ] = join( "|", @tmp2 );
                $cats_page[$i] = join( "|", @tmp );
                last;

            }
        }
        seek( TIFH, 0, 0 );
        flock( TIFH, 2 );
        print TIFH @cats_page;
        truncate( TIFH, tell(TIFH) );
        flock( TIFH, 8 );
        close(TIFH);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showUpdateHint ()
#------------------------------------------------------------------------------
#
# Функция показа jgrowl-сообщения.
#
#------------------------------------------------------------------------------

sub showUpdateHint {
    my $hint      = '';
    my $printflag = $_[0];
    my $procstat  = $_[1];
    my $procmess  = $_[2];
    $procmess =~ s/[\"\r\n]//g;
    $procmess =~ s/[\s]+/ /g;
    $procmess =~ s/(class=error)|(class=status)|(class=standart_text)/class=hintcont/g;
    my $ipdatetime = '';

    my $jgrowlhint = qq~
        <script type="text/javascript">
       (function(\$){
  \$(document).ready(function(){
  \$.jGrowl.defaults.closer = false;
  \$('\#updatehint').jGrowl("<div class=update_hint>\\
  %hint_content%
  </div>",
      { 
                life: 5000,
                theme: 'updatehint',
                speed: 'slow',
                animateOpen: {
                    height: "show",
                },
                animateClose: {
                    height: "hide",
                }
  });
  });
      })(jQuery);
    </script>
    <div id="updatehint" class="top-right" style="display:inline; margin-top:15px;"></div>
   ~;
    if (localtime =~ /([\d]+:[\d]+:[\d]+)/) {
        $ipdatetime = "$1";
    }
    my $message = qq~<p class=hinthead>$ipdatetime - Каталог успешно обновлен</p>~;
    if ($printflag == 1) {
        if ($procstat == 0) {
            # Операция завершилась ошибкой 
            $message = qq~<p class=hinthead>$ipdatetime - Операция завершилась ошибкой</p>~;
        }
        $hint = "$message\\\n$procmess\\";
    } else {
        if ($procstat == 1) {
            $hint = $message;
        }
    }
    $jgrowlhint =~ s/%hint_content%/$hint/g;
    return $jgrowlhint;
}

#------------------------------------------------------------------------------

END { }

1;
