if (   $access_modes[2] == 1
    || $access_modes[2] == 2 )    # actions compatible with access modes: r,rw
{

    # start of read-only actions

    if ( $env_params[1] eq 'admfeedback' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Feedbck::Casual::show_feedback_startpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ), 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    # end of read-only actions

    if ( $access_modes[2] == 2 )  # actions compatible just with access mode: rw
    {

        # start of read-write actions

        if ( $env_params[1] eq 'delfeedback' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                my @returnstatus = ( '', 0 );
                print $co->header( -type => 'text/html; charset=UTF-8' );
                @returnstatus = Feedbck::Casual::_inner_feedback_delete( $env_params[2] );
                Feedbck::Casual::show_feedback_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $returnstatus[0],
                    $returnstatus[1],
                     1 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admfeedbackmovedown' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Feedbck::Casual::_inner_feedback_sgmovedown(
                        $env_params[2] );
                }
                Feedbck::Casual::show_feedback_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ), 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admfeedbackmoveup' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Feedbck::Casual::_inner_feedback_sgmoveup( $env_params[2] );
                }
                Feedbck::Casual::show_feedback_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ), 0 )
                  ;
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        # end of read-write actions

    }    # Access modes: rw
}    # Access modes: r