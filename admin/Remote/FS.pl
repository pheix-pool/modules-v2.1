if ($access_modes[2] == 1 || $access_modes[2] == 2) # actions compatible with access modes: r,rw 
{

  # start of read-only actions

  if ($env_params[1] eq 'admrfs')
  {
    Pheix::Session::delSessionByTimeOut();
    if ($check_auth==1) {
	 print $co->header(-type=>'text/html; charset=UTF-8');
	 my $got_level = $env_params[2];
	 if ($got_level !~ /^[\d]+$/) {$got_level = -1;}
	 Remote::FS::showRFSHomePage($env_params[0],Pheix::Session::getCryptLoginBySid($env_params[0]),$got_level,0);
	 exit;
	}
    print $co->header(-type=>'text/html; charset=UTF-8');
    Pheix::Tools::showLoginPage();
    exit;
  };

  if ($env_params[1] eq 'admrfscommand')
  {
    Pheix::Session::delSessionByTimeOut();
    if ($check_auth==1) {
	 print $co->header(-type=>'text/html; charset=UTF-8');
	 my $got_command = $co->param("rfscommand");
	 Remote::FS::showRFSBrowse($env_params[0],Pheix::Session::getCryptLoginBySid($env_params[0]),$got_command,0);
	 exit;
	}
    print $co->header(-type=>'text/html; charset=UTF-8');
    Pheix::Tools::showLoginPage();
    exit;
  };  

  # end of read-only actions

  if ($access_modes[2] == 2) # actions compatible just with access mode: rw 
   {

   # start of read-write actions

   if ($env_params[1] eq 'admrfsdel')
   {
    Pheix::Session::delSessionByTimeOut();
    if ($check_auth==1) {
	 print $co->header(-type=>'text/html; charset=UTF-8');
	 print "admrfsdel action debug message";
	 exit;
	}
    print $co->header(-type=>'text/html; charset=UTF-8');
    Pheix::Tools::showLoginPage();
    exit;
   };

   # end of read-write actions

   } # Access modes: rw
} # Access modes: r