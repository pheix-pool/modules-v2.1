package Remote::FS;

use strict;
use warnings;

BEGIN {
    #for all servers
    use lib '../'; # lib path for work mode
}

##############################################################################
#
# File   :  FS.pm
#
##############################################################################
#
# Главный пакет управления удаленной ФС в адм. части CMS Pheix
#
##############################################################################

use POSIX;
use File::Path;
use Encode;

my $MODULE_NAME = "Remote";
my $MODULE_FOLDER = "admin/libs/modules/$MODULE_NAME/";
my $MODULE_DB_FILE  = "conf/system/remotefs.tnk";
my $INSTALL_FILE_PATH = "conf/system/install.tnk";

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModPwrStatus ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить статус модуля (включен или выключен).
# Функция возвращает статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub getModPwrStatus {
    my $file_to_parse = $INSTALL_FILE_PATH;
    if ( -e $file_to_parse ) {
        open( CFGFH, "<$file_to_parse" );
        my @filecontent = <CFGFH>;
        close(CFGFH);
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my @tmp = split( /\|/, $filecontent[$admplinx] );
            if ( $tmp[2] eq $MODULE_NAME ) { return $tmp[4]; }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showRFSHomePage ( $sesid, $crypted_login, $level_num, $print_flag, $mess )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# $crypted_login - зашифрованный идентификатор пользователя.
# $level_num - вложенность относительно каталога установки pheix.
# $print_flag - флаг управления печатью.
# $mess - дополнительное сообщение, выводимое по флагу.
# Вывод главной страницы административной части.
# Функция возвращает содержимое главной страницы административной части в
# HTML формате.
#
#------------------------------------------------------------------------------

sub showRFSHomePage {
 my $contentblock = '';
 open(FCFH,"<admin/skins/classic_.txt");
 my @scheme = <FCFH>;
 my $cnt = 0;
 my $sesion_id = $_[0];
 while($cnt<=$#scheme) {
    $scheme[$cnt] = Pheix::Tools::fillCommonTags($sesion_id,$scheme[$cnt]);
    if ($scheme[$cnt] =~ /\%content\%/) {$contentblock = &showRemoteFS(@_); $scheme[$cnt] =~ s/\%content\%/$contentblock/;}
    print $scheme[$cnt];
    $cnt++;
    }
 close(FCFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showRFSBrowse ( $sesid, $crypted_login, $command, $print_flag, $mess )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# $crypted_login - зашифрованный идентификатор пользователя.
# $command - команда.
# $print_flag - флаг управления печатью.
# $mess - дополнительное сообщение, выводимое по флагу.
# Вывод страницы результата выполнения команды на удаленной ФС.
# Функция возвращает содержимое страницы в HTML формате.
#
#------------------------------------------------------------------------------

sub showRFSBrowse {
 my $contentblock = '';
 open(FCFH,"<admin/skins/classic_.txt");
 my @scheme = <FCFH>;
 my $cnt = 0;
 my $sesion_id = $_[0];
 while($cnt<=$#scheme) {
    $scheme[$cnt] = Pheix::Tools::fillCommonTags($sesion_id,$scheme[$cnt]);
    if ($scheme[$cnt] =~ /\%content\%/) {$contentblock = &doRFSBrowse(@_); $scheme[$cnt] =~ s/\%content\%/$contentblock/;}
    print $scheme[$cnt];
    $cnt++;
    }
 close(FCFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showRemoteFS ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Вывод инофрмации об удаленной файловой системе.
# Функция возвращает информацию об удаленной ФС.
#
#------------------------------------------------------------------------------

sub showRemoteFS {
    my $wd_tree = '';
    my $level = $_[2];
    my $working_dir = `pwd`;
    my @workdir_arr = split(/\//, $working_dir);

    if ($level > -1) {
        @workdir_arr = @workdir_arr[0..$level];
        $working_dir = join("/",@workdir_arr)
    }    
    
    for(my $i=0; $i <= $#workdir_arr; $i++) {
        $workdir_arr[$i] = qq~<a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admrfs&amp;level=$i" class="simplelink">$workdir_arr[$i]</a>~;
    }
    
    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr         =
      Config::ModuleMngr::getConfigValue( $MODULE_FOLDER, $xml_description_file,
        "description" );
    my $fsinfocntnt   = qq~<div class=admin_header>$module_descr</div>~;
    $fsinfocntnt   = qq~<div class="cmstext pheix-fs-path" style="margin:20px 0 20px 0;"><a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admrfs&amp;level=-1">Pheix</a> :: ~.join(" /",@workdir_arr)."</div>";
    if ($level == -1) {
        #$wd_tree = `tree -d $working_dir`;
        if (!$wd_tree) {
            $wd_tree = `ls -la $working_dir`;
        }
    } else {
        $wd_tree = `ls -la $working_dir`;
    }
    $wd_tree =~ s/[\n\r]+/<br>/g;
    $wd_tree =~ s/\s/&nbsp;/g;
    $fsinfocntnt .= qq~<div class="cmstext" style="margin:0 0 20px 0;"><tt>$wd_tree</tt></div>~;
    $fsinfocntnt .= qq~<form method=post action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admrfscommand">
        <input type="text" name="rfscommand" class="input" style="width:100%;" value="" placeholder="введите команду">
        <input type="submit" class="button" style="width:150px; margin:10px 0 0 0;" value="Выполнить">
    <form>~;
    return $fsinfocntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doRFSBrowse ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Выполнение команды на удаленной файловой системе.
# Функция возвращает результат выполнения команды на удаленной ФС.
#
#------------------------------------------------------------------------------

sub doRFSBrowse {
    my $wd_tree = '';
    my $command = $_[2];
    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr         =
      Config::ModuleMngr::getConfigValue( $MODULE_FOLDER, $xml_description_file,
        "description" );
    my $fsinfocntnt   = qq~<div class="cmstext" style="margin:20px 0 20px 0;"><a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admrfs&amp;level=-1">Pheix</a> :: $command</div>~;
    if ($command) {
        $wd_tree = `$command`;
        if (!$wd_tree) {
            $wd_tree = "$command successful!";
        }
    }
    $wd_tree =~ s/[\n\r]+/<br>/g;
    $wd_tree =~ s/\s/&nbsp;/g;
    $fsinfocntnt .= qq~<div class="cmstext" style="margin:0 0 20px 0;"><tt>$wd_tree </tt></div>~;
    $fsinfocntnt .= qq~<form method=post action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admrfscommand">
        <input type="text" name="rfscommand" class="input" style="width:100%;" value="" placeholder="введите команду">
        <input type="submit" class="button" style="width:150px; margin:10px 0 0 0;" value="Выполнить">
    <form>~;
    return $fsinfocntnt;
}

#------------------------------------------------------------------------------

END { }

1;
