if (   $access_modes[2] == 1
    || $access_modes[2] ==
    2 )    # actions compatible with access modes: r,rw
{

    # start of read-only actions

    if ( $env_params[1] eq 'admsystemgallery' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $got_page = $env_params[2];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            Gallery::System::sysgallery_startpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $got_page, 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admgalcrobjform' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Gallery::System::_inner_sysgallery_createpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                0, 1 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admgaladdfromshopcat' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Gallery::System::_inner_sysgallery_createpage_fromshopcat2(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $env_params[3] );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admgaleditobj' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Gallery::System::_inner_sysgallery_editpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $env_params[3],
                $co->param('parentcat') == $env_params[3] ? 1 : 0);
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admgaleditphotos' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Gallery::System::sysgallery_editphotopage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admgalgetall' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $gallery_list;
            eval {
                $gallery_list =
                  Gallery::System::getGalleryList( $co->param('term') );
            };
            if ($@) {
                $gallery_list = qq~[{ "label": "Error while fetching galleries list!", "value": "-1", "id": "0" }]~;
            }
            print $gallery_list;
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admgalgetcarousel' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $gallery_carousel;
            my $gallery_id       = $env_params[2];
            if ( $gallery_id !~ /^[\d]+$/ ) { $gallery_id = 0; }
            eval {
                $gallery_carousel =
                  Gallery::System::getGalleryCarousel( $env_params[0],
                    $gallery_id, 0 );
            };
            if ($@) {
                $gallery_carousel =
                  Gallery::System::showGalleryErr( $env_params[0],
                    $gallery_id );
            }
            print $gallery_carousel;
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admgalgetpool' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $gallerypool;
            my $gallery_id       = $env_params[2];
            if ( $gallery_id !~ /^[\d]+$/ ) { $gallery_id = 0; }
            eval {
                $gallerypool =
                  Gallery::System::getGalleryImgPool( $env_params[0],
                    $gallery_id );
            };
            if ($@) {
                $gallerypool =
                  Gallery::System::showGalleryErr( $env_params[0],
                    $gallery_id );
            }
            print $gallerypool;
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    # end of read-only actions

    if ( $access_modes[2] == 2 )  # actions compatible just with access mode: rw
    {

        # start of read-write actions

        if ( $env_params[1] eq 'admgalpostobj' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $uplmess = '';
                my $objID   = time;
                my $objsort = time - 1391000000;
                my $pcat    = $co->param('parentcat');
                my $objname = $co->param('header');
                my $objpath = $co->param('shopcatpath');
                my $objdesc = $co->param('comment');
                my $objgroup = 
                    $co->param('groupattr') eq '1' || 
                    $co->param('groupattr') eq '2' ?
                    $co->param('groupattr') : 0;
                $objdesc =~ s/[\r\n]+/\<br\>/g;
                $objpath =~ s/[\r\n]+//g;
                if ( $objpath !~ /^images\// ) { $objpath = 0; }
                my $objresrv = 0;

                my @rc = 
                    Gallery::System::add_to_gallery( $objsort, $objID, $objpath,
                    $objname, $objdesc, $objgroup, $objresrv );
                if ($objgroup == 0 || $objgroup == 1) {
                    Gallery::System::sysgallery_editphotopage( $env_params[0],
                        Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                        $objID, 0 );
                } elsif ($objgroup == 2) {
                    eval {
                        Shopcat::V3::show_shopcat2_startpage( $env_params[0],
                        Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                        $rc[0], $rc[1], 1, $pcat || 0 );
                    }
                }
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admgalmodobj' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $uplmess;
                my $objID   = $env_params[2];
                my $objdate = $co->param('date');
                my $pcat    = $co->param('parentcat');
                my $objname = $co->param('header');
                my $objdesc = $co->param('comment');
                my $objgroup = 
                    $co->param('groupattr') eq '1' || 
                    $co->param('groupattr') eq '2' ?
                    $co->param('groupattr') : 0;
                $objdesc =~ s/[\r\n]+/\<br\>/g;
                my $objresrv = 0;

                my @rc = Gallery::System::modify_gallery( $objID, $objdate,
                    $objname, $objdesc, $objgroup, $objresrv );
                if ($objgroup == 0 || $objgroup == 1) {
                    if ($pcat > 0) {
                        Gallery::System::sysgallery_editphotopage( $env_params[0],
                            Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                            $objID, 0 );
                    } else {
                        Gallery::System::sysgallery_startpage( $env_params[0],
                            Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                            $env_params[3], 0 );
                    }
                } elsif ($objgroup == 2) {
                    if ($pcat > 0) {
                        eval {
                            Shopcat::V3::show_shopcat2_startpage( $env_params[0],
                                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                                $rc[0], $rc[1], 1, $pcat || 0 );
                        };
                    } else {
                        Gallery::System::sysgallery_startpage( $env_params[0],
                            Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                            $env_params[3], 0 );
                    }
                }                    
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admgaldelobj' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $download_path =
                  Gallery::System::_inner_sysgallery_getpath_by_id(
                    $env_params[2] );
                Gallery::System::delete_gallery( $env_params[2] );
                Gallery::System::accurate_del_files_fromdir( $env_params[2],
                    $download_path );
                Gallery::System::doGalleryGroupErase( $env_params[2] );
                Gallery::System::sysgallery_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    0, 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admgaleditphotomulti' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $uplmess     = '';
                my $objID       = $env_params[2];
                my $download_path =
                  Gallery::System::_inner_sysgallery_getpath_by_id(
                    $env_params[2] );

                my @multiplefiles = $co->param('photofile');
                if (@multiplefiles) {
                    my $autoresize         = $co->param('autoresize');
                    for my $i (0..$#multiplefiles ) {
                        my $got_filename = $multiplefiles[$i];
                        if ( $got_filename ne '' ) {
                            my $fileindex =
                            Gallery::System::getlastuploadindex($objID);
                            if ( $fileindex < 10 ) {
                                $fileindex = "0" . $fileindex;
                            }
                            my @ext_mass = split /\./, $got_filename;
                            my $filename =
                            $objID . "_$fileindex." . $ext_mass[$#ext_mass];

                            if ( $got_filename && $filename =~ /\.jpg$/i ) {
                                $filename = lc($filename);
                                $uplmess .= Gallery::System::download_orig_photo(
                                    $got_filename, $filename, 0,
                                    $autoresize, $objID )
                                . "<br>";
                            } else {
                                $uplmess = '<!--error-->*.png files are not supported';
                                last;
                            }
                        }
                    } #for
                }
                Gallery::System::sysgallery_editphotopage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2], 2, $uplmess );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admgalerase' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $galid = $env_params[2] =~ /[\d]+/ ? $env_params[2] : 0;
                if ($galid > 0) {
                    Gallery::System::doGalleryErase($galid);
                }
                Gallery::System::sysgallery_editphotopage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2], 0, 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admdelimgfromsystemgallery' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $download_path =
                  Gallery::System::_inner_sysgallery_getpath_by_id(
                    $env_params[2] );
                my @images = reverse(
                    Gallery::System::get_sysgallery_img_bynum(
                        $download_path, $env_params[2]
                    )
                );
                for ( my $k = 0 ; $k <= $#images ; $k++ ) {
                    my $file_name = "$env_params[2]_$env_params[3]";
                    if ( $images[$k] =~ /^$file_name\./ ) {
                        unlink("$download_path/$images[$k]");
                        last;
                    }
                }
                Gallery::System::sysgallery_editphotopage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2], 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admgaladdtogroup' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $rc = Gallery::System::doGalToGroupAdd($co, $env_params[2]);
                Gallery::System::sysgallery_editphotopage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2], 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admsysgalmovedown' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Gallery::System::sysgallery_sgmovedown( $env_params[2] );
                }
                Gallery::System::sysgallery_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[3], 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admsysgalmoveup' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Gallery::System::sysgallery_sgmoveup( $env_params[2] );
                }
                Gallery::System::sysgallery_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[3], 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admgalgrouperase' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $galid = $env_params[2] =~ /[\d]+/ ? $env_params[2] : 0;
                if ($galid > 0) {
                    Gallery::System::doGalleryGroupErase($galid);
                }
                Gallery::System::sysgallery_editphotopage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2], 0, 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admgaldelfromgroup' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $galid = $env_params[2] =~ /[\d]+/ ? $env_params[2] : 0;
                my $galtd = $env_params[3] =~ /[\d]+/ ? $env_params[3] : 0;
                if ($galid > 0) {
                    Gallery::System::doGalleryGalFromGroup( $galid, $galtd );
                }
                Gallery::System::sysgallery_editphotopage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2], 0, 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        # end of read-write actions

    }    # Access modes: rw
}    # Access modes: r