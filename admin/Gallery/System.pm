package Gallery::System;

use strict;
use warnings;

BEGIN {
    #for all servers
    use lib '../';    # lib path for work mode

    # lib path for systax check during module install
    use lib 'admin/libs/modules/'; 
    use lib 'admin/libs/extlibs/';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';
}

##############################################################################
#
# File   :  System.pm
#
##############################################################################
#
# Модуль управления системными галереями изображений
#
##############################################################################

use POSIX;
use File::Path;
use Encode;
use Config::ModuleMngr qw(getSingleSetting getModXml getConfigValue);
use Pheix::Tools qw(getDateUpdate);
use JSON;
use Fcntl qw(:DEFAULT :flock :seek);

my $MODULE_NAME       = "Gallery";
my $MODULE_FOLDER     = "admin/libs/modules/$MODULE_NAME/";
my $MODULE_DB_FILE    = "conf/system/sysgallery.tnk";
my $INSTALL_FILE_PATH = "conf/system/install.tnk";
my $ADMIN_SKIN_PATH   = 'admin/skins/classic_.txt';
my $IMAGES_ROOTPATH   = 'images/systemgallery/upload';
my $RESIZEHRZ         = 600;
my $RESIZEVRT         = 400;

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_power_status_for_module ()
#------------------------------------------------------------------------------
#
# Получить статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub get_power_status_for_module {
    if ( -e $INSTALL_FILE_PATH ) {
        open( my $inputfh, "<", $INSTALL_FILE_PATH );
        my @filecontent = <$inputfh>;
        close($inputfh);
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my @tmp = split( /\|/, $filecontent[$admplinx] );
            if ( $tmp[2] eq $MODULE_NAME ) { return $tmp[4]; }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# sysgallery_startpage ()
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода галереи из CMS.
#
#------------------------------------------------------------------------------

sub sysgallery_startpage {
    my ($sesion_id) = @_;
    open( my $inputfh, "<", $ADMIN_SKIN_PATH );
    my @scheme = <$inputfh>;
    close($inputfh);
    my $cnt = 0;
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $contentblock = &_inner_sysgallery_startpage(@_);
            $scheme[$cnt] =~ s/\%content\%/$contentblock/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# sysgallery_editphotopage ()
#------------------------------------------------------------------------------
#
# Промежуточная функция редактирования фотографий объекта галереи.
#
#------------------------------------------------------------------------------

sub sysgallery_editphotopage {
    my ($sesion_id) = @_;
    open( my $inputfh, "<", $ADMIN_SKIN_PATH );
    my @scheme = <$inputfh>;
    close($inputfh);
    my $cnt = 0;
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $contentblock = &_inner_sysgallery_editphotopage(@_);
            $scheme[$cnt] =~ s/\%content\%/$contentblock/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_files_array ()
#------------------------------------------------------------------------------
#
# Получение списка файлов некоторого каталога.
#
#------------------------------------------------------------------------------

sub get_files_array {
    my ($dir) = @_;
    $dir =~ s/\\/\//g;
    opendir( DIR, $dir );
    my @files = grep { ( !/^\.+$/ ) && !( -d "$dir/$_" ) } readdir DIR;
    closedir(DIR);
    return @files;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# accurate_del_files_fromdir ()
#------------------------------------------------------------------------------
#
# Корректное удаление фото, связанных с объектом.
#
#------------------------------------------------------------------------------

sub accurate_del_files_fromdir {
    my ( $file, $dir ) = @_;
    my @files = &get_files_array($dir);
    for my $kinx ( 0 .. $#files ) {
        my @tmp = split /\_/, $files[$kinx];
        if ( $tmp[0] eq $file ) {
            unlink $dir . "/$files[$kinx]";
        }
    }
    my @existedfiles = &get_files_array($dir);
    if ( !@existedfiles ) {
        rmdir($dir);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_sysgallery_img_bynum ()
#------------------------------------------------------------------------------
#
# Получение списка объектов по ID и каталогу.
#
#------------------------------------------------------------------------------

sub get_sysgallery_img_bynum {
    my ( $dir, $id ) = @_;
    my @object_images = ();
    my @files         = &get_files_array($dir);
    for my $kinx ( 0 .. $#files ) {
        my @tmp = split( /\_/, $files[$kinx] );
        if ( $tmp[0] eq $_[1] ) {
            push( @object_images, $files[$kinx] );
        }
    }
    @object_images = sort { $a cmp $b } @object_images;
    return @object_images;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# download_orig_photo ()
#------------------------------------------------------------------------------
#
# Загрузить фотограция для объекта.
#
#------------------------------------------------------------------------------

sub download_orig_photo {
    my ($fieldpoiter, $filename, $reserved, $doresize, $galid) = @_;
    my $res;
    if ( $fieldpoiter ) {
        my $download_path = _inner_sysgallery_getpath_by_id( $galid );
        if ( !( -e $download_path ) ) {
            mkpath($download_path);
        }
        $res = doMultiFileUpload( $download_path, $fieldpoiter, $filename );
        chmod 0664, "$download_path/$filename";
        if ( $doresize == 1 ) {
            auto_foto_resize("$download_path/$filename");
        }
    }
    return $res;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# auto_foto_resize ()
#------------------------------------------------------------------------------
#
# Автоматическое изменение размеров фото.
#
#------------------------------------------------------------------------------

sub auto_foto_resize {
    use Image::Magick;
    my ($file_path) = @_;
    my $image_index = 0;
    if (-e $file_path) {
        my $image = new Image::Magick;
        my ($globe_x, $globe_y, $size, $format) = $image->Ping($file_path);
        $image->Read($file_path);
        # Horizontal photo
        if ($globe_x > $globe_y) {
            my $think_width = getSingleSetting( $MODULE_NAME, 0, 'resizehznvalue' );
            if ( $think_width <= 0 || $think_width !~ /^[\d]+$/gsx ) {
                $think_width = $RESIZEHRZ;
            }
            my $scale_koeff = ($globe_x/$think_width);
            if (($scale_koeff < 1)) {$scale_koeff = 1;}
            my $scaleheight = floor(($globe_y/$scale_koeff));
            my $scalewidth = floor(($globe_x/$scale_koeff));
            my $status = $image->Scale( width=>$scalewidth, height=>$scaleheight);
            if (!$status) {
                $image->Write($file_path);
            }
        }
        # Vertical photo
        if ($globe_x <= $globe_y) {
            my $think_width = getSingleSetting( $MODULE_NAME, 0, 'resizevrtvalue' );
            if ( $think_width <= 0 || $think_width !~ /^[\d]+$/gsx ) {
                $think_width = $RESIZEVRT;
            }
            my $scale_koeff = ($globe_x/$think_width);
            if (($scale_koeff < 1)) {$scale_koeff = 1;}
            my $scaleheight = floor(($globe_y/$scale_koeff));
            my $scalewidth = floor(($globe_x/$scale_koeff));
            my $status = $image->Scale( width=>$scalewidth, height=>$scaleheight);
            if (!$status) {
                $image->Write($file_path);
            }
        }
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_sysgallery_details_by_ID ()
#------------------------------------------------------------------------------
#
# Получение массива детализации галереи по ID.
#
#------------------------------------------------------------------------------

sub get_sysgallery_details_by_ID {
    my ($galid) = @_;
    open( my $inputfh, "<", $MODULE_DB_FILE );
    my @indexes = <$inputfh>;
    close($inputfh);
    for my $l ( 0 .. $#indexes ) {
        my @tmp = split( /\|/, $indexes[$l] );
        $tmp[1] =~ s/[\D]+//gsx;
        if ( $tmp[1] eq $galid ) { return @tmp; }
    }
    return;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getlastuploadindex ()
#------------------------------------------------------------------------------
#
# Получение индекса последней фотографии.
#
#------------------------------------------------------------------------------

sub getlastuploadindex {
    my $lastindex = 1;
    my @indexarray;
    my $download_path = &_inner_sysgallery_getpath_by_id( $_[0] );
    my @galleryfiles = &get_sysgallery_img_bynum( $download_path, $_[0] );
    for my $i ( 0 .. $#galleryfiles ) {
        if ( $galleryfiles[$i] =~ /\_([\d]+)\./sx ) {
            push( @indexarray, $1 );
        }
    }
    @indexarray = sort { $b <=> $a } @indexarray;
    if ( $#indexarray > -1 ) {
        $lastindex = $indexarray[0] + 1;
    }
    return $lastindex;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# add_to_gallery ()
#------------------------------------------------------------------------------
#
# Добавление объекта в БД.
#
#------------------------------------------------------------------------------

sub add_to_gallery {
    my $outputfh;
    if ( -e $MODULE_DB_FILE ) {
        open( $outputfh, "+<", $MODULE_DB_FILE );
    }
    else {
        open( $outputfh, ">", $MODULE_DB_FILE );
    }
    my @indexes       = <$outputfh>;
    my $date          = getDateUpdate();
    my $gallery_name  = $_[3];
    my $gallery_descr = $_[4];
    $gallery_name =~ s/\"/\&quot;/g;
    $gallery_descr =~ s/\"/\&quot;/g;
    push( @indexes,
        "$_[0]|$_[1]|$date|$_[2]|$gallery_name|$gallery_descr|$_[5]|$_[6]\n" );
    seek( $outputfh, 0, 0 );
    flock( $outputfh, LOCK_EX );
    print $outputfh @indexes;
    truncate( $outputfh, tell($outputfh) );
    flock( $outputfh, LOCK_UN );
    close($outputfh);
    return (qq~<p class=hintcont>Список галерей успешно обновлен!</p>~,1);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# modify_gallery ()
#------------------------------------------------------------------------------
#
# Изменение объекта в БД.
#
#------------------------------------------------------------------------------

sub modify_gallery {
    if ( -e $MODULE_DB_FILE ) {
        open( my $outputfh, "+<", $MODULE_DB_FILE );
        my @indexes = <$outputfh>;
        for my $kinx ( 0 .. $#indexes ) {
            my @tmp = split( /\|/, $indexes[$kinx] );
            $tmp[1] =~ s/[\D]+//g;
            if ( $tmp[1] eq $_[0] ) {
                $indexes[$kinx] =
                  "$tmp[0]|$_[0]|$_[1]|$tmp[3]|$_[2]|$_[3]|$_[4]|$_[5]\n";
            }
        }
        seek( $outputfh, 0, 0 );
        flock( $outputfh, LOCK_EX );
        print $outputfh @indexes;
        truncate( $outputfh, tell($outputfh) );
        flock( $outputfh, LOCK_UN );
        close($outputfh);
        return (qq~<p class=hintcont>Список галерей успешно обновлен!</p>~,1);
    }
    else {
        return (qq~<p class=hintcont>Файл БД галерей <b>$MODULE_DB_FILE</b> не найлен!</p>~,0);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# delete_gallery ()
#------------------------------------------------------------------------------
#
# Удаление объекта из БД.
#
#------------------------------------------------------------------------------

sub delete_gallery {
    if ( -e $MODULE_DB_FILE ) {
        open( my $outputfh, "+<", $MODULE_DB_FILE );
        my @indexes = <$outputfh>;
        my $count   = 0;
        for my $kinx ( 0 .. $#indexes ) {
            my $check = $indexes[$kinx];
            $check =~ s/[\r\n]+//gsx;
            if ($check) {
                $count++;
            }
        }
        if ( $count > 1 ) {
            for my $i ( 0 .. $#indexes ) {
                my @temp_arr = split /\|/, $indexes[$i];
                $temp_arr[1] =~ s/[\D]+//gsx;
                if ( $_[0] eq $temp_arr[1] ) {
                    my @ostatok   = @indexes[ $i + 1 .. $#indexes ];
                    my @pre_array = @indexes[ 0 .. $i - 1 ];
                    @indexes = ( @pre_array, @ostatok );
                    last;
                }
            }
            seek( $outputfh, 0, 0 );
            flock( $outputfh, LOCK_EX );
            print $outputfh @indexes;
            truncate( $outputfh, tell($outputfh) );
            flock( $outputfh, LOCK_UN );
            close($outputfh);
        }
        else {
            close($outputfh);
            if ( -e $MODULE_DB_FILE ) {
                unlink $MODULE_DB_FILE;
            }
        }
        return 0;
    }
    else {
        return -1;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# sysgallery_sgmoveup ()
#------------------------------------------------------------------------------
#
# Функция передвижения системной галереи вверх по списку.
#
#------------------------------------------------------------------------------

sub sysgallery_sgmoveup {
    my ($galid) = @_;
    my $_hidev3 = getSingleSetting( $MODULE_NAME, 0, 'hideshopcat' );
    if ( -e $MODULE_DB_FILE ) {
        open my $fh, "+<", $MODULE_DB_FILE;
        my @cats_page = sort { $b cmp $a } <$fh>;
        if ($_hidev3 != 1) {
            for my $i ( 0 .. $#cats_page ) {
                my @tmp = split /\|/, $cats_page[$i];
                if ( $tmp[0] == $galid ) {
                    my @tmp2 = split /\|/, $cats_page[ $i - 1 ];
                    my $buf = $tmp2[0];
                    $tmp2[0] = $tmp[0];
                    $tmp[0]  = $buf;
                    $cats_page[ $i - 1 ] = join( "|", @tmp2 );
                    $cats_page[$i] = join( "|", @tmp );
                    last;
                }
            }
        } else {
            my $_inx;
            for my $i ( 0 .. $#cats_page ) {
                my @tmp = split /\|/, $cats_page[$i];
                if ( $tmp[0] == $galid ) {
                    $_inx = $i;
                    last;
                }
            }
            if ($_inx > 0) {
                my $_inx_rplc;
                for my $i ( 0 .. ($_inx-1) ) {
                    if ($cats_page[$i] !~ /images\/shopcat/) {
                        $_inx_rplc = $i;
                    }
                }
                my @tmp = split /\|/, $cats_page[ $_inx ];
                my @tmp_rplc = split /\|/, $cats_page[ $_inx_rplc ];
                my $buf = $tmp[0];
                $tmp[0] = $tmp_rplc[0];
                $tmp_rplc[0] = $buf;
                $cats_page[ $_inx ] = join( "|", @tmp );
                $cats_page[ $_inx_rplc ] = join( "|", @tmp_rplc );
            }
        }
        seek $fh, 0, 0;
        flock $fh, LOCK_EX;
        print $fh @cats_page;
        truncate $fh, tell $fh;
        flock $fh, LOCK_UN ;
        close $fh;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# sysgallery_sgmovedown ()
#------------------------------------------------------------------------------
#
# Функция передвижения системной галереи вниз по списку.
#
#------------------------------------------------------------------------------

sub sysgallery_sgmovedown {
    my ($galid) = @_;
    my $_hidev3 = getSingleSetting( $MODULE_NAME, 0, 'hideshopcat' );
    if ( -e $MODULE_DB_FILE ) {
        open my $fh, "+<", $MODULE_DB_FILE;
        my @cats_page = sort { $b cmp $a } <$fh>;
        if ($_hidev3 != 1) {
            for my $i ( 0 .. $#cats_page ) {
                my @tmp = split /\|/, $cats_page[$i];
                if ( $tmp[0] == $galid ) {
                    my @tmp2 = split /\|/, $cats_page[ $i + 1 ];
                    my $buf = $tmp2[0];
                    $tmp2[0] = $tmp[0];
                    $tmp[0]  = $buf;
                    $cats_page[ $i + 1 ] = join( "|", @tmp2 );
                    $cats_page[$i] = join( "|", @tmp );
                    last;
                }
            }
        } else {
            my $_inx;
            for my $i ( 0 .. $#cats_page ) {
                my @tmp = split /\|/, $cats_page[$i];
                if ( $tmp[0] == $galid ) {
                    $_inx = $i;
                    last;
                }
            }
            if ($_inx > 0) {
                my $_inx_rplc;
                for my $i ( ($_inx+1) .. $#cats_page ) {
                    if ($cats_page[$i] !~ /images\/shopcat/) {
                        $_inx_rplc = $i;
                        last;
                    }
                }
                my @tmp = split /\|/, $cats_page[ $_inx ];
                my @tmp_rplc = split /\|/, $cats_page[ $_inx_rplc ];
                my $buf = $tmp[0];
                $tmp[0] = $tmp_rplc[0];
                $tmp_rplc[0] = $buf;
                $cats_page[ $_inx ] = join( "|", @tmp );
                $cats_page[ $_inx_rplc ] = join( "|", @tmp_rplc );
            }
        }
        seek $fh, 0, 0 ;
        flock $fh, LOCK_EX;
        print $fh @cats_page;
        truncate $fh, tell $fh;
        flock $fh, LOCK_UN;
        close $fh;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_sysgallery_startpage ()
#------------------------------------------------------------------------------
#
# Основная функция вывода данных из галереи.
#
#------------------------------------------------------------------------------

sub _inner_sysgallery_startpage {
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $process_status     = 0;
    my $SYSGALLERY_content = qq~<div class=admin_header>$module_descr</div>~;
    my $pix_on_page        = 5;
    my $information        = '';
    my $random             = rand();
    my $curr_page          = $_[2];
    my @small_files_array  = ();
    my @pix_on_this_page   = ();
    my $counter            = 0;

    if ( $_[3] == 1 ) {
        my $ipdatetime = '';
        if ( localtime =~ /([\d]+:[\d]+:[\d]+)/ ) { $ipdatetime = "$1:"; }
        $information =
"<div class=status>$ipdatetime Информация успешно обновлена!</div><br>\n";
    }
    else {
        if ( $_[3] == 2 ) {
            $information = "<div class=error>$_[4]</div><br>\n";
        }
    }

    $SYSGALLERY_content .=
qq~$information<div style="margin: 0px 0px 2px 0px; width:100%" class="container"><a href="javascript:doSesChckLoad('$_[0]', '$ENV{SCRIPT_NAME}', '&amp;action=admgalcrobjform&amp;gallerytype=0');">Создать системную галерею фотографий</a></div>~;

    if ( -e $MODULE_DB_FILE ) {
        my $_hideflag = getSingleSetting( $MODULE_NAME, 0, 'hideshopcat' );
        my $_hidealrt = qq~<div class="hide-policy-block stxt">Для вашего удобства мы скрыли некоторые объекты на этой странице. Изменить политики отображения можно в <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admsetup&amp;name=$MODULE_NAME">настройках модуля</a>.</div>~;
        open( my $inputfh, "<", $MODULE_DB_FILE );
        @small_files_array = sort { $b cmp $a } <$inputfh>;
        close($inputfh);
        if ($_hideflag == 1) {
            @small_files_array = grep { $_ !~ /images\/shopcat/ } @small_files_array;
        }

        # Форсированный выход - не показываем скрытые галереи

        if (!@small_files_array) {
            return $SYSGALLERY_content.$_hidealrt;
        }

        # Формирование навигации по страницам

        my $pages = floor( $#small_files_array / $pix_on_page );
        if ($pages < 0) {$pages = 0;} 

        if (   ( $pages * $pix_on_page <= $#small_files_array )
            || ( $pages < 1 ) )
        {
            $pages++;
        }

        if ( $_[2] >= $pages ) { $curr_page = 0; }
        my $page_linx = '&nbsp;-&nbsp;';
        for ( my $i = 1 ; $i <= $pages ; $i++ ) {
            if ( ( $i - 1 ) != $curr_page ) {
                $page_linx .=
"<a href=\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admsystemgallery&amp;page="
                  . ( $i - 1 )
                  . "\" class=admpageslink>"
                  . $i
                  . "</a>&nbsp;-&nbsp;";
            }
            else {
                $page_linx .=
                    "<span class=lightadminpagelink>"
                  . $i
                  . "</span>&nbsp;-&nbsp;";
            }
        }

        if ( $curr_page >= $pages ) { $curr_page = $pages - 1; }
        my $end = ( ( $curr_page + 1 ) * $pix_on_page - 1 );
        if ( $end > $#small_files_array ) { $end = $#small_files_array; }
        @pix_on_this_page =
          @small_files_array[ $curr_page * $pix_on_page .. $end ];

        # Формирование навигации по страницам

        # Таблица с объектами

        $SYSGALLERY_content .=
qq~\n<table cellspacing=0 cellpadding=7 width="100%" class="tabselect">\n~;
        $counter = ( $curr_page * $pix_on_page + 1 );
        for ( my $pixi = 0 ; $pixi <= $#pix_on_this_page ; $pixi++ ) {
            my $thrumb  = '';
            my $bgcolor = 'class="tr01"';

            $pix_on_this_page[$pixi] =~ s/[\r\n]+//g;
            if ( $counter % 2 == 0 ) { $bgcolor = 'class="tr02"'; }

            my @tmp_arr = split( /\|/, $pix_on_this_page[$pixi] );
            my $sysgalID = $tmp_arr[1];
            $sysgalID =~ s/[\D]+//s;
            my $photo_action = qq~
                <div class="pheix-badge-cont">
                <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgaleditphotos&amp;item=$sysgalID"
                   class="fa-action-link"><i class="fa fa-image" title="Редактировать фотографии"></i></a>
                   %warn%
                   </div>&nbsp;~;
            my $photo_warn = '<span class="pheix-badge-cont-data pheix-badge-warn"><i class="fa fa-warning"></i></span>';
            my $alt_desc = $tmp_arr[4];
            $alt_desc =~ s/\"/\&quot;/g;
            my $download_path = &_inner_sysgallery_getpath_by_id($sysgalID);
            my @image =
              reverse( &get_sysgallery_img_bynum( $download_path, $sysgalID ) );

            if ( $image[0] ne "" && $tmp_arr[6] eq '0') {
                if ( -e "$download_path/$image[0]" ) {
                    my ($i_w, $i_h) = getImgResDimensions("$download_path/$image[0]");
                    $thrumb = qq~
                        <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgaleditphotos&amp;item=$sysgalID"
                           class=simple_link>
                           <img src="$download_path/$image[0]?$random"
                                width="$i_w"
                                height="$i_h"
                                title="Уменьшенная фотография"
                                alt="Уменьшенная фотография"
                                border="0"
                                style="border-width: 1px 1px 1px 1px; border-style: solid; border-color: #C0C0C0;"></a>~;
                }
                else {
                    $thrumb = qq~<div class="imgeditor-no-photo">Фото не загружено!</div>~;
                }
            } else {
                if ($tmp_arr[6] eq '0') {
                    $thrumb = qq~<div class="imgeditor-no-photo">Фото не загружено!</div>~;
                } elsif ($tmp_arr[6] eq '1') {
                    $thrumb = qq~<div class="sysgal-fa-img">
                        <a class="sysgal-fa-img-link" href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgaleditphotos&amp;item=$sysgalID">
                        <i class="fa fa-folder-open" aria-hidden="true"></i></a></div>~;
                } elsif ($tmp_arr[6] eq '2') {
                    $photo_action = '';
                    $thrumb = qq~<div class="sysgal-fa-img">
                        <i class="fa fa-sitemap" aria-hidden="true"></i></div>~;
                }
            }
            if ($thrumb =~ /no\-photo/) {
                $photo_action =~ s/%warn%/$photo_warn/gi;
            } else {
                $photo_action =~ s/%warn%//gi;
            }
            if ( $alt_desc eq '' ) {
                if ( $tmp_arr[5] eq '' ) {
                    $alt_desc = qq~<i>Поле заголовка не заполнено!!!</i>~;
                }
                else { $alt_desc = qq~<font color=#C0C0C0>$tmp_arr[5]</font>~; }
            }

            my $updownlinks;
            my $nextpage    = $curr_page;
            my $prevpage    = $curr_page;
            if ( $pixi == $#pix_on_this_page ) { $nextpage++; }
            if ( $pixi == 0 ) {
                $prevpage--;
                if ( $prevpage < 0 ) { $prevpage = 0; }
            }
            if ( $process_status != 3 ) {
                if ( $#pix_on_this_page > 0 ) {
                    my $sysgesortID = $tmp_arr[0];
                    $sysgesortID =~ s/[\D]+//s;
                    $updownlinks =
qq~<a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admsysgalmoveup&amp;sgid=$sysgesortID&amp;page=$prevpage" class="fa-action-link">
                           <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a><a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admsysgalmovedown&amp;sgid=$sysgesortID&amp;page=$nextpage" class="fa-action-link">
                           <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>~;
                    if ( $pixi == 0 && $curr_page == 0 ) {
                        $updownlinks =
qq~<span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span><a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admsysgalmovedown&amp;sgid=$sysgesortID&amp;page=$curr_page" class="fa-action-link"><i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>~;
                    }
                    if (   $pixi == $#pix_on_this_page
                        && $curr_page == ( $pages - 1 ) )
                    {
                        $updownlinks =
qq~<a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admsysgalmoveup&amp;sgid=$sysgesortID&amp;page=$curr_page" class="fa-action-link">
                           <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a><span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
                    }
                }
                else {
                    if ( $curr_page > 0 ) {
                        my $sysgesortID = $tmp_arr[0];
                        $sysgesortID =~ s/[\D]+//s;
                        $updownlinks =
qq~<a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admsysgalmoveup&amp;sgid=$sysgesortID&amp;page=$prevpage" class="fa-action-link">
                           <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a><span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
                    }
                    else {
                        $updownlinks =
qq~<span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span><span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
                    }
                }
            }
            else {
                $updownlinks =
qq~<span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span><span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
            }

            my $path = &_inner_sysgallery_getpath_by_id($sysgalID);
            my $content_page = qq~<tr valign="top" $bgcolor>
                <td width="10%" valign="middle" align="center" class="standart_text">$updownlinks</td>
                <td width="10%" valign="middle" align="center" class="standart_text">$thrumb</td>
                <td width="50%" class=standart_text><div style="font-size:11pt;"><b><span style="color:#C0C0C0">$counter.</span> $alt_desc</b></div><div class=cmstext style="padding-top:10; font-size:10pt; color:#606060">$tmp_arr[5]</div>Корневой каталог: <span class=cmstext style="font-size:10pt; color:#B0B0B0">$path</span></td>
                <td width="30%" class=standart_text align=right valign=middle>$photo_action<a href="javascript:doSesChckLoad('$_[0]', '$ENV{SCRIPT_NAME}', '&amp;action=admgaleditobj&amp;item=$sysgalID&amp;page=$curr_page');" class="fa-action-link"><i class="fa fa-pencil-square-o" title="Редактировать описание галереи"></i></a>
                <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgaldelobj&amp;item=$sysgalID','Вы уверены, что хотите удалить системную галерею &quot;$alt_desc&quot; безвозвратно?')" class="fa-action-link"><i class="fa fa-trash" title="Удалить галерею"></i></a></td></tr>\n~;
            $SYSGALLERY_content .= $content_page;
            $counter++;
        }
        $SYSGALLERY_content .= "</table>";

        #Таблица с объектами

        $SYSGALLERY_content =
          $SYSGALLERY_content . "<p align=right>$page_linx</p>" . ( $_hideflag == 1 ? $_hidealrt : '');
    }
    else {
        $SYSGALLERY_content .=
qq~<div class="pheix-error-alert">Файл записей системной галереи $MODULE_DB_FILE не найден!</div>~;
    }
    return $SYSGALLERY_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_sysgallery_createpage ()
#------------------------------------------------------------------------------
#
# Основная функция вывода формы создания галереи.
#
#------------------------------------------------------------------------------

sub _inner_sysgallery_createpage {
    my $temp_id = time;
    my $SYSGALLERY_content .= qq~
        <div class="sysgal-popup">
        <form id="sysgal-create" method="post" action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalpostobj" enctype="multipart/form-data">
        <div class=popup_header>Создание системной галереи</div>
        <div class="sgal-form-elem">Название раздела галереи: <font color="#A0A0A0">[обязательное поле]</font></div>
        <div class="sgal-form-elem"><input type=text name=header value="" class=input><input type=hidden name=shopcatpath value="0"></div>
        <div class="sgal-form-elem">Описание галереи: <font color="#A0A0A0">[обязательное поле]</font></div>
        <div class="sgal-form-elem"><textarea name=comment rows=5 cols=5 class=input></textarea></div>
        <div class="sgal-form-elem">
            <input type="checkbox" id="c1" name="groupattr" value="1"/><label for="c1" class=checkbox_label><span></span>Использовать как группу галерей</label></div>
        <div class="sgal-form-elem"><input type=button value="Сохранить изменения" class="button" onclick="document.forms['sysgal-create'].submit()"></div>
        </form>
        </div>
        ~;
    return $SYSGALLERY_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_sysgallery_editpage ()
#------------------------------------------------------------------------------
#
# Основная функция вывода формы редактирования описания галереи.
#
#------------------------------------------------------------------------------

sub _inner_sysgallery_editpage {
    my $selected_1;
    my $selected_2;
    my $video_content;
    my $information;
    my @gallery_details;
    my $random = rand();
    my $_scv3 = $_[4] || 0;
    my $SYSGALLERY_content;

    open( my $inputfh, "<", $MODULE_DB_FILE );
    my @indexes = <$inputfh>;
    close($inputfh);

    if ( $#indexes > -1 ) {
        foreach (@indexes) {
            my @temp_arr = split( /\|/, $_ );
            $temp_arr[1] =~ s/[\D]+//s;
            if ( $temp_arr[1] eq $_[2] ) { @gallery_details = @temp_arr; last; }
        }
        if ( $#gallery_details > -1 ) {
            $gallery_details[$#gallery_details] =~ s/[\r\n]+//g;
            $gallery_details[5] =~ s/\<br\>/\n/g;
            my $_grp = $gallery_details[6] eq '1' ? 'checked' : '';
            my $_grp_2 = $gallery_details[6] eq '2' ? 'checked' : '';

            my $_gr_block = qq~
              <div class="sgal-form-elem">
                <input type="checkbox" id="c1" $_grp name="groupattr" value="1"/><label for="c1" class=checkbox_label><span></span>Использовать как группу галерей</label></div>
            ~;
            if ($gallery_details[6] == 2) {
                my $_pcat;
                if ($_scv3) {
                    $_pcat = $_[3];
                }
                $_gr_block = qq~
                  <input type="hidden" value="$_pcat" name="parentcat">
                  <div class="sysgal-radio-block">
                    <input id="radio_1" $_grp type="radio" name="groupattr" checked value="1" hidden/>
                    <label for="radio_1" class="hoverable">Использовать как группу галерей</label>
                  </div>
                  <div class="sysgal-radio-block sysgal-margin-bot">
                    <input id="radio_2" $_grp_2 type="radio" name="groupattr" value="2" hidden/>
                    <label for="radio_2" class="hoverable">Использовать вложенные галереи</label>
                  </div>
                ~;
            }
            my $_frm_act = qq~$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalmodobj&amp;item=$gallery_details[1]&amp;page=$_[3]~;
            if ($_scv3) {
                $_frm_act = qq~$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalmodobj&amp;item=$gallery_details[1]&amp;parentcat=$_[3]~;
            }
            $SYSGALLERY_content .= qq~
    <div class="sysgal-popup">
    <form id="sysgal-modify" method=post action="$_frm_act" enctype="multipart/form-data">
    <div class=popup_header>Управление системной галереей</div>
    <div class="sgal-form-elem">Название раздела галереи: <font color="#A0A0A0">[обязательное поле]</font></div>
    <div class="sgal-form-elem"><input type=text name=header value="$gallery_details[4]" class=input></div>
    <div class="sgal-form-elem">Дата:</div>
    <div class="sgal-form-elem-date">
        <input type=text name=date value="$gallery_details[2]" class=input>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:Currdate(1);" class=simple_link>Текущая дата</a></div>
    <div class="sgal-form-elem">Описание раздела галереи: <font color="#A0A0A0">[обязательное поле]</font></div>
    <div class="sgal-form-elem"><textarea name=comment rows=5 cols=5 class=input>$gallery_details[5]</textarea></div>
    <div class="sgal-form-elem">
    $_gr_block
    <div class="sgal-form-elem"><input type=submit value="Сохранить данные" class=button></div>
    </form>
    </div>
            ~;
        }
        else {
            $SYSGALLERY_content .=
'<table cellspacing=0 cellpadding=0 border=0 width=100%><tr><td class=class=standart_text><div class=error>Информация по указанному объекту отсутствует (\$#news_details = -1)!</div></td></tr></table>';
        }
    }
    else {
        $SYSGALLERY_content .=
'<table cellspacing=0 cellpadding=0 border=0 width=100%><tr><td class=class=standart_text><div class=error>Список объектов пуст (\$#indexes = -1)!</div></td></tr></table>';
    }

    return $SYSGALLERY_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_sysgallery_editphotopage ()
#------------------------------------------------------------------------------
#
# Основная функция вывода формы редактирования фотографий галереи.
#
#------------------------------------------------------------------------------

sub _inner_sysgallery_editphotopage {
    my ($sid, $lgn, $gid, $fl, $mess ) = @_;
    my $uploadedfotoslist;
    my $gallery_path      = _inner_sysgallery_getpath_by_id($gid);
    my $cells_in_row      = 5;
    my @gallerydetails       = get_sysgallery_details_by_ID($gid);
    my $random               = rand();
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $_isgr = $gallerydetails[6] eq '1' ? 1 : 0;
    my $SYSGALLERY_content = qq~<div class=admin_header>$module_descr</div>~;

    if ($fl == 2 && $mess =~ /error/) {
        $SYSGALLERY_content .= "<div class=\"pheix-error-alert\">$mess</div>";
    }

    if ( $#gallerydetails > -1) {
        my @galleryimages =
            reverse( get_sysgallery_img_bynum( $gallery_path, $gid ) );
        if ( $#galleryimages < 0 && $_isgr == 0) {
            $uploadedfotoslist = "<p class=status>Фотографий в текущей системной галереи не найдено!</p>";
        }
        elsif ( $#galleryimages > -1 && $_isgr == 0 ) {

            my $photos     = ( $#galleryimages + 1 );
            my $rows       = ceil( $photos / $cells_in_row );
            my $cells      = $rows * $cells_in_row;
            my $cell_width = floor( 100 / $cells_in_row );

            $uploadedfotoslist = qq~<table width=100% cellspacing=15 border=0>~;

            for ( my $k1 = 0 ; $k1 < $rows ; $k1++ ) {

                $uploadedfotoslist .= '<tr>';

                for ( my $k2 = 0 ; $k2 < $cells_in_row ; $k2++ ) {
                    my $image_index = $k1 * $cells_in_row + $k2;

                    if ( $galleryimages[$image_index] ) {
                        my $image_postfix = -1;
                        if ( $galleryimages[$image_index] =~
                            /^[0-9]+_([0-9]+)\./ )
                        {
                            $image_postfix = $1;
                        }
                        my ($i_w, $i_h) = getImgResDimensions("$gallery_path/$galleryimages[$image_index]");
                        $uploadedfotoslist .= qq~
                            <td align="center"
                                valign="middle"
                                width="$cell_width%"
                                style="padding:5px; background-color:#FFFFFF">
                                <div id="ex3"><span><a href="$gallery_path/$galleryimages[$image_index]?$random" class="fancybox">
                                    <img src="$gallery_path/$galleryimages[$image_index]?$random"
                                         border=0 alt=""
                                         style="border-width: 1px 1px 1px 1px; border-style: solid; border-color: #C0C0C0;" width="$i_w" height="$i_h"></a></span>
                                    <div style="background-color:red">
                                        <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$sid&amp;action=admdelimgfromsystemgallery&amp;galid=$gid&amp;imgid=$image_postfix','Вы действительно хотите удалить эту фотографию с сервера?');" 
                                           title="Удалить фотографию"><img src="images/admin/cms/png/delete_foto.png" title="Удалить фотографию" alt="Удалить фотографию"></a>
                                    </div>
                                </div></td>~;
                    }
                    else {
                        $uploadedfotoslist .=
                          qq~<td align=center width="$cell_width%">&nbsp;</td>~;
                    }
                }

                $uploadedfotoslist .= '</tr>';
            }
            $uploadedfotoslist .= qq~</tr></table>~;
        } elsif ( $_isgr == 1 ) {
            $uploadedfotoslist .= getGroupContent( $sid, $gid );
        }
        my $emu_file_input;
        if ($_isgr == 0) {
            $emu_file_input = sysgallery_input_htmlcode( $sid, $gid );
        }
        $SYSGALLERY_content .= qq~
    <p class=standart_text><a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admsystemgallery" class=simple_link><b>Системная галерея</b></a> / Редактирование фотографий раздела системной галереи</p>
    <p class=standart_text><b>«$gallerydetails[4]»</b> <font color="#c0c0c0">(корневой каталог: $gallery_path)</font></p>
    <table cellspacing=5 cellpadding=15 class=standart_text bgcolor="#FDE4C6" width=100%>
    <tr><td style="background-image:url('images/admin/xfoto_bg.jpg')">
    <table cellspacing=0 cellpadding=5 border=0 class=standart_text width="100%" style="background-repeat:no-repeat; background-position: right bottom;color:#fff; background-image:url('images/admin/xfotologo3.png')">
    <tr><td class=standart_text>$uploadedfotoslist</td></tr>~;
    
    if ($emu_file_input) {
        $SYSGALLERY_content .= qq~<tr><td class=standart_text>&nbsp;</td></tr>
            <tr><td class=standart_text>$emu_file_input</td></tr>~;
    }
    
    $SYSGALLERY_content .= qq~</table></td></tr></table><p>&nbsp;</p>~;
    }
    else {
        $SYSGALLERY_content .= qq~
        <p class=standart_text><a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admsystemgallery" class=simple_link><b>Системная галерея</b></a> / Галерея не найдена</p>
        <div class="pheix-error-alert">Системная галерея с идентификатором $gid не найдена!</div>
    ~;
    }

    return $SYSGALLERY_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# sysgallery_input_htmlcode ()
#------------------------------------------------------------------------------
#
# Функция вывода HTML кода для стилизованного поля file input.
#
#------------------------------------------------------------------------------

sub sysgallery_input_htmlcode {
    my ($sid, $galid) = @_;
    my $gallery_path  = _inner_sysgallery_getpath_by_id($galid);
    my @galleryimages = get_sysgallery_img_bynum( $gallery_path, $galid );
    my $erasedisabled = @galleryimages ? '' : '-hidden';
    my $SYSGALLERY_content = qq~
    <div class="imgeditor-photo-block">
    <form method="post" action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admgaleditphotomulti&amp;item=$galid" enctype="multipart/form-data">
    <div class="imgeditor-header">Добавить&nbsp;полноразмерную&nbsp;фотографию&nbsp;[<img src="images/admin/cms/filetypes/jpg.png" alt="">]&nbsp;:</div>
    <div class="f_imulation_wrap">
    <div class="im_input">
        <input value="Выберите файл фотографии" type="text">
    </div>
    <input id="imulated" type="file" min="1" max="9999" name="photofile" multiple="true" />
    </div>
    <script type="text/javascript">
    \$('.im_input input').click(function(){
     \$('.im_input input').css('color','#C0C0C0');
     \$('.im_input input').val('Выберите файл фотографии');
     \$("#imulated").val('');
     \$('#imulated').trigger('click');
    });
    \$('#imulated').change(function(){
     \$('.im_input input').css('color','#505050');
     var names = [];
     for (var i = 0; i < \$(this).get(0).files.length; ++i) {
        names.push(\$(this).get(0).files[i].name);
     }
     \$('.im_input input').val(names);
    });
    </script>
    <div class="imgeditor-resize"><input type="checkbox" id="c1" name="autoresize" value="1"/><label for="c1" class=checkbox_label><span></span>Автоматическое изменение размеров фотографии после загрузки</label></div>
    <input value="Добавить фотографию" class="installbutton" type="submit">
   </form>
   </div>
   <div  class="imgeditor-erase-block$erasedisabled">
   <input value="Очистить галерею"
          class="installbutton"
          type="button"
          onclick="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$sid&amp;action=admgalerase&amp;item=$galid','Вы действительно хотите удалить все изображения из галереи?');">
   </div>~;
    return $SYSGALLERY_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_sysgallery_getlink_for_shopcat2 ()
#------------------------------------------------------------------------------
#
# Функция генерации ссылки на галерею для shopcat2.
#
#------------------------------------------------------------------------------

sub _inner_sysgallery_getlink_for_shopcat2 {
    my ( $session_id, $root_folder, $shopid, $catid, $catname, $itemid,
        $linkflag )
      = @_;
    my $ret_gallery_link;
    my $gallery_is_switchedoff = &get_power_status_for_module();
    if ( $gallery_is_switchedoff == 0 ) {
        my @iconfiles;
        if ( $linkflag == 1 ) {
            @iconfiles = ( 'png/image-add.png', 'png/image.png' );
            $ret_gallery_link = qq~
                <a  href="javascript:doSesChckLoad('$session_id', '$ENV{SCRIPT_NAME}', '&amp;action=admgaladdfromshopcat&amp;shopid=$shopid&amp;catid=$catid')">
                <img src="images/admin/cms/$iconfiles[0]" alt="" title="Создать фотогалерею «$catname»" border=0></a>
            ~;
        } else {
            $ret_gallery_link = qq~
                <div class="pheix-badge-cont">
                <a  href="javascript:doSesChckLoad('$session_id', '$ENV{SCRIPT_NAME}', '&amp;action=admgaladdfromshopcat&amp;shopid=$shopid&amp;catid=$catid')"
                    class="fa-action-link"><i class="fa fa-image" title="Создать фотогалерею «$catname»"></i></a>
                <span class="pheix-badge-cont-data pheix-badge-status"><i class="fa fa-plus-square"></i></span></div>
            ~;
        }
        if ( -e $MODULE_DB_FILE ) {
            open( my $inputfh, "<", $MODULE_DB_FILE );
            my @gallery_records = <$inputfh>;
            close($inputfh);
            for my $j ( 0 .. $#gallery_records ) {
                my @tmp_ = split /\|/, $gallery_records[$j];
                my $look_for_path = $root_folder . "/" . $catid;
                if ( $root_folder =~ /\/n$/ ) {
                    $look_for_path = $root_folder . "/" . $itemid;
                }
                if ( $tmp_[3] eq $look_for_path ) {
                    if ($tmp_[6] == 2 && $look_for_path =~ /\/n-1\//) {
                        my $_pid = 0;
                        eval {
                            $_pid = Shopcat::V3::getParentCatId($shopid, $catid);
                        };
                        $ret_gallery_link = qq~<a href="javascript:doSesChckLoad('$session_id', '$ENV{SCRIPT_NAME}', '&amp;action=admgaleditobj&amp;item=$tmp_[1]&amp;parentcat=$_pid');" style="border: none;"><img src="images/admin/cms/$iconfiles[1]" alt="" title="Редактировать фотогалерею «$catname»" border=0></a>~;
                    } else {
                        $ret_gallery_link = qq~<div class="pheix-badge-cont">
                            <a href="$ENV{SCRIPT_NAME}?id=$session_id&amp;action=admgaleditphotos&amp;galleryid=$tmp_[1]" class="fa-action-link">
                            <i class="fa fa-image" title="Создать фотогалерею «$catname»"></i></a></a></div>
                        ~;
                    }
                    last;
                }
            }
        }
    }
    return $ret_gallery_link;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_sysgallery_createpage_fromshopcat2 ()
#------------------------------------------------------------------------------
#
# Функция вывода формы создания галереи из модуля shopcat2.
#
#------------------------------------------------------------------------------

sub _inner_sysgallery_createpage_fromshopcat2 {
    my @category_details;
    my $shopid           = $_[2];
    my $catid            = $_[3];
    my $shopcatpath;
    my $_pid;
    eval {
        @category_details =
          Shopcat::V3::_inner_shopcat2_getdetails_array_for_id( $shopid,
            $catid );
    };
    my $crsysgallery_fromshopcat2_content;
    if (@category_details) {
        my $catname;
        for ( my $j = 3 ; $j <= $#category_details ; $j++ ) {
            my $catname_ = $category_details[$j];
            my $id;
            if ($category_details[$j] =~ /\[([\d]+)\]/) {
                $id = $1;
                if ($id > 9999) { $id = $id % 10000; }
            }
            if ( $id == $catid ) {
                my $isnminus1 = 0;
                
                if ( $catname_ =~ /\[\*\*\]/ ) { $isnminus1 = 1; }
                $catname_ =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                $catname = $catname_;
                if ($isnminus1) {
                    if ($category_details[$j-1] =~ /\[([\d]+)\]/) {
                        $_pid = $1;
                    }
                    $shopcatpath = "images/shopcat/$shopid/levels/n-1/$catid";
                }
                else {
                    $shopcatpath =
                      "images/shopcat/$shopid/levels/n/$category_details[0]";
                }
            }
        }
        my $_gr_block = qq~
          <div class="sgal-form-elem">
            <input type="checkbox" id="c1" name="groupattr" value="1"/><label for="c1" class=checkbox_label><span></span>Использовать как группу галерей</label></div>
        ~;
        if ($shopcatpath =~ /\/n-1\//) {
            $_gr_block = qq~
              <input type="hidden" value="$_pid" name="parentcat">
              <div class="sysgal-radio-block">
                <input id="radio_1" type="radio" name="groupattr" checked value="1" hidden/>
                <label for="radio_1" class="hoverable">Использовать как группу галерей</label>
              </div>
              <div class="sysgal-radio-block">
                <input id="radio_2" type="radio" name="groupattr" value="2" hidden/>
                <label for="radio_2" class="hoverable">Использовать вложенные галереи</label>
              </div>             
            ~;
        }
        $crsysgallery_fromshopcat2_content .= qq~
          <div class="sysgal-popup">
          <form id="sysgal-create-shopcat" method=post action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalpostobj" ENCTYPE="multipart/form-data">
          <div class=popup_header>Создание системной галереи</div>
          <div class="sgal-form-elem">Название раздела галереи: <font color="#A0A0A0">[обязательное поле]</font></div>
          <div class="sgal-form-elem"><input type=text name=header value="$catname" class=input><input type=hidden name=shopcatpath value="$shopcatpath"></div>
          <div class="sgal-form-elem">Описание галереи: <font color="#A0A0A0">[обязательное поле]</font></div>
          <div class="sgal-form-elem">
            <textarea name=comment rows=5 cols=5 class=input>Системная галерея для хранения фотографий и изображений для категории «$catname» общего каталога продукции</textarea></div>
          $_gr_block  
          <div class="sgal-form-elem sysgal-margin-top"><input type=submit value="Сохранить изменения" class="button"></div>
          </form>
          </div>
        ~;
    }
    else {
        $crsysgallery_fromshopcat2_content =
'<div class=error>Информация о заданной категории не найдена.<br>Убедитесь, что модуль shopcat2 установлен в системе!</div>';
    }
    return $crsysgallery_fromshopcat2_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_sysgallery_getpath_by_id ()
#------------------------------------------------------------------------------
#
# Функция получения пути к корневому каталогу галереии по ID.
#
#------------------------------------------------------------------------------

sub _inner_sysgallery_getpath_by_id {
    my $gallery_id        = $_[0];
    my $gallery_root_path = $IMAGES_ROOTPATH;
    if ( -e $MODULE_DB_FILE ) {
        open( my $inputfh, "<", $MODULE_DB_FILE );
        my @gallery_records = <$inputfh>;
        close($inputfh);
        for my $j ( 0 .. $#gallery_records ) {
            my @tmp_ = split /\|/, $gallery_records[$j];
            $tmp_[1] =~ s/[\D]+//g;
            if ( $tmp_[1] eq $gallery_id ) {
                if ( $tmp_[3] =~ /^images\// ) {
                    $gallery_root_path = $tmp_[3];
                }
                last;
            }
        }
    }
    return $gallery_root_path;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getGalleryList ( $query )
#------------------------------------------------------------------------------
#
# $query - поисковый запрос.
# Вывод списка всех системных галерей в JSON.
# Функция возвращает список всех системных галерей в HTML формате.
#
#------------------------------------------------------------------------------

sub getGalleryList {
    my $query = $_[0];
    my $jsoncontent;
    my @json;
    $query =~ s/[\n\r\t]+//g;
    my $_galactv = get_power_status_for_module();
    if ( -e $MODULE_DB_FILE && $_galactv == 0) {
        open( my $inputfh, "<", $MODULE_DB_FILE );
        my @gallery_records = <$inputfh>;
        close($inputfh);

        for ( my $j = 0 ; $j <= $#gallery_records ; $j++ ) {
            my $json;
            my $blnk       = '- skip search -';
            my $json_blank = qq~{"label":"$blnk", "value":"-1", "id":"0"}~;
            my @ar         = split( /\|/, $gallery_records[$j] );
            $ar[1] =~ s/[\D]+//g;
            $ar[4] =~ s/[\n\r\t]+//g;
            $ar[4] =~ s/\"/&quot;/g;

            if ( $query eq '*' && $query ne '' && $query !~ /^[\s]+$/ ) {
                if (   $ar[1] =~ /^[\d]+$/
                    && $ar[4] !~ /^[\s]+$/
                    && $ar[4] ne ''
                    && $ar[6] eq '0' )
                {
                    if ( !@json ) {
                        push( @json, $json_blank );
                    }
                    $json = qq~{ "label" : "$ar[4]", "value" : "$ar[4]", "id" : "$ar[1]" }~;
                    push( @json, $json );
                }
            }
            else {
                if ( $query ne '' && $query !~ /^[\s]+$/ ) {
                    my $enc_query =
                      quotemeta( Encode::decode( "utf8", $query ) );
                    my $enc_data = Encode::decode( "utf8", $ar[4] );
                    if (   $ar[1] =~ /^[\d]+$/
                        && $ar[4] !~ /^[\s]+$/
                        && $ar[4] ne ''
                        && $ar[6] eq '0'
                        && $enc_data =~ /$enc_query/i )
                    {
                        if ( !@json ) {
                            push( @json, $json_blank );
                        }
                        $json = qq~{ "label" : "$ar[4]", "value" : "$ar[4]", "id" : "$ar[1]" }~;
                        push( @json, $json );
                    }
                }
            }

        }
    } elsif ($_galactv == 1) {
        my $json = qq~{ "label": "Module Gallery::System is inactive!", "value": "-1", "id": "0" }~;
        push( @json, $json );
    }
    if (@json) {
        $jsoncontent = "[" . join( ",", @json ) . "]";
    }
    else {
        my $mess = "По запросу '$query' ничего найдено";
        $jsoncontent = qq~[{"label": "$mess", "value":"-1", "id":"0"}]~;
    }
    return $jsoncontent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getGalleryCarousel ( $sesid, $gallery_id, $imgpath )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# $gallery_id - идентификатор галереи.
# $imgpath - путь до изображения, которое следует выделить
# Вывод preview-карусели изображений для заданной галереи.
# Функция возвращает preview-карусель изображений для заданной галереи в
# HTML формате.
#
#------------------------------------------------------------------------------

sub getGalleryCarousel {
    my $carouselContent;
    my ($sid, $galid, $imgpath) = @_;
    my $galpath = _inner_sysgallery_getpath_by_id($galid);
    my @galleryimages =
      reverse( get_sysgallery_img_bynum( $galpath, $galid ) );
    if (@galleryimages) {
        my $list;
        my $inum = $#galleryimages;
        if ( $inum < 2 ) { $inum = 2; }
        for ( my $j = 0 ; $j <= $inum ; $j++ ) {
            my $bgdimensions;
            my $href;
            my $ipath = "$galpath/$galleryimages[$j]";
            if ( $galleryimages[$j] ) {
                $href = "javascript:GalPathAdd('$galid', $inum, $j, '$ipath')";
                my @img_dimens = getImgWidthHeight($ipath);
                if ( $img_dimens[0] > -1 && $img_dimens[1] > -1 ) {
                    if ( $img_dimens[0] > $img_dimens[1] ) {
                        $bgdimensions = 'auto 95px';
                    }
                    if ( $img_dimens[0] <= $img_dimens[1] ) {
                        $bgdimensions = '120px auto';
                    }
                }
                else {
                    $bgdimensions = "120px auto";
                }
            }
            else {
                $href         = 'javascript:void(0);';
                $ipath        = "images/social_bg.png";
                $bgdimensions = "auto auto";
            }
            my $random   = rand;
            my $imgblock = qq~
            <div class="slick-slide-div">
            <a  href="$href"
                style="
                    display: block; 
                    background-image: url('$ipath?$random');
                    background-size: $bgdimensions; 
                    background-repeat: no-repeat;
                    background-position: center center; 
                    width: 100px; height: 75px; 
                    border: solid 2px #D5D5D5;
                "
                id="$galid\_$j"></a></div>
            ~;
            if ( $ipath eq $imgpath && ( $imgpath ne '' || $imgpath != 0 ) ) {
                $imgblock =~ s/#D5D5D5/#FF0000/;
                $list = $imgblock . $list;
            }
            else {
                $list .= $imgblock;
            }
        }
        if ($list) {
            $carouselContent = qq~
            <script type="text/javascript">
            jQuery('.wl-carousel').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                arrows: true,
                dots: true,
            });
            </script>
            <div class="wl-carousel" id="wl-carousel"
              style="padding: 10px; margin-left:46px; width:400px;">$list</div>
            ~;
        }
    }
    else {
        my $_realgalid = checkGalleryId($galid);
        if ($_realgalid == 1) {
            my $href =
                Pheix::Tools::getQueryStr( $sid, 'admgaleditphotos', $galid );
            $carouselContent = qq~
                <div class="wl-err">&nbsp;В заданной галереи нет изображений:
                    <a  class="simple_link"
                        href="$href"><b>наполнить галерею &rarr;</b></a></div>~;
        }
    }
    return $carouselContent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getGalleryImgPool ( $sesid, $gallery_id, $imgpath )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# $gallery_id - идентификатор галереи.
# $imgpath - путь до изображения, которое следует выделить
# Вывод preview-карусели изображений для заданной галереи.
# Функция возвращает preview-карусель изображений для заданной галереи в
# HTML формате.
#
#------------------------------------------------------------------------------

sub getGalleryImgPool {
    my $imgPoolContent;
    my ($sesid, $galid) = @_;
    my $galpath = &_inner_sysgallery_getpath_by_id($galid);
    my @galleryimages =
      reverse( &get_sysgallery_img_bynum( $galpath, $galid ) );
    if (@galleryimages) {
        my $pool;
        my $inum = $#galleryimages;
        for ( my $j = 0 ; $j <= $inum ; $j++ ) {
            my $bgdimensions;
            my $href;
            my $ipath = "$galpath/$galleryimages[$j]";
            if ( $galleryimages[$j] ) {
                $href = "javascript:GalPathCkeditorDialogAdd('$galid', $inum, $j, '$ipath')";
                my @img_dimens = &getImgWidthHeight($ipath);
                if ( $img_dimens[0] > -1 && $img_dimens[1] > -1 ) {
                    if ( $img_dimens[0] > $img_dimens[1] ) {
                        $bgdimensions = 'auto 95px';
                    }
                    if ( $img_dimens[0] <= $img_dimens[1] ) {
                        $bgdimensions = '120px auto';
                    }
                }
                else {
                    $bgdimensions = "120px auto";
                }
            }
            my $random   = rand;
            my $linebr;
            if ( ($j+1) % 5 == 0 ) {
                $linebr = "<br>";
            } else {
                $linebr = '';
            }
            $pool .= qq~
            <a  href="$href"
                style="
                    margin:5px;
                    display: inline-block; 
                    background-image: url('$ipath?$random');
                    background-size: $bgdimensions; 
                    background-repeat: no-repeat;
                    background-position: center center; 
                    width: 100px; height: 75px; 
                    border: solid 2px #D5D5D5;
                    cursor:pointer;"
                id="$galid\_$j"></a>$linebr
            ~;
        }
        if ($pool) {
            $imgPoolContent = qq~
            <div class="wl-carousel" id="wl-carousel"
              style="text-align:center; padding: 10px; margin-left:0px; width:670px;">$pool</div>
            ~;
        }
    }
    else {
        $imgPoolContent = qq~<div class="wl-err">&nbsp;В заданной галереи нет изображений.</div>~;
    }
    return $imgPoolContent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showGalleryErr ( $sesid, $gallery_id )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии.
# $galid - идентификатор галереи.
# Вывод сообщения об ошибке доступа к заданной галереи.
# Функция возвращает сообщениe об ошибке доступа к заданной галереи в
# HTML формате.
#
#------------------------------------------------------------------------------

sub showGalleryErr {
    my ($sid, $galid) = @_;
    my $mess =
      '&nbsp;Ошибка доступа к заданной галереи';
    my $carouselContent = qq~
        <div class="wl-err">$mess (id=$galid)</div>~;
    return $carouselContent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getImgWidthHeight ( $path )
#------------------------------------------------------------------------------
#
# $path - путь до изображения.
# Получения длины и ширины изображения.
# Функция возвращает массив из двух элементов: длина и ширина изображения,
# заданного входной переменной $path.
#
#------------------------------------------------------------------------------

sub getImgWidthHeight {
    use Image::Magick;
    my $path    = $_[0];
    my $globe_x = -1;
    my $globe_y = -1;
    if ( -e $path ) {
        my $image = new Image::Magick;
        my $size; my $format;
        ($globe_x, $globe_y, $size, $format) = $image->Ping($path);
    }
    return ( $globe_x, $globe_y );
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doMultiFileUpload ($path, $fieldpntr, $newname )
#------------------------------------------------------------------------------
#
# $path - путь к каталогу загрузки файлов на сервере
# $fieldpntr - указатель на поле формы
# $newname - новое имя файла
# Функция статус мульти загрузки нескольких файлов на сервер.
#
#------------------------------------------------------------------------------

sub doMultiFileUpload {
    my ($path, $fieldpntr, $newname) = @_;
    if ($path) {
        if ($fieldpntr) {
            my $file     = $fieldpntr;
            my $filename = $file;
            $filename =~ s/^.*(\\|\/)//;

            #For IE
            $filename =~ s/ +/\_/g;

            #For Opera
            $filename =~ s/\"//g;
            $filename = $newname if $newname;
            if ( open( my $outpuFH, ">", "$path/$filename" ) ) {
                binmode($outpuFH);
                while ( my $bytesread = read( $file, my $buffer, 1024 ) ) {
                    print $outpuFH $buffer;
                }
                close($outpuFH);
                my $filesize = ( -s "$path/$filename" );
                return qq~<div class="standart_text">$path/$filename...&nbsp;<span class="status">загружен: </span>$filesize B</div>~;
            } else {
                return qq~<div class="standart_text">$path/$filename...&nbsp;<span class="error">file opening is failed</span></div>~;
            }
        } else {
            return qq~<div class="standart_text"><span class="error">currupted field pointer</span></div>~;
        }
    } else {
        return qq~<div class="standart_text"><span class="error">invalid path - $path</span></div>~;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getImgResDimensions ($path )
#------------------------------------------------------------------------------
#
# $path - путь к файлу на сервере
# Получить размеры для превью изображения.
#
#------------------------------------------------------------------------------

sub getImgResDimensions {
    my ($path) = @_;
    my $scaleheight;
    my $scalewidth;
    if (-e $path) {
        my ($globe_x, $globe_y) = getImgWidthHeight($path);
        if ($globe_x > $globe_y) {
            my $think_width = 122;
            my $scale_koeff = ($globe_x/$think_width);
            if (($scale_koeff < 1)) {$scale_koeff = 1;}
            $scaleheight = floor(($globe_y/$scale_koeff));
            $scalewidth = floor(($globe_x/$scale_koeff));
        }
        if ($globe_x <= $globe_y) {
            my $think_width = 87;
            if ( $think_width <= 0 || $think_width !~ /^[\d]+$/gsx ) {
                $think_width = $RESIZEVRT;
            }
            my $scale_koeff = ($globe_x/$think_width);
            if (($scale_koeff < 1)) {$scale_koeff = 1;}
            $scaleheight = floor(($globe_y/$scale_koeff));
            $scalewidth = floor(($globe_x/$scale_koeff));
        }
    }
    return ($scalewidth, $scaleheight);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doGalleryErase ($path )
#------------------------------------------------------------------------------
#
# $galid - идентификатор галереи.
# Очистить галерею с заданным идентификатором.
#
#------------------------------------------------------------------------------

sub doGalleryErase {
    my ($galid) = @_;
    if ($galid > 0) {
        my $galpath = &_inner_sysgallery_getpath_by_id($galid);
        unlink glob($galpath.'/'.$galid.'*');
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getGroupContent ( $sesid, $galid  )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# $galid - идентификатор галереи.
# Вывод содержимого блока редактирования групповой галереей.
#
#------------------------------------------------------------------------------

sub getGroupContent {
    my ( $sid, $gid ) = @_;
    my $_cntnt;
    my $_fl;
    my $_gpath = _inner_sysgallery_getpath_by_id( $gid ).q{/}.$gid.'.galconcat';
    if ( -e $_gpath ) {
        open my $fh, "<", $_gpath or die "unable to open $_gpath: ".$!;
        my @_db = <$fh>;
        close $fh or die "unable to close $_gpath: ".$!;
        my $cnt = 1;
        for my $i (0..$#_db) {
            my @_rec = split /\|/, $_db[$i];
            $_rec[0] =~ s/[\D]+//gi;
            if ( $_rec[0] ne $gid ) {
                $_cntnt =~ s/sysgal\-nf\-block/sysgal\-gr\-block/gi;
                $_cntnt .= qq~<div class="sysgal-nf-block">
                    <div class="sysgal-gr-descr">$cnt. <a class="sysgal-dotlink" href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admgaleditphotos&amp;gid=$_rec[0]">$_rec[1]</a>
                    <span class="sysgal-gr-act">
                        <a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admgaldelfromgroup&amp;gid=$gid&amp;galtodel=$_rec[0]"><i class="fa fa-remove"></i></a></span></div>
                </div>~;
                $_fl = 1;
                $cnt++;
            }
        }
    }
    if (!$_cntnt) {
        $_cntnt .= qq~<div class="sysgal-nf-block">В заданной группе галереи не найдены!</div>~;
        $_fl = 0;
    } else {
        ;
    }
    $_cntnt .= getGroupInputs( $sid, $gid, $_fl );
    return $_cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getGroupInputs ()
#------------------------------------------------------------------------------
#
# $id - идентификатор сессии.
# $galid - идентификатор галереи.
# $fl - флаг управления печатью.
# Функция вывода HTML кода полей для группы галерей.
#
#------------------------------------------------------------------------------

sub getGroupInputs {
    my ($sid, $galid, $fl) = @_;
    my $_cntnt;
    my $_sel;
    my $_style = $fl == 1 ? '' : '-hidden';
    my $_glist = getGalleryList('*');
    my $_json = JSON::XS::decode_json($_glist);
    if ($_json) {
        for my $val ( @{$_json} ) {
           if ($val->{id} > 0) {
               $_sel .= '<option value="'.encode('utf-8', $val->{id}).'">'.
               encode('utf-8', $val->{label}).'</option>';
           }
        }
    } 
    $_cntnt = qq~
    <div class="imgeditor-photo-block">
    <form method="post" action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admgaladdtogroup&amp;item=$galid" enctype="multipart/form-data">
    <div class="imgeditor-select"><select name="gallery-select" class="installbutton">$_sel</select>
    <input value="Добавить галерею" class="installbutton" type="submit"></div>
    </form></div>
    <div  class="imgeditor-erase-block$_style">
    <input value="Очистить группу"
          class="installbutton"
          type="button"
          onclick="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$sid&amp;action=admgalgrouperase&amp;item=$galid','Вы действительно хотите удалить заданную галерею из группы?');">
    </div>~;
    return $_cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doGalToGroupAdd ()
#------------------------------------------------------------------------------
#
# $cgi_p - указатель на cgi объект.
# $gid - идентификатор галереи.
# Функция добавлению галереи в группу.
#
#------------------------------------------------------------------------------

sub doGalToGroupAdd {
    my ($cgi_p, $gid) = @_;
    my $_gid = $cgi_p->param('gallery-select') || 0;
    my $_gpath = _inner_sysgallery_getpath_by_id( $gid );
    my $_gfnam = $_gpath.q{/}.$gid.'.galconcat';
    if ( -e $_gfnam ) {
        open my $fh, "<", $_gfnam or die "unable to open $_gfnam: ".$!;
        my @_db = <$fh>;
        close $fh or die "unable to close $_gfnam: ".$!;
        my @_tmpdb = grep { $_ =~ /$_gid\|/gi } @_db;
        if (!@_tmpdb) {
            my @_dets = get_sysgallery_details_by_ID($_gid);
            push(@_db, join("|", $_gid, $_dets[4])."\n");
            open my $fh, ">", $_gfnam or die "unable to open $_gfnam: ".$!;
            flock( $fh, LOCK_EX ) or die 'unable to lock mailbox: ' . $!;
            print $fh @_db;
            truncate $fh, tell($fh);
            flock( $fh, LOCK_UN ) or die 'unable to unlock mailbox: ' . $!;
            close $fh or die "unable to close $_gfnam: ".$!;
        }
    } else {
        if (!(-e $_gpath)) {
            mkpath($_gpath);
        }
        my @_dets = get_sysgallery_details_by_ID($_gid);
        open my $fh, ">", $_gfnam or die "unable to open $_gfnam: ".$!;
        flock( $fh, LOCK_EX ) or die 'unable to lock mailbox: ' . $!;
        print $fh join("|", $_gid, $_dets[4])."\n";
        truncate $fh, tell($fh);
        flock( $fh, LOCK_UN ) or die 'unable to unlock mailbox: ' . $!;
        close $fh or die "unable to close $_gfnam: ".$!;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doGalleryGroupErase ()
#------------------------------------------------------------------------------
#
# $gid - идентификатор галереи.
# Функция очистки группы галерей.
#
#------------------------------------------------------------------------------

sub doGalleryGroupErase {
    my ( $gid ) = @_;
    my $_gpath = _inner_sysgallery_getpath_by_id( $gid ).q{/}.$gid.'.galconcat';
    if ( -e $_gpath ) {
        unlink $_gpath;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doGalleryGalFromGroup ()
#------------------------------------------------------------------------------
#
# $gid - идентификатор галереи.
# $_gid - идентификатор галереи, которую следует удалить.
# Функция удаления заданной галереи из группы галерей.
#
#------------------------------------------------------------------------------

sub doGalleryGalFromGroup {
    my ( $gid, $_gid ) = @_;
    my $_gpath = _inner_sysgallery_getpath_by_id( $gid ).q{/}.$gid.'.galconcat';
    if ( -e $_gpath ) {
        open my $fh, "<", $_gpath or die "unable to open $_gpath: ".$!;
        my @_db = grep { $_ !~ /$_gid\|/gi } <$fh>;
        close $fh or die "unable to close $_gpath: ".$!;
        if (@_db) {
            open my $fh, ">", $_gpath or die "unable to open $_gpath: ".$!;
            flock( $fh, LOCK_EX ) or die 'unable to lock mailbox: ' . $!;
            print $fh @_db;
            truncate $fh, tell($fh);
            flock( $fh, LOCK_UN ) or die 'unable to unlock mailbox: ' . $!;
            close $fh or die "unable to close $_gpath: ".$!;
        } else {
            unlink $_gpath;
        }
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# checkGalleryId ()
#------------------------------------------------------------------------------
#
# galid - идентификатор галереи.
# Проверка идентификатора галереи: реальный или фейк.
#
#------------------------------------------------------------------------------

sub checkGalleryId {
    my ($_gid) = @_;
    my $found;
    if (-e $MODULE_DB_FILE) {
        open( my $fh, "<", $MODULE_DB_FILE );
        my @_rows = <$fh>;
        close($fh);
        if (@_rows) {
            for my $i (0..$#_rows) {
                my @_cols = split( /\|/, $_rows[$i] );
                if ( $_cols[1] eq $_gid ) {
                    $found = 1;
                    last;
                }
            }
        }
    }
    return $found;
}

#------------------------------------------------------------------------------

END { }

1;
