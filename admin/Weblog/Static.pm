package Weblog::Static;

use strict;
use warnings;

##############################################################################
#
# File   :  Static.pm
#
##############################################################################
#
# Главный пакет управления модулем «Блог» в административной части CMS Pheix
#
##############################################################################

BEGIN {
    #for all servers
    use lib '../';    # lib path for work mode

    # lib path for systax check during module install
    use lib 'admin/libs/modules/';
    use lib 'admin/libs/extlibs/';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';
}

use POSIX;
use Fcntl qw(:DEFAULT :flock :seek);
use Config::ModuleMngr qw(
    getSingleSetting
    getModXml
    getConfigValue
    showUpdateHint
    getModPowerOn
    getProtoSrvName
);

my $MODULES_PATH    = 'admin/libs/modules';
my $ADMIN_SKIN_PATH = 'admin/skins/classic_.txt';
my $MODULE_DB_FILE  = "conf/system/weblog.tnk";
my $MODULE_PG_PATH  = "conf/pages";

my $MODULE_NAME     = "Weblog";
my $MODULE_FOLDER   = "$MODULES_PATH/$MODULE_NAME/";

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showHtmlEditor ( )
#------------------------------------------------------------------------------
#
# Функция вывода страницы с HTML редактором.
#
#------------------------------------------------------------------------------

sub showHtmlEditor {
    my $js_css_addons = qq~ -->
    <link rel=stylesheet href="codemirror/lib/codemirror.css">
    <script src="codemirror/lib/codemirror.js"></script>
    <script src="codemirror/mode/xml/xml.js"></script>
    <script src="codemirror/mode/javascript/javascript.js"></script>
    <script src="codemirror/mode/css/css.js"></script>
    <script src="codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="codemirror/addon/edit/matchbrackets.js"></script>
    <!--~;

    open( my $inputFH, "<", $ADMIN_SKIN_PATH );
    my @scheme = <$inputFH>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $login = getHtmlEditor(@_);
            $scheme[$cnt] =~ s/\%content\%/$login/;
        }
        if ( $scheme[$cnt] =~ /\%scripts_for_this_page\%/ ) {
            my $login = getHtmlEditor(@_);
            $scheme[$cnt] =~ s/\%scripts_for_this_page\%/$js_css_addons/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($inputFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_weblog_startpage ( )
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода weblog-записей из CMS.
#
#------------------------------------------------------------------------------

sub show_weblog_startpage {
    open( my $inputFH, "<", $ADMIN_SKIN_PATH );
    my @scheme = <$inputFH>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $login = _inner_weblog_startpage(@_);
            $scheme[$cnt] =~ s/\%content\%/$login/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($inputFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# create_weblog_page ( )
#------------------------------------------------------------------------------
#
# Промежуточная функция создания weblog-записи в CMS.
#
#------------------------------------------------------------------------------

sub create_weblog_page {
    open( my $inputFH, "<", $ADMIN_SKIN_PATH );
    my @scheme = <$inputFH>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $login = _inner_weblog_createrazdelpage(@_);
            $scheme[$cnt] =~ s/\%content\%/$login/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($inputFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# edit_weblog_page ( )
#------------------------------------------------------------------------------
#
# Промежуточная функция редактирования weblog-записи в CMS.
#
#------------------------------------------------------------------------------

sub edit_weblog_page {
    open( my $inputFH, "<", $ADMIN_SKIN_PATH );
    my @scheme = <$inputFH>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $login = _inner_weblog_editpage(@_);
            $scheme[$cnt] =~ s/\%content\%/$login/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($inputFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_weblog_podrazdeltextpage ( )
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода weblog-записи в WYSIWYG-редакторе.
#
#------------------------------------------------------------------------------

sub show_weblog_podrazdeltextpage {
    open( my $inputFH, "<", $ADMIN_SKIN_PATH );
    my @scheme = <$inputFH>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $login = _inner_weblog_podrazdeltextpage(@_);
            $scheme[$cnt] =~ s/\%content\%/$login/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($inputFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_startpage ( )
#------------------------------------------------------------------------------
#
# Внутренняя функция вывода главной страницы со списком всех weblog-записей.
#
#------------------------------------------------------------------------------

sub _inner_weblog_startpage {

    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $weblog_content_variable =
      qq~<div class=admin_header>$module_descr</div>~;
    my $process_status  = $_[3];
    my $process_message = $_[4];
    my $weblog_counter  = 0;
    my $pix_on_page     = 10;
    my $curr_page       = $_[2];
    my $filter;
    my $filter_request;
    my $original_f_req;
    my $information;

    if ( $process_status == 1 ) {
        my $ipdatetime = '';
        if ( localtime =~ /([\d]+:[\d]+:[\d]+)/ ) { $ipdatetime = "$1:"; }
        $information = "<div class=status>$ipdatetime Информация успешно обновлена!</div><br>\n";
    }
    else {
        if ( $process_status == 2 ) {
            $information = "<div class=error>$process_message</div><br>\n";
        }
        else {
            if ( $process_status == 3 ) {
                $filter         = $process_message;
                $filter_request = $_[5];
                $original_f_req = $filter_request;
            }
        }
    }

    $weblog_content_variable .= qq~
        $information
        <div style="margin: 0px 0px 2px 0px; width:100%" class="container">
            <a href="javascript:doSesChckLoad('$_[0]', '$ENV{SCRIPT_NAME}', '&amp;action=crweblograzdel')">Создать новую запись в блоге</a>
        </div>
        ~;

    if ( -e $MODULE_DB_FILE ) {

        open( my $inputFH, "<", $MODULE_DB_FILE );
        my @weblog_on_curr_page = sort { $b cmp $a } <$inputFH>;
        close $inputFH;

        if ( $filter eq 'exactsearch' ) {
            $filter_request =~ s/(\.html)|(\.htm)$//gi;
            my @searchresults = ();
            for (
                my $inxonpage = 0 ;
                $inxonpage <= $#weblog_on_curr_page ;
                $inxonpage++
              )
            {
                if ( $weblog_on_curr_page[$inxonpage] =~ /$filter_request/ ) {
                    push( @searchresults, $weblog_on_curr_page[$inxonpage] );
                }
            }
            if ( $#searchresults == -1 ) {
                $weblog_content_variable .= qq~
                    <p class="standart_text" style="font-size:12pt;" align="center">
                    По запросу <i>$original_f_req</i> ничего не найдено!</p>
                ~;
                $process_status = 0;
                $original_f_req = '';

                #return $weblog_content_variable;
            }
            else { @weblog_on_curr_page = @searchresults; }
        }

        # Вывод раздела в список

        if ( $#weblog_on_curr_page > -1 ) {
            $weblog_content_variable .= qq~
                <div align="right" style="margin-bottom:20px;">
                    <form   name="searchweblog"
                            method=post
                            action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=searchweblog&amp;page=$curr_page"
                            style="margin:0">
                        <input type="text" name="search" id="search" placeholder="поиск по страницам" value="$original_f_req" class="input-styled">&nbsp;
                        <input type="submit" value="Вперед!" class="installbutton" style="width:100px; height:31px;">
                    </form>
                </div>
                ~;

           # Формирование навигации по страницам

            my $page_linx;
            my $pages     = 0;

            if ( $process_status != 3 ) {

                $pages = floor( $#weblog_on_curr_page / $pix_on_page );

                if (   ( $pages * $pix_on_page <= $#weblog_on_curr_page )
                    || ( $pages < 1 ) )
                {
                    $pages++;
                }

                if ( $curr_page >= $pages ) { $curr_page = 0; }
                $page_linx = '&nbsp;-&nbsp;';
                for ( my $i = 1 ; $i <= $pages ; $i++ ) {
                    if ( ( $i - 1 ) != $curr_page ) {
                        $page_linx .= "<a href=\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblog&amp;page="
                          . ( $i - 1 )
                          . "\" class=admpageslink>"
                          . $i
                          . "</a>&nbsp;-&nbsp;";
                    }
                    else {
                        $page_linx .=
                            "<span class=lightadminpagelink>"
                          . $i
                          . "</span>&nbsp;-&nbsp;";
                    }
                }

                if ( $curr_page >= $pages ) { $curr_page = $pages - 1; }
                my $end = ( ( $curr_page + 1 ) * $pix_on_page ) - 1;
                if ( $end > $#weblog_on_curr_page ) {
                    $end = $#weblog_on_curr_page;
                }
                @weblog_on_curr_page =
                  @weblog_on_curr_page[ $curr_page * $pix_on_page .. $end ];

            }
            else {
                $page_linx = qq~
                    &nbsp;-&nbsp;<a href=\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblog&amp;page=$curr_page\"
                                    class=admpageslink>Назад</a>&nbsp;-&nbsp;~;
            }

            # Формирование навигации по страницам $seotags_content_variable .= join("<br>",@items_on_this_page);

            my $weblog_counter = 0;
            $weblog_content_variable .= qq~<table cellspacing="0" cellpadding="10" width="100%" class="tabselect">~;
            for (
                my $inxonpage = 0 ;
                $inxonpage <= $#weblog_on_curr_page ;
                $inxonpage++
              )
            {
                my $rand    = rand();
                my @tmp_arr = split( /\|/, $weblog_on_curr_page[$inxonpage] );
                my $bgcolor = 'class="tr01"';
                if ( $weblog_counter % 2 == 0 ) { $bgcolor = 'class="tr02"'; }
                $tmp_arr[$#tmp_arr] =~ s/[\n]+//g;

                my $updownlinks = '';
                my $nextpage    = $curr_page;
                my $prevpage    = $curr_page;
                if ( $inxonpage == $#weblog_on_curr_page ) { $nextpage++; }
                if ( $inxonpage == 0 ) {
                    $prevpage--;
                    if ( $prevpage < 0 ) { $prevpage = 0; }
                }
                if ( $process_status != 3 ) {
                    if ( $#weblog_on_curr_page > 0 ) {
                        my $weblogsortID = $tmp_arr[0];
                        $weblogsortID =~ s/[\D]+//s;
                        $updownlinks = qq~
                            <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblogmoveup&amp;sgid=$weblogsortID&amp;page=$prevpage"
                               class="fa-action-link">
                            <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>
                            <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblogmovedown&amp;sgid=$weblogsortID&amp;page=$nextpage"
                               class="fa-action-link">
                            <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>~;
                        if ( $inxonpage == 0 && $curr_page == 0 ) {
                            $updownlinks = qq~
                                <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>
                                <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblogmovedown&amp;sgid=$weblogsortID&amp;page=$curr_page"
                                   class="fa-action-link">
                                <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>~;
                        }
                        if (   $inxonpage == $#weblog_on_curr_page
                            && $curr_page == ( $pages - 1 ) )
                        {
                            $updownlinks = qq~
                            <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblogmoveup&amp;sgid=$weblogsortID&amp;page=$curr_page"
                               class="fa-action-link">
                            <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>
                            <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
                        }
                    }
                    else {
                        if ( $curr_page > 0 ) {
                            my $weblogsortID = $tmp_arr[0];
                            $weblogsortID =~ s/[\D]+//s;
                            $updownlinks = qq~
                                <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblogmoveup&amp;sgid=$weblogsortID&amp;page=$prevpage"
                                   class="fa-action-link">
                                <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>
                                <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                                ~;
                        }
                        else {
                            $updownlinks = qq~
                                <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>
                                <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                                ~;
                        }
                    }
                }
                else {
                    $updownlinks = qq~
                        <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>
                        <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                    ~;
                }
                my @urls = ();
                my @sub_sr = split( /\,/, $tmp_arr[3] );
                for ( my $j = 0 ; $j <= $#sub_sr ; $j++ ) {
                    push( @urls,
                        qq~$sub_sr[$j]<span style="color:#C0C0C0">.html</span>~
                    );
                }
                my $urls = join( '<br>', @urls );
                my $jsdelmess = "Вы уверены, что хотите удалить запись в блоге &quot;$tmp_arr[3]&quot; безвозвратно?";
                my $jsdelurl  = "$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=delweblog&amp;razdelid=$tmp_arr[1]&amp;page=$curr_page";
                my $warning   = $tmp_arr[-1] == 1 ? '<span class="pheix-badge-cont-data pheix-badge-warn"><i class="fa fa-warning" title="Редактировался HTML код!"></i></span>' : '';
                $weblog_content_variable .= qq~
                  <tr $bgcolor>
                     <td width="10%" class="standart_text" align="center">$updownlinks</td>
                     <td width="60%" class="standart_text" style="font-size:11pt;"><b>$urls</b>
                       <div class=cmstext style="padding-top:10; font-size:10pt; color:#606060">$tmp_arr[7]</div>
                       <div class=cmstext style="font-size:10pt; color:#A0A0A0">$tmp_arr[8]</div>
                     </td>
                     <td width="30%" class="standart_text" align=right>
                     <div class="pheix-badge-cont">
                     <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=addtexttoweblograzdel&amp;razdelid=$tmp_arr[1]&amp;page=$curr_page"
                        class="fa-action-link">
                       <i class="fa fa-paragraph" title="Редактировать в WYSISWG редакторе"></i></a>
                       $warning
                     </div>
                     <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=addhtmltoweblograzdel&amp;razdelid=$tmp_arr[1]&amp;page=$curr_page"
                        class="fa-action-link">
                       <i class="fa fa-code" title="Добавить HTML контент"></i></a>
                     <a href="javascript:doSesChckLoad('$_[0]','$ENV{SCRIPT_NAME}','&amp;action=editweblog&amp;razdelid=$tmp_arr[1]&amp;page=$curr_page')"
                        class="fa-action-link">
                       <i class="fa fa-pencil-square-o" title="Редактировать запись"></i></a>
                     <a href="javascript:Confirm('$jsdelurl','$jsdelmess')" class="fa-action-link">
                       <i class="fa fa-trash" title="Удалить запись"></i></a></td></tr>~;
                $weblog_counter++;
            }
            $weblog_content_variable .=
              qq~</table><p align=right>$page_linx</p>~;
        }
        else {
            my $filesize = ( -s $MODULE_DB_FILE );
            $weblog_content_variable .= qq~
                <div class="pheix-error-alert">Список записей пуст - $MODULE_DB_FILE size=$filesize.</div>~;
        }

    }    # if (-e $MODULE_DB_FILE)
    else {
        $weblog_content_variable .= qq~<div class="pheix-error-alert">Список записей пуст - файл $MODULE_DB_FILE не найден.</div>~;
    }
    return $weblog_content_variable;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_createrazdelpage ( )
#------------------------------------------------------------------------------
#
# Внутренняя функция вывода основной формы создания новой weblog-записи.
#
#------------------------------------------------------------------------------

sub _inner_weblog_createrazdelpage {
    my $formname                           = "slickgalleryform";
    my $page_ref                           = $_[2];
    my $weblog_createrazdelpage_date       = Pheix::Tools::getDateUpdate();
    my $_galjson = "\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalgetall\"";
    my $_galactv;
    eval {
        $_galactv = Gallery::System::get_power_status_for_module();
    };
    if ($@ || $_galactv == 1) {
        $_galjson = "\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admwlautocompletecap\"";
    }
    my $weblog_createrazdelpage_createform = qq~
        <script type="text/javascript">
        jQuery(function(){
           var formname = "$formname";
           function log( message ) {
               top.document.forms[formname].elements["galleryid"].value = message;
               top.document.forms[formname].elements["hiddengalleryid"].value = message;
           }
           jQuery( "#galsrch" ).autocomplete({
               source: $_galjson,
               minLength: 1,
               select: function( event, ui ) {
                   log( ui.item ?
                   ui.item.id :
                   "-1" );
               },
               close: function( event, ui ) {
                   var itemValue = top.document.forms[formname].elements["gallerysearch"].value;
                   if (itemValue == -1) {
                       top.document.forms[formname].elements["gallerysearch"].value = '';
                       jQuery("#galCarousel").empty();
                   } else {
                       var itemId = top.document.forms[formname].elements["hiddengalleryid"].value;
                       if (itemId > 0 ) {
                           doInlineDataLoad("galCarousel", "$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalgetcarousel&amp;galid="+itemId);
                       }
                   }
               },
         });
       });
       </script>

       <div class="popup_header">Создание новой записи</div>
       <div class=standart_text style="margin: 0 0 20px 1px">
           <a  href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblog&amp;page=$page_ref"
               class=simple_link><b>Записи в блоге</b></a> / Добавление новой записи в блог
       </div>
       <form   name="$formname"
               method=post
               enctype="multipart/form-data"
               action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=crnewweblograzdel&amp;page=$page_ref"
               style="margin:0">

       <table cellspacing=0 cellpadding=2 border=0 width="800">
           <tr>
               <td width="300" valign="top">
                <div class="div-td-lft">
                 <div>ЧПУ страницы <i>(без .html)</i>: <span class="text-grey">[обязательное поле]</span></div>
                 <div><input   type="text" required placeholder="ЧПУ страницы"
                               name="seochpu"
                               value=""
                               class="input"
                               style="width:100%" ></div>
                 <div>Текст для тега <i>&lt;TITLE&gt;&lt;/TITLE&gt;</i>:</div>
                 <div><input   type="text" placeholder="Текст для тега TITLE"
                               name="seotitle"
                               value=""
                               class="input"
                               style="width:100%"></div>
                 <div>Текст для тега <i>&lt;META name=&quot;description&quot; /&gt;</i>:</div>
                 <div><input   type="text" placeholder="Текст для тега META.description"
                               name="seometadescr"
                               value=""
                               class="input"
                               style="width:100%"></div>
                 <div>Текст для тега <i>&lt;META name=&quot;keywords&quot; /&gt;</i>:</div>
                 <div><input   type="text" placeholder="Текст для тега META.keywords"
                               name="seometakeys"
                               value=""
                               class="input"
                               style="width:100%"></div>
                 <div>Дата:</div>
                 <div><input type=text name=date value="$weblog_createrazdelpage_date" class=input style="width:50%"></div>
                 <div>Заголовок записи в блоге: <span class="text-grey">[обязательное поле]</span></div>
                 <div><input type=text name=header value="" class=input style="width:100%" required placeholder="Заголовок записи"></div>
                 <div>Краткое содержание записи: <span class="text-grey">[обязательное поле]</span></div>
                 <div><textarea    name="comment"
                                   rows="5" cols="5"
                                   class="input"
                                   style="width:100%; height:100px;"
                                   required placeholder="Краткое содержание записи"></textarea></div>
                 <div><input type=submit value="Сохранить изменения" class="button" style="width:150px;"></div>
                </div></td>
               <td width="500" valign="top">
                <div class="div-td-rght">
                 <div>Twitter-аккоунт автора:</span></div>
                 <div><input   type="text" placeholder="\@twitteraccount"
                               name="twitteraccount"
                               value=""
                               class="input"
                               style="width:100%" ></div>
                 <div>Связать с фотогалереей:</div>
                 <div><input   type="text" placeholder="Введите название существующей галереи"
                               name="gallerysearch"
                               id="galsrch"
                               value=""
                               class="input"
                               style="width:100%"></div>
                 <div>Идентификатор фотогалереи:</div>
                 <div>
                 <input   type="hidden" value="" name="hiddengalleryid">
                 <input   type="text" disabled
                               name="galleryid"
                               id="galselectedid"
                               value="0"
                               class="input-disabled"
                               style="width:60%"></div>
                 <div>Путь до отображаемого на Twitter-карте изображения:</div>
                 <div><input   type="text" placeholder="Выберите изображение из галереи или введите путь"
                               name="twitterimage"
                               id="twtrimg"
                               value=""
                               class="input"
                               style="width:100%"></div>
                 <div><input type="checkbox" id="c2" name="publishcard" value="1"/>
                 <label for="c2" class=cb-lbl-2><span></span>Опубликовать twitter-карту</label>
                 </div>
                 <div id="galCarousel" style="padding:0; margin:0 0 0 5px; background:#F1F1F1; width:495px"></div>
                 <!--<div class="ui-widget" style="margin-top:2em; font-family:Arial">
                 Result:<div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content"></div></div>-->
               </div></td>
           </tr>
       </table>
       </form>
    ~;
    return $weblog_createrazdelpage_createform;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_createrazdelpage ( )
#------------------------------------------------------------------------------
#
# Внутренняя функция вывода формы редактирования новой weblog-записи.
#
#------------------------------------------------------------------------------

sub _inner_weblog_editpage {
    my $formname       = "slickgalleryform";
    my $information;
    my @file_arr       = ();
    my $skobki_hr;
    my $random         = rand();
    my @weblog_details = ();
    my $page_ref       = $_[3];
    my $wl_editform;
    my $_galjson = "\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalgetall\"";
    my $_galactv;
    open( my $inputFH, "<", $MODULE_DB_FILE );
    my @indexes = <$inputFH>;
    close($inputFH);

    if ( $#indexes > -1 ) {
        foreach ( my $kinx = 0 ; $kinx <= $#indexes ; $kinx++ ) {
            my @temp_arr = split( /\|/, $indexes[$kinx] );
            if ( $temp_arr[1] eq $_[2] ) { @weblog_details = @temp_arr; last; }
        }

        $weblog_details[8] =~ s/\<br\>/\n/g;
        $weblog_details[$#weblog_details] =~ s/[\n]+//g;

        if ($weblog_details[13] == 1) {$weblog_details[13] = 'checked';}
            else {$weblog_details[13] = '';}

        my $gallery_carousel;
        my $twitterimage     = $weblog_details[12];
        my $gallery_id       = $weblog_details[11];
        if ( $gallery_id !~ /^[\d]+$/ ) { $gallery_id = 0; }
        eval {
            $gallery_carousel =
                Gallery::System::getGalleryCarousel( $_[0],
                    $gallery_id, $twitterimage );
            $_galactv = Gallery::System::get_power_status_for_module();
        };
        if ($@ || $_galactv == 1) {
            if ($gallery_id > 0) {
                my $mess = '&nbsp;Ошибка доступа к заданной галереи';
                $gallery_carousel = qq~
                    <div class="wl-err">$mess (id=$gallery_id)</div>
                ~;
            }
            $_galjson = "\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admwlautocompletecap\"";
        }
        $wl_editform .= qq~
        <script type="text/javascript">
        jQuery(function(){
           var formname = "$formname";
           function log( message ) {
               top.document.forms[formname].elements["galleryid"].value = message;
               top.document.forms[formname].elements["hiddengalleryid"].value = message;
               top.document.forms[formname].elements["twitterimage"].value = '';
           }
           jQuery( "#galsrch" ).autocomplete({
               //source: availableTags,
               source: $_galjson,
               minLength: 1,
               select: function( event, ui ) {
                   log( ui.item ?
                   ui.item.id :
                   "-1" );
               } ,
               close: function( event, ui ) {
                   var itemValue = top.document.forms[formname].elements["gallerysearch"].value;
                   if (itemValue == -1) {
                       top.document.forms[formname].elements["gallerysearch"].value = '';
                       jQuery("#galCarousel").empty();
                   } else {
                       var itemId = top.document.forms[formname].elements["hiddengalleryid"].value;
                       if (itemId > 0 ) {
                           doInlineDataLoad("galCarousel", "$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalgetcarousel&amp;galid="+itemId);
                       }
                   }
               }
         });
       });
       </script>

        <div class="popup_header">Редактирование записи в блоге</div>
        <div class=standart_text style="margin: 0 0 20px 1px">
               <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblog&amp;page=$page_ref" class=simple_link>
               <b>Записи в блоге</b></a> / Редактирование записи «$weblog_details[3]»
        </div>
        <form  name="$formname"
               method=post enctype="multipart/form-data"
               action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=modweblograzdel&amp;blogmessid=$weblog_details[1]&amp;page=$page_ref"
               style="margin:0">

        <table cellspacing=0 cellpadding=2 border=0 width="800">
           <tr>
               <td width="300" valign="top">
                <div class="div-td-lft">
                 <div>ЧПУ страницы <i>(без .html)</i>: <span class="text-grey">[обязательное поле]</span></div>
                 <div><input   type="text" required placeholder="ЧПУ страницы"
                               name="seochpu"
                               value="$weblog_details[3]"
                               class="input"
                               style="width:100%" ></div>
                 <div>Текст для тега <i>&lt;TITLE&gt;&lt;/TITLE&gt;</i>:</div>
                 <div><input   type="text" placeholder="Текст для тега TITLE"
                               name="seotitle"
                               value="$weblog_details[4]"
                               class="input"
                               style="width:100%"></div>
                 <div>Текст для тега <i>&lt;META name=&quot;description&quot; /&gt;</i>:</div>
                 <div><input   type="text" placeholder="Текст для тега META.description"
                               name="seometadescr"
                               value="$weblog_details[5]"
                               class="input"
                               style="width:100%"></div>
                 <div>Текст для тега <i>&lt;META name=&quot;keywords&quot; /&gt;</i>:</div>
                 <div><input   type="text" placeholder="Текст для тега META.keywords"
                               name="seometakeys"
                               value="$weblog_details[6]"
                               class="input"
                               style="width:100%"></div>
                 <div>Дата:</div>
                 <div><input type=text name=date value="$weblog_details[2]" class=input style="width:50%">&nbsp;&nbsp;&nbsp;&nbsp;
                      <a href="javascript:Currdate(4,'$formname');" class="simple_link">Текущая дата</a></div>
                 <div>Заголовок записи в блоге: <span class="text-grey">[обязательное поле]</span></div>
                 <div><input type=text name=header value="$weblog_details[7]" class=input style="width:100%" required placeholder="Заголовок записи"></div>
                 <div>Краткое содержание записи: <span class="text-grey">[обязательное поле]</span></div>
                 <div><textarea    name="comment"
                                   rows="5" cols="5"
                                   class="input"
                                   style="width:100%; height:100px;"
                                   required placeholder="Краткое содержание записи">$weblog_details[8]</textarea></div>
                 <div><input type=submit value="Сохранить изменения" class="button" style="width:150px;"></div>
                </div></td>
               <td width="500" valign="top">
                <div class="div-td-rght">
                 <div>Twitter-аккоунт автора:</span></div>
                 <div><input   type="text" placeholder="\@twitteraccount"
                               name="twitteraccount"
                               value="$weblog_details[9]"
                               class="input"
                               style="width:100%" ></div>
                 <div>Связать с фотогалереей:</div>
                 <div><input   type="text" placeholder="Введите название существующей галереи"
                               name="gallerysearch"
                               id="galsrch"
                               value="$weblog_details[10]"
                               class="input"
                               style="width:100%"></div>
                 <div>Идентификатор фотогалереи:</div>
                 <div>
                 <input   type="hidden" value="$gallery_id" name="hiddengalleryid">
                 <input   type="text" disabled
                               name="galleryid"
                               id="galselectedid"
                               value="$gallery_id"
                               class="input-disabled"
                               style="width:60%"></div>
                 <div>Путь до отображаемого на Twitter-карте изображения:</div>
                 <div><input   type="text" placeholder="Выберите изображение из галереи или введите путь"
                               name="twitterimage"
                               id="twtrimg"
                               value="$twitterimage"
                               class="input"
                               style="width:100%"></div>
                 <div><input type="checkbox" id="c2" name="publishcard" value="1" $weblog_details[13]/>
                 <label for="c2" class=cb-lbl-2><span></span>Опубликовать twitter-карту</label>
                 </div>
                 <div id="galCarousel" style="padding:0; margin:0 0 0 5px; background:#F1F1F1; width:495px">$gallery_carousel</div>
               </div></td>
           </tr>
        </table>
        </form>
        ~;
        } else {
        $wl_editform .= qq~
            <p class=error>
                Список подразделов пуст (\$#indexes = $#indexes)!</p>
        ~;
    }
    return $wl_editform;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_add ( )
#------------------------------------------------------------------------------
#
# Добавление записи в БД.
#
#------------------------------------------------------------------------------

sub _inner_weblog_add {
    my $outputFH;
    my @args = @_;
    if ( -e $MODULE_DB_FILE ) {
        open( $outputFH, "+<", $MODULE_DB_FILE );
    }
    else { open( $outputFH, ">", $MODULE_DB_FILE ); }
    my @indexes = <$outputFH>;
    my $date    = Pheix::Tools::getDateUpdate();
    for (my $i = 0; $i <= $#args; $i++) {
        $args[$i] =~ s/[\r\n\t]//gi;
    }
    push( @indexes, join( "|", @_) . "\n" );
    seek( $outputFH, 0, 0 );
    flock( $outputFH, LOCK_EX );
    print $outputFH @indexes;
    truncate( $outputFH, tell($outputFH) );
    flock( $outputFH, LOCK_UN );
    close($outputFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_delete ( )
#------------------------------------------------------------------------------
#
# Удаление записи из БД.
#
#------------------------------------------------------------------------------

sub _inner_weblog_delete {
    open( my $outputFH, "+<", $MODULE_DB_FILE );
    my @indexes = <$outputFH>;
    my $count = 0;
    foreach ( my $kinx = 0 ; $kinx <= $#indexes ; $kinx++ ) {
        my $check = $indexes[$kinx];
        $check =~ s/[\n]+//g;
        if ( $check ne '' ) { $count++; }
    }
    if ( $count > 1 ) {
        for ( my $i = 0 ; $i <= $#indexes ; $i++ ) {
            my @temp_arr = split( /\|/, $indexes[$i] );
            if ( $_[0] eq $temp_arr[1] ) {
                my @ostatok   = @indexes[ $i + 1 .. $#indexes ];
                my @pre_array = @indexes[ 0 .. $i - 1 ];
                @indexes   = ( @pre_array, @ostatok );
                if ( -e "conf/pages/$_[0].txt" ) {
                    unlink("conf/pages/$_[0].txt");
                }
                last;
            }
        }
        seek( $outputFH, 0, 0 );
        flock( $outputFH, LOCK_EX );
        print $outputFH @indexes;
        truncate( $outputFH, tell($outputFH) );
        flock( $outputFH, LOCK_UN );
        close($outputFH);
    }
    # $count > 1
    else {
        close($outputFH);
        if ( ( -e "conf/system/weblog.tnk" ) ) {
            unlink("conf/system/weblog.tnk");
        }
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_modify ( )
#------------------------------------------------------------------------------
#
# Изменение записи из БД.
#
#------------------------------------------------------------------------------

sub _inner_weblog_modify {
    my @args = @_;
    open my $fh, "+<", $MODULE_DB_FILE ;
    my @_rows = <$fh>;
    for my $i (0..$#args) {
        $args[$i] =~ s/[\r\n\t]//gi;
    }
    for my $i (0..$#_rows) {
        my @_cols = split( /\|/, $_rows[$i] );
        if ( $args[1] eq $_cols[1] ) {
            $_cols[-1] =~ s/[\r\n]//gi;
            my @_nvals = @args;
            $_nvals[0] = $_cols[0];
            $_nvals[1] = $_cols[1];
            $_rows[$i] = join( "|", @_nvals, $_cols[-1] ) . "\n";
            last;
        }
    }
    seek $fh, 0, 0;
    flock $fh, LOCK_EX;
    print $fh @_rows;
    truncate( $fh, tell($fh) );
    flock $fh, LOCK_UN;
    close $fh;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_getdetails ( )
#------------------------------------------------------------------------------
#
# Детализация информации о weblog-записи из БД.
#
#------------------------------------------------------------------------------

sub _inner_weblog_getdetails {
    open( my $inputFH, "<", $MODULE_DB_FILE );
    my @indexes  = <$inputFH>;
    close($inputFH);
    my $mod_time = time;
    my $date     = Pheix::Tools::getDateUpdate();
    for ( my $i = 0 ; $i <= $#indexes ; $i++ ) {
        my @temp_arr = split( /\|/, $indexes[$i] );
        if ( $_[0] eq $temp_arr[1] ) { return @temp_arr; }
    }
    return;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_files_array ( )
#------------------------------------------------------------------------------
#
# Получение списка файлов некоторого каталога.
#
#------------------------------------------------------------------------------

sub get_files_array {
    my $dir = $_[0];
    $dir =~ s/\\/\//g;
    opendir DIR, $dir;
    my @files = grep { ( !/^\.+$/ ) and !( -d "$dir/$_" ) } readdir DIR;
    closedir DIR;
    return @files;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_podrazdeltextpage ( )
#------------------------------------------------------------------------------
#
# Внутренняя функция вывода формы редактирования weblog-записи
# в WYSIWYG-редакторе.
#
#------------------------------------------------------------------------------

sub _inner_weblog_podrazdeltextpage {
    my ($sid, $cryptlgn, $podrazdelid, $page_ref, $printflag) = @_;
    my $information;
    my $page_html;
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $weblog_content_variable =
        qq~<div class=admin_header>$module_descr</div>~;

    if ( $printflag == 1 ) {
        my $ipdatetime = '';
        if ( localtime =~ /([\d]+:[\d]+:[\d]+)/ ) { $ipdatetime = "$1:"; }
        $information = qq~
          <div class=status>
             $ipdatetime Информация успешно обновлена!</div><br>\n
        ~;
    }

    if ( -e "$MODULE_PG_PATH/$podrazdelid.txt" ) {
        open my $fh, "<", "$MODULE_PG_PATH/$podrazdelid.txt";
        $page_html = join( "", <$fh> );
        close $fh;
    }

    my @weblog_podrazdel_details = _inner_weblog_getdetails($podrazdelid);
    my $_tag = getEditorTag($podrazdelid);

    if ( $#weblog_podrazdel_details > -1 ) {
        $weblog_content_variable .= qq~
          <div class=standart_text style="margin: 0 0 20px 1px">
            <a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admweblog&amp;page=$page_ref" class=simple_link>
            <b>Записи в блоге</b></a> / Редактирование содержимого записи «$weblog_podrazdel_details[3]» <i style="color:#808080">(id=$podrazdelid)</i>
          </div>
          $information
          <form method=post
                action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admweblogpodrazdelpost&amp;radelid=$podrazdelid&amp;page=$page_ref"
                enctype="multipart/form-data"
                style="margin:0 0 20px 0;">
          <textarea id="weblogpodrazdelpostcontent" name="pagecontent" rows=5 cols=5>$page_html</textarea>
          <script type="text/javascript">
              CKEDITOR.replace(
                'pagecontent',
                {
                    pheixScript: '$ENV{SCRIPT_NAME}', pheixSessionId: '$sid', pheixGalDivId: 'weblog-divgal-postcontent',
                    on: {
                        save: function(evt) {
                            var edtrTag = $_tag;
                            if (edtrTag == 1) {
                                if (!confirm('Данные ранее редактировались HTML-редактором! Сохранение может уничтожить созданную разметку. Продолжить?')) {
                                    return false;
                                }
                            }
                        }
                    }
                }
              );
          </script>
          <input type=hidden name="preview" value="0">
          </form>
        ~;
    }
    else {
        $weblog_content_variable = qq~
            <div class="pheix-error-alert">
            Несуществующая запись id=$podrazdelid, данные не обнаружены!</div>
        ~;
    }
    return $weblog_content_variable;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_savepodrazdelcontent ( )
#------------------------------------------------------------------------------
#
# Сохранение содержимого weblog-записи из WYSIWYG-редакторе в файл.
#
#------------------------------------------------------------------------------

sub _inner_weblog_savepodrazdelcontent {
    my ($content_to_save, $podrazdelid, $editortag) = @_;
    if ( $podrazdelid =~ /^[\d]+$/ ) {
        open my $fh, ">", "$MODULE_PG_PATH/$podrazdelid.txt";
        print $fh $content_to_save;
        close $fh;
        setEditorTag($podrazdelid, $editortag);
        return 1;
    }
    return 0;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_sgmoveup ( )
#------------------------------------------------------------------------------
#
# Функция передвижения записи в блоге вверх по списку.
#
#------------------------------------------------------------------------------

sub _inner_weblog_sgmoveup {

    if ( -e $MODULE_DB_FILE  ) {
        open( my $outputFH, "+<", $MODULE_DB_FILE );
        my @cats_page = sort { $b cmp $a } <$outputFH>;

        for ( my $i = 0 ; $i <= $#cats_page ; $i++ ) {
            my @tmp = split( /\|/, $cats_page[$i] );

            #print "<h5>$tmp[0]:$i</h5>";
            if ( $tmp[0] == $_[0] ) {
                my @tmp2 = split( /\|/, $cats_page[ $i - 1 ] );
                my $buf = $tmp2[0];
                $tmp2[0] = $tmp[0];
                $tmp[0]  = $buf;
                $cats_page[ $i - 1 ] = join( "|", @tmp2 );
                $cats_page[$i] = join( "|", @tmp );
                last;
            }
        }
        seek( $outputFH, 0, 0 );
        flock( $outputFH, LOCK_EX );
        print $outputFH @cats_page;
        truncate( $outputFH, tell($outputFH) );
        flock( $outputFH, LOCK_UN );
        close($outputFH);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_sgmovedown ( )
#------------------------------------------------------------------------------
#
# Функция передвижения записи в блоге вниз по списку.
#
#------------------------------------------------------------------------------

sub _inner_weblog_sgmovedown {

    if ( -e $MODULE_DB_FILE  ) {
        open( my $outputFH, "+<", $MODULE_DB_FILE );
        my @cats_page = sort { $b cmp $a } <$outputFH>;

        for ( my $i = 0 ; $i <= $#cats_page ; $i++ ) {
            my @tmp = split( /\|/, $cats_page[$i] );

            #print "<h4>$tmp[0]</h4>";
            if ( $tmp[0] == $_[0] ) {
                my @tmp2 = split( /\|/, $cats_page[ $i + 1 ] );
                my $buf = $tmp2[0];
                $tmp2[0] = $tmp[0];
                $tmp[0]  = $buf;
                $cats_page[ $i + 1 ] = join( "|", @tmp2 );
                $cats_page[$i] = join( "|", @tmp );
                last;
            }
        }
        seek( $outputFH, 0, 0 );
        flock( $outputFH, LOCK_EX );
        print $outputFH @cats_page;
        truncate( $outputFH, tell($outputFH) );
        flock( $outputFH, LOCK_UN);
        close($outputFH);
    }
}

#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# getHtmlEditor ( @_ )
#----------------------------------------------------------------------------
#
# @_ - массив всходных аргументов функции showHtmlEditor()
# Вывод HTML кода содержимого записи в в редакторе.
# Функция выводит HTML код содержимого записи в редакторе CodeMirror.
#
#----------------------------------------------------------------------------

sub getHtmlEditor {
    my $sesid           = $_[0];
    my $podrazdelid     = $_[2];
    my $page_ref        = $_[3];
    my $uploadstatus    = $_[5];
    my $information;
    my $recordHTML;
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $HtmlEditorContent =
      qq~<div class=admin_header>$module_descr</div>~;
    if ( $_[4] == 1 ) {
        my $ipdatetime = '';
        if ( localtime =~ /([\d]+:[\d]+:[\d]+)/ ) { $ipdatetime = "$1:"; }
        $information = qq~
          $uploadstatus
          <div class=status>
             $ipdatetime Информация успешно обновлена!</div><br>
        ~;
    }

    my @weblog_podrazdel_details = &_inner_weblog_getdetails($podrazdelid);
    if ( $#weblog_podrazdel_details > -1 ) {
        if (-e "$MODULE_PG_PATH/$podrazdelid.txt") {
            open( my $inputFH, "<", "$MODULE_PG_PATH/$podrazdelid.txt" );
            $recordHTML = join("",<$inputFH>);
            $recordHTML =~ s/\<\/textarea/\&lt;\/textarea/g;
            close($inputFH);
        }
        my $add_htmlfile_form = &getDownloadHtmlForm($sesid, $podrazdelid, $page_ref);
        $HtmlEditorContent .= qq~
          <div class=standart_text style="margin: 0 0 20px 1px">
            <a href="$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admweblog&amp;page=$page_ref" class=simple_link>
            <b>Записи в блоге</b></a> / Редактирование HTML содержимого записи «$weblog_podrazdel_details[3]» <i style="color:#808080">(id=$podrazdelid)</i>
          </div>
          $information
          <div class="spoiler-wrapper" style="width:98%">
            <div class="spoiler folded">
                <a href="#" class="spoilerlink">
                    Загрузить файл с HTML разметкой на сервер</a></div>
            <div class="spoiler-text">$add_htmlfile_form</div>
          </div>
          <script type="text/javascript">
           jQuery(document).ready(function(){
               jQuery('.spoiler-text').hide()
               jQuery('.spoiler').click(function(){
                   jQuery(this).toggleClass("folded").toggleClass("unfolded").next().slideToggle()
               })
           })
          </script>
          <div class="akatabselect">
          <form   style="margin:0"
                  method="post"
                  action="$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admwebloghtmlpost&amp;blogmessid=$podrazdelid&amp;page=$page_ref">
           <div class="htmleditor">
             <textarea name="pagehtmlcontent" id="WeblogHtmlEditor">$recordHTML</textarea>
           </div>
           <div align=right>
             <input value="Сохранить изменения" class="installbutton" type="submit">
           </div>
          </form>
          </div>
          <script>
            var editor = CodeMirror.fromTextArea(document.getElementById("WeblogHtmlEditor"), {
                lineNumbers: true,
                mode: "text/html",
                matchBrackets: true
                });
          </script>
        ~;
    }
    else {
        $HtmlEditorContent .= qq~
            <div class="pheix-error-alert">
            Несуществующая запись id=$podrazdelid, данные не обнаружены!</div>
        ~;
    }
    return $HtmlEditorContent;
}

#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# getDownloadHtmlForm ( $session_id )
#----------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод формы загрузки HTML-файла с содержимым записи на сервер.
# Функция возвращает HTML код формы загрузки HTML-файла на сервер.
#
#----------------------------------------------------------------------------

sub getDownloadHtmlForm() {
    my ($sesid, $podrazdelid, $page_ref) = @_;
    my $installModuleFormHtml = qq~
        <table cellspacing="2" cellpadding="0" width="100%">
        <tr><td align="right" valign="bottom">&nbsp;</td></tr>
        <tr><td valign="bottom">
        <form
            method="post"
            action="$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admwebloghtmldload&amp;blogmessid=$podrazdelid&amp;page=$page_ref"
            enctype="multipart/form-data">
         <table
            cellspacing="2"
            cellpadding="9"
            width="100%"
            class="messagelist"
            style="background-image:url('images/social_bg.png');height:94">
         <tr>
            <td
                width="100%"
                class="s_t"
                style="font-family:Arial,Tahoma;color:#505050;font-size:12pt;">
                <table cellspacing="0" cellpadding="0" width="205">
                <tr>
                    <td width="160" class="inst_mod_head">
                        Загрузка&nbsp;файла&nbsp;на&nbsp;сервер&nbsp;[</td>
                    <td width="25">
                        <img
                            src="images/admin/cms/filetypes/txt.png"
                            border=0
                            alt="">
                    </td>
                    <td width="20" class="inst_mod_head">]&nbsp;:</td>
                </tr></table>
            </td></tr>
            <tr><td width="100%">
            <div class="f_imulation_wrap">
                <div class="im_input">
                    <input type="text"value="Выберите файл c HTML разметкой"/>
                </div>
                <input
                    type="file"
                    required
                    size=30
                    name="uploadfile"
                    id="imulated"
                    style="margin-left:-124px;margin-top:7px;height:20px"/>
            </div>
            <script type="text/javascript">
                \$('.im_input input').click(function(){
                    \$('.im_input input').css('color','#C0C0C0');
                    \$('.im_input input').val('Выберите файл c HTML разметкой');
                    \$("#imulated").val('');
                    \$('#imulated').trigger('click');
                });
                \$('#imulated').change(function(){
                    \$('.im_input input').css('color','#505050');
                    \$('.im_input input').val(\$(this).val());
                });
            </script>
            </td></tr>
            <tr><td width="100%">
                <input
                    type="submit"
                    value="Загрузить на сервер"
                    class="installbutton">
            </td></tr>
        </table>
        </form>
        </td></tr>
        </table>
    ~;
    return $installModuleFormHtml;
}

#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# doFileUpload ( $filehandle, $podrazdelid )
#----------------------------------------------------------------------------
#
# $filehandle - указатель на файл
# $podrazdelid - идентификатор подраздела
# Загрузка файла на сервер.
# Функция возвращает массив (<статус_загрузки>, <имя_загруженного_файла>) или
# сообщение об ошибке в HTML формате.
#
#----------------------------------------------------------------------------

sub doFileUpload {
    my ($file_to_upload, $podrazdelid) = @_;
    my $res;
    my $path_to_upload = $MODULE_PG_PATH;
    my $tmp_file_name  = $podrazdelid . '.txt';
    if ( $file_to_upload ne '' ) {
        $res = Pheix::Tools::doFileUpload( $path_to_upload, 'uploadfile',
            $tmp_file_name, 0, 0 );
        return ( $res, $tmp_file_name );
    } else {
        return ( qq~<div class=error>Файл не найден!</div>~, 0 );
    }
}

#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# setEditorTag ( $podrazdelid, $tag )
#----------------------------------------------------------------------------
#
# $podrazdelid - идентификатор подраздела
# tag - маркер редактора
# Функция маркирует запись в БД с заданным идентификатором маркером
# соответствующего редактора.
#
#----------------------------------------------------------------------------

sub setEditorTag {
    my ($podrazdelid, $tag) = @_;
    if ( -e $MODULE_DB_FILE  ) {
        open my $fh, '<', $MODULE_DB_FILE;
        my @_rows = <$fh>;
        close $fh;
        for my $i (0..$#_rows) {
            my @_cols = split /\|/, $_rows[$i];
            if ($podrazdelid == $_cols[1]) {
                $_cols[-1] = $tag."\n";
                $_rows[$i] = join '|', @_cols;
                last;
            }
        }
        open $fh, ">", $MODULE_DB_FILE or
            die "unable to open $MODULE_DB_FILE: ".$!;
        seek $fh, 0, 0;
        flock $fh, LOCK_EX or
            die "unable to lock $MODULE_DB_FILE: ".$!;
        print $fh @_rows;
        truncate $fh, tell $fh;
        flock $fh, LOCK_UN or
            die "unable to unlock $MODULE_DB_FILE: ".$!;
        close $fh or
            die "unable to close $MODULE_DB_FILE: ".$!;
    }
}

#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# getEditorTag ( $podrazdelid )
#----------------------------------------------------------------------------
#
# $podrazdelid - идентификатор подраздела
# Функция возвращает маркер редактора для заданной записи.
#
#----------------------------------------------------------------------------

sub getEditorTag {
    my ($podrazdelid ) = @_;
    my $_tag = 0;
    if ( -e $MODULE_DB_FILE  ) {
        open my $fh, '<', $MODULE_DB_FILE;
        my @_rows = <$fh>;
        close $fh;
        for my $i (0..$#_rows) {
            my @_cols = split /\|/, $_rows[$i];
            if ($podrazdelid == $_cols[1]) {
                $_cols[-1] =~ s/[\r\n]+//gi;
                if ($_cols[-1] == 1) {
                    $_tag = 1;
                }
                last;
            }
        }
    }
    return $_tag;
}

#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# getWlSitemap ( )
#----------------------------------------------------------------------------
#
# У функции нет входных аргументов
# Функция возвращает массив хэшей url-date.
#
#----------------------------------------------------------------------------

sub getWlSitemap {
    my @hashlist;
    my $_off = getModPowerOn($MODULE_NAME);
    my $_set = getSingleSetting($MODULE_NAME, 1, 'exportsitemap');
    if ( -e $MODULE_DB_FILE  && ( $_off == 0 ) && ( $_set == 1))  {
        open my $fh, '<', $MODULE_DB_FILE;
        my @_rows = <$fh>;
        close $fh;
        for my $i (0..$#_rows) {
            my ($_sort, $_id, $_date, $_actn, @_othr) = split /\|/, $_rows[$i];
            my $timestamp = strftime '%Y-%m-%d', localtime($_id);
            my $_fname = 'conf/pages/'.$_id.'.txt';
            if (-e $_fname) {
                eval {
                    $timestamp = strftime '%Y-%m-%d',
                        localtime((stat($_fname))[9]);
                };
            }
            my $_url = getProtoSrvName().'/'.$_actn.'.html';
            my %_rec = ( 'loc' => $_url, 'lastmod' => $timestamp );
            push @hashlist, \%_rec;
        }
    }
    return @hashlist;
}

#----------------------------------------------------------------------------

END { }

1;
