if ( $access_modes[2] == 1 || $access_modes[2] == 2 )   # actions compatible with access modes: r,rw
{

    # start of read-only actions

    if ( $env_params[1] eq 'admweblog' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $got_page = $env_params[2];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            Weblog::Static::show_weblog_startpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $got_page, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'crweblograzdel' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[2];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Weblog::Static::_inner_weblog_createrazdelpage(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $got_page );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'editweblog' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[3];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Weblog::Static::_inner_weblog_editpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $got_page, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'searchweblog' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $filter_request = $co->param('search');
            my $got_page      = $env_params[2];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            Weblog::Static::show_weblog_startpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $got_page, 3, 'exactsearch', $filter_request );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'addtexttoweblograzdel' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $got_page = $env_params[3];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            Weblog::Static::show_weblog_podrazdeltextpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $got_page, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'addhtmltoweblograzdel' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $got_page = $env_params[3];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            Weblog::Static::showHtmlEditor( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $got_page, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admwlautocompletecap' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print qq~[{ "label": "Module Gallery::System is inactive!", "value": "-1", "id": "0" }]~;
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    # end of read-only actions

    if ( $access_modes[2] == 2 )  # actions compatible just with access mode: rw
    {

        # start of read-write actions

        if ( $env_params[1] eq 'crnewweblograzdel' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                my $got_page     = 0;
                my $flag         = 0;
                my $uplmess      = '';
                my $weblog_sort  = time;
                my $weblog_id    = $weblog_sort;
                my $seochpu      = $co->param('seochpu');
                my $seotitle     = $co->param('seotitle');
                my $seometadescr = $co->param('seometadescr');
                my $seometakeys  = $co->param('seometakeys');
                my $date         = $co->param('date');
                my $header       = $co->param('header');
                my $comment      = $co->param('comment');
                my $twitteracc   = $co->param('twitteraccount');
                my $gallerysrch  = $co->param('gallerysearch');
                my $galleryid    = $co->param('hiddengalleryid');
                my $twitterimg   = $co->param('twitterimage');
                my $tcpublich    = $co->param('publishcard');
                if ($tcpublich !~ /^[\d]+$/) {$tcpublich = 0;}
                if ($tcpublich < 0 || $tcpublich > 1) {$tcpublich = 0;}
                $comment =~ s/[\r\n]+/\<br\>/g;

                if ( $seochpu ne '' && $comment ne '' && $header ne '' ) {
                    Weblog::Static::_inner_weblog_add(
                        $weblog_sort, $weblog_id,   $date,
                        $seochpu,     $seotitle,    $seometadescr,
                        $seometakeys, $header,      $comment,
                        $twitteracc,  $gallerysrch, $galleryid,
                        $twitterimg,  $tcpublich
                    );
                }
                else {
                    $flag    = 2;
                    $uplmess = "***error: found empty elements in form!";
                }

                print $co->header( -type => 'text/html; charset=UTF-8' );
                Weblog::Static::show_weblog_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $flag, $uplmess );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'delweblog' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                Weblog::Static::_inner_weblog_delete( $env_params[2] );
                Weblog::Static::show_weblog_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $SHOW_MESS );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'modweblograzdel' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                my $flag         = 1;
                my $uplmess      = '';
                my $weblog_sort  = $env_params[2];
                my $weblog_id    = $env_params[2];
                my $seochpu      = $co->param('seochpu');
                my $seotitle     = $co->param('seotitle');
                my $seometadescr = $co->param('seometadescr');
                my $seometakeys  = $co->param('seometakeys');
                my $date         = $co->param('date');
                my $header       = $co->param('header');
                my $comment      = $co->param('comment');
                my $twitteracc   = $co->param('twitteraccount');
                my $gallerysrch  = $co->param('gallerysearch');
                my $galleryid    = $co->param('hiddengalleryid');
                my $twitterimg   = $co->param('twitterimage');
                my $tcpublich    = $co->param('publishcard');
                if ($tcpublich !~ /^[\d]+$/) {$tcpublich = 0;}
                if ($tcpublich < 0 || $tcpublich > 1) {$tcpublich = 0;}
                $comment =~ s/[\r\n]+/\<br\>/g;

                if ( $seochpu ne '' && $comment ne '' && $header ne '' ) {
                    Weblog::Static::_inner_weblog_modify(
                        $weblog_sort, $weblog_id,   $date,
                        $seochpu,     $seotitle,    $seometadescr,
                        $seometakeys, $header,      $comment,
                        $twitteracc,  $gallerysrch, $galleryid,
                        $twitterimg,  $tcpublich
                    );
                }
                else {
                    $flag    = 2;
                    $uplmess = "***error: found empty elements in form!";
                }

                print $co->header( -type => 'text/html; charset=UTF-8' );
                Weblog::Static::show_weblog_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $flag, $uplmess );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admweblogpodrazdelpost' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                Weblog::Static::_inner_weblog_savepodrazdelcontent(
                    $co->param('pagecontent'),
                    $env_params[2], 0 );
                Weblog::Static::show_weblog_podrazdeltextpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2], $got_page, $SHOW_MESS );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admweblogmovedown' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Weblog::Static::_inner_weblog_sgmovedown( $env_params[2] );
                }
                Weblog::Static::show_weblog_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $DO_QUIET );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admweblogmoveup' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Weblog::Static::_inner_weblog_sgmoveup( $env_params[2] );
                }
                Weblog::Static::show_weblog_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $DO_QUIET );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admwebloghtmlpost' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                Weblog::Static::_inner_weblog_savepodrazdelcontent(
                    $co->param('pagehtmlcontent'),
                    $env_params[2], 1 );
                Weblog::Static::showHtmlEditor( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2], $got_page, $SHOW_MESS );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admwebloghtmldload' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                my @upload_result =
                  Weblog::Static::doFileUpload( $co->param('uploadfile'),
                    $env_params[2] );
                Weblog::Static::showHtmlEditor(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2],
                    $got_page,
                    $SHOW_MESS,
                    $upload_result[0]
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        # end of read-write actions

    }    # Access modes: rw
}    # Access modes: r