package Rss::Yandex;

use strict;
use warnings;

BEGIN {
    #for all servers
    use lib '../';    # lib path for work mode

    # lib path for systax check during module install
    use lib 'admin/libs/modules/';
    use lib 'admin/libs/extlibs/';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';
}

##############################################################################
#
# File   :  Yandex.pm
#
##############################################################################
#
# Главный пакет парсинга фида новостей Яндекса в адм. части CMS Pheix
#
##############################################################################

use POSIX;
use File::Path;
use Encode;
use Config::ModuleMngr;
use Pheix::Tools;
use LWP::UserAgent;
use XML::LibXML;
use Date::Parse;

my $MODULE_NAME       = "Rss";
my $MODULE_FOLDER     = "admin/libs/modules/$MODULE_NAME/";
my $INSTALL_FILE_PATH = "conf/system/install.tnk";
my $MODULE_DB_FILE =
  getConfigValue( $MODULE_FOLDER, getModXml($MODULE_FOLDER), "tankfile" );
my $ADMIN_SKIN_PATH = 'admin/skins/classic_.txt';
my $TOTAL_RECORDS_ONPAGE = getSingleSetting( $MODULE_NAME, 0, 'recordsonpage' );

if ($TOTAL_RECORDS_ONPAGE <= 0) {
    $TOTAL_RECORDS_ONPAGE = 1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModPwrStatus ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub getModPwrStatus {
    my $file_to_parse = $INSTALL_FILE_PATH;
    if ( -e $file_to_parse ) {
        open( CFGFH, "<$file_to_parse" );
        my @filecontent = <CFGFH>;
        close(CFGFH);
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my @tmp = split( /\|/, $filecontent[$admplinx] );
            if ( $tmp[2] eq $MODULE_NAME ) { return $tmp[4]; }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# RSSYshowHomePage ( $sesid, $selector, $level_num, $print_flag, $mess )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# $selector - селектор функции вывода контента.
# $level_num - вложенность относительно каталога установки pheix.
# $print_flag - флаг управления печатью.
# $mess - дополнительное сообщение, выводимое по флагу.
# Вывод главной страницы административной части.
#
#------------------------------------------------------------------------------

sub RSSYshowHomePage {
    my $sid      = $_[0];
    my $selector = $_[1];
    open my $ifh, "<", $ADMIN_SKIN_PATH;
    my @tmpl = <$ifh>;
    close $ifh;
    for my $i ( 0 .. $#tmpl ) {
        $tmpl[$i] = fillCommonTags( $sid, $tmpl[$i] );
        if ( $tmpl[$i] =~ /\%content\%/ ) {
            my $contentblock;
            if ( $selector == 1 ) {
                $contentblock = showParsedRssResult(@_);
            }
            elsif ( $selector == 2 ) {
                ;
            }
            else {
                $contentblock = showRssYandex(@_);
            }
            $tmpl[$i] =~ s/\%content\%/$contentblock/;
        }
        print $tmpl[$i];
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showRssYandex ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Вывод информации об RSS фиде Яндекса.
#
#------------------------------------------------------------------------------

sub showRssYandex {
    my $tankfile;
    my ($sesid, $selector, $page, $printflg, $procstat, $procmess) = @_;
    my $xmldescr = getModXml($MODULE_FOLDER);
    my $mdescr   = getConfigValue( $MODULE_FOLDER, $xmldescr, "description" );

    my $hint = showRssYaUpdateHint( $printflg, $procstat, $procmess );
    my $rsscntnt = qq~
        <div class=admin_header>$mdescr</div>
        $hint
        <div style="margin: 0px 0px 2px 0px; width:100%" class="container">
            <a href="$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admrssparse">Загрузить новости с фида Яндекса</a>
        </div>~;

    if ( -e $MODULE_DB_FILE ) {
        $tankfile = $MODULE_DB_FILE;
    }
    else {
        $tankfile = 'n/a';
    }

# $rsscntnt .= qq~<div class="stxt">Модуль <b>&lt;$MODULE_NAME&gt;</b> интегрирован успешно (sesid:$sesid, tankfile:$tankfile)</div>~;
    $rsscntnt .= RSSYshowNewsTab($sesid, $page);
    return $rsscntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showParsedRssResult ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Вывод результата разбора RSS фида Яндекса.
#
#------------------------------------------------------------------------------

sub showParsedRssResult {
    my $tankfile;
    my $rsscntnt;
    my $procstat;
    my $procmess;
    my $printflg = 1;
    my $sesid    = $_[0];
    my $xmldescr = getModXml($MODULE_FOLDER);
    my $mdescr   = getConfigValue( $MODULE_FOLDER, $xmldescr, "description" );
    my $url =
      Encode::encode( "utf8", getSingleSetting( $MODULE_NAME, 0, 'feedurl' ) );

    if ($url) {
        my $ua = LWP::UserAgent->new;
        $ua->agent( "PheixNewsCrawler/" . getPheixVersion() );
        my $req = HTTP::Request->new( GET => $url );
        my $res = $ua->request($req);

        if ( $res->is_success ) {
            my $rc = doFeedXmlParse( $res->content );
            $procstat = 1;
            $procmess = '<p class=hintcont>Запрос выполнен успешно: скачано '
              . length( $res->content )
              . ' байт<br><b>В базу добавлено '
              . $rc
              . ' новостей</b></p>';
        }
        else {
            $procstat = 0;
            $procmess = '<p class=hintcont>Ошибка при выполнении запроса: <b>'
              . $res->status_line
              . '</b></p>';
        }
    }

    my $hint = showRssYaUpdateHint( $printflg, $procstat, $procmess );
    $rsscntnt = qq~
        <div class=admin_header>$mdescr</div>
        $hint
        <div style="margin: 0px 0px 2px 0px; width:100%" class="container">
            <a href="$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admrssparse">Загрузить новости с фида Яндекса</a>
        </div>~;
    $rsscntnt .= RSSYshowNewsTab($sesid, 0);
    return $rsscntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doFeedXmlParse ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Разбор RSS фида Яндекса.
#
#------------------------------------------------------------------------------

sub doFeedXmlParse {
    my $tstmp = time;
    my $xml   = $_[0];
    my $found = 0;
    my @news_array;
    my $doc = XML::LibXML->load_xml( string => $xml );
    foreach my $items ( $doc->findnodes('/rss/channel/item') ) {
        my $title;
        my $descr;
        my $date;
        foreach my $itemdetails ( $items->nonBlankChildNodes() ) {
            if ( $itemdetails->nodeName() eq 'title' ) {
                if ( defined $itemdetails->textContent()
                    && $itemdetails->textContent() !~ /^[\r\n\s]+$/sx )
                {
                    $title = $itemdetails->textContent();
                    $title =~ s/[\r\n]+//sxgi;
                    $title =~ s/[\s\t]+/ /sxg;
                }
            }
            if ( $itemdetails->nodeName() eq 'description' ) {
                if ( defined $itemdetails->textContent()
                    && $itemdetails->textContent() !~ /^[\r\n\s]+$/sx )
                {
                    $descr = $itemdetails->textContent();
                    $descr =~ s/[\r\n]+//sxgi;
                    $descr =~ s/[\s\t]+/ /sxg;
                }
            }
            if ( $itemdetails->nodeName() eq 'pubDate' ) {
                if ( defined $itemdetails->textContent()
                    && $itemdetails->textContent() !~ /^[\r\n\s]+$/sx )
                {
                    $date = $itemdetails->textContent();
                    $date =~ s/[\r\n]+//sxgi;
                    $date =~ s/[\s\t]+/ /sxg;
                }
            }
        }
        if (   defined $title
            && $title ne ''
            && defined $descr
            && $descr ne ''
            && defined $date
            && $date ne '' )
        {
            my $record = $tstmp + $found;
            my $newsid = str2time($date) || $record;
            push @news_array,
              join( "|",
                #$record, $newsid,
                $newsid, $newsid,
                getNewsPublishDate($newsid),
                encode( "utf8", $title ),
                encode( "utf8", $descr ) );
            $found++;
        }
    }
    if ( $found && @news_array ) {
        if ( -e $MODULE_DB_FILE ) {
            open my $ifh, "<", $MODULE_DB_FILE;
            my @savednews = <$ifh>;
            close $ifh;
            my @actual_news;
            my $newnews_found = 0;
            for my $i ( 0 .. $#news_array ) {
                my @details = split /\|/, $news_array[$i];
                my $exists = 0;
                for my $j ( 0 .. $#savednews ) {
                    my @db_col = split /\|/, $savednews[$j];
                    if ( $db_col[1] == $details[1] ) {
                        $exists = 1;
                        last;
                    }
                }
                if ( !$exists ) {
                    $newnews_found++;
                    push( @actual_news, $news_array[$i] );
                }
            }
            @news_array = @actual_news;
            $found      = $newnews_found;
            if ( $found && @news_array ) {
                open my $ofh, ">", $MODULE_DB_FILE;
                flock( $ofh, 2 );
                seek( $ofh, 0, 2 );
                print $ofh join( "\n", @news_array ) . "\n"
                  . join( '', @savednews );
                flock( $ofh, 8 );
                close $ofh;
            }
        }
        else {
            open my $ofh, ">", $MODULE_DB_FILE;
            print $ofh join "\n", @news_array;
            close $ofh;
        }
    }
    return $found;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getNewsPublishDate ( $unixtime )
#------------------------------------------------------------------------------
#
# $unixtime - дата в формате unix timestamp.
# Функция возвращает строку типа "23, Январь 2015".
#
#------------------------------------------------------------------------------

sub getNewsPublishDate {
    my $date = localtime( $_[0] );
    $date =~ s/[\.\:\,]//g;
    $date =~ s/[\s]+/ /g;
    my @date_array = split( / /, $date );
    @date_array = ( $date_array[2], $date_array[1], $date_array[4] );
    if ( $date_array[1] eq 'Jan' ) { $date_array[1] = 'Январь' }
    if ( $date_array[1] eq 'Feb' ) { $date_array[1] = 'Февраль' }
    if ( $date_array[1] eq 'Mar' ) { $date_array[1] = 'Март' }
    if ( $date_array[1] eq 'Apr' ) { $date_array[1] = 'Апрель' }
    if ( $date_array[1] eq 'May' ) { $date_array[1] = 'Май' }
    if ( $date_array[1] eq 'Jun' ) { $date_array[1] = 'Июнь' }
    if ( $date_array[1] eq 'Jul' ) { $date_array[1] = 'Июль' }
    if ( $date_array[1] eq 'Aug' ) { $date_array[1] = 'Август' }
    if ( $date_array[1] eq 'Sep' ) { $date_array[1] = 'Сентябрь' }
    if ( $date_array[1] eq 'Oct' ) { $date_array[1] = 'Октябрь' }
    if ( $date_array[1] eq 'Nov' ) { $date_array[1] = 'Ноябрь' }
    if ( $date_array[1] eq 'Dec' ) { $date_array[1] = 'Декабрь' }
    return "$date_array[0], $date_array[1] $date_array[2]";
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showRssYaUpdateHint ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Напечатать hint jGrowl
#
#------------------------------------------------------------------------------

sub showRssYaUpdateHint {
    my $hint      = '';
    my $printflag = $_[0];
    my $procstat  = $_[1];
    my $procmess  = $_[2];
    $procmess =~ s/[\r\n]//g;
    $procmess =~ s/[\s]+/ /g;
    my $ipdatetime = '';

    my $jgrowlhint = qq~
        <script type="text/javascript">
            (function(jQuery){
                  jQuery(document).ready(function(){
                  jQuery.jGrowl.defaults.closer = false;
                  jQuery('\#updatehint').jGrowl("<div class=update_hint>\\
                      %hint_content%
                      </div>",
                      {
                          life: 10000,
                          theme: 'updatehint',
                          speed: 'slow',
                          animateOpen: {
                              height: "show",
                          },
                          animateClose: {
                              height: "hide",
                          }
                      });
                  });
            })(jQuery);
        </script>
        <div id="updatehint" class="top-right" style="display:inline; margin-top:15px;"></div> 
    ~;

    if ( localtime =~ /([\d]+:[\d]+:[\d]+)/ ) {
        $ipdatetime = "$1";
    }
    my $message = qq~<p class=hinthead>$ipdatetime - Операция выполнена</p>~;
    if ( $printflag == 1 ) {

        if ( $procstat == 0 ) {

            # Операция завершилась ошибкой
            $message = qq~<p class=hinthead>$ipdatetime - Операция завершилась ошибкой</p>~;
        }
        $hint = "$message\\\n$procmess\\";
    }
    else {
        if ( $procstat == 1 ) {
            $hint = $message;
        }
    }
    $jgrowlhint =~ s/%hint_content%/$hint/g;
    return $jgrowlhint;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# RSSYshowNewsTab ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Нативная функция вывода таблицы с новостями.
#
#------------------------------------------------------------------------------

sub RSSYshowNewsTab {
    my $rsscntnt;
    my $sesid     = $_[0];
    my $page      = $_[1] || 0;
    if ( -e $MODULE_DB_FILE ) {
        open my $ifh, "<", $MODULE_DB_FILE;
        my @dbnews = sort { $b cmp $a } <$ifh>;
        close $ifh;

        my ($navi, @news) = getPageLinksNavi($sesid, $page, @dbnews);

        # Вывод раздела в список

        if ( $#news > -1 ) {
            $rsscntnt .= qq~
                <table cellspacing="0" cellpadding="10" width="100%" class="tabselect">
                ~;
            for my $inxonpage ( 0 .. $#news ) {
                my $rand    = rand();
                my @tmp_arr = split( /\|/, $news[$inxonpage] );
                my $bgcolor = 'class="tr01"';
                if ( $inxonpage % 2 == 0 ) { $bgcolor = 'class="tr02"'; }
                $tmp_arr[$#tmp_arr] =~ s/[\n]+//g;

                my $updownlinks = qq~
                        <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>&nbsp;
                        <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                    ~;
                my $itemnum = $TOTAL_RECORDS_ONPAGE * $page + ($inxonpage+1);
                my $decinum = ($itemnum-1) > 0 ? ($itemnum-1) : 0; 
                $rsscntnt .= qq~
                    <tr $bgcolor style="height:20px;">
                    <td width="10%" class="standart_text" align="center">$updownlinks</td>
                    <td width="80%" class="standart_text" style="font-size:11pt;"><b>
                        <span style="color:#C0C0C0">$itemnum.</span> <span style="color:#707070">$tmp_arr[3]</span>
                        <p class=cmstext style="font-size:10pt; color:#A0A0A0">$tmp_arr[4]</p>
                        <p class=cmstext style="font-size:10pt; color:#C0C0C0">Дата публикации: $tmp_arr[2]</p></td>
                    <td width="10%" class="standart_text" align=right>
                        <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admrssdel&amp;rssid=$tmp_arr[0]&amp;num=$decinum','Вы уверены, что хотите удалить новость &quot;$tmp_arr[3]&quot; безвозвратно?')" class="fa-action-link">
                    <i class="fa fa-trash" title="Удалить новость &quot;$tmp_arr[3]&quot;"></i></a></td></tr>\n~;
            }
            $rsscntnt .= qq~</table><p align="right">$navi</p>~;
        }
        else {
            $rsscntnt .= qq~
                <p class=error>Список записей пуст -&nbsp;
                файл $MODULE_DB_FILE пуст&nbsp;
                (\$#news = $#news)!</p>
            ~;
        }

    }    # if (-e $MODULE_DB_FILE)
    else {
        $rsscntnt .= qq~
            <p class=error>Список записей пуст -&nbsp;
            файл $MODULE_DB_FILE не найден!</p>
        ~;
    }
    return $rsscntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getPageLinksNavi ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Вывод постраничной навигации.
#
#------------------------------------------------------------------------------

sub getPageLinksNavi {
    my $page_linx ;
    my $pages     = 0;
    my $total_rec = $TOTAL_RECORDS_ONPAGE;
    my $sesid = $_[0];
    my $curr_page = $_[1];
    my @data = @_[2..$#_];

    $pages = floor( $#data / $total_rec );

    if (   ( $pages * $total_rec <= $#data )
        || ( $pages < 1 ) )
    {
        $pages++;
    }

    if ( $curr_page >= $pages ) { $curr_page = 0; }
    $page_linx = '&nbsp;-&nbsp;';
    for ( my $i = 1 ; $i <= $pages ; $i++ ) {
        if ( ( $i - 1 ) != $curr_page ) {
            $page_linx .= "<a href=\"$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admrss&amp;page="
              . ( $i - 1 )
              . "\" class=admpageslink>"
              . $i
              . "</a>&nbsp;-&nbsp;";
        }
        else {
            $page_linx .=
              "<span class=lightadminpagelink>" . $i . "</span>&nbsp;-&nbsp;";
        }
    }

    if ( $curr_page >= $pages ) { $curr_page = $pages - 1; }
    my $end = ( ( $curr_page + 1 ) * $total_rec ) - 1;
    if ( $end > $#data ) {
        $end = $#data;
    }
    @data =
      @data[ $curr_page * $total_rec .. $end ];
    return ($page_linx, @data);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# delRssRecord ( $recid )
#------------------------------------------------------------------------------
#
# $recid - идентификатор записи.
# Удаление записи из локальной базы данных.
#
#------------------------------------------------------------------------------

sub delRssRecord {
    my @rc_stat = (0, q{});
    my ($recid) = @_;
    if ( -e $MODULE_DB_FILE ) {
        my @updated_arr;
        open my $ifh, "<", $MODULE_DB_FILE;
        my @data = <$ifh>;
        close $ifh;
        for my $i (0..$#data) {
            my @tmp = split /\|/, $data[$i];
            if ($tmp[0] != $recid && defined $tmp[0] && $tmp[0] =~ /^[\d]+$/) {
                push @updated_arr, $data[$i];
            }
        }
        if (@updated_arr) {
            open my $ofh, ">", $MODULE_DB_FILE;
            flock( $ofh, 2 );
            seek( $ofh, 0, 2 );
            print $ofh @updated_arr;
            flock( $ofh, 8 );
            close $ofh;
            @rc_stat = (1, '<p class=hintcont>Запись <b>'.$recid.'</b> успешно удалена</p>');
        } else {
            unlink $MODULE_DB_FILE;
            @rc_stat = (1, "<p class=hintcont>Все записи удалены — удаляем $MODULE_DB_FILE</p>");
        }
    } else {
        @rc_stat = (1, "<p class=hintcont>Файл $MODULE_DB_FILE не найден</p>");
    }
    return @rc_stat;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getPageOfNum ( $num )
#------------------------------------------------------------------------------
#
# $num - номер записи.
# Получить страницу по номеру записи.
#
#------------------------------------------------------------------------------

sub getPageOfNum {
    my $rc    = 0;
    my ($num) = @_;
    my $curr_page;
    my $total_rec = $TOTAL_RECORDS_ONPAGE;
    if ( -e $MODULE_DB_FILE ) {
        open my $ifh, "<", $MODULE_DB_FILE;
        my @rawdata = <$ifh>;
        close $ifh;

        my @data;

        for my $i (0..$#rawdata) {
            if ($rawdata[$i] ne '' && $rawdata[$i] !~ /^[\r\n\s]+$/) {
                push @data, $rawdata[$i];
            }
        }

        my $pages = floor( $#data / $total_rec );

        if ( ( $pages * $total_rec <= $#data ) || ( $pages < 1 ) ) {
            $pages++;
        }

        if ($num > ($#data+1)) { $curr_page = $pages - 1; }
        if ($num < 0) { $curr_page = 0; }

        for my $i (0..$pages) {
            my $lim1 = $i * $total_rec;
            my $lim2 = $lim1 + $total_rec - 1;
            if ($num >= $lim1 && $num <= $lim2) {
                $rc = $i;
                last;
            } 
        }
        if ($rc == $pages) {
            $rc--;
            if ($rc < 0) {
                $rc = 0;
            }
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------

END { }

1;
