if (   $access_modes[2] == 1
    || $access_modes[2] == 2 )    # actions compatible with access modes: r,rw
{

    # start of read-only actions

    if ( $env_params[1] eq 'admrss' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $got_page = $env_params[2];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            Rss::Yandex::RSSYshowHomePage( $env_params[0], 0, $got_page, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    # end of read-only actions
    if ( $access_modes[2] == 2 )  # actions compatible just with access mode: rw
    {

        # start of read-write actions

        if ( $env_params[1] eq 'admrssparse' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                Rss::Yandex::RSSYshowHomePage( $env_params[0], 1, 0, $DO_QUIET );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admrssdel' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @rc_vars;
                my $topage  = 0;
                my $rss_id  =  $env_params[2] || 0;
                my $got_num = $env_params[3] || 0;
                if ( $got_num !~ /^[\d]+$/ ) { $got_num = 3; }
                if ($rss_id =~ /^[\d]+$/) {
                    @rc_vars = Rss::Yandex::delRssRecord($rss_id);
                }
                $topage = Rss::Yandex::getPageOfNum($got_num);
                Rss::Yandex::RSSYshowHomePage( $env_params[0], 0, $topage, 1, @rc_vars  );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        # end of read-write actions

    }    # Access modes: rw
}    # Access modes: r