package Xaoslab::EEG;

use strict;
use warnings;

BEGIN {
    #for all servers
    use lib '../';    # lib path for work mode

    # lib path for systax check during module install
    use lib 'admin/libs/modules/';
    use lib 'admin/libs/extlibs/';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';
}

##############################################################################
#
# File   :  EEG.pm
#
##############################################################################

use POSIX;
use Config::ModuleMngr qw(showUpdateHint getModXml getConfigValue);
use Pheix::Session qw(getStringFromHexStr);
use Fcntl qw(:DEFAULT :flock :seek);

#------------------------------------------------------------------------------
# new ()
#------------------------------------------------------------------------------

sub new {
    my ($class,$pfx) = @_;
    my $MODULE_NAME  = "Xaoslab";
    my $MODULE_FLDR  = $pfx . 'admin/libs/modules/' . $MODULE_NAME;
    my $MOD_XML      = getModXml($MODULE_FLDR);
    my $self         = {
        name     => getConfigValue( $MODULE_FLDR, $MOD_XML, 'use_identifier' ),
        version  => getConfigValue( $MODULE_FLDR, $MOD_XML, 'version' ),
        mod_name => $MODULE_NAME,
        mod_fldr => $MODULE_FLDR,
        mod_dscr => getConfigValue( $MODULE_FLDR, $MOD_XML, 'description' ),
        mod_inst => $pfx . 'conf/system/install.tnk',
        mod_tmpl => $pfx . 'admin/skins/classic_.txt',
        mod_dbfn => $pfx . getConfigValue( $MODULE_FLDR, $MOD_XML, 'tankfile' ),
        mod_upth => $pfx . getConfigValue( $MODULE_FLDR, $MOD_XML, 'uploadpath' ),
        mod_scrt => getStatsJsCss(),
    };
    bless $self, $class;
    return $self;
}

#------------------------------------------------------------------------------
# getStatsJsCss ( )
#------------------------------------------------------------------------------

sub getStatsJsCss {
    return qq~
    <script type="text/javascript">
    function audioStopped(id) {
        jQuery('#ctrl_' + id).attr('isPlaying', 'no');
        jQuery('#ctrl_' + id).removeClass('bluetext');
    }
    function audioPlayed(id) {
        jQuery('#ctrl_' + id).attr('isPlaying', 'yes');
        jQuery('#ctrl_' + id).addClass('bluetext');
    }
    function playPause(id) {
        var audio = document.querySelector('#' + id);
        if ( jQuery('#ctrl_' + id).attr('isPlaying') === 'yes' ) {
            audio.pause();
            audio.currentTime = 0;
            audioStopped(id);
        } else {
            audio.play();
            audioPlayed(id);
        }
    }
    </script>
    ~;
}

#------------------------------------------------------------------------------
# showRFSHomePage ( $sesid, ... )
#------------------------------------------------------------------------------

sub showXEEGHomePage {
    my ($self, $sid, $pg, $prflag, $stat, $mess) = @_;
    if (-e $self->{mod_tmpl}) {
        open my $fh, "<", $self->{mod_tmpl}
            or die "unable to open " . $self->{mod_tmpl} . ": $!";
        my @tmpl    = <$fh>;
        close $fh or die "unable to close " . $self->{mod_tmpl} . ": $!";
        for my $cnt (0..$#tmpl) {
            $tmpl[$cnt] =
                Pheix::Tools::fillCommonTags( $sid, $tmpl[$cnt] );
            if ( $tmpl[$cnt] =~ /\%scripts_for_this_page\%/ ) {
                $tmpl[$cnt] =~ s/(\<\!\-\-)|(\-\-\>)//gi;
                my $scrt = $self->{mod_scrt};
                $tmpl[$cnt] =~ s/\%scripts_for_this_page\%/$scrt/;
            }
            if ( $tmpl[$cnt] =~ /\%content\%/ ) {
                my $content =
                    $self->getHomePage($sid, $pg, $prflag, $stat, $mess);
                $tmpl[$cnt] =~ s/\%content\%/$content/;
            }
            print $tmpl[$cnt];
        }
    } else {
        print '<h4>Pheix: unable to open file '.$self->{mod_tmpl}.'</h4>'
    }
}

#------------------------------------------------------------------------------
# getHomePage ( $sid, ... )
#------------------------------------------------------------------------------

sub getHomePage {
    my ($self, $sid, $pg, $prflag, $stat, $mess) = @_;

    my $cntnt_ =
        '<div class=a_hdr>' . $self->{mod_dscr} . '</div>' .
        showUpdateHint($prflag, $stat, $mess);
    $cntnt_   .=
        $self->getUploadFileForm($sid) .
        $self->getProcessedFiles($sid);
    return $cntnt_;
}

#----------------------------------------------------------------------------------------
# getUploadFileForm ( $sid )
#----------------------------------------------------------------------------------------

sub getUploadFileForm {
    my ($self, $sid) = @_;
    my $form = qq~
        <table cellspacing="2" cellpadding="0" width="100%">
        <tr><td align="right" valign="bottom">&nbsp;</td></tr>
        <tr><td valign="bottom">
        <form
            method="post"
            action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admxaoslabupload"
            enctype="multipart/form-data">
         <table
            cellspacing="2"
            cellpadding="9"
            width="100%"
            class="messagelist"
            style="background-image:url('images/social_bg.png');height:94">
         <tr>
            <td
                width="100%"
                class="s_t"
                style="font-family:Arial,Tahoma;color:#505050;font-size:12pt;">
                <table cellspacing="0" cellpadding="0" width="205">
                <tr>
                    <td width="160" class="inst_mod_head">
                        Загрузить&nbsp;новый&nbsp;аудиофайл&nbsp;[</td>
                    <td width="25">
                        <div class="pheix-upload-icon">
                            <i class="fa fa-file-audio-o"
                               aria-hidden="true" title=".wav/.mp3"></i>
                        </div>
                    </td>
                    <td width="20" class="inst_mod_head">]&nbsp;:</td>
                </tr></table>
            </td></tr>
            <tr><td width="100%">
            <div class="f_imulation_wrap">
                <div class="im_input">
                    <input type="text"value="Выберите файл"/>
                </div>
                <input
                    type="file"
                    required
                    size=30
                    name="uploadfile"
                    id="imulated"
                    style="margin-left:-124px;margin-top:7px;height:20px"/>
            </div>
            <script type="text/javascript">
                jQuery('.im_input input').click(function(){
                    jQuery('.im_input input').css('color','#C0C0C0');
                    jQuery('.im_input input').val('Выберите файл модуля');
                    jQuery("#imulated").val('');
                    jQuery('#imulated').trigger('click');
                });
                jQuery('#imulated').change(function(){
                    jQuery('.im_input input').css('color','#505050');
                    jQuery('.im_input input').val(jQuery(this).val());
                });
            </script>
            </td></tr>
            <tr><td width="100%">
                <input
                    type="submit"
                    value="Загрузить новый файл"
                    class="installbutton">
            </td></tr>
        </table>
        </form>
        </td></tr>
        </table>
    ~;
    return qq~
    <div class="spoiler-wrapper" style="width:98%">
        <div class="spoiler folded">
            <a href="javascript:void(0);" class="spoilerlink">
                Загрузить новый аудиофайл в систему</a></div>
        <div class="spoiler-text">$form</div>
    </div>
    <script type="text/javascript">
           jQuery(document).ready(function(){
               jQuery('.spoiler-text').hide()
               jQuery('.spoiler').click(function(){
                   jQuery(this).toggleClass("folded").toggleClass("unfolded").next().slideToggle()
               })
           })
    </script>
    ~;
}

#----------------------------------------------------------------------------------------
# getProcessedFiles ( $sid )
#----------------------------------------------------------------------------------------

sub getProcessedFiles {
    my $pftab;
    my ($self, $sid) = @_;
    if ( !(-e $self->{mod_dbfn}) ) {
        $pftab = '<div class="pheix-error-alert">Файл данных ' .
            $self->{mod_dbfn} . ' модуля ' .
            $self->{name} . ' не найден!</div>';
    } else {
        $pftab = '<div class="pheix-admin-tab">';
        $pftab .= $self->getProcFilesTab($sid);
        $pftab .= '</div>';
    }
    return $pftab;
}

#----------------------------------------------------------------------------------------
# getProcFilesTab ( $sid )
#----------------------------------------------------------------------------------------

sub getProcFilesTab {
    my $tab;
    my ($self, $sid) = @_;
    open my $fh, "<", $self->{mod_dbfn} or die "unable to open " . $self->{mod_dbfn} . ": $!";
    my @recs = reverse <$fh>;
    close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
    for my $i (0..$#recs) {
        $recs[$i] =~ s/[\r\n\t]+//gi;
        my $cntr  = ($i+1);
        my @cols  = split(/\|/,$recs[$i]);
        my %data  = (
            'srtid' => $cols[0],
            'recid' => $cols[1],
            'fpath' => $cols[2],
            'usrid' => $cols[3],
            'hminm' => $cols[4],
            'reslt' => ( $cols[5] == 0 ? 'not processed' : 'processed' ),
            'jsonp' => ( $cols[6] eq 'undefined' ? 'none' : $cols[6] ),
            'csvpt' => ( $cols[7] eq 'undefined' ? 'none' : $cols[7] )
        );
        (my $delmess = $data{hminm}) =~ s/'/\\'/g;
        my $userdetails = getStringFromHexStr($data{usrid});
        my $bgclr = ( $cntr % 2 == 0 ) ? 'tr01' : 'tr02';
        my $uploaddate = localtime($data{recid});
        my $plot = ( ($data{reslt} ne 'not processed') && ( -e $data{jsonp} )  ) ? "<a target=\"_blank\" class=\"statloglink\" href=\"$ENV{SCRIPT_NAME}?id=$sid&amp;action=admxaoslabplot&amp;objid=$data{recid}\">plot</a>" : $data{reslt};
        my $reproc = ( $plot eq 'not processed') ?
            qq~<span class="fa-nonaction-span" title="Перезапустить обработку можно после того, как она завершится">
                <i class="fa fa-refresh"></i></span>~ :
            qq~<a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admxaoslabreproc&amp;objid=$data{recid}&amp;page=0" class="fa-action-link">
                <i class="fa fa-refresh"></i></a>~;
        my $csv = ( !defined($data{csvpt}) || $data{csvpt} eq 'none' || ! -e $data{csvpt} || $data{reslt} eq 'not processed' ) ?
            qq~<span class="fa-nonaction-span" title="CSV-файл не найден!">
                <i class="fa fa-file"></i></span>~ :
            qq~<a href="/media/upload/$data{recid}_data.txt" target="_blank" class="fa-action-link">
                <i class="fa fa-file"></i></a>~;
        $tab .= qq~
        <div class="pheix-admin-tab-container">
          <div class="pheix-admin-tab-row $bgclr">
            <div class="pheix-admin-tab-cell-l">
              <audio id="audio_$data{recid}" src="$data{fpath}" crossorigin="anonymous" onended="javascript:audioStopped('audio_$data{recid}');"></audio>
              <p class="pheix-admin-tab-row-header">
                <span class="pheix-admin-tab-row-legend">$cntr.</span>&nbsp;
                $data{fpath}</p>
              <p><span class="pheix-admin-tab-row-legend">
                Name/description:</span> $data{hminm}</p>
              <p><span class="pheix-admin-tab-row-legend">
                ID:</span> $data{recid}</p>
              <p><span class="pheix-admin-tab-row-legend">
                Date:</span> $uploaddate</p>
              <p><span class="pheix-admin-tab-row-legend">
                Added by:</span> $userdetails</p>
              <p><span class="pheix-admin-tab-row-legend">
                Proccess status:</span> $plot</p>
            </div>
            <div class="pheix-admin-tab-cell-r">
              <a id="ctrl_audio_$data{recid}" href="javascript:playPause('audio_$data{recid}');" class="fa-action-link">
                <i class="fa fa-play"></i></a>
              $reproc
              $csv
              <a href="$data{fpath}" class="fa-action-link">
                <i class="fa fa-download"></i></a>
              <a href="javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}', '&amp;action=admxaoslabeditobj&amp;objid=$data{recid}&amp;page=0')" class="fa-action-link">
                <i class="fa fa-pencil-square-o"></i></a>
              <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$sid&amp;action=admxaoslabdelobj&amp;objid=$data{recid}&amp;page=0','Вы уверены, что хотите удалить объект &quot;$delmess&quot; из базы данных?')" class="fa-action-link">
                <i class="fa fa-trash"></i></a>
            </div>
          </div>
        </div>
        ~;
    }
    return $tab;
}

#------------------------------------------------------------------------------
# saveXaoslabData( $self, $sortid, ... )
#------------------------------------------------------------------------------

sub saveXaoslabData {
    my ($self, $sortid, $recid, $fpath, $userdetails, $hminame,
        $processed, $jsonpath, $csvpath ) = @_;
    my @recs;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
    }
    push( @recs,
          join("|",
              $sortid,
              $recid,
              $fpath,
              $userdetails,
              $hminame,
              $processed,
              $jsonpath,
              $csvpath
          ) . "\n"
    );
    open my $fh, ">", $self->{mod_dbfn} or
        die "unable to open " . $self->{mod_dbfn} . ": $!";
    flock( $fh, LOCK_EX ) or die "unable to lock mailbox: $!";
    print $fh @recs;
    truncate $fh, tell($fh);
    flock( $fh, LOCK_UN ) or die "unable to unlock mailbox: $!";
    close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
    return 1;
}

#------------------------------------------------------------------------------
# getXaoslabData( $self, $recid, ... )
#------------------------------------------------------------------------------

sub getXaoslabData {
    my ($self, $recid ) = @_;
    my @recs;
    my %data;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            $recs[$i] =~ s/[\r\n\t]+//gi;
            my @cols  = split(/\|/,$recs[$i]);
            %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4],
                'reslt' => ( $cols[5] == 0 ? 'not processed' : 'processed' ),
                'jsonp' => ( $cols[6] eq 'undefined' ? 'none' : 'plot' ),
                'csvpt' => ( $cols[7] eq 'undefined' ? 'none' : 'data' ),
            );
            if ( $data{recid} == $recid ) {
                last;
            }
        }
    }
    return %data;
}

#------------------------------------------------------------------------------
# getXaoslabUnprocessedData( $self, $type )
#------------------------------------------------------------------------------

sub getXaoslabUnprocessedData {
    my ($self, $type ) = @_;
    my @ret;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        my @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            $recs[$i] =~ s/[\r\n\t]+//gi;
            my @cols  = split(/\|/,$recs[$i]);
            my %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4],
                'reslt' => $cols[5],
                'jsonp' => $cols[6],
                'csvpt' => $cols[7]
            );
            #print $data{fpath}."\n";
            if ( $data{reslt} == 0 ) {
                push(@ret,\%data);
            }
        }
    }
    return @ret;
}

#------------------------------------------------------------------------------
# setXaoslabDataProcessed( $self, $recid, $jsfile )
#------------------------------------------------------------------------------

sub setXaoslabDataProcessed {
    my ($self, $recid, $jsfile, $csvfile ) = @_;
    my $updated = 0;
    if ( -e $self->{mod_dbfn} && -e $jsfile ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        my @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            my $rcrd = $recs[$i];
            $rcrd =~ s/[\r\n\t]+//gi;
            my @cols  = split(/\|/,$rcrd);
            my %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4],
                'reslt' => $cols[5],
                'jsonp' => $cols[6],
                'csvpt' => $cols[7]
            );
            if ( $data{recid} == $recid ) {
                $data{reslt} = 1;
                $data{jsonp} = defined($jsfile) ? $jsfile :  'undefined';
                $data{csvpt} = defined($csvfile) ? $csvfile :  'undefined';
                $recs[$i] = join('|',
                    $data{srtid},
                    $data{recid},
                    $data{fpath},
                    $data{usrid},
                    $data{hminm},
                    $data{reslt},
                    $data{jsonp},
                    $data{csvpt}
                ) . "\n";
                $updated++;
                last;
            }
        }
        if ( $updated > 0) {
            open my $fh, ">", $self->{mod_dbfn} or
                die "unable to open " . $self->{mod_dbfn} . ": $!";
            flock( $fh, LOCK_EX ) or die "unable to lock mailbox: $!";
            print $fh @recs;
            truncate $fh, tell($fh);
            flock( $fh, LOCK_UN ) or die "unable to unlock mailbox: $!";
            close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        }
    }
    return $updated
}

#------------------------------------------------------------------------------
# setXaoslabData( $self, $recid, ... )
#------------------------------------------------------------------------------

sub setXaoslabData {
    my ($self, $recid, $key, $value ) = @_;
    my @recs;
    my %data;
    my $rc = 0;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            my $rcrd = $recs[$i];
            $rcrd    =~ s/[\r\n\t]+//gi;
            my @cols = split(/\|/,$rcrd);
            %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4],
                'reslt' => $cols[5],
                'jsonp' => $cols[6],
                'csvpt' => $cols[7],
            );
            if ( $data{recid} == $recid ) {
                if ( exists $data{$key} ) {
                    $data{$key} = $value;
                    $recs[$i]   = join("|",
                        $data{srtid},
                        $data{recid},
                        $data{fpath},
                        $data{usrid},
                        $data{hminm},
                        $data{reslt},
                        $data{jsonp},
                        $data{csvpt},
                    ) . "\n";
                }
                open my $fh, ">", $self->{mod_dbfn} or
                    die "unable to open " . $self->{mod_dbfn} . ": $!";
                flock( $fh, LOCK_EX ) or die "unable to lock mailbox: $!";
                print $fh @recs;
                truncate $fh, tell($fh);
                flock( $fh, LOCK_UN ) or die "unable to unlock mailbox: $!";
                close $fh or
                    die "unable to close " . $self->{mod_dbfn} . ": $!";
                $rc = 1;
                last;
            }
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------
# deleteXaoslabData( $self, $recid, ... )
#------------------------------------------------------------------------------

sub deleteXaoslabData {
    my ($self, $recid ) = @_;
    my @recs;
    my %data;
    if ( -e $self->{mod_dbfn} ) {
        my @updated;
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            my $rcrd  = $recs[$i];
            $rcrd     =~ s/[\r\n\t]+//gi;
            my @cols  = split(/\|/,$rcrd);
            %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4],
                'reslt' => $cols[5],
                'jsonp' => $cols[6],
                'csvpt' => $cols[7]
            );
            if ( $data{recid} != $recid ) {
                push(@updated,$recs[$i]);
            } else {
                if ( -e $data{fpath} ) {
                    unlink( $data{fpath} );
                }
                if ( -e $data{jsonp} ) {
                    unlink( $data{jsonp} );
                }
                if ( -e $data{csvpt} ) {
                    unlink( $data{csvpt} );
                }
            }
        }
        if ( @updated ) {
            open my $fh, ">", $self->{mod_dbfn} or
                die "unable to open " . $self->{mod_dbfn} . ": $!";
            flock( $fh, LOCK_EX ) or die "unable to lock mailbox: $!";
            print $fh @updated;
            truncate $fh, tell($fh);
            flock( $fh, LOCK_UN ) or die "unable to unlock mailbox: $!";
            close $fh or
                die "unable to close " . $self->{mod_dbfn} . ": $!";
        } else {
            if ( -e $self->{mod_dbfn} ) {
                unlink( $self->{mod_dbfn} );
            }
        }
    }
    return 1;
}

#------------------------------------------------------------------------------
# showXEEGEditForm( $self, $sid, ... )
#------------------------------------------------------------------------------

sub showXEEGEditForm {
    my ($self,$sid,$objid,$pg,$printflag) = @_;
    my %data = $self->getXaoslabData($objid);
    my $hdr  = 'Редактирование описания объекта';
    my $form = qq~
       <div class="popup_header">$hdr</div>
       <div class=standart_text style="margin-left:1px"><a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admxaoslabeeg" class=simple_link><b>$self->{name}</b></a> / $hdr</div>
       <form id="editxaoslabobj" method=post action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admxaoslabmodifyobj&amp;objid=$objid&amp;page=$pg" style="margin:0">
         <table cellspacing=0 cellpadding=2 border=0 class="pheix-margins-25px">
           <tr><td class=standart_text>Название/описание: <font color="#A0A0A0">[обязательное поле]</font></td></tr>\n
           <tr><td class=standart_text><input placeholder="Введите описание объекта" required name=objectdescr value="$data{hminm}" class=input style="width:100%;"></td></tr>\n
           <tr><td class=standart_text><input type=submit value="Сохранить изменения" class="button" style="width:150px"></td></tr>\n
         </table>
       </form>
    ~;
    return $form;
}

#------------------------------------------------------------------------------
# drawChart( $self, $sid, $recid )
#------------------------------------------------------------------------------

sub drawChart {
    my ($self,$sid,$recid) = @_;
    my %data  = $self->getXaoslabData($recid);
    my $chart = qq~
    <html>
    <head>
        <meta charset="utf-8">
        <title>EEG plot for «$data{hminm}»</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
        <script src="https://unpkg.com/wavesurfer.js"></script>
        <style>
            body { margin:0px; padding:0px; background-color:#000; }
            canvas#myChart { position: absolute; z-index: 1; height:100vh; background-color:#fff; }
            .container { position: absolute; width:100%; height:100vh; margin:0px; padding:0px; }
            .table { display:table; width:100%; height:100%; margin:0px; padding:0px; }
            .table_cell { display:table-cell; width:100%; height:100%; vertical-align:bottom; margin:0px; padding:0px; }
            .player { margin:0px; padding:0px; display: inline-block; position: relative; width:100%; height:250px; background: linear-gradient(to top, rgba(0,0,0,1), rgba(0,0,0,.5)); z-index: 2; }
            .waveform { margin:0px; padding:0px; }
            .hint { font-size:10pt; color: #fff; font-family: 'Roboto', sans-serif; position: absolute; z-index: 3; width:100%; text-align:center; top: calc( 100vh - 40px ); }
            .playaxe { position: absolute; left: 0px; top: 0px; width: 2px; height: 100vh; margin: 0px; padding: 0px; background-color: rgba(255,0,0,0.6); z-index: 2;  }
    		</style>
        <script>
        var fftjson = null;
        function ffteeg( jsondata ) {
            if ( typeof jsondata === 'undefined' || jsondata == null ) {
                throw new Error("ffteeg(): JSON data load is failed: no data to store!");
            } else {
                fftjson = jsondata;
                console.log('ffteeg(): JSON data is loaded');
            }
            return 1;
        }
        </script>
    </head>
    <body id="body">
        <canvas id="myChart" style="margin-left:0px;"></canvas>
        <div class="container">
            <div class="table">
                <div class="table_cell">
                    <div class="player">
                        <div class="waveform"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="playaxe"></div>
        <div class="hint">Double-click on plot to stop playing</div>
        <script type="text/javascript" src="media/upload/$recid\_data.js"></script>
        <script type="text/javascript">var iters=[],data_delta=[],data_theta=[],data_alpha=[],data_beta=[],data_gamma=[],data_eeg_6=[],data_eeg_7=[],data_eeg_8=[];if("undefined"==typeof fftjson||null==fftjson)throw new Error("JSON data is not available!");for(var i=0;i<fftjson.eeg.length;i++)data_delta.push(fftjson.eeg[i][0]),data_theta.push(fftjson.eeg[i][1]),data_alpha.push(fftjson.eeg[i][2]),data_beta.push(fftjson.eeg[i][3]),data_gamma.push(fftjson.eeg[i][4]),data_eeg_6.push(fftjson.eeg[i][5]),data_eeg_7.push(fftjson.eeg[i][6]),data_eeg_8.push(fftjson.eeg[i][7]),iters.push(i);var ctx=document.getElementById("myChart").getContext("2d"),myChart=new Chart(ctx,{type:"line",data:{labels:iters,datasets:[{label:"δ-ритм",data:data_delta,fill:!1,backgroundColor:"rgba(173, 173, 170, 0.8)",borderColor:"rgba(173, 173, 170, 0.5)"},{label:"θ-ритм",data:data_theta,fill:!1,backgroundColor:"rgba(54, 162, 235, 0.8)",borderColor:"rgba(54, 162, 235, 0.5)"},{label:"α-ритм",data:data_alpha,fill:!1,backgroundColor:"rgba(4, 207, 14, 0.8)",borderColor:"rgba(4, 207, 14, 0.5)"},{label:"β-ритм",data:data_beta,fill:!1,backgroundColor:"rgba(217, 22, 191, 0.8)",borderColor:"rgba(217, 22, 191, 0.5)"},{label:"γ-ритм",data:data_gamma,fill:!1,backgroundColor:"rgba(245, 220, 0, 0.8)",borderColor:"rgba(245, 220, 0, 0.5)"},{label:"ритм №5",data:data_eeg_6,fill:!1,backgroundColor:"rgba(57, 250, 192, 0.8)",borderColor:"rgba(57, 250, 192, 0.5)",hidden:!0},{label:"ритм №6",data:data_eeg_7,fill:!1,backgroundColor:"rgba(242, 174, 170, 0.8)",borderColor:"rgba(242, 174, 170, 0.5)",hidden:!0},{label:"ритм №7",data:data_eeg_8,fill:!1,backgroundColor:"rgba(171, 171, 245, 0.8)",borderColor:"rgba(171, 171, 245, 0.5)",hidden:!0}]},options:{layout:{padding:{left:0,right:0,top:0,bottom:0}},scales:{yAxes:[{ticks:{min:-100},display:!1}],xAxes:[{ticks:{display:!1}}]}}});</script>
        <script type="text/javascript">jQuery(document).ready(function(){var e=jQuery("#myChart").css("width"),r=jQuery("#myChart").css("height");console.log("canvas width : "+e),console.log("canvas height: "+r),jQuery(".container").css("height",parseInt(r)+10+"px")});var wavesurfer=WaveSurfer.create({container:".waveform",waveColor:"white",progressColor:"#63ffac",height:250});wavesurfer.load("media/upload/$recid\.mp3"),wavesurfer.on("seek",function(e){var r=e*wavesurfer.getDuration();console.log(r),wavesurfer.play(r)}),jQuery("#body").dblclick(function(){wavesurfer.stop(),wavesurfer.pause()});wavesurfer.on('audioprocess',function(){if(wavesurfer.isPlaying()){var axe=parseInt(jQuery(".waveform wave wave").css("width"));jQuery(".playaxe").css("left",(axe-2)+"px")}});wavesurfer.on('pause',function(){jQuery(".playaxe").css("left","0px")})</script>
    </body>
    </html>
    ~;
    return $chart;
}

#------------------------------------------------------------------------------

END { }

1;
