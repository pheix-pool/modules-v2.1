if ($access_modes[2] == 1 || $access_modes[2] == 2) # actions compatible with access modes: r,rw
{
    my $xeeg = Xaoslab::EEG->new;
    # start of read-only actions
    if (
        ( defined( $env_params[1] ) ) &&
        ( $env_params[1] eq 'admxaoslabeeg' )
    ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $sid     = $env_params[0];
            my $gotpage = $env_params[2];
            if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
            $xeeg->showXEEGHomePage( $sid, $gotpage, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }
    if (
        ( defined( $env_params[1] ) ) &&
        ( $env_params[1] eq 'admxaoslabeditobj' )
    ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $sid     = $env_params[0];
            my $objid   = $env_params[2] || 0;
            my $gotpage = $env_params[3];
            if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
            print $xeeg->showXEEGEditForm( $sid, $objid, $gotpage, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }
    if (
        ( defined( $env_params[1] ) ) &&
        ( $env_params[1] eq 'admxaoslabplot' )
    ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $sid     = $env_params[0];
            my $objid   = $env_params[2] || 0;
            print $xeeg->drawChart( $sid, $objid );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }
    # end of read-only actions
    if ($access_modes[2] == 2) # actions compatible just with access mode: rw
    {
        # start of read-write actions
        if (
            ( defined( $env_params[1] ) ) &&
            ( $env_params[1] eq 'admxaoslabupload' )
        ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $upl_res;
                my $sid     = $env_params[0];
                my $status  = 1;
                my $fld     = 'uploadfile';
                my $gotpage = $env_params[2];
                if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
                my ($ext)  = $co->param($fld) =~ /(\.[^.]+)$/;
                if ( $ext eq '.wav' || $ext eq '.mp3') {
                    my $fid     = time;
                    my $fid_new = $fid . $ext;
                    if ( -e $xeeg->{mod_upth} ) {
                        $upl_res = Pheix::Tools::doFileUpload(
                            $xeeg->{mod_upth},
                            $fld,
                            $fid_new,
                            0,
                            0
                        );
                        $upl_res =~ s/\"//gi;
                        $upl_res =~ s/\sclass\=[a-z0-9\_\-]+//gi;
                        $upl_res =~ s/<\/?[a-z]+>//gi;
                        if ( $upl_res !~ /error/ ) {
                            # save to db
                            my $pth = $xeeg->{mod_upth};
                            $pth =~ s/[\/]+$//gi;
                            $xeeg->saveXaoslabData(
                                floor(rand(ceil($fid % 100000))*100000),  # sort id
                                $fid,                       # record id
                                $pth . '/' . $fid_new,      # file path
                                getHexStrFromString(
                                    getDecryptFioBySesId($sid)
                                ),                          # owner details
                                'sample file ' . $fid_new,  # file hmi name
                                0,                          # is processed
                                'undefined'                 # processed json path
                            );
                        }
                    } else {
                        $upl_res =
                            '<p class=hintcont hintdata=error>' .
                            'Target folder <strong>' . $xeeg->{mod_upth} .
                            '</strong> not existed!</p>';
                    }
                    $upl_res =
                        '<p class=hintcont>' . $upl_res . '</p>';
                } else {
                    $upl_res =
                        '<p class=hintcont hintdata=error><strong>*'. $ext .
                        '</strong> files are not supported!</p>';
                }
                if ( $upl_res =~ /error/ ) {
                    $status = 0;
                }
                $xeeg->showXEEGHomePage(
                    $sid,
                    $gotpage,
                    $SHOW_MESS,
                    $status,
                    $upl_res
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }
        if (
            ( defined( $env_params[1] ) ) &&
            ( $env_params[1] eq 'admxaoslabmodifyobj' )
        ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $sid     = $env_params[0];
                my $objid   = $env_params[2] || 0;
                my $gotpage = $env_params[3];
                if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
                my $status  = $xeeg->setXaoslabData(
                    $objid,
                    'hminm',
                    $co->param('objectdescr')
                );
                $xeeg->showXEEGHomePage(
                    $sid,
                    $gotpage,
                    $SHOW_MESS,
                    $status,
                    ($status == 0 ?
                        '<p class=hintcont>Unable to set data to object id=' . $objid . '</p>':
                        '<p class=hintcont>Record id=' . $objid . ' is successfully updated!</p>'
                    )
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }
        if (
            ( defined( $env_params[1] ) ) &&
            ( $env_params[1] eq 'admxaoslabdelobj' )
        ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $sid     = $env_params[0];
                my $objid   = $env_params[2] || 0;
                my $gotpage = $env_params[3];
                if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
                my $status  = $xeeg->deleteXaoslabData( $objid );
                $xeeg->showXEEGHomePage( $sid, $gotpage, $DO_QUIET );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }
        if (
            ( defined( $env_params[1] ) ) &&
            ( $env_params[1] eq 'admxaoslabreproc' )
        ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $sid     = $env_params[0];
                my $objid   = $env_params[2] || 0;
                my $gotpage = $env_params[3];
                if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
                $xeeg->setXaoslabData( $objid, 'reslt', 0 );
                $xeeg->showXEEGHomePage( $sid, $gotpage, $DO_QUIET );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }
        # end of read-write actions
    } # Access modes: rw
} # Access modes: r
