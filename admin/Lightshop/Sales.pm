package Lightshop::Sales;

use strict;
use warnings;

BEGIN {
    #for all servers
    use lib '../'; # lib path for work mode

    # lib path for systax check during module install
    use lib 'admin/libs/modules/';
    use lib 'admin/libs/extlibs/';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';
}

##############################################################################
#
# File   :  Sales.pm
#
##############################################################################
#
# Модуль управления интернет-магазином
#
##############################################################################

use POSIX;
use Encode;
use Config::ModuleMngr;

my $MODULE_NAME          = 'Lightshop';
my $MODULE_FOLDER        = "admin/libs/modules/$MODULE_NAME/";
my $MODULE_DOWNLOAD_PATH = 'upload/rekvizity';
my $MODULE_CLIENT_DB     = 'conf/system/lightshopclientdb.tnk';
my $MODULE_DEAL_DB       = 'conf/system/lightshopdealsdb.tnk';
my $ADMIN_SKIN_PATH      = 'admin/skins/classic_.txt';

#------------------------------------------------------------------------------

my $GLOBAL_CURRENCY = Encode::encode("utf8", Config::ModuleMngr::getSingleSetting( $MODULE_NAME, 1,
            'currency' ));
my $PROTO = getSingleSetting('Config', 0, 'workviaproto') == '1' ? 'https://' : 'http://';

#------------------------------------------------------------------------------
# show_lightshop_startpage ()
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода записей из CMS (вывод главной страницы).
#
#------------------------------------------------------------------------------

sub show_lightshop_startpage {
    my ($sesid) = @_;
    open(my $inputfh, "<", $ADMIN_SKIN_PATH);
    my @scheme = <$inputfh>;
    close($inputfh);
    for my $cnt (0..$#scheme) {
        $scheme[$cnt] = Pheix::Tools::fillCommonTags($sesid, $scheme[$cnt]);
        if ($scheme[$cnt] =~ /\%content\%/) {
            my $login = &_inner_lightshop_startpage(@_);
            $scheme[$cnt] =~ s/\%content\%/$login/;
        }
        print $scheme[$cnt];
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_lightshop_addkvitance ()
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода записей из CMS (добавление квитанции или чека).
#
#------------------------------------------------------------------------------

sub show_lightshop_addkvitance {
    my ($sesid) = @_;
    open(my $inputfh, "<", $ADMIN_SKIN_PATH);
    my @scheme = <$inputfh>;
    close($inputfh);
    for my $cnt (0..$#scheme) {
        $scheme[$cnt] = Pheix::Tools::fillCommonTags($sesid,$scheme[$cnt]);
        if ($scheme[$cnt] =~ /\%content\%/) {
            my $login = &_inner_lightshop_addkvitance(@_);
            $scheme[$cnt] =~ s/\%content\%/$login/;
        }
    print $scheme[$cnt];
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_lightshop_startpage ()
#------------------------------------------------------------------------------
#
# Вывод главной страницы.
#
#------------------------------------------------------------------------------

sub _inner_lightshop_startpage {
    my ($sesid, $cryptedlgn, $curr_page) = @_;
    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr = Config::ModuleMngr::getConfigValue($MODULE_FOLDER,$xml_description_file,"description");
    my $lightshop_content_variable = qq~<div class=admin_header>$module_descr</div>~.showLightShopUpdateHint($_[5],$_[4],$_[3]);
    my $lightshop_counter = 0;
    my $pix_on_page = 10;
    my $userarea = Shopcat::V3::getV3UserareaStatus();
    if (!defined($curr_page) || $curr_page !~ /^[\d]+$/ || $curr_page < 0) {
        $curr_page = 0;
    }

    if (-e $MODULE_DEAL_DB) {
        open (my $inputfh, "<", $MODULE_DEAL_DB);
        my @lightshop_on_curr_page = sort {$b cmp $a} <$inputfh>;
        close($inputfh);

        if ($#lightshop_on_curr_page>-1) {

            # Формирование навигации по страницам

            my $pages = floor($#lightshop_on_curr_page/$pix_on_page);
            if ( ($pages*$pix_on_page<=$#lightshop_on_curr_page) || ($pages<1) ) {
                $pages++;
            }
            if ( $curr_page >= $pages ) {$curr_page = $pages-1;}
            my $page_linx = '&nbsp;-&nbsp;';
            for my $i (1..$pages) {
                if ( ($i-1) != $curr_page ) {
                    $page_linx .= "<a href=\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshop&amp;page=".($i-1)."\" class=admpageslink>".$i."</a>&nbsp;-&nbsp;";
                } else {
                    $page_linx .= "<span class=lightadminpagelink>".$i."</span>&nbsp;-&nbsp;";
                }
            }
            if ( $curr_page >= $pages ) {$curr_page = $pages - 1;}
            my $end = (($curr_page+1)*$pix_on_page)-1;
            if ( $end>$#lightshop_on_curr_page ) {
                $end=$#lightshop_on_curr_page;
            }
            @lightshop_on_curr_page = @lightshop_on_curr_page[$curr_page*$pix_on_page..$end];

            # Формирование навигации по страницам $seotags_content_variable .= join("<br>",@items_on_this_page);
            $lightshop_content_variable .= qq~<table cellspacing="0" cellpadding="10" width="100%" class="tabselect">~;

            for my $inxonpage (0..$#lightshop_on_curr_page) {
                my $updownlinks;
                my $rand = rand();
                my @tmp_arr = split (/\|/,$lightshop_on_curr_page[$inxonpage]);
                my $dealid = $tmp_arr[0];
                my $userid = $tmp_arr[2];
                $dealid =~ s/[\D]+//s;
                $userid =~ s/[\D]+//s;
                my $bgcolor='class="tr01"';
                if ($lightshop_counter % 2 == 0) {$bgcolor='class="tr02"';}
                $tmp_arr[-1] =~ s/[\n]+//g;

                my $nextpage = $curr_page;
                my $prevpage = $curr_page;

                if ($inxonpage == $#lightshop_on_curr_page) { $nextpage++; }
                if ($inxonpage == 0) {
                    $prevpage--;
                    if ( $prevpage < 0 ) {
                        $prevpage=0;
                    }
                }

                if ($#lightshop_on_curr_page > 0) {
                    $updownlinks = qq~<a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshopmoveup&amp;sgid=$dealid&amp;page=$prevpage" class="fa-action-link">
                        <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>&nbsp;
                        <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshopmovedown&amp;sgid=$dealid&amp;page=$nextpage" class="fa-action-link">
                           <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>~;
                    if ($inxonpage == 0 && $curr_page == 0) {
                        $updownlinks = qq~<span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>&nbsp;
                           <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshopmovedown&amp;sgid=$dealid&amp;page=$curr_page" class="fa-action-link">
                           <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>~;
                    }
                    if ($inxonpage == $#lightshop_on_curr_page && $curr_page == ($pages-1))  {
                         $updownlinks = qq~<a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshopmoveup&amp;sgid=$dealid&amp;page=$curr_page" class="fa-action-link">
                           <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>&nbsp;
                           <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
                    }
                } else  {
                    if ($curr_page > 0) {
                        $updownlinks = qq~<a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshopmoveup&amp;sgid=$dealid&amp;page=$prevpage" class="fa-action-link">
                            <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>&nbsp;
                            <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
                    } else {
                        $updownlinks = qq~<span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>&nbsp;<span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
                    }
                }
                my $deal_status = "Сделка в работе";
                my $attachments = "";
                my @links_array = (0,0,0); my $links_str = '';
                my $user_login = &get_details_by_GLOBAL_id($tmp_arr[2],2);
                my $user_fio = &get_details_by_GLOBAL_id($tmp_arr[2],5);
                my $user_addr = &get_details_by_GLOBAL_id($tmp_arr[2],6);
                my $user_email = &get_details_by_GLOBAL_id($tmp_arr[2],4);
                my @dealattachments = &get_img_by_id('images/postdata/upload',$tmp_arr[0]);

                if ($#dealattachments > -1)  {
                    $attachments = '<img src="images/admin/cms/png/thumbnail.png" alt="" border=0>';
                    for my $i (0..$#dealattachments) {
                        if ($dealattachments[$i] =~ /_0/) {$links_array[0] = $dealattachments[$i];}
                        if ($dealattachments[$i] =~ /_1/) {$links_array[1] = $dealattachments[$i];}
                        if ($dealattachments[$i] =~ /_2/) {$links_array[2] = $dealattachments[$i];}
                    }
                    $links_str = join("|",@links_array);
                }
                my $deal_content;
                my @bought_cds = split(/\./,$tmp_arr[3]);
                my $font_color = 'black';

                if ($tmp_arr[7] == -1) {$font_color = '#CC0033'; $deal_status = "Сделка не состоялась";}
                if ($tmp_arr[7] == 0) {$font_color = '#FFAA33'; $deal_status = "Сделка в работе";}
                if ($tmp_arr[7] == 1) {$font_color = '#009900'; $deal_status = "Сделка успешно состоялась";}

                for my $i (0..$#bought_cds) {
                    my @tt_mp = split(/\-/,$bought_cds[$i]);
                    my @my_cd_name = &get_item_details_v3($tt_mp[0],$tt_mp[1]);
                    my $itemlink   = $userarea ? "href=\"shopcatalog_$my_cd_name[0]\_$tt_mp[0].html\"" : '';
                    if ($my_cd_name[0] != -1) {
                        $deal_content .= ($i+1).". <a $itemlink class=simplelink target=_blank>$my_cd_name[5] ($my_cd_name[8])</a> \«$my_cd_name[7]\», $tt_mp[2] шт.";
                    } else {
                        $deal_content .= "<font color=red>Error!</font>";
                    }
                    if ($i<$#bought_cds) {
                        $deal_content .= "<br>";
                    }
                }
                my $totalcost = $tmp_arr[4]+$tmp_arr[6];
                if ($deal_content =~ /Error!/) {
                    $deal_content = "<span class=error>Ошибка с целостностью БД - в заказе присутствуют несуществующие позиции!</span>";
                }

                my $rekviz_filepath = $MODULE_DOWNLOAD_PATH."/".$userid.".xml";
                my $rekvizity_info;
                if (-e $rekviz_filepath) {
                    $rekvizity_info = qq~<br>Реквизиты юридического лица: <a href="/$rekviz_filepath" target="_blank" class="lightshop-rekvizity-link">$PROTO$ENV{SERVER_NAME}/$rekviz_filepath</a>~;
                }
                $lightshop_content_variable .= qq~
                    <tr $bgcolor style="height:20px;">
                        <td width="10%" class="standart_text" align="center">$updownlinks</td>
                        <td width="75%" class="standart_text" style="font-size:11pt;">
                            <b>$tmp_arr[1]</b>
                            <div class=cmstext style="padding-top:10; font-size:10pt; color:#606060">Пользователь: <a href="mailto:$user_email" class=simple_link>$user_fio</a> aka $user_login</div>
                            <div class=cmstext style="font-size:10pt; color:#A0A0A0">Номер заказа: $tmp_arr[0]<br>Статус: $deal_status<br>Итого: $tmp_arr[4] $GLOBAL_CURRENCY $rekvizity_info</div>
                            <div class=cmstext style="margin-top:10px;font-size:10pt; color:#606060">Позиции в заказе:
                                <div style="margin-top:5px;padding:10px; background-color:white;">$deal_content</div>
                            </div></td>
                            <td width="15%" class="standart_text" align=right>
                                <a href="javascript:doSesChckLoad('$_[0]', '$ENV{SCRIPT_NAME}', '&amp;action=admlightshopedituser&amp;userid=$tmp_arr[2]&amp;page=$curr_page');"
                                   class="fa-action-link">
                                   <i class="fa fa-address-book-o" title="Редактировать данные контрагента по сделке &quot;$tmp_arr[0]&quot;"></i></a>
                                <a href="javascript:doSesChckLoad('$_[0]', '$ENV{SCRIPT_NAME}', '&amp;action=admlightshopedit&amp;dealid=$tmp_arr[0]&amp;page=$curr_page&amp;ajax=true&amp;height=320');" class="fa-action-link">
                                  <i class="fa fa-pencil-square-o" title="Редактировать данные сделки &quot;$tmp_arr[0]&quot"></i></a>
                                <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshopdel&amp;dealid=$tmp_arr[0]&amp;page=$curr_page','Вы уверены, что хотите удалить все данные по заказу №$tmp_arr[0] безвозвратно?');" class="fa-action-link">
                                <i class="fa fa-trash" title="Удалить сделку &quot;$tmp_arr[0]&quot;"></i></a></td>
                    </tr>
                ~;
                $lightshop_counter++;
            }
            $lightshop_content_variable .= qq~</table><p align=right>$page_linx</p>~;
        } else {
            $lightshop_content_variable .= qq~<p class=error>Список записей пуст - файл conf/system/lightshop.tnk пуст (\$#lightshop_on_curr_page = $#lightshop_on_curr_page)!</p>~;
        }
    } else {$lightshop_content_variable .= qq~<p class=error>Список записей пуст - файл conf/system/lightshop.tnk не найден!</p>~;
    }
    return $lightshop_content_variable;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_lightshop_usereditpage ()
#------------------------------------------------------------------------------
#
# Вывод основной формы редатирования пользователя.
#
#------------------------------------------------------------------------------

sub _inner_lightshop_usereditpage {
    my ($sesid, $cryptlgn, $userid, $page_ref) = @_;
    my $information;
    my @file_arr;
    my $lightshop_editpage_editform;
    my $skobki_hr;
    my $rekviztab;
    my $random = rand();
    my @userdetails_details;

    if ($page_ref !~ /^[\d]+$/) {$page_ref = 0;}

    open ( my $inputfh, "<", $MODULE_CLIENT_DB );
    my @indexes = <$inputfh>;
    close($inputfh);

    if ($#indexes > -1) {
        for my $i(0..$#indexes) {
            my $client_record = $indexes[$i];
            $client_record =~ s/[\r\n]+//g;
            my @temp_arr = split(/\|/,$client_record);
            if ($temp_arr[0] eq $userid) {
                @userdetails_details = @temp_arr;
                last;
            }
        }

        $userdetails_details[6] =~ s/\<br\>/\n/g;
        $userdetails_details[6] =~ s/[\r\n]+$//g;
        #$userdetails_details[8] .= "\n";

        my $mainform = qq~
            <p><label>ФИО пользователя: </label><br><input required type=text name=fio value="$userdetails_details[5]" class=input style="width:100%"></p>
            <p><label>Дата регистрации: </label><br><input required type=text name=date value="$userdetails_details[1]" class=input style="width:75%">&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:Currdate(1,'lightshopmoduser')" class=simple_link>Текущая дата</a></p>
            <p><label>Login: </label><br><input required type=text name=login value="$userdetails_details[2]" class=input style="width:30%"></p>
            <p><label>Пароль: </label><br><input required type=text name=passw value="$userdetails_details[3]" class=input style="width:30%"></p>
            <p><label>Адрес электронной почты: </label><br><input required type=text name=email value="$userdetails_details[4]" class=input style="width:100%"></p>
            <p><label>Телефон: </label><br><input required type=text name=phone value="$userdetails_details[7]" class=input style="width:30%"></p>
            <p><label>Адрес доставки: </label><br><textarea required name=address rows=5 cols=5 class=input style="width:100%;height:90">$userdetails_details[6]</textarea></p>
            <input type=hidden name=comment value="-" class="input">
            <input name="paytype" value="%paytype%" type="hidden">
            <p><input class=button type=Reset value="Очистить" style="width:90px">&nbsp;<input type=submit value="Отправить" class=button style="width:90px"></p>
        ~;

        my @rekvizity = &get_objects_by_id($MODULE_DOWNLOAD_PATH,$_[2]);

        if (defined($rekvizity[0]) && $rekvizity[0] ne '') {
            my @rekvizity = getRekvizityFromFile($MODULE_DOWNLOAD_PATH."/".$rekvizity[0]);
            for my $ri (0..$#rekvizity) {
                $rekvizity[$ri] = Encode::encode("utf8", $rekvizity[$ri]);
            }

            $rekviztab = qq~
                <table class="lightshop-edit-userdata-fulltab"><tr>
                    <td valign="top" class="lightshop-edit-userdata-fulltab-tdleft">%mainfields%</td>
                    <td valign="top" class="lightshop-edit-userdata-fulltab-tdright"><p><label>Название организации: </label><br><input name="orgrekviz0" required="" value="$rekvizity[0]" type="text" class=input style="width:100%"></p>
                        <p><label>Юридический адрес: </label><br><input name="orgrekviz1" required="" value="$rekvizity[1]" type="text" class=input style="width:100%"></p>
                        <p><label>ИНН: </label><br><input name="orgrekviz2" required="" value="$rekvizity[2]" type="text" class=input style="width:100%"></p>
                        <p><label>КПП: </label><br><input name="orgrekviz3" required="" value="$rekvizity[3]" type="text" class=input style="width:100%"></p>
                        <p><label>ОКПО: </label><br><input name="orgrekviz4" required="" value="$rekvizity[4]" type="text" class=input style="width:100%"></p>
                        <p><label>Расчетный счет: </label><br><input name="orgrekviz5" required="" value="$rekvizity[5]" type="text" class=input style="width:100%"></p>
                        <p><label>Корреспондентский счет: </label><br><input name="orgrekviz6" required="" value="$rekvizity[6]" type="text" class=input style="width:100%"></p>
                        <p><label>Наименование банка: </label><br><input name="orgrekviz7" required="" value="$rekvizity[7]" type="text" class=input style="width:100%"></p>
                        <p><label>БИК: </label><br><input name="orgrekviz8" required="" value="$rekvizity[8]" type="text" class=input style="width:100%"></p>
                        <input name="orgrekvizlen" value="$#rekvizity" type="hidden">
                    </td>
                </tr></table>
            ~;
        }
        if (defined $rekviztab && $rekviztab ne '') {
            $mainform  =~ s/%paytype%/0/gisx;
            $rekviztab =~ s/%mainfields%/$mainform/gisx;
        } else {
            $mainform  =~ s/%paytype%/1/gisx;
            $rekviztab = qq~<div class="lightshop-edit-userdata-simplediv">$mainform</div>~;
        }

        $lightshop_editpage_editform .= qq~
            <div class="standart_text">
                <p class="pheix-fancy-header">Редактирование сделки</p>
                <p><a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshop"
                   class=simple_link><b>Заказы и покупки</b></a> / </b>$userdetails_details[5] aka $userdetails_details[2]</p>
            <form id="lightshopmoduser"
                  name="lightshopmoduser"
                  method=post
                  enctype="multipart/form-data"
                  action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshopusrmodify&amp;userid=$_[2]&amp;page=$page_ref">
                  $rekviztab
            </form></div>~;
    } else {
        $lightshop_editpage_editform .= qq~<div class="error">Список пользователей пуст (\$#indexes = $#indexes)</div>~;
    }
    return $lightshop_editpage_editform;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_lightshop_editdeal ()
#------------------------------------------------------------------------------
#
# Вывод основной формы редатирования сделки.
#
#------------------------------------------------------------------------------

sub _inner_lightshop_editdeal {
    my ($sesid, $cryptlgn, $dealid, $page_ref) = @_;
    my @dealdetails_details;
    my $deal_content;
    my $information;
    my $lightshop_editpage_dealeditform;

    if ($page_ref !~ /^[\d]+$/) {$page_ref = 0;}

    open (my $inputfh, "<", $MODULE_DEAL_DB);
    my @indexes = <$inputfh>;
    close($inputfh);

    if ($#indexes > -1) {
        for my $inx (0..$#indexes)  {
            my $deal_record = $indexes[$inx];
            $deal_record =~ s/[\r\n]+//g;
            my @temp_arr = split(/\|/,$deal_record);
            if ($temp_arr[0] eq $dealid) {
                @dealdetails_details = @temp_arr;
                last;
            }
        }

        $dealdetails_details[6] =~ s/\<br\>/\n/g;
        $dealdetails_details[6] =~ s/[\r\n]+$//g;
        # $userdetails_details[8] .= "\n";
        my $user_fio = &get_details_by_GLOBAL_id($dealdetails_details[2],5);

        my @select_array = ('','','');
        if ($dealdetails_details[7] == -1) {$select_array[2]="selected";}
        if ($dealdetails_details[7] == 0) {$select_array[1]="selected";}
        if ($dealdetails_details[7] == 1) {$select_array[0]="selected";}

        my $mainform = qq~
            <p><label>ФИО покупателя: </label><br><input DISABLED type=text value="$user_fio" class=input style="width:100%"></p>
            <p><label>Дата покупки: </label><br><input required type=text name=date value="$dealdetails_details[1]" class=input style="width:50%">&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:Currdate(1,'lightshopeditdeal')" class=simple_link>Текущая дата</a></p>
            <p><label>Сумма заказа (без доставки), $GLOBAL_CURRENCY: </label><br><input required type=text name=summa value="$dealdetails_details[4]" class=input style="width:100%"></p>
            <p><label>Сумма скидки, $GLOBAL_CURRENCY: </label><br><input required type=text name=track value="$dealdetails_details[5]" class=input style="width:100%"></p>
            <p><label>Доставка, $GLOBAL_CURRENCY: </label><br><input required type=text name=comment value="$dealdetails_details[6]" class=input style="width:100%"></p>
            <p><label>Состояние сделки: </label><br>
            <select required name="status" class="input" style="width:50%">
                <option $select_array[0] value="1">Успешно завершена</option>
                <option $select_array[1] value="0">В работе</option>
                <option $select_array[2] value="-1">Аннулирована</option>
            </select>
            </p>
            <p><input class=button type=Reset value="Очистить" style="width:90px">&nbsp;<input type=submit value="Отправить" class=button style="width:90px"></p>
        ~;

        $lightshop_editpage_dealeditform .= qq~
            <div class=standart_text>
                <p class="pheix-fancy-header">Редактирование сделки</p>
                <p><a  href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshop"
                    class=simple_link><b>Заказы и покупки</b></a> / Сделка №$dealdetails_details[0] от $dealdetails_details[1]</p>
	        <form id="lightshopeditdeal"
	              name="lightshopeditdeal"
	              method=post ENCTYPE="multipart/form-data"
	              action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admlightshopdealmodify&amp;userid=$_[2]&amp;page=$page_ref"
	              class="lightshop-edit-dealdata-form">
	              $mainform
	        </form></div>
	    ~;
    } else {
        $lightshop_editpage_dealeditform .= qq~<div class="error">Список сделок пуст (\$#indexes = $#indexes)!</div>~;
    }
    return $lightshop_editpage_dealeditform;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_lightshop_usermodify ()
#------------------------------------------------------------------------------
#
# Редактирование учетной записи пользователя.
#
#------------------------------------------------------------------------------

sub _inner_lightshop_usermodify {
    my ($userid, @userdata) = @_;
	my $retcontent;
	my $retcode = 0;
	if (-e $MODULE_CLIENT_DB) {
		open (my $inputfh, "<", $MODULE_CLIENT_DB);
		my @clients_strings = <$inputfh>;
		close($inputfh);
		for my $i (0..@clients_strings) {
			my $client_record = $clients_strings[$i];
			$client_record =~ s/[\r\n]+ //g;
			my @tmp_arr = split(/\|/,$client_record);
			if ($tmp_arr[0] eq $userid)
			{
				$clients_strings[$i] = join("|", $userid, @userdata, 0)."\n";
				$retcode = 1;
				$retcontent = qq~
				    <p class=hintcont>Данные пользователя <font class=hintspan>$tmp_arr[5]</font> успешно отредактированы!</p>
				~;
				last;
			}
		}
		if ($retcode == 1) {
		    open ( my $outputfh, ">", $MODULE_CLIENT_DB );
            flock( $outputfh, 2 );
            seek( $outputfh, 0, 2 );
            print $outputfh @clients_strings;
            flock( $outputfh, 8 );
            close( $outputfh );
		} else {
		    $retcontent = qq~
		      <p class=hintcont>Невозможно отредактировать пользователя <font class=hintspan>$userid</font>!<br>
		      Пользователь с заданным идентификатором не найден!</p>
		      ~;
		}
	} else {
	    $retcontent = qq~
	       <p class=hintcont>Невозможно отредактировать пользователя <font class=hintspan>$userid</font>!<br>
	       Файл <font class=hintspan>$MODULE_CLIENT_DB</font> не найден!</p>
	       ~;
	}
	return($retcontent,$retcode);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_lightshop_dealmodify ()
#------------------------------------------------------------------------------
#
# Редактирование данных по сделке.
#
#------------------------------------------------------------------------------

sub _inner_lightshop_dealmodify {
    my ($dealid, @dealdata) = @_;
    my $retcontent;
    my $retcode = 0;
    if (!defined($dealid)) {$dealid = -1;}
    if (-e $MODULE_DEAL_DB) {
		open (my $inputfh, "<", $MODULE_DEAL_DB);
		my @deals_strings = <$inputfh>;
		close($inputfh);
		if (@deals_strings) {
		    for(my $i=0; $i<=@deals_strings; $i++)
		    {
		        my $deals_record = $deals_strings[$i];
		        if (defined($deals_record)) {
		            $deals_record =~ s/[\r\n]+ //g;
		            my @tmp_arr = split(/\|/,$deals_record);
		            if ($tmp_arr[0] eq $dealid)
		            {
		                $deals_strings[$i] = "$tmp_arr[0]|$dealdata[0]|$tmp_arr[2]|$tmp_arr[3]|$dealdata[1]|$dealdata[2]|$dealdata[3]|$dealdata[4]\n";
		                $retcode = 1;
		            }
		        }
		    }
		}
		if ($retcode == 1) {
		    open ( my $outputfh, ">", $MODULE_DEAL_DB );
            flock( $outputfh, 2 );
            seek( $outputfh, 0, 2 );
            print $outputfh @deals_strings;
            flock( $outputfh, 8 );
            close( $outputfh );
		    $retcontent = qq~
		      <p class=hintcont>Сделка <font class=hintspan>$dealid</font> успешно отредактирована!</p>
		      ~;
		} else {
		    $retcontent = qq~
		      <p class=hintcont>Сделка <font class=hintspan>$dealid</font> не найдена в базе данных!</p>
		      ~;
		}
	} else {
	    $retcontent = qq~
          <p class=hintcont>Невозможно отредактировать сделку <font class=hintspan>$dealid</font>!<br>
	      Файл <font class=hintspan>$MODULE_DEAL_DB</font> не найден!</p>
	      ~;
	}
	return($retcontent,$retcode);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_lightshop_dealdelete ()
#------------------------------------------------------------------------------
#
# Удаление данных о сделке из БД.
#
#------------------------------------------------------------------------------

sub _inner_lightshop_dealdelete {
	my ($dealid) = @_;
	my $retcontent;
    my $retcode = 0;
	my @new_deals_strings = ();
	if (-e $MODULE_DEAL_DB) {
		open (my $inputfh, "<", $MODULE_DEAL_DB);
		my @deals_strings = <$inputfh>;
		close($inputfh);
		for my $i (0..@deals_strings) {
			my $deals_record = $deals_strings[$i];
			if (defined($deals_record) && $deals_record !~ /^[\r\n\s]+$/ && $deals_record ne '') {
			    $deals_record =~ s/[\r\n]+ //g;
			    my @tmp_arr = split(/\|/,$deals_record);
			    if ($tmp_arr[0] ne $dealid && $tmp_arr[0] =~ /^[\d]+$/) {
			        push(@new_deals_strings,$deals_strings[$i]);
			    } else {
			        my $rc = delUserByUid($tmp_arr[2]);
			    }
			}
		}
		if ($#new_deals_strings > -1) {
		    open ( my $outputfh, ">", $MODULE_DEAL_DB );
			flock( $outputfh, 2 );
			seek( $outputfh, 0, 2 );
			print $outputfh @new_deals_strings;
			flock( $outputfh, 8 );
			close( $outputfh );
			$retcode = 1;
		} else {
		    unlink($MODULE_DEAL_DB);
		    $retcode = 1;
		}
	} else {
	    $retcontent = qq~
         <p class=hintcont>Невозможно удалить сделку <font class=hintspan>$dealid</font>!<br>
	     Файл <font class=hintspan>$MODULE_DEAL_DB</font> не найден!</p>
		 ~;
	}
	if ($retcode == 1) {
	    $retcontent = qq~
		  <p class=hintcont>База данных сделок успешно обновлена!</p>
		  ~;
	}
	return($retcontent,$retcode);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_details_by_GLOBAL_id ()
#------------------------------------------------------------------------------
#
# Получение информации о пользователе из базы данных  (конкретный столбец).
#
#------------------------------------------------------------------------------

sub get_details_by_GLOBAL_id {
    my ($userid, $col) = @_;
    my $return_value;
    open (my $inputfh, "<", $MODULE_CLIENT_DB);
    my @f_strings = <$inputfh>;
    close($inputfh);
    for my $i (0..$#f_strings) {
        my $client_record = $f_strings[$i];
        $client_record =~ s/[\r\n]+//g;
        my @tmp_arr = split(/\|/,$client_record);
        if ($tmp_arr[0] eq $userid) {
            $return_value = $tmp_arr[$col];
            last
        }
	}
	return $return_value;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_img_by_id ()
#------------------------------------------------------------------------------
#
# Получение списка картинок по ID и каталогу.
#
#------------------------------------------------------------------------------

sub get_img_by_id {
    my @object_images;
    my ($dir, $id) = @_;
    if (-e $dir) {
        my @files = &get_files_array($dir);
        for my $i (0..$#files) {
            my @tmp = split(/\_/,$files[$i]);
            if ($tmp[0] eq $id) {
                push(@object_images,$files[$i]);
            }
        }
    }
    @object_images = sort {$a cmp $b} @object_images;
    return @object_images;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_objects_by_id ()
#------------------------------------------------------------------------------
#
# Получение списка объектов по ID и каталогу.
#
#------------------------------------------------------------------------------

sub get_objects_by_id {
    my @objects;
    my ( $dir, $id ) = @_;
    if (-e $dir) {
        my @files = &get_files_array($dir);
        for my $i (0..$#files) {
            if ($files[$i] =~ /^$id/) {
                push(@objects, $files[$i]);
            }
        }
        @objects = sort {$a cmp $b} @objects;
    }
    return @objects;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_files_array ()
#------------------------------------------------------------------------------
#
# Получение списка файлов некоторого каталога.
#
#------------------------------------------------------------------------------

sub get_files_array {
    my ($dir) = @_;
    $dir =~ s/\\/\//g;
    opendir (my $dirfh, $dir);
    my @files = grep {(!/^\.+$/) && !(-d "$dir/$_")} readdir $dirfh;
    closedir($dirfh);
    return @files;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_item_details_v3 ()
#------------------------------------------------------------------------------
#
# Портированная для V3 функция получения детализации заказа.
#
#------------------------------------------------------------------------------

sub get_item_details_v3 {
    my ($shopid, $itemid) = @_;
    my $pricefile;

    eval {
        $pricefile = Shopcat::V3::getPriceFileFullPath($shopid);
    };

    if (-e $pricefile) {
        open (my $inputfh, "<", $pricefile);
        my @f_strings = <$inputfh>;
        close ($inputfh);
        for my $i (0..$#f_strings) {
            $f_strings[$i] =~ s/[\r\n]+//g;
            my @params = split(/\|/,$f_strings[$i]);
            $params[0] =~ s/[\W]+//g;
            if ($params[0] eq $itemid) {
                my $id = 0;
                my $cat_id = 0;
                if ($params[$#params] =~ /\[([\d]+)\]/) {$id = $1;}
                if ($params[$#params-1] =~ /\[([\d]+)\]/) {$cat_id = $1;}

                my $cat_descr = Shopcat::V3::_inner_shopcat2_browse_get_description_for_n_minus_1($shopid,$cat_id,$id);
                if ($cat_descr =~ 'No description record' || $cat_descr =~ 'Descriptions file not found') {
                    $cat_descr = qq~<span style="color:#C0C0C0">Описание отсутсвует</span>~;
                }

                # FIXME - wtf???
                my $navi_block = Shopcat::V3::_inner_shopcat2_get_navi_n_header_for_id($id);
                $navi_block =~ s/textlink\-navi/textlink/g;
                my $itemname = $params[$#params];
                $itemname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                my @new_params = ($id,$navi_block,$params[2],'','',$itemname,'',$cat_descr,$params[0]);

                return @new_params;
            }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_lightshop_sgmoveup ()
#------------------------------------------------------------------------------
#
# Функция передвижения записи в блоге вверх по списку.
#
#------------------------------------------------------------------------------

sub _inner_lightshop_sgmoveup {
    my ($dealid) = @_;
    if (-e $MODULE_DEAL_DB) {
        open (my $outputfh, "+<", $MODULE_DEAL_DB);
        my @cats_page = sort {$b cmp $a} <$outputfh>;
        for my $i (0..$#cats_page) {
            my @tmp = split (/\|/,$cats_page[$i]);
            if ($tmp[0] == $dealid) {
                my @tmp2 = split (/\|/,$cats_page[$i-1]);
                my $buf = $tmp2[0];
                $tmp2[0]=$tmp[0];
                $tmp[0]=$buf;
                $cats_page[$i-1] = join("|",@tmp2);
                $cats_page[$i] = join("|",@tmp);
                last;
            }
        }
        seek($outputfh, 0, 0);
        flock($outputfh, 2);
        print $outputfh @cats_page;
        truncate($outputfh, tell($outputfh));
        flock($outputfh, 8);
        close($outputfh);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_lightshop_sgmovedown ()
#------------------------------------------------------------------------------
#
# Функция передвижения записи в блоге вниз по списку.
#
#------------------------------------------------------------------------------

sub _inner_lightshop_sgmovedown {
    my ($dealid) = @_;
    if (-e $MODULE_DEAL_DB) {
        open (my $outputfh, "+<", $MODULE_DEAL_DB);
        my @cats_page = sort {$b cmp $a} <$outputfh>;
        for my $i (0..$#cats_page) {
            my @tmp = split (/\|/,$cats_page[$i]);
            if ($tmp[0] == $dealid) {
                my @tmp2 = split (/\|/,$cats_page[$i+1]);
                my $buf = $tmp2[0];
                $tmp2[0]=$tmp[0];
                $tmp[0]=$buf;
                $cats_page[$i+1] = join("|",@tmp2);
                $cats_page[$i] = join("|",@tmp);
                last;
            }
        }
        seek($outputfh, 0, 0);
        flock($outputfh, 2);
        print $outputfh @cats_page;
        truncate($outputfh, tell($outputfh));
        flock($outputfh, 8);
        close($outputfh);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getFullFilePathById ()
#------------------------------------------------------------------------------
#
# Получить путь в ФС по идентификатору.
#
#------------------------------------------------------------------------------

sub getFullFilePathById {
    my ($id) = @_;
    my $filepath;
    my $foundfile;
    my $searchdir = $MODULE_DOWNLOAD_PATH;
    my @files = &get_files_array($searchdir);
    foreach(@files) {
        if ($_ =~ /^$id/) {
            $foundfile = $_;
            last;
        }
    }
    if ($foundfile) {
        $filepath = $searchdir."/".$foundfile;
    }
    return $filepath;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showLightShopUpdateHint ()
#------------------------------------------------------------------------------
#
# Напечатать hint jgrowl.
#
#------------------------------------------------------------------------------

sub showLightShopUpdateHint {
    my $hint      = '';
    my $printflag = $_[0];
    my $procstat  = $_[1];
    my $procmess  = $_[2];
    $procmess =~ s/[\"\r\n]//g;
    $procmess =~ s/[\s]+/ /g;
    $procmess =~ s/(class=error)|(class=status)|(class=standart_text)/class=hintcont/g;
    my $ipdatetime = '';

    my $jgrowlhint = qq~
        <script type="text/javascript">
 	      (function(\$){
		  \$(document).ready(function(){
		  \$.jGrowl.defaults.closer = false;
		  \$('\#updatehint').jGrowl("<div class=update_hint>\\
		  %hint_content%
		  </div>",
		      {
                life: 5000,
                theme: 'updatehint',
                speed: 'slow',
                animateOpen: {
                    height: "show",
                },
                animateClose: {
                    height: "hide",
                }
			  });
		  });
	      })(jQuery);
	    </script>
	    <div id="updatehint" class="top-right" style="display:inline; margin-top:15px;"></div>
 	  ~;

    if (localtime =~ /([\d]+:[\d]+:[\d]+)/) {
        $ipdatetime = "$1";
    }
    my $message = qq~<p class=hinthead>$ipdatetime - Данные успешно обновлены</p>~;
    if (defined($printflag) && $printflag == 1) {

        if ($procstat == 0) {
            # Операция завершилась ошибкой
            $message = qq~<p class=hinthead>$ipdatetime - Операция завершилась ошибкой</p>~;
        }
        $hint = "$message\\\n$procmess\\";
    } else {
        if (defined($procstat) && $procstat == 1) {
            $hint = $message;
        }
    }
    $jgrowlhint =~ s/%hint_content%/$hint/g;
    return $jgrowlhint;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getRekvizityFromFile ( $filepath)
#------------------------------------------------------------------------------
#
# Получить массив реквизитов из заданного файла.
#
#------------------------------------------------------------------------------

sub getRekvizityFromFile {
    my ( $filepath ) = @_;
    my @rekvizity;
    if (-e $filepath) {
        my $doc = XML::LibXML->load_xml( location => $filepath );
        my @rekxml = $doc->findnodes('/organization/rekvizit/value');
        for my $i (0..8) {
            my $rekvalue = $rekxml[$i]->textContent();
            if ($rekvalue !~ /^[\r\n\s]+$/sx && $rekvalue ne '') {
                eval {
                    #$rekvalue = Encode::encode("utf8", $rekvalue);
                };
                push(@rekvizity, $rekvalue);
            } else {
                push(@rekvizity, '')
            }
        }
        return @rekvizity;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doRekvizitySave ()
#------------------------------------------------------------------------------
#
# Сохранить реквизиты.
#
#------------------------------------------------------------------------------

sub doRekvizitySave {
    my ($userid, $filepath, @rvalues) = @_;
    my $rc = -1;

    my $rekviz_item = qq~
        <rekvizit>
            <name>\%1</name>
            <value>\%2</value>
        </rekvizit>~;

    my $xml_template =qq~<?xml version="1.0" encoding="UTF-8"?>
    <?xml-stylesheet type="text/xsl" href="/css/xsl/lightshop.xsl"?>
    <organization>
        <clientid>$userid</clientid>\%1
    </organization>\n~;

    if ( !( -e $MODULE_DOWNLOAD_PATH ) ) { mkpath($MODULE_DOWNLOAD_PATH); }
    my @rtitles = &getOrgRekvizitTitles();
    if ( $#rvalues == $#rtitles ) {
        my $data_to_save = '';
        for my $i (0..$#rvalues) {
            my $item  = $rekviz_item;
            my $value = $rvalues[$i];
            my $title = Encode::decode("utf8", $rtitles[$i]);
            $item =~ s/\%1/$title/;
            $item =~ s/\%2/$value/;
            $data_to_save .= $item;
        }
        if ( $data_to_save ne '' ) {
            $xml_template =~ s/\%1/$data_to_save/;
            open( my $outputfh, ">", $MODULE_DOWNLOAD_PATH."/".$filepath);
            seek($outputfh, 0, 0);
            flock($outputfh, 2);
            print $outputfh $xml_template;
            truncate($outputfh, tell($outputfh));
            flock($outputfh, 8);
            close($outputfh);
            $rc = 0;
        }
        else {
            $rc = -2;
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getOrgRekvizitTitles ()
#------------------------------------------------------------------------------
#
# Получить список названий реквизитов.
#
#------------------------------------------------------------------------------

sub getOrgRekvizitTitles {
    my @rekvizit_titles = (
        'Название организации',
        'Юридический адрес',
        'ИНН',
        'КПП',
        'ОКПО',
        'Расчетный счет',
        'Корреспондентский счет',
        'Наименование банка',
        'БИК',
    );
    return @rekvizit_titles;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# isLoginUnique ()
#------------------------------------------------------------------------------
#
# Проверка логина на уникальность.
#
#------------------------------------------------------------------------------

sub isLoginUnique {
    my ($login, $userid) = @_;
    my @rc = ('', 1);
    if ( -e $MODULE_CLIENT_DB ) {
        open( my $inputfh, $MODULE_CLIENT_DB );
        my @recs = <$inputfh>;
        close($inputfh);
        for my $i (0..$#recs) {
            my @cols = split( /\|/, $recs[$i] );
            if ( $cols[2] =~ /^$login$/ && $cols[0] !~ /^$userid$/) {
                @rc = ('<p class="hintcont">Имя пользователя <font class="hintspan">'.$login.'</font> уже занято!</p>', 0);
                last;
            }
        }
    }
    return @rc;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# delUserByUid ()
#------------------------------------------------------------------------------
#
# Удалить пользователя по его идентификатору.
#
#------------------------------------------------------------------------------

sub delUserByUid {
    my ($userid) = @_;
    my @userdb;
    my $rc = -1;
    if ( -e $MODULE_CLIENT_DB ) {
        open( my $inputfh, $MODULE_CLIENT_DB );
        my @recs = <$inputfh>;
        close($inputfh);
        for my $i (0..$#recs) {
            my @cols = split( /\|/, $recs[$i] );
            if ( $cols[0] !~ /^$userid$/ && $cols[0] =~ /^[\d]+$/) {
                push(@userdb, $recs[$i]);
            }
        }
        if ($#userdb > -1) {
            open( my $outputfh, ">", $MODULE_CLIENT_DB);
            seek($outputfh, 0, 0);
            flock($outputfh, 2);
            print $outputfh @userdb;
            truncate($outputfh, tell($outputfh));
            flock($outputfh, 8);
            close($outputfh);
            $rc = 0;
        } else {
            unlink($MODULE_CLIENT_DB);
            $rc = 0;
        }
        my $rekviz_filepath = $MODULE_DOWNLOAD_PATH."/".$userid.".xml";
        if (-e $rekviz_filepath) {
            unlink($rekviz_filepath);
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------

END { }

return 1;
