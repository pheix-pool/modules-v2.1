if (   $access_modes[2] == 1
    || $access_modes[2] == 2 )    # actions compatible with access modes: r,rw
{

    # start of read-only actions

    if ( $env_params[1] eq 'admlightshop' ) {
        Pheix::Session::delSessionByTimeOut();
        print $co->header( -type => 'text/html; charset=UTF-8' );
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[2];
            if ( defined($got_page) && $got_page !~ /^[\d]+$/ ) { 
                $got_page = 0; 
            }
            Lightshop::Sales::show_lightshop_startpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid($env_params[0]),
                $got_page, 0 );
        }
        else { Pheix::Tools::showLoginPage(); }
        exit;
    }

    if ( $env_params[1] eq 'admlightshopedituser' ) {
        Pheix::Session::delSessionByTimeOut();
        print $co->header( -type => 'text/html; charset=UTF-8' );
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[3];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }

            print Lightshop::Sales::_inner_lightshop_usereditpage(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid($env_params[0]),
                $env_params[2], $got_page, 0 );
        }
        else { Pheix::Tools::showLoginPage(); }
        exit;
    }

    if ( $env_params[1] eq 'admlightshopedit' ) {
        Pheix::Session::delSessionByTimeOut();
        print $co->header( -type => 'text/html; charset=UTF-8' );
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[3];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }

            print Lightshop::Sales::_inner_lightshop_editdeal( $env_params[0],
                Pheix::Session::getCryptLoginBySid($env_params[0]),
                $env_params[2], $got_page, 0 );
        }
        else { Pheix::Tools::showLoginPage(); }
        exit;
    }

    if ( $access_modes[2] == 2 )  # actions compatible just with access mode: rw
    {

         # start of read-write actions

        if ( $env_params[1] eq 'admlightshopusrmodify' ) {
            Pheix::Session::delSessionByTimeOut();
            print $co->header( -type => 'text/html; charset=UTF-8' );
            if ( $check_auth == 1 ) {
                my $userid   = $env_params[2];
                if ($userid !~ /^[\d]+$/) {
                    $userid = 0;
                }
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }

                my $rekviz_check = 1;
                my @rekvizarr = ();
                my $rekvizlen    = $co->param('orgrekvizlen');
                if ($rekvizlen !~ /^[\d]+$/ || $rekvizlen < 0) {$rekvizlen = 0;}
                for my $rinx (0..$rekvizlen) {
                    my $cur_rekviz = Encode::decode("utf8", $co->param('orgrekviz'.$rinx));
                    push(@rekvizarr, $cur_rekviz);
                    if (!defined($cur_rekviz) || $cur_rekviz eq '') {
                        $rekviz_check = 0;
                    }
                }

                my @returnstatus = ( '', 0 );
                my $paytype     = $co->param('paytype');
                my $userfio     = $co->param("fio");
                my $userregdate = $co->param("date") || Lightshop::Sales::LIGHTSHOP_date_format();
                my $userlogin   = $co->param("login");
                my $userpassw   = $co->param("passw");
                my $useremail   = $co->param("email");
                my $useraddr    = $co->param("address");
                my $userphone   = $co->param("phone");
                my $usercomment = '-';
                $useraddr =~ s/[\r\n]+/\<br\>/g;
                @returnstatus = Lightshop::Sales::isLoginUnique($userlogin, $userid);
                if ($returnstatus[1] == 0) {
                    $userlogin = '';
                }

                my $saverc;

                if ( $paytype == 1 ) { $rekviz_check = 1; }

                if (   $userid > 0
                       && $userfio ne ''
                       && $userlogin ne ''
                       && $userpassw ne ''
                       && $useremail ne ''
                       && $useraddr ne ''
                       && $userphone ne ''
                       && $rekviz_check == 1) { 
                           if ( $paytype == 0 && $rekviz_check == 1) {
                               my $filename = lc($userid.'.xml'); 
                               $saverc = Lightshop::Sales::doRekvizitySave( $userid, $filename, @rekvizarr );
                           }
                           @returnstatus = Lightshop::Sales::_inner_lightshop_usermodify(
                                $userid, $userregdate, $userlogin,
                                $userpassw,     $useremail,   $userfio,
                                $useraddr,      $userphone,   $usercomment
                           );
                       }
                Lightshop::Sales::show_lightshop_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid($env_params[0]),
                    $got_page, $returnstatus[0], $returnstatus[1], 1 );
            }
            else { Pheix::Tools::showLoginPage(); }
            exit;
        }

        if ( $env_params[1] eq 'admlightshopdealmodify' ) {
            Pheix::Session::delSessionByTimeOut();
            print $co->header( -type => 'text/html; charset=UTF-8' );
            if ( $check_auth == 1 ) {
                my @returnstatus = ( '', 0 );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                my $dealdate   = $co->param("date");
                my $dealsumma  = $co->param("summa");
                my $dealtrack  = $co->param("track");
                my $dealstatus = $co->param("status");
                if ( $dealstatus != -1 && $dealstatus != 0 && $dealstatus != 1 )
                {
                    $dealstatus = 0;
                }
                my $dealcomment = $co->param("comment");
                $dealcomment =~ s/[\r\n]+/\<br\>/g;
                @returnstatus = Lightshop::Sales::_inner_lightshop_dealmodify(
                    $env_params[2], $dealdate,    $dealsumma,
                    $dealtrack,     $dealcomment, $dealstatus
                );

                Lightshop::Sales::show_lightshop_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid($env_params[0]),
                    $got_page, $returnstatus[0], $returnstatus[1], 1 );
            }
            else { Pheix::Tools::showLoginPage(); }
            exit;
        }

        if ( $env_params[1] eq 'admlightshopdel' ) {
            Pheix::Session::delSessionByTimeOut();
            print $co->header( -type => 'text/html; charset=UTF-8' );
            if ( $check_auth == 1 ) {
                my @returnstatus = ( '', 0 );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                my @rekvizity =
                  Lightshop::Sales::get_objects_by_id( 'images/rekvizity',
                    $env_params[2] );
                if (defined($rekvizity[0])) {
                    if ( -e "images/rekvizity/$rekvizity[0]" ) {
                        unlink("images/rekvizity/$rekvizity[0]");
                    }
                }
                @returnstatus = Lightshop::Sales::_inner_lightshop_dealdelete( $env_params[2] );
                Lightshop::Sales::show_lightshop_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid($env_params[0]),
                    $got_page, $returnstatus[0], $returnstatus[1], 1  );
            }
            else { Pheix::Tools::showLoginPage(); }
            exit;
        }

        if ( $env_params[1] eq 'admlightshopmovedown' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Lightshop::Sales::_inner_lightshop_sgmovedown(
                        $env_params[2] );
                }
                Lightshop::Sales::show_lightshop_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid($env_params[0]),
                    $got_page, 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admlightshopmoveup' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Lightshop::Sales::_inner_lightshop_sgmoveup(
                        $env_params[2] );
                }
                Lightshop::Sales::show_lightshop_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid($env_params[0]),
                    $got_page, 0 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        # end of read-write actions

    }    # Access modes: rw
}    # Access modes: r