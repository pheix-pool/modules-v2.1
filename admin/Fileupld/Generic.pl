if ($access_modes[2] == 1 || $access_modes[2] == 2) # actions compatible with access modes: r,rw
{
    my $fileup = Fileupld::Generic->new;
    # start of read-only actions
    if (
        ( defined( $env_params[1] ) ) &&
        ( $env_params[1] eq $fileup->{mod_sact} )
    ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $sid     = $env_params[0];
            my $gotpage = $env_params[2];
            if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
            $fileup->showHomePage( $sid, $gotpage, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }
    if (
        ( defined( $env_params[1] ) ) &&
        ( $env_params[1] eq 'fileupld-objdescedit' )
    ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $sid     = $env_params[0];
            my $objid   = $env_params[2] || 0;
            my $gotpage = $env_params[3];
            if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
            print $fileup->showFileDescrEditForm(
                $sid, $objid, $gotpage, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }
    # end of read-only actions
    if ($access_modes[2] == 2) # actions compatible just with access mode: rw
    {
        # start of read-write actions
        if (
            ( defined( $env_params[1] ) ) &&
            ( $env_params[1] eq 'fileupld-upload' )
        ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $upl_res;
                my $sid     = $env_params[0];
                my $status  = 1;
                my $fld     = 'uploadfile';
                my $gotpage = $env_params[2];
                if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
                my ($ext)  = $co->param($fld) =~ /(\.[^.]+)$/;
                if ( defined $ext && $ext ne '' ) {
                    my $fid     = time;
                    my $fid_new = $fid . $ext;
                    if ( -e $fileup->{mod_upth} ) {
                        $upl_res = Pheix::Tools::doFileUpload(
                            $fileup->{mod_upth},
                            $fld,
                            $fid_new,
                            0,
                            0
                        );
                        $upl_res =~ s/\"//gi;
                        $upl_res =~ s/\sclass\=[a-z0-9\_\-]+//gi;
                        $upl_res =~ s/<\/?[a-z]+>//gi;
                        if ( $upl_res !~ /error/ ) {
                            # save to db
                            my $pth = $fileup->{mod_upth};
                            $pth =~ s/[\/]+$//gi;
                            $fileup->saveFileData(
                                floor(rand(ceil($fid % 100000))*100000),  # sort id
                                $fid,                       # record id
                                $pth . '/' . $fid_new,      # file path
                                getHexStrFromString(
                                    getDecryptFioBySesId($sid)
                                ),                          # owner details
                                'sample file ' . $fid_new   # file hmi name
                            );
                        }
                    } else {
                        $upl_res =
                            '<p class=hintcont hintdata=error>' .
                            'Target folder <strong>' . $fileup->{mod_upth} .
                            '</strong> not existed!</p>';
                    }
                    $upl_res =
                        '<p class=hintcont>' . $upl_res . '</p>';
                } else {
                    $upl_res =
                        '<p class=hintcont hintdata=error><strong>*'. $ext .
                        '</strong> files are not supported!</p>';
                }
                if ( $upl_res =~ /error/ ) {
                    $status = 0;
                }
                $fileup->showHomePage(
                    $sid,
                    $gotpage,
                    $SHOW_MESS,
                    $status,
                    $upl_res
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }
        if (
            ( defined( $env_params[1] ) ) &&
            ( $env_params[1] eq 'fileupld-objdescmodify' )
        ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $sid     = $env_params[0];
                my $objid   = $env_params[2] || 0;
                my $gotpage = $env_params[3];
                if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
                my $status  = $fileup->setFileData(
                    $objid,
                    'hminm',
                    $co->param('objectdescr')
                );
                $fileup->showHomePage(
                    $sid,
                    $gotpage,
                    $SHOW_MESS,
                    $status,
                    ($status == 0 ?
                        '<p class=hintcont>Unable to set data to object id=' . $objid . '</p>':
                        '<p class=hintcont>Record id=' . $objid . ' is successfully updated!</p>'
                    )
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }
        if (
            ( defined( $env_params[1] ) ) &&
            ( $env_params[1] eq 'fileupld-delobj' )
        ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $sid     = $env_params[0];
                my $objid   = $env_params[2] || 0;
                my $gotpage = $env_params[3];
                if ( $gotpage !~ /^[\d]+$/ ) { $gotpage = 0; }
                my $status  = $fileup->deleteFileData( $objid );
                $fileup->showHomePage( $sid, $gotpage, $DO_QUIET );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }
        # end of read-write actions
    } # Access modes: rw
} # Access modes: r
