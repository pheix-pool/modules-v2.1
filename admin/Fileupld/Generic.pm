package Fileupld::Generic;

use strict;
use warnings;

BEGIN {
    #for all servers
    use lib '../';    # lib path for work mode

    # lib path for systax check during module install
    use lib 'admin/libs/modules/';
    use lib 'admin/libs/extlibs/';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';
}

##############################################################################
#
# File   :  Generic.pm
#
##############################################################################

use POSIX;
use Config::ModuleMngr qw(showUpdateHint getModXml getConfigValue);
use Pheix::Session qw(getStringFromHexStr);
use Fcntl qw(:DEFAULT :flock :seek);

#------------------------------------------------------------------------------
# new ()
#------------------------------------------------------------------------------

sub new {
    my ($class,$pfx) = @_;
    my $MODULE_NAME  = "Fileupld";
    my $MODULE_FLDR  = $pfx . 'admin/libs/modules/' . $MODULE_NAME;
    my $MOD_XML      = getModXml($MODULE_FLDR);
    my $self         = {
        name     => getConfigValue( $MODULE_FLDR, $MOD_XML, 'use_identifier' ),
        version  => getConfigValue( $MODULE_FLDR, $MOD_XML, 'version' ),
        mod_name => $MODULE_NAME,
        mod_fldr => $MODULE_FLDR,
        mod_dscr => getConfigValue( $MODULE_FLDR, $MOD_XML, 'description' ),
        mod_inst => $pfx . 'conf/system/install.tnk',
        mod_tmpl => $pfx . 'admin/skins/classic_.txt',
        mod_dbfn => $pfx . getConfigValue( $MODULE_FLDR, $MOD_XML, 'tankfile' ),
        mod_upth => $pfx . getConfigValue( $MODULE_FLDR, $MOD_XML, 'uploadpath' ),
        mod_scrt => getStatsJsCss(),
        mod_sact => getConfigValue( $MODULE_FLDR, $MOD_XML, 'startaction' ),
    };
    bless $self, $class;
    return $self;
}

#------------------------------------------------------------------------------
# getStatsJsCss ( )
#------------------------------------------------------------------------------

sub getStatsJsCss {
    return qq~
    <!--<script type="text/javascript"></script>-->
    ~;
}

#------------------------------------------------------------------------------
# showHomePage ( $sesid, ... )
#------------------------------------------------------------------------------

sub showHomePage {
    my ($self, $sid, $pg, $prflag, $stat, $mess) = @_;
    if (-e $self->{mod_tmpl}) {
        open my $fh, "<", $self->{mod_tmpl}
            or die "unable to open " . $self->{mod_tmpl} . ": $!";
        my @tmpl    = <$fh>;
        close $fh or die "unable to close " . $self->{mod_tmpl} . ": $!";
        for my $cnt (0..$#tmpl) {
            $tmpl[$cnt] =
                Pheix::Tools::fillCommonTags( $sid, $tmpl[$cnt] );
            if ( $tmpl[$cnt] =~ /\%scripts_for_this_page\%/ ) {
                $tmpl[$cnt] =~ s/(\<\!\-\-)|(\-\-\>)//gi;
                my $scrt = $self->{mod_scrt};
                $tmpl[$cnt] =~ s/\%scripts_for_this_page\%/$scrt/;
            }
            if ( $tmpl[$cnt] =~ /\%content\%/ ) {
                my $content =
                    $self->getHomePage($sid, $pg, $prflag, $stat, $mess);
                $tmpl[$cnt] =~ s/\%content\%/$content/;
            }
            print $tmpl[$cnt];
        }
    } else {
        print '<h4>Pheix: unable to open file '.$self->{mod_tmpl}.'</h4>'
    }
}

#------------------------------------------------------------------------------
# getHomePage ( $sid, ... )
#------------------------------------------------------------------------------

sub getHomePage {
    my ($self, $sid, $pg, $prflag, $stat, $mess) = @_;

    my $cntnt_ =
        '<div class=a_hdr>' . $self->{mod_dscr} . '</div>' .
        showUpdateHint($prflag, $stat, $mess);
    $cntnt_   .=
#         qq~<b>$self->{name}</b> module is succesfully integrated to admin area.~;
        $self->getUploadFileForm($sid) .
        $self->getFilesTabAdapter($sid);
    return $cntnt_;
}

#------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# getUploadFileForm ( $sid )
#----------------------------------------------------------------------------------------

sub getUploadFileForm {
    my ($self, $sid) = @_;
    my $form = qq~
        <table cellspacing="2" cellpadding="0" width="100%">
        <tr><td align="right" valign="bottom">&nbsp;</td></tr>
        <tr><td valign="bottom">
        <form
            method="post"
            action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=fileupld-upload"
            enctype="multipart/form-data">
         <table
            cellspacing="2"
            cellpadding="9"
            width="100%"
            class="messagelist"
            style="background-image:url('images/social_bg.png');height:94">
         <tr>
            <td
                width="100%"
                class="s_t"
                style="font-family:Arial,Tahoma;color:#505050;font-size:12pt;">
                <div class="inst_mod_head _phx-inblck">
                        Загрузить&nbsp;новый&nbsp;файл&nbsp;[</div>
                <div class="_phx-inblck _phx-posr _phx-t3">
                    <div class="pheix-upload-icon">
                       <i class="fa fa-file"
                           aria-hidden="true" title=".*"></i>
                    </div>
                </div>
                <div class="inst_mod_head _phx-inblck">]&nbsp;:</div>
            </td></tr>
            <tr><td width="100%">
            <div class="f_imulation_wrap">
                <div class="im_input">
                    <input type="text"value="Выберите файл"/>
                </div>
                <input
                    type="file"
                    required
                    size=30
                    name="uploadfile"
                    id="imulated"
                    style="margin-left:-124px;margin-top:7px;height:20px"/>
            </div>
            <script type="text/javascript">
                jQuery('.im_input input').click(function(){
                    jQuery('.im_input input').css('color','#C0C0C0');
                    jQuery('.im_input input').val('Выберите файл');
                    jQuery("#imulated").val('');
                    jQuery('#imulated').trigger('click');
                });
                jQuery('#imulated').change(function(){
                    jQuery('.im_input input').css('color','#505050');
                    jQuery('.im_input input').val(jQuery(this).val());
                });
            </script>
            </td></tr>
            <tr><td width="100%">
                <input
                    type="submit"
                    value="Загрузить новый файл"
                    class="installbutton">
            </td></tr>
        </table>
        </form>
        </td></tr>
        </table>
    ~;
    return qq~
    <div class="spoiler-wrapper" style="width:98%">
        <div class="spoiler folded">
            <a href="javascript:void(0);" class="spoilerlink">
                Загрузить новый файл в систему</a></div>
        <div class="spoiler-text">$form</div>
    </div>
    <script type="text/javascript">
           jQuery(document).ready(function(){
               jQuery('.spoiler-text').hide()
               jQuery('.spoiler').click(function(){
                   jQuery(this).toggleClass("folded").toggleClass("unfolded").next().slideToggle()
               })
           })
    </script>
    ~;
}

#----------------------------------------------------------------------------------------
# getFilesTabAdapter ( $sid )
#----------------------------------------------------------------------------------------

sub getFilesTabAdapter {
    my $pftab;
    my ($self, $sid) = @_;
    if ( !(-e $self->{mod_dbfn}) ) {
        $pftab = '<div class="pheix-error-alert">Файл данных ' .
            $self->{mod_dbfn} . ' модуля ' .
            $self->{name} . ' не найден!</div>';
    } else {
        $pftab = '<div class="pheix-admin-tab">';
        $pftab .= $self->getFilesTab($sid);
        $pftab .= '</div>';
    }
    return $pftab;
}

#----------------------------------------------------------------------------------------
# getFilesTab ( $sid )
#----------------------------------------------------------------------------------------

sub getFilesTab {
    my $tab;
    my ($self, $sid) = @_;
    open my $fh, "<", $self->{mod_dbfn} or die "unable to open " . $self->{mod_dbfn} . ": $!";
    my @recs = reverse <$fh>;
    close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
    for my $i (0..$#recs) {
        $recs[$i] =~ s/[\r\n\t]+//gi;
        my $cntr  = ($i+1);
        my @cols  = split(/\|/,$recs[$i]);
        my %data  = (
            'srtid' => $cols[0],
            'recid' => $cols[1],
            'fpath' => $cols[2],
            'usrid' => $cols[3],
            'hminm' => $cols[4]
        );
        (my $delmess = $data{hminm}) =~ s/'/\\'/g;
        my $userdetails = getStringFromHexStr($data{usrid});
        my $bgclr = ( $cntr % 2 == 0 ) ? 'tr01' : 'tr02';
        my $uploaddate = localtime($data{recid});
        $tab .= qq~
        <div class="pheix-admin-tab-container">
          <div class="pheix-admin-tab-row $bgclr">
            <div class="pheix-admin-tab-cell-l">
              <p class="pheix-admin-tab-row-header">
                <span class="pheix-admin-tab-row-legend">$cntr.</span>&nbsp;
                $data{fpath}</p>
              <p><span class="pheix-admin-tab-row-legend">
                Name/description:</span> $data{hminm}</p>
              <p><span class="pheix-admin-tab-row-legend">
                ID:</span> $data{recid}</p>
              <p><span class="pheix-admin-tab-row-legend">
                Date:</span> $uploaddate</p>
              <p><span class="pheix-admin-tab-row-legend">
                Added by:</span> $userdetails</p>
            </div>
            <div class="pheix-admin-tab-cell-r _phx-rght">
              <a href="javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}', '&amp;action=fileupld-objdescedit&amp;objid=$data{recid}&amp;page=0')" class="fa-action-link">
                <i class="fa fa-pencil-square-o"></i></a>
              <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$sid&amp;action=fileupld-delobj&amp;objid=$data{recid}&amp;page=0','Вы уверены, что хотите удалить объект &quot;$delmess&quot; из базы данных?')" class="fa-action-link">
                <i class="fa fa-trash"></i></a>
            </div>
          </div>
        </div>
        ~;
    }
    return $tab;
}

#------------------------------------------------------------------------------
# saveFileData( $sortid, ... )
#------------------------------------------------------------------------------

sub saveFileData {
    my ($self, $sortid, $recid, $fpath, $userdetails, $hminame ) = @_;
    my @recs;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
    }
    push( @recs,
          join("|",
              $sortid,
              $recid,
              $fpath,
              $userdetails,
              $hminame
          ) . "\n"
    );
    open my $fh, ">", $self->{mod_dbfn} or
        die "unable to open " . $self->{mod_dbfn} . ": $!";
    flock( $fh, LOCK_EX ) or die "unable to lock mailbox: $!";
    print $fh @recs;
    truncate $fh, tell($fh);
    flock( $fh, LOCK_UN ) or die "unable to unlock mailbox: $!";
    close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
    return 1;
}

#------------------------------------------------------------------------------
# getFileData( $recid )
#------------------------------------------------------------------------------

sub getFileData {
    my ($self, $recid ) = @_;
    my @recs;
    my %data;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            $recs[$i] =~ s/[\r\n\t]+//gi;
            my @cols  = split(/\|/,$recs[$i]);
            %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4]
            );
            if ( $data{recid} == $recid ) {
                last;
            }
        }
    }
    return %data;
}

#------------------------------------------------------------------------------
# setFileData( $recid, $key, $value )
#------------------------------------------------------------------------------

sub setFileData {
    my ($self, $recid, $key, $value ) = @_;
    my @recs;
    my %data;
    my $rc = 0;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            my $rcrd = $recs[$i];
            $rcrd    =~ s/[\r\n\t]+//gi;
            my @cols = split(/\|/,$rcrd);
            %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4]
            );
            if ( $data{recid} == $recid ) {
                if ( exists $data{$key} ) {
                    $data{$key} = $value;
                    $recs[$i]   = join("|",
                        $data{srtid},
                        $data{recid},
                        $data{fpath},
                        $data{usrid},
                        $data{hminm}
                    ) . "\n";
                }
                open my $fh, ">", $self->{mod_dbfn} or
                    die "unable to open " . $self->{mod_dbfn} . ": $!";
                flock( $fh, LOCK_EX ) or die "unable to lock mailbox: $!";
                print $fh @recs;
                truncate $fh, tell($fh);
                flock( $fh, LOCK_UN ) or die "unable to unlock mailbox: $!";
                close $fh or
                    die "unable to close " . $self->{mod_dbfn} . ": $!";
                $rc = 1;
                last;
            }
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------
# deleteFileData( $recid )
#------------------------------------------------------------------------------

sub deleteFileData {
    my ($self, $recid ) = @_;
    my @recs;
    my %data;
    if ( -e $self->{mod_dbfn} ) {
        my @updated;
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            my $rcrd  = $recs[$i];
            $rcrd     =~ s/[\r\n\t]+//gi;
            my @cols  = split(/\|/,$rcrd);
            %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4]
            );
            if ( $data{recid} != $recid ) {
                push(@updated,$recs[$i]);
            } else {
                if ( -e $data{fpath} ) {
                    unlink( $data{fpath} );
                }
                if ( -e $data{jsonp} ) {
                    unlink( $data{jsonp} );
                }
                if ( -e $data{csvpt} ) {
                    unlink( $data{csvpt} );
                }
            }
        }
        if ( @updated ) {
            open my $fh, ">", $self->{mod_dbfn} or
                die "unable to open " . $self->{mod_dbfn} . ": $!";
            flock( $fh, LOCK_EX ) or die "unable to lock mailbox: $!";
            print $fh @updated;
            truncate $fh, tell($fh);
            flock( $fh, LOCK_UN ) or die "unable to unlock mailbox: $!";
            close $fh or
                die "unable to close " . $self->{mod_dbfn} . ": $!";
        } else {
            if ( -e $self->{mod_dbfn} ) {
                unlink( $self->{mod_dbfn} );
            }
        }
    }
    return 1;
}

#------------------------------------------------------------------------------
# showFileDescrEditForm( $sid, $objid, $pg, $printflag )
#------------------------------------------------------------------------------

sub showFileDescrEditForm {
    my ( $self, $sid, $objid, $pg, $printflag) = @_;
    my %data = $self->getFileData($objid);
    my $hdr  = 'Редактирование описания объекта';
    my $form = qq~
       <div class="popup_header">$hdr</div>
       <div class=standart_text style="margin-left:1px"><a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admfileupld" class=simple_link><b>$self->{name}</b></a> / $hdr</div>
       <form id="fileupld-obj" method=post action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=fileupld-objdescmodify&amp;objid=$objid&amp;page=$pg" style="margin:0">
         <table cellspacing=0 cellpadding=2 border=0 class="pheix-margins-25px">
           <tr><td class=standart_text>Название/описание: <font color="#A0A0A0">[обязательное поле]</font></td></tr>\n
           <tr><td class=standart_text><input placeholder="Введите описание объекта" required name=objectdescr value="$data{hminm}" class=input style="width:100%;"></td></tr>\n
           <tr><td class=standart_text><input type=submit value="Сохранить изменения" class="button" style="width:150px"></td></tr>\n
         </table>
       </form>
    ~;
    return $form;
}

#------------------------------------------------------------------------------

END { }

1;
