if (   $access_modes[2] == 1
    || $access_modes[2] == 2 )    # actions compatible with access modes: r,rw
{

    # start of read-only actions

    if ( $env_params[1] eq 'admnews' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[2];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            News::Archive::showNAHomePage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $got_page, $DO_QUIET );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admnewsadd' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print News::Archive::getNewsCreatFrm( $env_params[0] );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }
    
    if ( $env_params[1] eq 'admnewsedit' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[3];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print News::Archive::getNewsEditFrm( 
                    $env_params[0], $env_params[2], $got_page);
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admnewsgetallgal' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $gallery_list = '';
            eval {
                $gallery_list =
                  Gallery::System::getGalleryList( $co->param('term') );
            };
            if ($@) {
                $gallery_list = qq~[{ "label": "Gallery::System module is not installed!", "value": "-1", "id": "0" }]~;
            }
            print $gallery_list;
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admnewsgetgalcarousel' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $gallery_carousel;
            my $gallery_id       = $env_params[2];
            if ( $gallery_id !~ /^[\d]+$/ ) { $gallery_id = 0; }
            eval {
                $gallery_carousel =
                  Gallery::System::getGalleryCarousel( $env_params[0],
                    $gallery_id, 0 );
            };
            if ($@) {
                $gallery_carousel =
                  Gallery::System::showGalleryErr( $env_params[0],
                    $gallery_id );
            }
            print $gallery_carousel;
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'searchnews' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $filter_request = $co->param('search');
            my $got_page      = $env_params[2];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            News::Archive::showNAHomePage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $got_page, 3, 'exactsearch', $filter_request );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( $env_params[1] eq 'admnewsautocompletecap' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print qq~[{ "label": "Module Gallery::System is inactive!", "value": "-1", "id": "0" }]~;
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    # end of read-only actions

    if ( $access_modes[2] == 2 )  # actions compatible just with access mode: rw
    {

        # start of read-write actions

        if ( $env_params[1] eq 'admdonewscreate' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                my $got_page     = 0;
                my $flag         = $DO_QUIET;
                my $uplmess      = '';
                my $news_sort  = time;
                my $news_id    = $news_sort;
                my $seochpu      = $co->param('seochpu');
                my $seotitle     = $co->param('seotitle');
                my $seometadescr = $co->param('seometadescr');
                my $seometakeys  = $co->param('seometakeys');
                my $date         = $co->param('date');
                my $header       = $co->param('header');
                my $comment      = $co->param('comment');
                my $links        = $co->param('links');
                my $wysiwygnews  = $co->param('wysiwygnews');
                my $twitteracc   = $co->param('twitteraccount');
                my $gallerysrch  = $co->param('gallerysearch');
                my $galleryid    = $co->param('hiddengalleryid');
                my $twitterimg   = $co->param('twitterimage');
                my $tcpublich    = $co->param('publishcard');
                if ($tcpublich !~ /^[\d]+$/) {$tcpublich = 0;}
                if ($tcpublich < 0 || $tcpublich > 1) {$tcpublich = 0;}

                my @ln_analize = split( /\r\n/, $links );
                my @ln_array   = ();
                for(my $i=0; $i <= $#ln_analize; $i++) {
                    my $ln_val = $ln_analize[$i];
                    $ln_val =~ s/[\r\n\t]+//g;
                    if ($ln_val ne '') {push(@ln_array, $ln_val);}
                }
                $links = join("<br>", @ln_array);

                $comment     =~ s/[\r\n]+/\<br\>/g;
                $wysiwygnews =~ s/[\r\n\t]+//g;

                if ( $seochpu ne '' && 
                     $comment ne '' && 
                     $header ne '' ) {
                    News::Archive::createNewsRecord(
                        $news_sort, $news_id, $date,
                        $seochpu,     $seotitle,  $seometadescr,
                        $seometakeys, $header,    $comment,
                        $wysiwygnews, $links,     $twitteracc,
                        $gallerysrch, $galleryid, $twitterimg,
                        $tcpublich
                    );
                    if ( defined($SHOW_MESS) ) {
                        $flag    = $SHOW_MESS;
                    } else {
                        $flag    = 1;
                    }
                }
                else {
                    $flag    = 2;
                    $uplmess = "***error: found empty elements in form!";
                }

                print $co->header( -type => 'text/html; charset=UTF-8' );
                News::Archive::showNAHomePage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $flag, $uplmess );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }
        
        if ( $env_params[1] eq 'admdonewsmodify' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                my $got_page     = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                my $flag         = $DO_QUIET;
                my $uplmess      = '';
                my $news_sort    = $env_params[2];
                my $news_id      = $news_sort;
                my $seochpu      = $co->param('seochpu');
                my $seotitle     = $co->param('seotitle');
                my $seometadescr = $co->param('seometadescr');
                my $seometakeys  = $co->param('seometakeys');
                my $date         = $co->param('date');
                my $header       = $co->param('header');
                my $comment      = $co->param('comment');
                my $links        = $co->param('links');
                my $wysiwygnews  = $co->param('wysiwygnews');
                my $twitteracc   = $co->param('twitteraccount');
                my $gallerysrch  = $co->param('gallerysearch');
                my $galleryid    = $co->param('hiddengalleryid');
                my $twitterimg   = $co->param('twitterimage');
                my $tcpublich    = $co->param('publishcard');
                if ($tcpublich !~ /^[\d]+$/) {$tcpublich = 0;}
                if ($tcpublich < 0 || $tcpublich > 1) {$tcpublich = 0;}

                my @ln_analize = split( /\r\n/, $links );
                my @ln_array   = ();
                for(my $i=0; $i <= $#ln_analize; $i++) {
                    my $ln_val = $ln_analize[$i];
                    $ln_val =~ s/[\r\n\t]+//g;
                    if ($ln_val ne '') {push(@ln_array, $ln_val);}
                }
                $links = join("<br>", @ln_array);

                $comment     =~ s/[\r\n]+/\<br\>/g;
                $wysiwygnews =~ s/[\r\n\t]+//g;

                if ( $seochpu ne '' && 
                     $comment ne '' && 
                     $header ne '' ) {
                    News::Archive::saveNews(
                        $news_sort, $news_id, $date,
                        $seochpu,     $seotitle,  $seometadescr,
                        $seometakeys, $header,    $comment,
                        $wysiwygnews, $links,     $twitteracc,
                        $gallerysrch, $galleryid, $twitterimg,
                        $tcpublich
                    );
                    if ( defined($SHOW_MESS) ) {
                        $flag    = $SHOW_MESS;
                    } else {
                        $flag    = 1;
                    }
                }
                else {
                    $flag    = 2;
                    $uplmess = "***error: found empty elements in form!";
                }

                print $co->header( -type => 'text/html; charset=UTF-8' );
                News::Archive::showNAHomePage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $flag, $uplmess );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }        
        
        if ( $env_params[1] eq 'delnews' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                News::Archive::delNews( $env_params[2] );
                
                while($got_page > -1) {
                    my $chk = News::Archive::chckNewsPage($got_page);
                    if ($chk) {
                        last;
                    } else {
                        $got_page--;
                    }
                }
                
                News::Archive::showNAHomePage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $SHOW_MESS );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }        

        if ( $env_params[1] eq 'admnewsmovedown' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    News::Archive::moveNewsDown( $env_params[2] );
                }
                News::Archive::showNAHomePage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $DO_QUIET );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( $env_params[1] eq 'admnewsmoveup' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    News::Archive::moveNewsUp( $env_params[2] );
                }
                News::Archive::showNAHomePage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page, $DO_QUIET );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        # end of read-write actions

    }    # Access modes: rw
}    # Access modes: r