package News::Archive;

use strict;
use warnings;

BEGIN { }

##############################################################################
#
# File   :  Archive.pm
#
##############################################################################
#
# Главный пакет управления новостной лентой в административной части CMS Pheix
#
##############################################################################

# Поддержка UTF-8 на уровне переменных, regexp и строковых функций
use Encode;
use POSIX;

my $MODULE_NAME        = "News";
my $MODULE_FOLDER      = "admin/libs/modules/$MODULE_NAME/";
my $MODULE_DB_FILE     = "conf/system/news.tnk";
my $INSTALL_FILE_PATH  = "conf/system/install.tnk";
my $ADMIN_SKIN_PATH    = 'admin/skins/classic_.txt';
my $MODULE_RECS_ONPAGE = 5;

my $MODULE_SCRIPTS     = qq~
    <!-- TABS -->
    <link type="text/css" rel="stylesheet" href="css/easytabs/easy-responsive-tabs.css">
    <script src="js/easytabs/easyResponsiveTabs.js" type="text/javascript"></script>
~;

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModPwrStatus ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить статус модуля (включен или выключен).
# Функция возвращает статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub getModPwrStatus {
    my $file_to_parse = $INSTALL_FILE_PATH;
    if ( -e $file_to_parse ) {
        open( CFGFH, "<$file_to_parse" );
        my @filecontent = <CFGFH>;
        close(CFGFH);
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my @tmp = split( /\|/, $filecontent[$admplinx] );
            if ( $tmp[2] eq $MODULE_NAME ) { return $tmp[4]; }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showNAHomePage ( $sesid, $crypted_login, $page_num, $print_flag, $mess )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# $crypted_login - зашифрованный идентификатор пользователя.
# $page_num - номер страницы, которую требуется вывести.
# $print_flag - флаг управления печатью.
# $mess - дополнительное сообщение, выводимое по флагу.
# Вывод главной страницы административной части.
# Функция возвращает содержимое главной страницы административной части в
# HTML формате.
#
#------------------------------------------------------------------------------

sub showNAHomePage {
    my $contentblock = '';
    open( FCFH, "<", $ADMIN_SKIN_PATH );
    my @scheme    = <FCFH>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        if ( $scheme[$cnt] =~ /\%scripts_for_this_page\%/ ) {
            $scheme[$cnt] = $MODULE_SCRIPTS;
        }
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            $contentblock = &getNewsTable(@_);
            $scheme[$cnt] =~ s/\%content\%/$contentblock/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close(FCFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getNewsDebugMess ( $sesid  )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# Вывод отладочного сообщения модуля.
# Функция возвращает отладочное сообщение в HTML формате.
#
#------------------------------------------------------------------------------

sub getNewsDebugMess {
    my $sesid                = $_[0];
    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr         =
      Config::ModuleMngr::getConfigValue( $MODULE_FOLDER, $xml_description_file,
        "description" );
    my $newsContent = qq~<div class=admin_header>$module_descr</div>~;
    my $mess        = '&nbsp;Выполнен доступ к новостной ленте';
    $newsContent   .= qq~<div class="stxt">$mess (sesid=$sesid)</div>~;
    return $newsContent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getNewsTable ( $sesid, $crypted_login, $page_num, $print_flag, $mess )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# $crypted_login - зашифрованный идентификатор пользователя.
# $page_num - номер страницы, которую требуется вывести.
# $print_flag - флаг управления печатью.
# $mess - дополнительное сообщение, выводимое по флагу.
# Вывод таблицы новостной ленты с постраничным листанием.
# Функция возвращает таблицу новостной ленты в HTML формате.
#
#------------------------------------------------------------------------------

sub getNewsTable {
    my $sesid                = $_[0];
    my $curPg                = $_[2];
    my $pStat                = $_[3];
    my $pMess                = $_[4];
    my $information          = '';
    my $filter               = '';
    my $search_request       = '';
    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr         =
      Config::ModuleMngr::getConfigValue( $MODULE_FOLDER, $xml_description_file,
        "description" );
    my $newsContent = qq~<div class=admin_header>$module_descr</div>~;
    $newsContent .= qq~
        $information
        <div style="margin: 0px 0px 2px 0px; width:100%" class="container">
            <a href="javascript:doSesChckLoad('$sesid', '$ENV{SCRIPT_NAME}', '&amp;action=admnewsadd')" 
               >Создать запись в новостной ленте</a>
        </div>
    ~;

    if ( $pStat == 1 ) {
        my $upTime = '';
        if ( localtime =~ /([\d]+:[\d]+:[\d]+)/ ) { $upTime = "$1:"; }
        $information = "<div class=status>$upTime Информация успешно обновлена!</div><br>\n";
    }
    else {
        if ( $pStat == 2 ) {
            $information = "<div class=error>$pMess</div><br>\n";
        }
        else {
            if ( $pStat == 3 ) {
                $filter = $pMess;
                $search_request = $_[5];
                # TODO filter/search request proccessing (look weblog)
            }
        }
    }
    $newsContent   .= $information;
    # my $mess   = '&nbsp;Выполнен доступ к новостной ленте';
    # $newsContent   .= qq~<div class="stxt">$mess ($sesid, $pStat, $pMess, $search_request)</div>~;
    $newsContent  .= &getNewsHtmlTab($sesid, $curPg, $MODULE_RECS_ONPAGE, $filter, $search_request);
    return $newsContent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getNewsCreatFrm ( $sesid )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# Вывод формы создания записи в новостной ленте.
# Функция возвращает форму создания записи в новостной ленте в HTML формате.
#
#------------------------------------------------------------------------------

sub getNewsCreatFrm {
    my $page_ref        = 0; # всегда сваливаемся на начальную страницу раздела
    my $creatDate       = &getDate();
    my $formname        = "slickgalleryform";
    my $_galjson = "\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalgetall\"";
    my $_galactv;
    eval {
        $_galactv = Gallery::System::get_power_status_for_module();
    };
    if ($@ || $_galactv == 1) {
        $_galjson = "\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admnewsautocompletecap\"";
    }
    my $creatFrm        = qq~

       <div class="popup_header">Создание новой записи</div>
       <div class=stxt style="margin: 0 0 20px 1px">
           <a  href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admnews&amp;page=$page_ref"
               class=simple_link><b>Новостная лента</b></a> / Добавление новой записи
       </div>
       <form   name="$formname"
               method=post 
               enctype="multipart/form-data"
               action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admdonewscreate&amp;page=$page_ref"
               style="margin:0">

       <table cellspacing=0 cellpadding=2 border=0 width="905">
           <tr>
               <td width="305" valign="top">
                <div class="div-td-lft">
                 <div>ЧПУ страницы <i>(без .html)</i>: <span class="text-grey">[обязательное поле]</span></div>
                 <div><input type=text
                          required
                          name=seochpu
                          placeholder="tekst-kotoryj-ponyaten-vsem"
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div>Текст для тега <i>&lt;TITLE&gt;&lt;/TITLE&gt;</i>:</div>
                 <div><input type=text
                          name=seotitle
                          placeholder="Текст для тега TITLE"
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div>Текст для тега <i>&lt;META name=&quot;description&quot; /&gt;</i>:</div>
                 <div><input type=text
                          name=seometadescr
                          placeholder="Текст для тега META.description"
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div>Текст для тега <i>&lt;META name=&quot;keywords&quot; /&gt;</i>:</div>
                 <div><input type=text
                          name=seometakeys
                          placeholder="Текст для тега META.keywords"
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div><input type=hidden name=date value="$creatDate"></div>
                 <div>Заголовок записи в блоге: <span class="text-grey">[обязательное поле]</span></div>
                 <div><input type=text
                          name=header
                          placeholder="Текст заголовка новости"
                          required
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div>Краткое содержание записи: <span class="text-grey">[обязательное поле]</span></div>
                 <div><textarea name=comment
                             required
                             placeholder="Краткий комментарий к новости"
                             class="news-in-grey"
                             style="width:290px; height:77px;"></textarea></div>
                 <div>Ссылки: <font color="#A0A0A0">[ <a class=simplelink
                                       href="javascript:addLinkWizard('$formname', 'links')"
                                       >помощник</a>&nbsp;]</font></div>
                 <div><textarea name=links
                             class="news-in-grey"
                             placeholder="&lt;a href=&quot;http://goo.gl&gt;Google&lt;/a&gt;"
                             style="width:290px; height:77px;"></textarea></div>
                </div></td>
               <td width="600" valign="top" align="right">
                  <script type="text/javascript" src="js/easytabs-init.js"></script>
                  <div style="width:585px;">
                   <div id="horizontalTab" style="padding-left:0;">
                    <ul class="resp-tabs-list">
                     <li class=stxt>Twitter Card</li>
                     <li class=stxt>WYSIWYG-редактор записи</li>
                    </ul>
                    <div class="resp-tabs-container">
                        <div class=news-tabs-form>
                            <div>Twitter-аккоунт автора:</span></div>
                            <div><input   type="text" placeholder="\@twitteraccount"
                                          name="twitteraccount"
                                          value=""
                                          class="input"
                                          style="width:100%" ></div>
                            <div>Связать с фотогалереей:</div>
                            <div><input   type="text" placeholder="Введите название существующей галереи"
                                          name="gallerysearch"
                                          id="galsrch"
                                          value=""
                                          class="input"
                                          style="width:100%"></div>
                            <input   type="hidden" value="0" name="hiddengalleryid">
                            <input   type="hidden"
                                     name="galleryid"
                                     id="galselectedid"
                                     value="0">
                            <div>Путь до отображаемого на Twitter-карте изображения:</div>
                            <div><input   type="text" placeholder="Выберите изображение из галереи или введите путь"
                                          name="twitterimage"
                                          id="twtrimg"
                                          value=""
                                          class="input"
                                          style="width:100%"></div>
                            <div><input type="checkbox" id="c2" name="publishcard" value="1" checked/>
                               <label for="c2" class=cb-lbl-2><span></span>Опубликовать twitter-карту</label>
                            </div>
                            <div id="galCarousel" style="padding:0 0 0 25px; margin:0 0 0 5px; background:#F1F1F1; width:515px"></div>
                        </div>
                        <div class=stxt>
                        <textarea id="wysiwygnewseditor-create" name="wysiwygnews"></textarea>
                        <script type="text/javascript">
                            CKEDITOR.replace( 'wysiwygnews', { width: '550', height: '239', removeButtons: 'Save,Underline,Subscript,Superscript', pheixScript: '$ENV{SCRIPT_NAME}', pheixSessionId: '$_[0]', pheixGalDivId: 'archive-divgal-postcontent-create' });
                        </script>
                        </div>
                    </div>
                   </div>
                  </div>
                  </td>
           </tr>
           <tr><td colspan="2">
            <input type=submit
               value="Создать новую запись"
               class="news-btn-create"
               style="margin-left:0px; width:290px;"></td></tr>
       </table>
       </form>
        <script type="text/javascript">
        jQuery(function(){
           var formname = "$formname";
           function log( message ) {
               top.document.forms[formname].elements["galleryid"].value = message;
               top.document.forms[formname].elements["hiddengalleryid"].value = message;
           }
           console.log("request to: $ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalgetall");
           jQuery( "#galsrch" ).autocomplete({
               source: $_galjson,
               minLength: 1,
               select: function( event, ui ) {
                   log( ui.item ?
                   ui.item.id :
                   "-1" );
               } ,
               close: function( event, ui ) {
                   var itemValue = top.document.forms[formname].elements["gallerysearch"].value;
                   if (itemValue == -1) {
                       top.document.forms[formname].elements["gallerysearch"].value = '';
                       jQuery("#galCarousel").empty();
                   } else {
                       var itemId = top.document.forms[formname].elements["hiddengalleryid"].value;
                       if (itemId > 0 ) {
                           doInlineDataLoad("galCarousel", "$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalgetcarousel&amp;galid="+itemId);
                       }
                   }
               }
         });
       });
       </script>
    ~;
    return $creatFrm;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getNewsEditFrm ( $sesid )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# Вывод формы создания записи в новостной ленте.
# Функция возвращает форму создания записи в новостной ленте в HTML формате.
#
#------------------------------------------------------------------------------

sub getNewsEditFrm {
    my $editFrm         = '';
    my $sesid           = $_[0];
    my $newsid          = $_[1];
    my $page_ref        = $_[2];
    my $creatDate       = &getDate();
    my $formname        = "slickgalleryform";
    my @newsRecCols     = &getNewsRecCols($newsid);
    my $_galjson = "\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admgalgetall\"";
    my $_galactv;

    if ($#newsRecCols > -1) {
        $newsRecCols[8]     =~ s/<br>/\n/gi;
        $newsRecCols[10]    =~ s/<br>/\n/gi;
        if ($newsRecCols[15] == 1) {
            $newsRecCols[15] = 'checked';
        } else {
            $newsRecCols[15] = '';
        }

        my $gallery_carousel;
        my $twitterimage     = $newsRecCols[14];
        my $gallery_id       = $newsRecCols[13];
        if ( $gallery_id !~ /^[\d]+$/ ) { $gallery_id = 0; }
        eval {
            $gallery_carousel =
                Gallery::System::getGalleryCarousel( $_[0],
                    $gallery_id, $twitterimage );
            $_galactv = Gallery::System::get_power_status_for_module();
        };
        if ($@ || $_galactv == 1) {
            my $mess = '&nbsp;Ошибка доступа к заданной галереи';
            $gallery_carousel = qq~
            <div class="wl-err">$mess (id=$gallery_id)</div>
            ~;
            $_galjson = "\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admwlautocompletecap\"";
        }

        $editFrm        = qq~
        <script type="text/javascript">
         jQuery(function(){
           var formname = "$formname";
           function log( message ) {
               top.document.forms[formname].elements["galleryid"].value = message;
               top.document.forms[formname].elements["hiddengalleryid"].value = message;
           }
           jQuery( "#galsrch" ).autocomplete({
               source: $_galjson,
               minLength: 1,
               select: function( event, ui ) {
                   log( ui.item ?
                   ui.item.id :
                   "-1" );
               } ,
               close: function( event, ui ) {
                   var itemValue = top.document.forms[formname].elements["gallerysearch"].value;
                   if (itemValue == -1) {
                       top.document.forms[formname].elements["gallerysearch"].value = '';
                       jQuery("#galCarousel").empty();
                   } else {
                       var itemId = top.document.forms[formname].elements["hiddengalleryid"].value;
                       if (itemId > 0 ) {
                           doInlineDataLoad("galCarousel", "$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admgalgetcarousel&amp;galid="+itemId);
                       }
                   }
               }
         });
        });
        </script>

        <div class="popup_header">Редактирование записи в новостной ленте</div>
        <div class=stxt style="margin: 0 0 20px 1px">
           <a  href="$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admnews&amp;page=$page_ref"
               class=simple_link><b>Новостная лента</b></a> / Редактирование записи в новостной ленте
        </div>
        <form   name="$formname"
               method=post 
               enctype="multipart/form-data"
               action="$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admdonewsmodify&amp;newsid=$newsid&amp;page=$page_ref"
               style="margin:0">

        <table cellspacing=0 cellpadding=2 border=0 width="905">
           <tr>
               <td width="305" valign="top">
                <div class="div-td-lft">
                 <div>ЧПУ страницы <i>(без .html)</i>: <span class="text-grey">[обязательное поле]</span></div>
                 <div><input type=text
                          required
                          name=seochpu
                          value="$newsRecCols[3]"
                          placeholder="tekst-kotoryj-ponyaten-vsem"
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div>Текст для тега <i>&lt;TITLE&gt;&lt;/TITLE&gt;</i>:</div>
                 <div><input type=text
                          name=seotitle
                          value="$newsRecCols[4]"
                          placeholder="Текст для тега TITLE"
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div>Текст для тега <i>&lt;META name=&quot;description&quot; /&gt;</i>:</div>
                 <div><input type=text
                          name=seometadescr
                          value="$newsRecCols[5]"
                          placeholder="Текст для тега META.description"
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div>Текст для тега <i>&lt;META name=&quot;keywords&quot; /&gt;</i>:</div>
                 <div><input type=text
                          name=seometakeys
                          value="$newsRecCols[6]"
                          placeholder="Текст для тега META.keywords"
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div>Дата:</div>
                 <div><input type=text name=date value="$newsRecCols[2]" class=input style="width:50%">&nbsp;&nbsp;&nbsp;&nbsp;
                      <a href="javascript:Currdate(4,'$formname');" class="simple_link">Текущая дата</a></div>
                 <div>Заголовок записи в блоге: <span class="text-grey">[обязательное поле]</span></div>
                 <div><input type=text
                          name=header
                          value="$newsRecCols[7]"
                          placeholder="Текст заголовка новости"
                          required
                          class="news-in-grey"
                          style="width:290px;"></div>
                 <div>Краткое содержание записи: <span class="text-grey">[обязательное поле]</span></div>
                 <div><textarea name=comment
                             required
                             placeholder="Краткий комментарий к новости"
                             class="news-in-grey"
                             style="width:290px; height:77px;">$newsRecCols[8]</textarea></div>
                 <div>Ссылки: <font color="#A0A0A0">[ <a class=simplelink
                                       href="javascript:addLinkWizard('$formname', 'links')"
                                       >помощник</a>&nbsp;]</font></div>
                 <div><textarea name=links
                             class="news-in-grey"
                             placeholder="&lt;a href=&quot;http://goo.gl&gt;Google&lt;/a&gt;"
                             style="width:290px; height:77px;">$newsRecCols[10]</textarea></div>
                </div></td>
               <td width="600" valign="top" align="right">
                  <script type="text/javascript" src="js/easytabs-init.js"></script>
                  <div style="width:585px;">
                   <div id="horizontalTab" style="padding-left:0;">
                    <ul class="resp-tabs-list">
                     <li class=stxt>Twitter Card</li>
                     <li class=stxt>WYSIWYG-редактор записи</li>
                    </ul>
                    <div class="resp-tabs-container">
                        <div class=news-tabs-form>
                            <div>Twitter-аккоунт автора:</span></div>
                            <div><input   type="text" placeholder="\@twitteraccount"
                                          name="twitteraccount"
                                          value="$newsRecCols[11]"
                                          class="input"
                                          style="width:100%" ></div>
                            <div>Связать с фотогалереей:</div>
                            <div><input   type="text" placeholder="Введите название существующей галереи"
                                          name="gallerysearch"
                                          id="galsrch"
                                          value="$newsRecCols[12]"
                                          class="input"
                                          style="width:100%"></div>
                            <input   type="hidden" value="$gallery_id" name="hiddengalleryid">
                            <input   type="hidden"
                                     name="galleryid"
                                     id="galselectedid"
                                     value="$gallery_id">
                            <div>Путь до отображаемого на Twitter-карте изображения:</div>
                            <div><input   type="text" placeholder="Выберите изображение из галереи или введите путь"
                                          name="twitterimage"
                                          id="twtrimg"
                                          value="$twitterimage"
                                          class="input"
                                          style="width:100%"></div>
                            <div><input type="checkbox" id="c2" name="publishcard" value="1" $newsRecCols[15]/>
                               <label for="c2" class=cb-lbl-2><span></span>Опубликовать twitter-карту</label>
                            </div>
                            <div id="galCarousel" style="padding:0 0 0 25px; margin:0 0 0 5px; background:#F1F1F1; width:515px">$gallery_carousel</div>
                        </div>
                        <div class=stxt>
                        <textarea id="wysiwygnewseditor-edit" name="wysiwygnews">$newsRecCols[9]</textarea>
                        <script type="text/javascript">
                        CKEDITOR.replace( 'wysiwygnews', { width: '550', height: '274', removeButtons: 'Save,Underline,Subscript,Superscript', pheixScript: '$ENV{SCRIPT_NAME}', pheixSessionId: '$_[0]', pheixGalDivId: 'archive-divgal-postcontent-edit' });
                        </script>
                        </div>
                    </div>
                   </div>
                  </div>
                  </td>
           </tr>
           <tr><td colspan="2">
            <input type=submit
               value="Редактировать запись"
               class="news-btn-create"
               style="margin-left:0px; width:290px;"></td></tr>
        </table>
        </form>
        ~;
    } else {
        $editFrm = qq~
            <p class=error>Запись с заданным идентификатором ($newsid) не найдена в новостной ленте!</p>
        ~;
    }
    return $editFrm;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDate ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Вывод даты в заданном формате.
# Функция возвращает дату в формате DD Месяц-по-русски, YYYY.
#
#------------------------------------------------------------------------------

sub getDate {
    my $date = localtime;
    $date =~ s/[\.\:\,]//g;
    $date =~ s/[\s]+/ /g;
    my @date_array = split( / /, $date );
    @date_array = ( $date_array[2], $date_array[1], $date_array[4] );
    if ( $date_array[1] eq 'Jan' ) { $date_array[1] = 'Январь' }
    if ( $date_array[1] eq 'Feb' ) { $date_array[1] = 'Февраль' }
    if ( $date_array[1] eq 'Mar' ) { $date_array[1] = 'Март' }
    if ( $date_array[1] eq 'Apr' ) { $date_array[1] = 'Апрель' }
    if ( $date_array[1] eq 'May' ) { $date_array[1] = 'Май' }
    if ( $date_array[1] eq 'Jun' ) { $date_array[1] = 'Июнь' }
    if ( $date_array[1] eq 'Jul' ) { $date_array[1] = 'Июль' }
    if ( $date_array[1] eq 'Aug' ) { $date_array[1] = 'Август' }
    if ( $date_array[1] eq 'Sep' ) { $date_array[1] = 'Сентябрь' }
    if ( $date_array[1] eq 'Oct' ) { $date_array[1] = 'Октябрь' }
    if ( $date_array[1] eq 'Nov' ) { $date_array[1] = 'Ноябрь' }
    if ( $date_array[1] eq 'Dec' ) { $date_array[1] = 'Декабрь' }
    return "$date_array[0] $date_array[1], $date_array[2]";
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# createNewsRecord ( $news_sort, $news_id, $date, $seochpu, $seotitle,
#                    $seometadescr, $seometakeys, $header, $comment,
#                    $wysiwygnews, $links )
#------------------------------------------------------------------------------
#
# $news_sort - идентификатор для сортировки.
# $news_id - уникальный идентификатор записи.
# $date - дата создания записи.
# $seochpu - ЧПУ записи.
# $seotitle - текст для тега TITLE.
# $seometadescr - текст для тега META.description.
# $seometakeys - текст для тега META.keywords
# $header - заголовок новости.
# $comment - краткий комментарий.
# $wysiwygnews - HTML код из WYSIWYG редактора CKEditor.
# $links - HTML код дополнительных ссылок.
# Сохранение данных в БД.
# Функция не возвращает каких-либо значений.
#
#------------------------------------------------------------------------------

sub createNewsRecord {
    if ( -e $MODULE_DB_FILE ) {
        open( NEWSFHDL, "+<", $MODULE_DB_FILE );
    } else {
        open( NEWSFHDL, ">", $MODULE_DB_FILE );
    }

    my @indexes = <NEWSFHDL>;
    my @in_args = @_;
    for(my $i=0; $i <= $#in_args; $i++) {
        $in_args[$i] =~ s/\|/\&\#124;/gi;
    }

    my $record = join( "|", @in_args, 0 );
    $record =~ s/[\r\n\t]//gi;
    push( @indexes,  $record . "\n" );
    seek( NEWSFHDL, 0, 0 );
    flock( NEWSFHDL, 2 );
    print NEWSFHDL @indexes;
    truncate( NEWSFHDL, tell(NEWSFHDL) );
    flock( NEWSFHDL, 8 );
    close(NEWSFHDL);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getNewsHtmlTab ( $sesid, $curPg, $recN )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# $curPg - номер страницы вывода записей.
# $recN  - число записей на странице.
# $filter - тип поиска по записям.
# $f_req  - поисковый запрос.
# Выводы записей новостной ленты в виде таблицы.
# Функция возвращает таблицу с записями новостной ленты в HTML формате.
#
#------------------------------------------------------------------------------

sub getNewsHtmlTab {
    my $sesid  = $_[0];
    my $curPg  = $_[1];
    my $recN   = $_[2];
    my $filter = $_[3];
    my $f_req  = $_[4];
    my $of_req = $f_req;
    my $newsTb = '';
    my $proc_s = -1;

    if ( -e $MODULE_DB_FILE ) {
        open( TDATFILE, "<", $MODULE_DB_FILE );
        my @newsOnPage = sort { $b cmp $a } <TDATFILE>;
        close TDATFILE;

        if ( $filter eq 'exactsearch' ) {
            $f_req =~ s/(\.html)|(\.htm)$//gi;
            my @searchresults = ();
            for (
                my $inxonpage = 0 ;
                $inxonpage <= $#newsOnPage;
                $inxonpage++
              )
            {
                if ( $newsOnPage[$inxonpage] =~ /$f_req/ ) {
                    push( @searchresults, $newsOnPage[$inxonpage] );
                }
            }
            if ( $#searchresults == -1 ) {
                $newsTb .= qq~
                    <p class="stxt" style="font-size:12pt;" align="center">
                    По запросу <i>$of_req</i> ничего не найдено!</p>
                ~;
                $proc_s = 0;
                $of_req = '';

                #return $newsTb;
            }
            else { @newsOnPage = @searchresults; }
        }

        # Вывод раздела в список

        if ( $#newsOnPage  > -1 ) {
            $newsTb .= qq~
                <div align="right" style="margin-bottom:20px;">
                    <form   name="searchweblog" 
                            method=post 
                            action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=searchnews&amp;page=$curPg" 
                            style="margin:0">
                        <input type="text" name="search" id="search" placeholder="поиск по записям в ленте" value="$of_req" class="input-styled">&nbsp;
                        <input type="submit" value="Вперед!" class="installbutton" style="width:100px; height:31px;">
                    </form>
                </div>
                ~;

        # Формирование навигации по страницам

        my $page_linx = '';
        my $pages     = 0;

        if ( $proc_s != 3 ) {
            $pages = floor( $#newsOnPage / $recN );
            if ( ( $pages * $recN <= $#newsOnPage ) || ( $pages < 1 ) ) {
                $pages++;
            }
            if ( $curPg >= $pages ) { $curPg = 0; }
            $page_linx = '&nbsp;-&nbsp;';
            for ( my $i = 1 ; $i <= $pages ; $i++ ) {
                if ( ( $i - 1 ) != $curPg ) {
                    $page_linx .= "<a href=\"$ENV{SCRIPT_NAME}?id=$sesid&amp;action=admnews&amp;page="
                               . ( $i - 1 ) . "\" class=admpageslink>$i</a>&nbsp;-&nbsp;";
                    } else {
                        $page_linx .=
                            "<span class=lightadminpagelink>$i</span>&nbsp;-&nbsp;";
                    }
                }

                if ( $curPg >= $pages ) { $curPg = $pages - 1; }
                my $end = ( ( $curPg + 1 ) * $recN ) - 1;
                if ( $end > $#newsOnPage ) {
                    $end = $#newsOnPage;
                }
                @newsOnPage =
                  @newsOnPage[ $curPg * $recN .. $end ];

            }
            else {
                $page_linx = qq~
                    &nbsp;-&nbsp;<a href=\"$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admweblog&amp;page=$curPg\" 
                                    class=admpageslink>Назад</a>&nbsp;-&nbsp;~;
            }

            # Формирование навигации по страницам 

            my $newsCntr = 0;
            $newsTb .= qq~<table cellspacing="0" cellpadding="10" width="100%" class="tabselect">~;
            for (
                my $inxonpage = 0 ;
                $inxonpage <= $#newsOnPage ;
                $inxonpage++
              )
            {
                my $rand    = rand();
                my @tmp_arr = split( /\|/, $newsOnPage[$inxonpage] );
                my $bgcolor = 'class="tr01"';
                if ( $newsCntr % 2 == 0 ) { $bgcolor = 'class="tr02"'; }
                $tmp_arr[$#tmp_arr] =~ s/[\n]+//g;

                my $updownlinks = '';
                my $nextpage    = $curPg;
                my $prevpage    = $curPg;
                if ( $inxonpage == $#newsOnPage ) { $nextpage++; }
                if ( $inxonpage == 0 ) {
                    $prevpage--;
                    if ( $prevpage < 0 ) { $prevpage = 0; }
                }
                if ( $proc_s != 3 ) {
                    if ( $#newsOnPage > 0 ) {
                        my $sortID = $tmp_arr[0];
                        $sortID =~ s/[\D]+//s;
                        $updownlinks = qq~
                            <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admnewsmoveup&amp;sgid=$sortID&amp;page=$prevpage"
                               class="fa-action-link">
                            <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>
                            <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admnewsmovedown&amp;sgid=$sortID&amp;page=$nextpage"
                               class="fa-action-link">
                            <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>~;
                        if ( $inxonpage == 0 && $curPg == 0 ) {
                            $updownlinks = qq~
                                <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>
                                <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admnewsmovedown&amp;sgid=$sortID&amp;page=$curPg"
                                   class="fa-action-link">
                                <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>~;
                        }
                        if (   $inxonpage == $#newsOnPage
                            && $curPg == ( $pages - 1 ) )
                        {
                            $updownlinks = qq~
                            <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admnewsmoveup&amp;sgid=$sortID&amp;page=$curPg"
                               class="fa-action-link">
                            <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>
                            <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>~;
                        }
                    }
                    else {
                        if ( $curPg > 0 ) {
                            my $sortID = $tmp_arr[0];
                            $sortID =~ s/[\D]+//s;
                            $updownlinks = qq~
                                <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admnewsmoveup&amp;sgid=$sortID&amp;page=$prevpage"
                                   class="fa-action-link">
                                <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>
                                <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                                ~;
                        }
                        else {
                            $updownlinks = qq~
                                <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>
                                <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                                ~;
                        }
                    }
                }
                else {
                    $updownlinks = qq~
                        <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>
                        <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                    ~;
                }
                my @urls = ();
                my @sub_sr = split( /\,/, $tmp_arr[3] );
                for ( my $j = 0 ; $j <= $#sub_sr ; $j++ ) {
                    push( @urls,
                        qq~$sub_sr[$j]<span style="color:#C0C0C0">.html</span>~
                    );
                }
                my $urls = join( '<br>', @urls );
                my $jsdelmess = "Вы уверены, что хотите удалить запись в новостной ленте &quot;$tmp_arr[3]&quot; безвозвратно?";
                my $jsdelurl  = "$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=delnews&amp;razdelid=$tmp_arr[1]&amp;page=$curPg";
                $newsTb .= qq~
                  <tr $bgcolor style="height:20;">
                     <td width="10%" class="standart_text" align="center">$updownlinks</td>
                     <td width="75%" class="standart_text" style="font-size:11pt;"><b>$urls</b>
                       <div class=cmstext style="padding-top:10; font-size:10pt; color:#606060">$tmp_arr[7]</div>
                       <div class=cmstext style="font-size:10pt; color:#A0A0A0">$tmp_arr[8]</div>
                     </td>
                     <td width="15%" class="standart_text" align=right>
                     <a href="javascript:doSesChckLoad('$_[0]','$ENV{SCRIPT_NAME}','&amp;action=admnewsedit&amp;razdelid=$tmp_arr[1]&amp;page=$curPg')"
                        class="fa-action-link">
                       <i class="fa fa-pencil-square-o" title="Редактировать запись в новостной ленте &quot;$tmp_arr[3]&quot;"></i></a>
                     <a href="javascript:Confirm('$jsdelurl','$jsdelmess')" class="fa-action-link">
                       <i class="fa fa-trash" title="Удалить запись в новостной ленте &quot;$tmp_arr[3]&quot;"></i></a></td></tr>~;
                $newsCntr++;
            }
            $newsTb .=
              qq~</table><p align=right>$page_linx</p>~;
        }
        else {
            $newsTb .= qq~
                <p class=error>Список записей пуст - файл $MODULE_DB_FILE пуст!</p>~;
        }

    }    # if (-e $MODULE_DB_FILE)
    else {
        $newsTb .= qq~<p class=error>Список записей пуст - файл $MODULE_DB_FILE не найден!</p>~;
    }        
    return $newsTb;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# moveNewsUp ( $sortid )
#------------------------------------------------------------------------------
#
# $sortid - идентификатор сортировки новости в ленте.
# Функция передвижения записи в блоге вверх по списку.
# Функция выполняет передвижение записи в блоге вверх по списку.
#
#------------------------------------------------------------------------------

sub moveNewsUp {
    my $sortid = $_[0];
    if ( -e $MODULE_DB_FILE  ) {
        open( TIFH, "+<", $MODULE_DB_FILE );
        my @recs = sort { $b cmp $a } <TIFH>;

        for ( my $i = 0 ; $i <= $#recs ; $i++ ) {
            my @tmp = split( /\|/, $recs[$i] );
            if ( $tmp[0] == $sortid ) {
                my @tmp2 = split( /\|/, $recs[ $i - 1 ] );
                my $buf = $tmp2[0];
                $tmp2[0] = $tmp[0];
                $tmp[0]  = $buf;
                $recs[ $i - 1 ] = join( "|", @tmp2 );
                $recs[$i] = join( "|", @tmp );
                last;
            }
        }
        seek( TIFH, 0, 0 );
        flock( TIFH, 2 );
        print TIFH @recs;
        truncate( TIFH, tell(TIFH) );
        flock( TIFH, 8 );
        close(TIFH);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# moveNewsDown ( $sortid )
#------------------------------------------------------------------------------
#
# $sortid - идентификатор сортировки новости в ленте.
# Функция передвижения записи в блоге вниз по списку.
# Функция выполняет передвижение записи в блоге вниз по списку.
#
#------------------------------------------------------------------------------

sub moveNewsDown {
    my $sortid = $_[0];
    if ( -e $MODULE_DB_FILE  ) {
        open( TIFH, "+<", $MODULE_DB_FILE );
        my @recs = sort { $b cmp $a } <TIFH>;

        for ( my $i = 0 ; $i <= $#recs ; $i++ ) {
            my @tmp = split( /\|/, $recs[$i] );
            if ( $tmp[0] == $sortid ) {
                my @tmp2 = split( /\|/, $recs[ $i + 1 ] );
                my $buf = $tmp2[0];
                $tmp2[0] = $tmp[0];
                $tmp[0]  = $buf;
                $recs[ $i + 1 ] = join( "|", @tmp2 );
                $recs[$i] = join( "|", @tmp );
                last;
            }
        }
        seek( TIFH, 0, 0 );
        flock( TIFH, 2 );
        print TIFH @recs;
        truncate( TIFH, tell(TIFH) );
        flock( TIFH, 8 );
        close(TIFH);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# delNews ( $newsid )
#------------------------------------------------------------------------------
#
# $newsid - идентификатор новости в ленте.
# Функция удаления записи из новостной ленты.
# Функция выполняет удаление записи из новостной ленты.
#
#------------------------------------------------------------------------------

sub delNews {
    my $newsid = $_[0];
    my $newsCnt = 0;
    open( TDDAATFH, "+<", $MODULE_DB_FILE );
    my @recs = <TDDAATFH>;
    for ( my $kinx = 0 ; $kinx <= $#recs ; $kinx++ ) {
        my $check = $recs[$kinx];
        $check =~ s/[\n]+//g;
        if ( $check ne '' ) { $newsCnt++; }
    }
    if ( $newsCnt > 1 ) {
        for ( my $i = 0 ; $i <= $#recs ; $i++ ) {
            my @temp_arr = split( /\|/, $recs[$i] );
            if ( $newsid eq $temp_arr[1] ) {
                my @ostatok   = @recs[ $i + 1 .. $#recs ];
                my @pre_array = @recs[ 0 .. $i - 1 ];
                @recs   = ( @pre_array, @ostatok );
                last;
            }
        }
        seek( TDDAATFH, 0, 0 );
        flock( TDDAATFH, 2 );
        print TDDAATFH @recs;
        truncate( TDDAATFH, tell(TDDAATFH) );
        flock( TDDAATFH, 8 );
        close(TDDAATFH);
    }
    # $newsCnt > 1
    else {
        close(TDDAATFH);
        if ( ( -e $MODULE_DB_FILE ) ) {
            unlink($MODULE_DB_FILE);
        }
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# chckNewsPage ( $curPg )
#------------------------------------------------------------------------------
#
# $curPg - номер страницы для вывода записей из новостной ленты.
# Проверка существования страницы для вывода записей из новостной ленты.
# Функция выполняет проверку существования страницы для вывода записей из 
# новостной ленты.
#
#------------------------------------------------------------------------------

sub chckNewsPage {
    my $recN    = $MODULE_RECS_ONPAGE;
    my $retcode = 0;
    my $curPg   = $_[0];
    if ( -e $MODULE_DB_FILE ) {
        open( TDATFILE, "<", $MODULE_DB_FILE );
        my @newsOnPage = <TDATFILE>;
        close TDATFILE;
        my $pages = floor( $#newsOnPage / $recN );
        if ( ( $pages * $recN <= $#newsOnPage ) || ( $pages < 1 ) ) {
            $pages++;
        }
        if ($curPg < $pages) {
            $retcode = 1;
        }
    }
    return $retcode;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getNewsRecCols ( $newsid )
#------------------------------------------------------------------------------
#
# $newsid - идентификатор новости в ленте.
# Функция получения полей записи из новостной ленты в массив.
# Функция выполняет получение полей записи из новостной ленты в массив.
#
#------------------------------------------------------------------------------

sub getNewsRecCols {
    my $newsid   = $_[0];
    my @details  = ();
    if ( -e $MODULE_DB_FILE ) {
        open( TDATFILE, "<", $MODULE_DB_FILE );
        my @recs = <TDATFILE>;
        close TDATFILE;
        for ( my $i = 0 ; $i <= $#recs ; $i++ ) {
            $recs[$i] =~ s/[\r\n\t]+//gi;
            my @temp_arr = split( /\|/, $recs[$i] );
            if ( $newsid eq $temp_arr[1] ) {
                @details = @temp_arr;
                last;
            }
        }
    }
    return @details;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# saveNews ( $newsid, @news_db_cols )
#------------------------------------------------------------------------------
#
# $newsid - идентификатор новости в ленте.
# @news_db_cols - массив полей для сохранения в базе данных.
# Функция сохранения полей записи новостной ленты в БД.
# Функция выполняет сохранение полей записи новостной ленты в БД.
#
#------------------------------------------------------------------------------

sub saveNews {
    my $newsid = $_[0];
    my @args = @_;
    open( TIFH, "+<", $MODULE_DB_FILE );
    my @indexes = <TIFH>;
    for (my $i = 0; $i <= $#args; $i++) {
        $args[$i] =~ s/[\r\n\t]//gi;
    }

    for ( my $i = 0 ; $i <= $#indexes ; $i++ ) {
        my @temp_arr = split( /\|/, $indexes[$i] );
        if ( $newsid eq $temp_arr[1] ) {
            my @new_values = @args;
            $new_values[0] = $temp_arr[0];
            $indexes[$i] = join( "|", @new_values, 0 ) . "\n";
            last;
        }
    }

    seek( TIFH, 0, 0 );
    flock( TIFH, 2 );
    print TIFH @indexes;
    truncate( TIFH, tell(TIFH) );
    flock( TIFH, 8 );
    close(TIFH);
}

#------------------------------------------------------------------------------

END { }

1;
