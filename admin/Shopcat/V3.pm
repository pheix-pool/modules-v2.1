package Shopcat::V3;

use strict;
use warnings;

BEGIN {
    # for all servers
    use lib '../';
    use lib '../../extlibs/';
    # lib path for systax check during module install
    use lib 'admin/libs/modules/';
    use lib 'admin/libs/extlibs/';
    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';
}
##############################################################################
#
# File   :  V3.pm
#
##############################################################################
#
# Главный пакет управления каталогом ShopCat::V3
#
##############################################################################

use Encode;
use POSIX;
use Digest::MD5 qw(md5);
use File::stat;
use File::Copy;
use File::Path;
use Shopcat::ParsePrice;
use Config::ModuleMngr qw(getSingleSetting getModXml getConfigValue getSettingFromGroupByAttr getSettingChildNum getProtoSrvName);
use Fcntl qw(:DEFAULT :flock :seek);
use Shopcat::ShopParams;
use Shopcat::4PUrl;

my $MODULE_NAME         = "Shopcat";
my $MODULE_FOLDER       = "admin/libs/modules/$MODULE_NAME/";
my $MODULE_DB_FILE      = "conf/system/shopcat2.tnk";
my $MODULE_SEO_DB_FILE  = "conf/system/shopcat2.seo.tnk";
my $ADMIN_SKIN_PATH     = 'admin/skins/classic_.txt';
my $MODULE_PRICEPOOL    = "conf/system/pricepool/";       # WARNING!!! / is needed at the end
my $MODULE_DATAPOOL     = "conf/pages/shopcat2";          # WARNING!!! / is NOT needed at the end
my $MODULE_LEFT_NAV     = "conf/pages/right-nav.txt";
my $MODULE_EXPAND_VALUE = getSingleSetting($MODULE_NAME, 0, 'tabexpandlimit') || 5;
my $PRICEFILE_DB_COLUMS = 5;

#------------------------------------------------------------------------------

my $GLOBAL_CURRENCY = Encode::encode("utf8", getSingleSetting( $MODULE_NAME, 1,
            'currency' ));

#------------------------------------------------------------------------------
# getV3UserareaStatus ( )
#------------------------------------------------------------------------------
#
# Получить статус пользовательской части (включена или выключена).
#
#------------------------------------------------------------------------------

sub getV3UserareaStatus {
    my $status = Config::ModuleMngr::getSingleSetting( $MODULE_NAME, 0, 'userarea' );
    return $status =~ /^[01]?$/ ? $status : 0;
}

#------------------------------------------------------------------------------
# getV3PowerStatus ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить статус модуля (включен или выключен).
# Функция возвращает статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub getV3PowerStatus {
    my $INSTALL_FILE_PATH = "conf/system/install.tnk";
    if ( -e $INSTALL_FILE_PATH ) {
        open my $fh, "<", $INSTALL_FILE_PATH ;
        my @rows = <$fh>;
        close $fh;
        foreach my $row (@rows) {
            my @cols = split( /\|/, $row );
            if ( $cols[2] eq $MODULE_NAME ) { return $cols[4]; }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------
# getShopcatJsCss ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Вывод CSS/JS кода модуля.
#
#------------------------------------------------------------------------------

sub getShopcatJsCss {
    my $cntnt_ = qq~
    <!-- TABS -->
    <link   type="text/css" rel="stylesheet"
            href="css/easytabs/easy-responsive-tabs.css">
    <script src="js/easytabs/easyResponsiveTabs.js"
            type="text/javascript"></script>
    ~;
    return $cntnt_;
}

#------------------------------------------------------------------------------
# showEditCatPage ( $sesion_id,  $crypted_userid, $shopid, $catid, $prinflag )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_userid - зашифрованный идентификатор пользователя.
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $prinflag - флаг управления печатью.
# Вывод страницы редактирования дополнительных описаний категории.
# Функция выводит страницу редактирования дополнительных описаний категории.
#
#------------------------------------------------------------------------------

sub showEditCatPage {
    open(my $inputfh,"<", $ADMIN_SKIN_PATH);
    my @scheme = <$inputfh>;
    close($inputfh);
    my $cnt = 0;
    my $sesion_id = $_[0];
    while($cnt<=$#scheme) {
        $scheme[$cnt] = Pheix::Tools::fillCommonTags($sesion_id,$scheme[$cnt]);
        if ($scheme[$cnt] =~ /\%content\%/) {
            my $itemstab = &getEditCatPage(@_);
            $scheme[$cnt] =~ s/\%content\%/$itemstab/;
        }
        if ( $scheme[$cnt] =~ /\%scripts_for_this_page\%/ ) {
            $scheme[$cnt] =~ s/(\<\!\-\-)|(\-\-\>)//gi;
            my $scrt = getShopcatJsCss();
            $scheme[$cnt] =~ s/\%scripts_for_this_page\%/$scrt/;
        }
    print $scheme[$cnt];
    $cnt++;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_files_array ( )
#------------------------------------------------------------------------------
#
# Получение списка файлов некоторого каталога.
#
#------------------------------------------------------------------------------

sub get_files_array {
    my $dir = $_[0];
    $dir =~ s/\\/\//g;
    opendir(DIR,$dir);
    my @files = grep {(!/^\.+$/) && !(-d "$dir/$_")} readdir DIR;
    closedir(DIR);
    return @files;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_shopcat2_startpage ( )
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода главной страницы каталога.
#
#------------------------------------------------------------------------------

sub show_shopcat2_startpage {
    open(my $inputfh,"<", $ADMIN_SKIN_PATH);
    my @scheme = <$inputfh>;
    close($inputfh);
    my $cnt = 0;
    my $sesion_id = $_[0];
    while($cnt<=$#scheme) {
        $scheme[$cnt] = Pheix::Tools::fillCommonTags($sesion_id,$scheme[$cnt]);
        if ($scheme[$cnt] =~ /\%content\%/) {
            my $login = &_inner_shopcat2_startpage(@_);
            $scheme[$cnt] =~ s/\%content\%/$login/;
        }
        if ( $scheme[$cnt] =~ /\%scripts_for_this_page\%/ ) {
            $scheme[$cnt] =~ s/(\<\!\-\-)|(\-\-\>)//gi;
            my $scrt = getShopcatJsCss();
            $scheme[$cnt] =~ s/\%scripts_for_this_page\%/$scrt/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showV3ItemsList ( )
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода списка позиций для категории.
#
#------------------------------------------------------------------------------

sub showV3ItemsList {
    my ($sesion_id) = @_;
    my $cnt = 0;

    open my $inputfh,"<", $ADMIN_SKIN_PATH or die "unable to open $ADMIN_SKIN_PATH: $!";
    my @scheme = <$inputfh>;
    close $inputfh or die "unable to close $ADMIN_SKIN_PATH";

    while($cnt<=$#scheme) {
        $scheme[$cnt] = Pheix::Tools::fillCommonTags($sesion_id,$scheme[$cnt]);
        if ($scheme[$cnt] =~ /\%content\%/) {
            my $itemstab = getV3ItemsPage(@_);
            $scheme[$cnt] =~ s/\%content\%/$itemstab/;
        }
        if ( $scheme[$cnt] =~ /\%scripts_for_this_page\%/ ) {
            $scheme[$cnt] =~ s/(\<\!\-\-)|(\-\-\>)//gi;
            my $scrt = getShopcatJsCss();
            $scheme[$cnt] =~ s/\%scripts_for_this_page\%/$scrt/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_shopcat2_edititem_page ( )
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода страницы редактирования позиции.
#
#------------------------------------------------------------------------------

sub show_shopcat2_edititem_page {
    open(my $inputfh,"<", $ADMIN_SKIN_PATH);
    my @scheme = <$inputfh>;
    close($inputfh);
    my $cnt = 0;
    my $sesion_id = $_[0];
    while($cnt<=$#scheme) {
        $scheme[$cnt] = Pheix::Tools::fillCommonTags($sesion_id,$scheme[$cnt]);
        if ($scheme[$cnt] =~ /\%content\%/) {
            my $itemstab = &_inner_shopcat2_edititem_page(@_);
            $scheme[$cnt] =~ s/\%content\%/$itemstab/;
        }
        if ( $scheme[$cnt] =~ /\%scripts_for_this_page\%/ ) {
            $scheme[$cnt] =~ s/(\<\!\-\-)|(\-\-\>)//gi;
            my $scrt = getShopcatJsCss();
            $scheme[$cnt] =~ s/\%scripts_for_this_page\%/$scrt/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_shopcat2_editcategory_page ( )
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода страницы редактирования описаний категории.
#
#------------------------------------------------------------------------------

sub show_shopcat2_editcategory_page {
    open(my $inputfh,"<", $ADMIN_SKIN_PATH);
    my @scheme = <$inputfh>;
    close($inputfh);
    my $cnt = 0;
    my $sesion_id = $_[0];
    while($cnt<=$#scheme) {
        $scheme[$cnt] = Pheix::Tools::fillCommonTags($sesion_id,$scheme[$cnt]);
        if ($scheme[$cnt] =~ /\%content\%/) {
            my $itemstab = &_inner_shopcat2_editcategory_page(@_);
            $scheme[$cnt] =~ s/\%content\%/$itemstab/;
        }
        if ( $scheme[$cnt] =~ /\%scripts_for_this_page\%/ ) {
            $scheme[$cnt] =~ s/(\<\!\-\-)|(\-\-\>)//gi;
            my $scrt = getShopcatJsCss();
            $scheme[$cnt] =~ s/\%scripts_for_this_page\%/$scrt/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_startpage ( )
#------------------------------------------------------------------------------
#
# Нативная функция вывода главной страницы.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_startpage {
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue($MODULE_FOLDER,$xml_description_file,"description");
    my $shopcat2_content_variable = qq~<div class=admin_header>$module_descr</div>~.showV3UpdateHint($_[4],$_[3],$_[2]);
    $shopcat2_content_variable .= &_inner_shopcat2_download_catfile_block($_[0]);
    $shopcat2_content_variable .= &getCatTreeTab($_[0],$_[5]);
    return $shopcat2_content_variable;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showV3UpdateHint ()
#------------------------------------------------------------------------------
#
# Напечатать hint jgrowl.
#
#------------------------------------------------------------------------------

sub showV3UpdateHint {
    my $hint      = '';
    my $printflag = $_[0];
    my $procstat  = $_[1];
    my $procmess  = $_[2];
    $procmess =~ s/[\r\n]//g;
    $procmess =~ s/[\s]+/ /g;
    my $ipdatetime = '';
    my $jgrowlhint = qq~
        <script type="text/javascript">
            (function(\$){
                jQuery(document).ready(function(){
                    jQuery.jGrowl.defaults.closer = false;
                    jQuery('\#updatehint').jGrowl("<div class=update_hint>\\
                    %hint_content%
                    </div>",
                        {
                            life: 10000,
                            theme: 'updatehint',
                            speed: 'slow',
                            animateOpen: {
                                height: "show",
                            },
                        animateClose: {
                            height: "hide",
                        }
                    });
                });
            })(jQuery);
        </script>
        <div id="updatehint" class="top-right" style="display:inline; margin-top:15px;"></div>
        ~;

    if (localtime =~ /([\d]+:[\d]+:[\d]+)/) {
        $ipdatetime = "$1";
    }
    my $message = qq~<p class=hinthead>$ipdatetime - Операция выполнена успешно!</p>~;
    if ($printflag == 1) {
        if ($procstat != 1) {
            # Операция завершилась ошибкой
            $message = qq~<p class=hinthead>$ipdatetime - Операция завершилась ошибкой</p>~;
        }
        $hint = "$message\\\n$procmess\\";
    } else {
        #$hint = $message;
        $jgrowlhint = '';
    }
    $jgrowlhint =~ s/%hint_content%/$hint/g;
    return $jgrowlhint;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getV3ItemsPage ()
#------------------------------------------------------------------------------
#
# Нативная функция вывода страницы со списком позиций.
#
#------------------------------------------------------------------------------

sub getV3ItemsPage {
    my ($sid, $crlgn, $shopid, $catid) = @_;
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue($MODULE_FOLDER,$xml_description_file,"description");
    my $shopcat2_content_variable = qq~<div class=admin_header>$module_descr</div>~.showV3UpdateHint($_[-1],$_[-2],$_[-3]);

    #my ($categorynavi, $table) = &_inner_shopcat2_cat_browseitems_table(@_);
    my ($categorynavi, $table) = getV3ItemsTable(@_);
    my $razdel;
    my @xlebnye_kroxi = split /\»/, $categorynavi;
    if ($xlebnye_kroxi[-1] ne '' && $xlebnye_kroxi[-1] !~ /^[\r\n\t\»]+$/) {
        $razdel = $xlebnye_kroxi[-1];
        $razdel =~ s/(&nbsp;)|(»)//gi;
    }

    $shopcat2_content_variable .= qq~
        <div style="margin: 0px 0px 2px 0px; width:100%" class="container">
            <a href="javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}', '&amp;action=asc2additemform&amp;shopid=$shopid&amp;catid=$catid')">Добавить новую позицию в категорию «$razdel»</a>
        </div>
        <p class="standart_text">
            <a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admshopcat2"
               class=simple_link><b>Управление каталогом</b></a> / $categorynavi</p>
        $table
     ~;
    return $shopcat2_content_variable;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_edititem_page ()
#------------------------------------------------------------------------------
#
# Нативная функция вывода страницы редактирования позиции.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_edititem_page {
    my $categorynavilink;
    my $table;
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue($MODULE_FOLDER,$xml_description_file,"description");
    my $shopcat2_content_variable = qq~<div class=admin_header>$module_descr</div>~.showV3UpdateHint($_[8],$_[7],$_[6]);
    ($categorynavilink, $table) = &_inner_shopcat2_cat_itemdetails(@_);
    $shopcat2_content_variable .= qq~
        <p class=standart_text>
            <a  href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admshopcat2"
                class=simple_link><b>Управление каталогом</b></a> / $categorynavilink</p>
        $table
    ~;
    return $shopcat2_content_variable;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_editcategory_page ()
#------------------------------------------------------------------------------
#
# Нативная функция вывода страницы редактирования описаний категории.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_editcategory_page {
    my $categorynavilink;
    my $table;
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue($MODULE_FOLDER,$xml_description_file,"description");
    my $shopcat2_content_variable = qq~<div class=admin_header>$module_descr</div>~.showV3UpdateHint($_[6],$_[5],$_[4]);

    ($categorynavilink,$table) = &_inner_shopcat2_categorydetails(@_);
    $shopcat2_content_variable .= qq~
        <p class=standart_text>
            <a  href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admshopcat2"
                class=simple_link><b>Управление каталогом</b></a> / $categorynavilink</p>
        $table
    ~;
    return $shopcat2_content_variable;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_download_catfile_block ()
#------------------------------------------------------------------------------
#
# Спойлер загрузки файла каталога.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_download_catfile_block {
    my $add_catfile_form = &_inner_shopcat2_download_catfile_form($_[0]);
    my $download_catfile_block = qq~
        <div class="spoiler-wrapper" style="width:98%">
            <div class="spoiler folded"><a href="javascript:void(0);" class="spoilerlink">Загрузить каталог на сервер</a></div>
            <div class="spoiler-text">$add_catfile_form</div>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function(){
            jQuery('.spoiler-text').hide()
            jQuery('.spoiler').click(function(){
            jQuery(this).toggleClass("folded").toggleClass("unfolded").next().slideToggle()
        })
    })
    </script>
    ~;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_download_catfile_form ()
#------------------------------------------------------------------------------
#
# Форма загрузки файла каталога на сервер.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_download_catfile_form() {
    my ($sid) = @_;
    my $_cntnt = qq~
    <table cellspacing="2" cellpadding="0" width="100%">
      <tr><td align="right" valign="bottom"></td></tr>
      <tr><td valign="bottom">
      <form method="post"
            action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2downloadcat"
            enctype="multipart/form-data">
        <table
               cellspacing="2"
               cellpadding="9"
               width="100%"
               class="messagelist"
               style="background-image:url('images/social_bg.png');height:94">
            <tr>
               <td
                   width="100%"
                   class="s_t"
                   style="font-family:Arial,Tahoma;color:#505050;font-size:12pt;">
                   <div class="inst_mod_head _phx-inblck">
                           Загрузить&nbsp;новый&nbsp;файл&nbsp;[</div>
                   <div class="_phx-inblck _phx-posr _phx-t3">
                       <div class="pheix-upload-icon">
                          <i class="fa fa-file-text-o"
                              aria-hidden="true" title=".txt"></i>
                       </div>
                   </div>
                   <div class="inst_mod_head _phx-inblck">]&nbsp;:</div>
               </td></tr>
               <tr><td width="100%">
               <div class="f_imulation_wrap">
                   <div class="im_input">
                       <input type="text"value="Выберите файл"/>
                   </div>
                   <input
                       type="file"
                       required
                       size=30
                       name="uploadcatalog"
                       id="imulated"
                       style="margin-left:-124px;margin-top:7px;height:20px"/>
               </div>
               <script type="text/javascript">
                   jQuery('.im_input input').click(function(){
                       jQuery('.im_input input').css('color','#C0C0C0');
                       jQuery('.im_input input').val('Выберите файл');
                       jQuery("#imulated").val('');
                       jQuery('#imulated').trigger('click');
                   });
                   jQuery('#imulated').change(function(){
                       jQuery('.im_input input').css('color','#505050');
                       jQuery('.im_input input').val(jQuery(this).val());
                   });
                   function UploadCatSubmit() {
                     confirmflag = 0;
                     message =
                         "Текущий каталог будет <span class=error>"+
                         "уничтожен</span> (замещен новым)! <b>Вы "+
                         "хотите продолжить?</b>";
                     jQuery.confirm({
                 	    title: 'Подтверждение',
                 	    content: message,
                 			width :'auto',
                 			useBootstrap: false,
                 	    buttons: {
                 	        confirm: function () {
                 	            top.document.forms[0].submit();;
                 	        },
                 	        cancel: function () {
                 	            ;
                 	        }
                 	    }
                 	}).css("font-family", "'PT Sans', sans-serif");
                   }
               </script>
               </td></tr>
               <tr><td width="100%">
                   <input type="button"
                          onclick="UploadCatSubmit()"
                          value="Загрузить файл"
                          class="installbutton">
               </td></tr>
           </table>
      </form></td></tr>
    </table>
    ~;
    return $_cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_catfile_stats ()
#------------------------------------------------------------------------------
#
# Вывод статистики по каталогу.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_catfile_stats {
    my $catfilestatus = qq~<span class="redtext">не найден</span>~;
    my $catfiledate = "неизвестно";
    my $itemsincat = 0;
    my $catsincat = 0;
    my $levelsincat = 0;
    if (-e $MODULE_DB_FILE) {
        my @uniquecats;
        $catfiledate = stat($MODULE_DB_FILE);
        $catfiledate = &_inner_shopcat2_date_format($catfiledate->mtime);
        $catfilestatus = qq~<span class="bluetext">ok</span>~;
        open(my $inputfh,"<", $MODULE_DB_FILE);
        my @maincat = <$inputfh>;
        close($inputfh);
        if ($#maincat > -1) {$itemsincat = $#maincat+1;}
        for(my $i=0; $i<=$#maincat; $i++) {
            my @arr=split(/\|/,$maincat[$i]);
            my $leveldepth = ($#arr+1)-3;
            if ($leveldepth > $levelsincat) {$levelsincat=$leveldepth;}
            for(my $j=0; $j<=$#arr-1; $j++) {
                if ($arr[$j] =~ /\[([\d]+)\]/) {
                    my $currid = $1;
                    my $unique = 0;
                    for(my $k=0; $k<=$#uniquecats; $k++) {
                        if ($uniquecats[$k] == $currid) {
                            $unique = 1;
                        }
                    }
                    if ($unique == 0) {push(@uniquecats,$currid);}
                }
            }
        }
        if ($#uniquecats > -1) {$catsincat = $#uniquecats+1;}
    }
    my $catalog_stats = qq~
      <p class="cmstext" style="font-size:12pt; color:#606060">Информация о текущем каталоге:</p>
      <div class="cmstext" style="margin:0 0 0 40px;">
        <div><span class=greytext>Файл БД каталога: </span>$MODULE_DB_FILE - $catfilestatus</div>
        <div><span class=greytext>Дата загрузки: </span> $catfiledate</div>
        <div><span class=greytext>Всего позиций в каталоге: </span>$itemsincat</div>
        <div><span class=greytext>Всего категорий в каталоге: </span> $catsincat</div>
        <div><span class=greytext>Максимальная глубина вложенности уровней категорий: </span> $levelsincat</div>
      </div>
    ~;
    return $catalog_stats;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_date_format ()
#------------------------------------------------------------------------------
#
# Форматированная дата.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_date_format {
    my $date = localtime;
    $date =~ s/[\.\:\,]//g;
    $date =~ s/[\s]+/ /g;
    my @date_array = split (/ /,$date);
    @date_array = ($date_array[2],$date_array[1],$date_array[4]);
    if ($date_array[1] eq 'Jan') {$date_array[1] = 'Январь'}
    if ($date_array[1] eq 'Feb') {$date_array[1] = 'Февраль'}
    if ($date_array[1] eq 'Mar') {$date_array[1] = 'Март'}
    if ($date_array[1] eq 'Apr') {$date_array[1] = 'Апрель'}
    if ($date_array[1] eq 'May') {$date_array[1] = 'Май'}
    if ($date_array[1] eq 'Jun') {$date_array[1] = 'Июнь'}
    if ($date_array[1] eq 'Jul') {$date_array[1] = 'Июль'}
    if ($date_array[1] eq 'Aug') {$date_array[1] = 'Август'}
    if ($date_array[1] eq 'Sep') {$date_array[1] = 'Сентябрь'}
    if ($date_array[1] eq 'Oct') {$date_array[1] = 'Октябрь'}
    if ($date_array[1] eq 'Nov') {$date_array[1] = 'Ноябрь'}
    if ($date_array[1] eq 'Dec') {$date_array[1] = 'Декабрь'}
    return "$date_array[0] $date_array[1], $date_array[2]";
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_browse_get_description_for_n_minus_1 ()
#------------------------------------------------------------------------------
#
# Получение из ДБ поля description для n-1 уровня.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_browse_get_description_for_n_minus_1 {
    my ($shopid, $cat_id, $model_id) = @_;
    my $description;
    my $filename = "$MODULE_DATAPOOL/$shopid/levels/n-1/$cat_id/descriptions.txt";
    my $debug_info = "\n<!--Descriptions from file: $filename-->\n";
    if (-e $filename) {
        open my $fh,"<", $filename;
        my @data = <$fh>;
        close $fh;
        my $found_flag = 0;
        for my $i (0..$#data) {
            my $record = $data[$i];
            my @data_columns = split(/[\;\|\t]/,$record);
            $data_columns[0] =~ s/[\D]//g;
            if ($data_columns[0] == $model_id) {
                $description = $data_columns[1];
                $description =~ s/[\r\n]+//g;
                $found_flag = 1;
            }
        }
        if (!$found_flag) {
            $description = "No description record in $filename, record should be in single string:<br>$model_id|description for $model_id";
        }
    } else {
        $description = "Descriptions file not found: $filename"
    }
    return $description;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_get_navi_n_header_for_id ()
#------------------------------------------------------------------------------
#
# Получение из строки навигации по категориям каталога для уровня n.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_get_navi_n_header_for_id {
    my ($cat_id) = @_;
    my $navi_block = -1;
    my @navi_links = ();
    my $header = "Каталог";
    if (-e $MODULE_DB_FILE) {
        open my $fh,"<", $MODULE_DB_FILE;
        my @maincat= <$fh>;
        close $fh;
        for my $i (0..$#maincat) {
            if ($maincat[$i]  =~ /(\[$cat_id\])/) {
                my $record = $maincat[$i];
                $record =~ s/[\r\n]//g;
                my @db_columns = split(/\|/,$record);
                for my $j (0..$#db_columns) {
                    if ($db_columns[$j] =~ /\[([\d]+)\]/) {
                        my $col_id = $1;
                        if ($col_id == $cat_id) {
                            $header = $db_columns[$j];
                            $header =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                            last;
                        } else {
                            my $column_record = $db_columns[$j];
                            $column_record =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                            push(@navi_links,"<a href=\"shopcatalog_$col_id.html\" class=\"shopcat2link-navi\">$column_record</a>");
                        }
                    }
                }
                last;
            }
        }
    }
    if (@navi_links) {
        $navi_block = qq~<a href="/" class="shopcat2link-navi">Главная</a> » <a href="shopcatalog.html" class="shopcat2link-navi">Каталог</a> » ~.join(" » ",@navi_links);
    } else {
        $navi_block = qq~<a href="/" class="shopcat2link-navi">Главная</a> » <a href="shopcatalog.html" class="shopcat2link-navi">Каталог</a>~
    }
    return ($header,$navi_block);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_upload_catalog_to_server ()
#------------------------------------------------------------------------------
#
# Загрузка каталога на сервер.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_upload_catalog_to_server {
    my ($file_to_upload) = @_;
    my $retcontent;
    my $retcode = 0;
    my $path_to_upload = 'conf/system';
    my $tmp_file_name  = 'raw_shopcat.txt';
    my $sets           = getSingleSetting( $MODULE_NAME, 0, 'usefilters' ) || 0;
    if ($file_to_upload ne '') {
        if ($file_to_upload =~ /(\.txt)|(\.csv)$/) {
            Pheix::Tools::doFileUpload($path_to_upload,"uploadcatalog",$tmp_file_name,0,0);
            unlink $MODULE_LEFT_NAV;
            ($retcontent, $retcode) = Shopcat::ParsePrice::doFileParse("$path_to_upload/$tmp_file_name", $sets);
            if ($retcode == 1) {
                unlink "$path_to_upload/$tmp_file_name";
            }
            return ($retcontent,$retcode);
        } else {
            return (qq~<p class=hintcont>Раcширение файла не соответсвует текстовому (.txt)!</p>~,0);
        }
    } else {
        return (qq~<p class=hintcont>Файл не найден (пустое имя)!</p>~,0);
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_delete_cat_from_catalog ()
#------------------------------------------------------------------------------
#
# Удаляем из каталога все записи, содержащие заданную категорию.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_delete_cat_from_catalog {
    my ($pricefile, $cattodelid) = @_;
    my $DISP_IN_RECORD  = 3;
    my $retcode         = 0;
    my $rejectedrecords = 0;
    my $retcontent;
    if ( -e $pricefile ) {
        my @newcat;
        open my $fh, "<", $pricefile;
        my @data = <$fh>;
        close $fh;
        for ( my $i = 0 ; $i <= $#data ; $i++ ) {
            my @records = split( /\|/, $data[$i] );
            my $foundid = 0;
            for ( my $j = $DISP_IN_RECORD ; $j <= ( $#records - 1 ) ; $j++ ) {
                if ( $records[$j] =~ /\[$cattodelid\]/ ) {
                    $foundid = 1;
                }
            }
            if ( $foundid == 0 ) {
                push( @newcat, $data[$i] );
            }
            else {
                $rejectedrecords++;
            }
        }
        if ( $#newcat > -1 && $#newcat < $#data ) {
            open my $fh, ">", $pricefile;
            flock $fh, LOCK_EX;
            print $fh @newcat;
            flock $fh, LOCK_UN;
            close $fh;
            $retcode    = 1;
            $retcontent = qq~
                <p class=hintcont>Категория <font class=hintspan>$cattodelid</font> успешно удалена<br>
                Всего удалено позиций: <font class=hintspan>%rejectedrecords%</font></p>
            ~;
        }
        else {
            $retcontent = 'вошли в else';
            if ( $#newcat == $#data ) {
                $retcontent = qq~<p class=hintcont>Категория <font class=hintspan>$cattodelid</font> не найдена в конечном прайс-листе!</p>~;
            }
            else {
                if ( $#newcat == -1 ) {
                    if ( -e $pricefile ) {
                        unlink($pricefile);
                        $rejectedrecords = $#data + 1;
                        $retcode         = 1;
                        $retcontent      = qq~
                            <p class=hintcont>Категория <font class=hintspan>$cattodelid</font> успешно удалена!<br>
                            Всего удалено позиций: <font class=hintspan>%rejectedrecords%</font></p></p>
                        ~;
                    }
                    else {
                        $retcontent = qq~
                            <p class=hintcont>Невозможно удалить категорию в конечном прайс-листе!<br>
                            Файл <font class=hintspan>$pricefile</font> не найден!</p>
                        ~;
                    }
                }
                else {
                    $retcontent = qq~
                        <p class=hintcont>
                        Файл: <font class=hintspan>$pricefile</font><br>
                        Размер исходного массива (\@data): <font class=hintspan>$#data</font><br>
                        Размер массива после обработки (\@newcat): <font class=hintspan>$#newcat</font></p>
                    ~;
                }
            }
        }
    }
    else {
        $retcontent = qq~
            <p class=hintcont>Невозможно удалить категорию <font class=hintspan>$cattodelid</font> в конечном прайс-листе!<br>
            Файл <font class=hintspan>$pricefile</font> не найден!</p>
        ~;
    }
    return ( $retcontent, $retcode, $rejectedrecords );
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_rename_cat_form ()
#------------------------------------------------------------------------------
#
# Вывод формы для редактирования названия категории.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_rename_cat_form {
    my $shopid = $_[1];
    my $catid = $_[2];
    my $page = $_[4];
    my $itemid = -1;
    my $res_index = 0;
    my $anchor = 0;
    my $item_description_textarea;
    if ($#_ > 2) {$res_index = 1; $itemid = $_[3];}
    my @resources = ('Редактирование названия категории товаров','Редактирование названия позиции','название категории','название позиции','asc2renamecat','asc2renameitem',"catid=$catid","catid=$catid&amp;itemid=$itemid");
    my $rename_cat_form = qq~
        <div class="popup_header">$resources[$res_index]</div>
        <div class=standart_text style="margin-left:1px"><a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admshopcat2" class=simple_link><b>Управление каталогом</b></a> / $resources[$res_index]<br><br></div>
    ~;
     my $catfile = $MODULE_DB_FILE;
     if ($res_index == 1) {$catfile = $MODULE_PRICEPOOL.&getPriceFile($shopid);}
     if (-e $catfile) {
         my $catname;
         open my $ifh,"<", $catfile or die "unable to open $catfile: $!";
         my @data = <$ifh>;
         close $ifh or die "unable to close: $!";
         for my $i (0..$#data) {
             my  $record = $data[$i];
             if ($record =~ /\[$catid\]/) {
                 $record =~ s/[\r\n]+//g;
                 my @arr = split(/\|/,$record);
                 if ($res_index == 0) {
                     for my $j (1..($#arr-1)) {
                         if ($arr[$j] =~ /\[$catid\]/) {
                             $catname = $arr[$j];
                             $catname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                             if ($arr[$j-1] =~ /\[([\d]+)\]/ || $arr[$j] =~ /\[\*\*\]/) {
                                 $anchor = $1;
                             } else {$anchor = $catid;}
                         }
                     }
                 } else {
                     $arr[0] =~ s/[\D]+//g;
                     if ($arr[0] eq $itemid && $itemid > -1) {
                         $catname = $arr[$#arr];
                         $catname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                         my $lastitemid = _inner_shopcat2_getlastitemid_byitemid($shopid, $itemid);
                         my $descripton = _inner_shopcat2_get_itemdescr_from_list($shopid,$catid,$lastitemid);
                         $descripton =~ s/\<br\>/\n/g;
                         $item_description_textarea = qq~
                                 <div class="stxt frm-div-marg">Краткое описание позиции: <font color="#A0A0A0">[обязательное поле]</font></div>
                             <div class="frm-div-marg">
                             <textarea placeholder="Введите краткое описание позиции"
                                       class="input shopcat-modaledit-textarea" required
                                       name=itemdescr>$descripton</textarea></div>~;
                     }
                 }
             }
         }
         $catname =~ s/"/&quot;/g;
         $catname =~ s/"/&quot;/g;
         my @seo_data = &getSeoData($shopid, $catid, $itemid);
         my $filters;
         my $f_num = getSettingChildNum($MODULE_NAME, 0, 'filters');
         if ($f_num > -1) {
            my @filters = decodeFiltersMaskById($shopid, $itemid);
            # $rename_cat_form .= join(", ",@filters);
            for my $i (0..$f_num) {
                my $f_name  = 'filter-'.(sprintf "%02d", $i+1);
                my $f_label = 'filters/'.$f_name;
                my $f_descr = encode("utf8", getSingleSetting($MODULE_NAME, 0, $f_label));
                my $checked = $filters[$i] ? 'checked' : '';
                $filters .= $f_descr ne '0' ? qq~<div class="frm-div-marg">
                    <input id="$f_name" name="$f_name" $checked value="1" type="checkbox">
                    <label for="$f_name" class="checkbox_label"><span></span>$f_descr</label></div>~ : '';
            }
         }
         my $prms = Shopcat::ShopParams->new;
         my $tabwidth = $itemid > 0 ? 'width="800px;"' : '';
         my $seotags = qq~
               <div id="f-s-labl" class="stxt frm-div-marg">Новое $resources[$res_index+2] для заданного фильтра</font></div>
               <div id="f-s-inpt" class="frm-div-marg"><input name="paramname" class="input shopcat-modaledit-input" type="text" placeholder="Новое $resources[$res_index+2]" value=""></div>
               <div class="stxt frm-div-marg">Текст для тега <i>&lt;TITLE&gt;&lt;/TITLE&gt;</i>:</div>
               <div class="frm-div-marg"><input name="seotitle" class="input shopcat-modaledit-input" type="text" placeholder="Текст для тега TITLE" value="$seo_data[2]"></div>
               <div class="stxt frm-div-marg">Текст для тега <i>&lt;META name=&quot;description&quot; /&gt;</i>:</div>
               <div class="frm-div-marg"><input name="seometadescr" class="input shopcat-modaledit-input" type="text" placeholder="Текст для META.description" value="$seo_data[3]"></div>
               <div class="stxt frm-div-marg">Текст для тега <i>&lt;META name=&quot;keywords&quot; /&gt;</i>:</div>
               <div class="frm-div-marg"><input name="seometakeys" class="input shopcat-modaledit-input" type="text" placeholder="Текст для META.keywords" value="$seo_data[4]"></div>
               <div class="stxt frm-div-marg">Краткое описание <font color="#A0A0A0">[поддерживается простейшая markup разметка]</font>:</div>
               <div class="frm-div-marg"><textarea name="entitydescr" class="input shopcat-modaledit-textarea" placeholder="Краткое описание"></textarea></div>
        ~;

         my $tabs = $prms->getParamTabTmpl($seotags, 'modal-1', $shopid, $catid, $itemid );

         $rename_cat_form .= qq~
            <form id="renamecategory"
                  method=post
                  action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=$resources[$res_index+4]&amp;shopid=$shopid&amp;$resources[$res_index+6]&amp;page=$page#anchor$anchor">
            <table $tabwidth><tr><td class="shopcat-modaledit-td-padding" valign=top width="520">
               <div class="stxt frm-div-marg">Новое $resources[$res_index+2]: <font color="#A0A0A0">[обязательное поле]</font></div>
               <div class="frm-div-marg">
               <input placeholder="Введите $resources[$res_index+2]"
                      required type=text
                      name=categoryname
                      value="$catname" class="input shopcat-modaledit-input"></div>
               $item_description_textarea
               <div class="stxt frm-div-marg">ЧПУ страницы <i>(без .html)</i>: <font color="#A0A0A0">[обязательное поле]</font></div>
               <div class="frm-div-marg"><input name="seochpu" value="$seo_data[1]" class="input shopcat-modaledit-input" type="text" placeholder="ЧПУ страницы"></div>
               $tabs
               <div class="frm-div-marg"><input type=submit value="Сохранить изменения" class="button shopcat-modaledit-butt"></div>
            </td>~;
        if ( $itemid > 0) {
            $rename_cat_form .= qq~<td valign=top class="td-filtr" width="280">
                <div class="stxt frm-div-filtrs">
                <div class="frm-div-marg frm-div-marg-header">Настройки фильтров для позиции:</div>
                $filters
                </div>
            </td>~;
        }
        $rename_cat_form .= qq~</tr></table></form>~;
     } else {$rename_cat_form .= qq~<p class=cmstext>Невозможно редактировать запись id=$catid - $catfile не найден!</p>\n~;}
     return $rename_cat_form;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_rename_cat_in_catalog ()
#------------------------------------------------------------------------------
#
# Редактируем название заданной категории во всех записях.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_rename_cat_in_catalog {
    my $manageflag = $_[3];
    my @resourses  = (
        'Категория', 'Позиция',
        'категорию', 'позицию'
    );
    my $catid         = $_[1];
    my $catname       = $_[2];
    my $retcontent    = '';
    my $editedrecords = 0;
    my $retcode       = 0;
    my $catfile       = $_[0];
    if ( -e $catfile ) {
        open my $fh, "<", $catfile;
        my @data = <$fh>;
        close $fh;
        for ( my $i = 0 ; $i <= $#data ; $i++ ) {
            my $record = $data[$i];
            if ( $manageflag == 0 ) {
                if ( $record =~ /\[$catid\]/ ) {
                    $record =~ s/[\r\n]+//g;
                    my @arr = split( /\|/, $record );
                    for ( my $j = 3 ; $j <= $#arr ; $j++ ) {
                        if ( $arr[$j] =~ /\[$catid\]/ ) {
                            $arr[$j] =~ s/(\[[\d]+\])(.+)$/$1$catname/g;
                            $editedrecords++;
                        }
                    }
                    $data[$i] = join( "|", @arr ) . "\n";
                }
            }
            else {
                if ( $manageflag == 1 ) {
                    $record =~ s/[\r\n]+//g;
                    my @arr = split( /\|/, $record );
                    if ( $arr[0] eq $catid ) {
                        $arr[$#arr] =~ s/(\[[\d]+\])(.+)$/$1$catname/g;
                        $editedrecords++;
                    }
                    $data[$i] = join( "|", @arr ) . "\n";
                }
            }
        }
        if ( $editedrecords > 0 ) {
            open my $fh, ">", $catfile;
            flock $fh, LOCK_EX;
            print $fh @data;
            flock $fh, LOCK_UN;
            close $fh;
            $retcode    = 1;
            $retcontent = qq~
                <p class=hintcont>$resourses[$manageflag]
                <font class=hintspan>$catid</font> отредактирована!<br>Всего изменено позиций:
                <font class=hintspan>%editeditems%</font></p>
            ~;
        }
        else {
            $retcontent = qq~
                <p class=hintcont>$resourses[$manageflag]
                <font class=hintspan>$catid</font> не найдена в каталоге!</p>
            ~;
        }
    }
    else {
        $retcontent = qq~<p class=hintcont>Невозможно редактировать $resourses[$manageflag+2] <font class=hintspan>$catid</font>.<br>Файл $catfile не нaйден!</p>~;
    }
    if ( $manageflag == 1 ) { $retcontent =~ s/%editeditems%/$editedrecords/g; }
    return ( $retcontent, $retcode, $editedrecords );
}


#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_cat_new_description_form ()
#------------------------------------------------------------------------------
#
# Вывод формы для редактирования названия категории.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_cat_new_description_form {
    my ($sid, $catid) = @_;
    my $catdescr;
    my $filenofoundmess;
    my $_cntnt = qq~
        <div class="popup_header">Редактирование глобального описания позиции</div>
        <div class=standart_text style="margin-left:1px"><a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admshopcat2" class=simple_link><b>Управление каталогом</b></a> / Редактирование глобального описания позиции<br><br></div>
    ~;
    my $catfile = "$MODULE_DATAPOOL/levels/n-1/$catid/descriptions.txt";
    if (-e $catfile) {
        my $catdescr;
        open my $fh, "<", $catfile;
        my @data = <$fh>;
        close $fh;
        for my $i (0..$#data) {
            my  $record = $data[$i];
            my @arr = split(/\|/,$record);
            $arr[0] =~ s/[\D]+//g;
            if ($arr[0] eq $catid) {
                $catdescr = $arr[1];
                $catdescr =~ s/[\r\n]+//g;
            }
        }
    } else {
        $filenofoundmess = qq~<p class="smalltext">Файл <b>$catfile</b> не найден!</p>~;
    }
    $_cntnt .= qq~
      <form id="renamecategory" method=post action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=asc2editglobalitemdescr&amp;catid=$catid" style="margin:0">
        <table cellspacing=0 cellpadding=2 border=0 width="100%">
          <tr><td class=standart_text>Новое название категории: <font color="#A0A0A0">[обязательное поле]</font></td></tr>\n
          <tr><td class=standart_text><textarea placeholder="Введите описание позиции, отображаемое в родительской категории" required name=categorydescr value="" class=input style="height:300px; width:500px;">$catdescr</textarea></td></tr>\n
          <tr><td class=standart_text><input type=submit value="Сохранить изменения" class="button" style="width:150px"></td></tr>\n
        </table>
      </form>
      $filenofoundmess
    ~;
    return $_cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_delete_item_from_catalog ()
#------------------------------------------------------------------------------
#
# Удаляем из каталога записи с заданным id.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_delete_item_from_catalog {
    my $retcontent;
    my $retcode = 0;
    my ($shopid, $catid, $itemdelid)= @_;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        my @newcat;
        open my $ifh, "<", $catfile or die "unable to open $catfile: $!";
        my @data = <$ifh>;
        close $ifh or die "unable to close: $!";
        for my $i (0..$#data) {
            my @tmp = split(/\|/,$data[$i]);
            $tmp[0] =~ s/[\D]+//g;
            if ($tmp[0] ne $itemdelid) {push(@newcat,$data[$i]);}
        }
        if ($#newcat>-1 && $#newcat < $#data) {
            open my $ofh, ">", $catfile or die "unable to open $catfile: $!";
            flock($ofh, LOCK_EX);
            print $ofh @newcat;
            flock($ofh, LOCK_UN);
            close $ofh or die "unable to close: $!";
            $retcode = 1;
            $retcontent = qq~<p class=hintcont>Позиция <font class=hintspan>$itemdelid</font> успешно удалена</p>~;
        } else {
            if ($#newcat == $#data) {
                $retcontent = qq~<p class=hintcont>Позиция <font class=hintspan>$itemdelid</font> не найдена!</p>~;
            } else {
                if ($#newcat == -1) {
                    unlink($catfile);
                    $retcode = 1;
                    $retcontent = qq~
                        <p class=hintcont>Позиция <font class=hintspan>$itemdelid</font> успешно удалена!<br>
                        Это последняя позиция в каталоге - удаляем <font class=hintspan>$catfile</font></p>~;
                }
            }
        }
    } else {
        $retcontent = qq~
            <p class=hintcont>Невозможно удалить позицию <font class=hintspan>$itemdelid</font><br>
            Файл <font class=hintspan>$catfile</font> не найден</p>~;
    }
    return ($retcontent,$retcode);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_existed_id ()
#------------------------------------------------------------------------------
#
# Найти запись с заданным id в БД (любую).
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_existed_id {
    my ($shopid, $id_to_be_checked) = @_;
    my $retcode = 0;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        open(CATFILEDESCRFH,"<$catfile");
        my @data = <CATFILEDESCRFH>;
        close CATFILEDESCRFH;
        for(my $i=0; $i<=$#data; $i++) {
            if ($data[$i] =~ /\[$id_to_be_checked\]/) {
                $retcode = 1;
                last;
            }
        }
    }
    return $retcode;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_getcatid_byitemid ()
#------------------------------------------------------------------------------
#
# Найти последний catid po itemid.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_getcatid_byitemid {
    my ($shopid, $itemid) = @_;
    my $retcode = -1;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        open(CATFILEDESCRFH,"<$catfile");
        my @data = <CATFILEDESCRFH>;
        close CATFILEDESCRFH;
        for(my $i=0; $i<=$#data; $i++) {
            my @arr = split(/\|/,$data[$i]);
            $arr[0] =~ s/[\D]+//g;
            if ($arr[0] eq $itemid) {
                if ($arr[$#arr-1] =~ /\[([\d]+)\]/) {$retcode = $1;}
                last;
            }
        }
    }
    return $retcode;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_getlastitemid_byitemid ()
#------------------------------------------------------------------------------
#
# Найти [itemid][**] po артикулу (arr[0]).
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_getlastitemid_byitemid {
    my ($shopid, $itemid) = @_;
    my $retcode = -1;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        open my $fh, "<", $catfile;
        my @data = <$fh>;
        close $fh;
        for my $i (0..$#data) {
            my @arr = split(/\|/,$data[$i]);
            $arr[0] =~ s/[\D]+//g;
            if ($arr[0] eq $itemid) {
                if ($arr[$#arr] =~ /\[([\d]+)\]/) {
                    $retcode = $1;
                    if ($retcode > 9999) {
                        $retcode = $retcode % 10000;
                    }
                }
                last;
            }
        }
    }
    return $retcode;
}


#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_cat_itemdetails ()
#------------------------------------------------------------------------------
#
# Вывод информации о конкретной позиции.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_cat_itemdetails {
    my ($sid, $cryptlgn, $shopid, $catid, $itemid, $page) = @_;
    my $itemcontent;
    my $navi = 'undefined';
    my $trtdcontent = "<p class=error>Позиция id=$itemid не найдена!</p>";
    my $catfile = $MODULE_PRICEPOOL.&getPriceFile($shopid);

    if (-e $catfile) {
        open my $ifh, "<", $catfile or die "unable to open $catfile: $!";
        my @data = <$ifh>;
        close $ifh or die "unable to close: $!";
        my @curr_ids_stamp;
        for my $i ( 0..$#data ) {
            my $record = $data[$i];
            $record =~ s/[\r\n]+//g;
            my @arr = split(/\|/,$record);
            $arr[0] =~ s/[\D]+//g;
            if ($arr[-2] =~ /\[($catid)\]/ && $arr[0] eq $itemid) {
                my $itemname = $arr[-1];
                $itemname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                my @navi_arr = ();
                for my $j (3..$#arr-1) {
                    my $catname_ = $arr[$j];
                    $catname_ =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                    push(@navi_arr,$catname_);
                }
                $navi = qq~<a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=asc2browseitems&amp;shopid=$shopid&amp;catid=$catid&amp;page=$page" class="simplelink"><b>~.join("&nbsp;&rarr; &nbsp;",@navi_arr)."</b></a> / $itemname";

                # настройки показа
                my $item_artkl = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblocksposlev', 'action', 'shopcatshowblockspos', 'artikul' );
                my $item_price = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblocksposlev', 'action', 'shopcatshowblockspos', 'price' );
                my $item_info  = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblocksposlev', 'action', 'shopcatshowblockspos', 'info' );
                my $item_param = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblocksposlev', 'action', 'shopcatshowblockspos', 'params' );
                my $item_anlog = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblocksposlev', 'action', 'shopcatshowblockspos', 'analogs' );
                my $item_files = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblocksposlev', 'action', 'shopcatshowblockspos', 'files' );

                $trtdcontent = qq~<p class="details_header"><span class="greytext">Наименование: </span><b>$itemname</b></p>~;

                if (!$item_artkl) {
                    $trtdcontent .= qq~<p class="details_header"><span class="greytext">Артикул: </span>$shopid-$itemid</p>~;
                }

                if (!$item_price) {
                    my $_curr = $GLOBAL_CURRENCY != 0 ? $GLOBAL_CURRENCY : '';
                    $trtdcontent .= qq~<p class="details_header"><span class="greytext">Цена: </span><a href="javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}', '&amp;action=asc2edititempriceform&amp;shopid=$shopid&amp;itemid=$itemid')" class="shopcat-greytext-link">$arr[2]</a> $_curr</p>~;
                }
                $trtdcontent .= getV3EditTabs(1, $sid, $shopid, $itemid, $item_info, $item_param, $item_anlog, $item_files);
                if ( $item_artkl || $item_price ||
                     $item_info || $item_info ||
                     $item_param || $item_anlog || $item_files ) {
                    $trtdcontent .= qq~<div class="hide-policy-block">Для вашего удобства мы скрыли некоторые объекты на этой странице. Изменить политики отображения можно в <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admsetup&amp;name=$MODULE_NAME">настройках модуля</a>.</div>~;
                }
                last;
            }
        }
    } else {
        $trtdcontent = qq~<p class=error>Невозможно детализировать позицию id=$itemid - $catfile не найден!</p>\n~;
    }
    $itemcontent = qq~<div class="cmstext">$trtdcontent</div>~;
    return ($navi,$itemcontent);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_browse_get_data_for_tab ()
#------------------------------------------------------------------------------
#
# Функция вывода данных на закладку уровня n.
#
#------------------------------------------------------------------------------

# TODO: _inner_shopcat2_browse_get_data_for_tab

sub _inner_shopcat2_browse_get_data_for_tab {
    my ($sid, $shopid, $level, $model, $datatype) = @_;
    my $tabdata;
    if ($level eq 'n') {
        $tabdata = show_shopcat2_item_in_editor($sid, 0, $shopid, $model, $datatype);
    } elsif ($level eq 'n-1') {
        $tabdata = showCatDescrEditor($sid, 0, $shopid, $model, $datatype);
    } else {
        $tabdata = '<p class="error">Invalid level: '.$level.'</p>';
    }
    #my $filename = "$MODULE_DATAPOOL/$shopid/levels/$level/$model/".$datatype.".txt";
    #if (-e $filename) {
    #    open my $fh, "<", $filename;
    #    $tabdata = join("",<$fh>);
    #    close $fh;
    #} else {$tabdata = "Tab file not found: $filename"}
    return $tabdata;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_shopcat2_item_in_editor ()
#------------------------------------------------------------------------------
#
# Вывод описания позиции в CKeditor.
#
#------------------------------------------------------------------------------

sub show_shopcat2_item_in_editor {
    my ($sid, $cryptlgn, $shopid, $itemid, $todoaction) = @_;
    my $editorcontent;
    my $tabdata;
    if ($todoaction =~ /^[a-z]+$/ && ($todoaction eq 'information' || $todoaction eq 'parameters' || $todoaction eq 'analogs' || $todoaction eq 'files')) {
        my $filename = "$MODULE_DATAPOOL/$shopid/levels/n/$itemid/".$todoaction.".txt";
        if (-e $filename) {
            open(CATFILEDESCRFH,"<$filename");
            $tabdata = join("",<CATFILEDESCRFH>);
            close CATFILEDESCRFH;
        }
        $editorcontent = qq~
            <div class="popup_header">Изменение описания позиции</div>
            <form method=post
                  action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2itemspostdescr&amp;shopid=$shopid&amp;itemid=$itemid&todo=$todoaction"
                  ENCTYPE="multipart/form-data"
                  style="margin:0">
            <div class="pheix-ckeditor-popupcontainer">
                <textarea id="shopcat2editor-item-$todoaction" name="editorcontent" required style="width:805px; height:450px;">$tabdata</textarea>
            </div>
            <script type="text/javascript">
                CKEDITOR.replace( 'shopcat2editor-item-$todoaction', { pheixScript: '$ENV{SCRIPT_NAME}', pheixSessionId: '$sid', pheixGalDivId: 'shct2-divgal-item-$todoaction' });
            </script>
            <input type=hidden name="preview" value="0">
            </form>
       ~;
    } else {$editorcontent = "<p class=error>Неправильно задан параметр имени текстового файла</p>";}
    return $editorcontent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_update_item_description ()
#------------------------------------------------------------------------------
#
# Вывод описания позиции в файл.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_update_item_description {
    my ($shopid, $itemid, $todoaction, $datatosave) = @_;
    my $retcontent;
    my $retcode = 0;
    if ($todoaction =~ /^[a-z]+$/ && ($todoaction eq 'information' || $todoaction eq 'parameters' || $todoaction eq 'analogs' || $todoaction eq 'files')) {
        if ($datatosave ne '' && $datatosave !~ /^[\s\t\r\n]+$/) {
            my $filename = "$MODULE_DATAPOOL/$shopid/levels/n/$itemid/".$todoaction.".txt";
            if (!(-e "$MODULE_DATAPOOL")) { mkdir($MODULE_DATAPOOL);}
            if (!(-e "$MODULE_DATAPOOL/$shopid")) { mkdir("$MODULE_DATAPOOL/$shopid");}
            if (!(-e "$MODULE_DATAPOOL/$shopid/levels")) { mkdir("$MODULE_DATAPOOL/$shopid/levels");}
            if (!(-e "$MODULE_DATAPOOL/$shopid/levels/n")) { mkdir("$MODULE_DATAPOOL/$shopid/levels/n");}
            if (!(-e "$MODULE_DATAPOOL/$shopid/levels/n/$itemid")) { mkdir("$MODULE_DATAPOOL/$shopid/levels/n/$itemid");}
            open my $fh,">", $filename;
            print $fh $datatosave;
            close $fh;
            $retcode = 1;
            $retcontent = qq~<p class=hintcont>Данные успешно сохранены в файл <font class=hintspan>$filename</font></p>~;
        } else {
            $retcode = 0;
            $retcontent = qq~<p class=hintcont>Невозможно сохранить пустые данные</p>~;
        }
    } else {
        $retcode = 0;
        $retcontent = qq~<p class=hintcont>Неправильно задан параметр имени текстового файла</p>~;
    }
    return($retcontent,$retcode);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_cat_item_price_form ()
#------------------------------------------------------------------------------
#
# Вывод формы для редактирования цены позиции.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_cat_item_price_form {
    my ($sid, $shopid, $itemid ) = @_;
    my $price = -1;
    my $_cntnt = qq~
         <div class="popup_header">Редактирование цены позиции</div>
         <div class=standart_text style="margin-left:1px"><a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admshopcat2" class=simple_link><b>Управление каталогом</b></a> / Редактирование цены позиции<br><br></div>
    ~;
    my $catfile = $MODULE_PRICEPOOL.getPriceFile($shopid);
    if (-e $catfile) {
        my $catname;
        open my $fh, "<", $catfile;
        my @data = <$fh>;
        close $fh;
        for my $i (0..$#data) {
            my $record = $data[$i];
            $record =~ s/[\r\n]+//g;
            my @arr = split(/\|/,$record);
            $arr[0] =~ s/[\D]+//g;
            if ($arr[0] eq $itemid && $itemid > -1) {
                $price = $arr[2];
            }
        }
        $_cntnt .= qq~
          <form id="renamecategory" method=post action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=asc2itemssaveprice&amp;shopid=$shopid&amp;itemid=$itemid" style="margin:0">
            <table cellspacing=0 cellpadding=2 border=0 width="100%">
              <tr><td class=standart_text>Введите новое целое значение цены: <font color="#A0A0A0">[обязательное поле]</font></td></tr>
              <tr><td class=standart_text><input placeholder="Введите новое целое значение цены" required type=text name=newprice value="$price" class=input style="width:500px;"></td></tr>
              <tr><td class=standart_text><input type=submit value="Сохранить изменения" class="button" style="width:150px"></td></tr>
            </table>
          </form>
        ~;
    } else {
        $_cntnt .= qq~<p class=cmstext>Невозможно редактировать запись id=$itemid - $catfile не найден!</p>~;
    }
    return $_cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_update_item_price ()
#------------------------------------------------------------------------------
#
# Функция изменения цены позиции.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_update_item_price {
    my ($shopid, $itemid, $newprice) = @_;
    my $retcontent;
    my $retcode = 0;
    my $editedrecords = 0;
    if (
        $newprice =~ /^[\d]+$/ ||
        $newprice =~ /^[\d]+\.[\d]+$/ ||
        $newprice eq '-1'
    ) {
        my $catfile = getPriceFileFullPath($shopid);
        if (-e $catfile) {
            open my $fh, "<", $catfile;
            my @data = <$fh>;
            close $fh;
            for my $i (0..$#data) {
                my @arr = split(/\|/,$data[$i]);
                $arr[0] =~ s/[\D]+//g;
                if ($arr[0] eq $itemid) {
                    $arr[2] = $newprice;
                    $editedrecords++;
                }
                $data[$i] = join("|",@arr);
                if ($editedrecords > 0) {last;}
            }
            if ($editedrecords > 0) {
                open my $fh, ">", $catfile;
                flock $fh, LOCK_EX;
                print $fh @data;
                flock $fh, LOCK_UN;
                close $fh;
                $retcode = 1;
                $retcontent = qq~<p class=hintcont>Данные успешно сохранены, установлена новая цена: <font class=hintspan>\$ $newprice</font></p>~;
            } else {
                $retcontent = qq~<p class=hintcont>Невозможно изменить цену для позиции <font class=hintspan>$itemid</font><br>Позиция с заданным идентификатором не найдена!</p>~;
            }
        } else {
            $retcontent = qq~<p class=hintcont>Невозможно изменить цену для позиции <font class=hintspan>$itemid</font><br>Файл <font class=hintspan>$catfile</font> не найден!</p>~;
        }
    } else {
        $retcode = 0; $retcontent = qq~<p class=hintcont>Неправильно задано значение цены (используются только цифры и точка)</p>~;
    }
    return($retcontent,$retcode);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_count_positions_in_cat ()
#------------------------------------------------------------------------------
#
# Функция подсчета количества позиций в категории n-1 уровня.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_count_positions_in_cat {
    my ($shopid, $catid) = @_;
    my $counter = 0;
    my $retcode = -1;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        open my $fh, "<", $catfile;
        my @data = <$fh>;
        close $fh;
        for(my $i=0; $i<=$#data; $i++) {
            my @arr = split(/\|/,$data[$i]);
            if ($arr[$#arr-1] =~ /\[($catid)\]/) {
            $counter++;
            }
        }
    }
    return $counter;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_shopcat2_categorydescr_in_editor ()
#------------------------------------------------------------------------------
#
# Вывод описания n-1 уровня в CKeditor.
#
#------------------------------------------------------------------------------

sub ___show_shopcat2_categorydescr_in_editor {
    my ($sid, $cryptlgn, $shopid, $catid, $todoaction) = @_;
    my $editorcontent;
    my $tabdata;
    if ($todoaction =~ /^[a-z]+$/ && ($todoaction eq 'information' || $todoaction eq 'primenenie' || $todoaction eq 'video' || $todoaction eq 'files')) {
        my $filename = "$MODULE_DATAPOOL/$shopid/levels/n-1/$catid/".$todoaction.".txt";
        if (-e $filename) {
            open(CATFILEDESCRFH,"<$filename");
            $tabdata = join("",<CATFILEDESCRFH>);
            close CATFILEDESCRFH;
        }
        $editorcontent = qq~
            <div class="popup_header">Изменение описания категории</div>
            <form method=post
                  action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2categorypostdescr&amp;shopid=$shopid&amp;catid=$catid&todo=$todoaction"
                  enctype="multipart/form-data"
                  style="margin:0">
            <div class="pheix-ckeditor-popupcontainer">
                <textarea id="shopcat2editor-cat-$todoaction" name="editorcontent" required style="width:500px; height:400px;">$tabdata</textarea>
            </div>
            <script type="text/javascript">
                CKEDITOR.replace( 'shopcat2editor-cat-$todoaction', { pheixScript: '$ENV{SCRIPT_NAME}', pheixSessionId: '$sid', pheixGalDivId: 'shct2-divgal-cat-$todoaction' });
            </script>
            <input type=hidden name="preview" value="0">
            </form>
        ~;
    } else {
        $editorcontent = "<p class=error>Неправильно задан параметр имени текстового файла</p>";
    }
    return $editorcontent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_update_categorynm1_description ()
#------------------------------------------------------------------------------
#
# Вывод описания n-1 уровня в файл.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_update_categorynm1_description {
    my ($shopid, $catid, $todoaction, $datatosave) = @_;
    my $retcontent;
    my $retcode = 0;
    if ($todoaction =~ /^[a-z]+$/ && ($todoaction eq 'information' || $todoaction eq 'primenenie' || $todoaction eq 'video' || $todoaction eq 'files')) {
        if ($datatosave ne '' && $datatosave !~ /^[\s\t\r\n]+$/) {
            my $filename = "$MODULE_DATAPOOL/$shopid/levels/n-1/$catid/".$todoaction.".txt";
            if (!(-e "$MODULE_DATAPOOL")) { mkdir("$MODULE_DATAPOOL");}
            if (!(-e "$MODULE_DATAPOOL/$shopid")) { mkdir("$MODULE_DATAPOOL/$shopid");}
            if (!(-e "$MODULE_DATAPOOL/$shopid/levels")) { mkdir("$MODULE_DATAPOOL/$shopid/levels");}
            if (!(-e "$MODULE_DATAPOOL/$shopid/levels/n-1")) { mkdir("$MODULE_DATAPOOL/$shopid/levels/n-1");}
            if (!(-e "$MODULE_DATAPOOL/$shopid/levels/n-1/$catid")) {
                mkdir("$MODULE_DATAPOOL/$shopid/levels/n-1/$catid");
                open my $fh, ">", $filename;
                flock $fh, LOCK_EX;
                print $fh $datatosave;
                flock $fh, LOCK_UN;
                close $fh;
                $retcode = 1;
                $retcontent = qq~<p class=hintcont>Данные успешно сохранены в файл <font class=hintspan>$filename</font></p>~;
            }
        } else {
            $retcode = 0;
            $retcontent = qq~<p class=hintcont>Невозможно сохранить пустые данные</p>~;
        }
    } else {
        $retcode = 0;
        $retcontent = qq~<p class=hintcont>Неправильно задан параметр имени текстового файла</p>~;
    }
    return($retcontent,$retcode);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_getdetails_array_for_id ()
#------------------------------------------------------------------------------
#
# Получить массив с данными для заданного id в БД.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_getdetails_array_for_id {
    my ($shoptid, $catid) = @_;
    my @retarray = ();
    my $catfile = getPriceFileFullPath($shoptid);
    if (-e $catfile) {
        open my $ifh,"<", $catfile;
        my @data = <$ifh>;
        close $ifh;
        for my $i (0..$#data) {
            my $record = $data[$i];
            $record =~ s/[\n\r\t]+//gsx;
            my @dbcol = split /\|/, $record;
            for my $j (0..$#dbcol) {
                if ($dbcol[$j] =~ /\[([\d]+)\]/) {
                    my $id = $1;
                    if ($id > 9999) { $id = $id % 10000; }
                    if ($id == $catid) {
                        @retarray = @dbcol;
                        last;
                    }
                }
            }
        }
    }
    return @retarray;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_save_itemdescr ()
#------------------------------------------------------------------------------
#
# Сохранение описания позиции в файл descriptions.txt для блока Варианты.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_save_itemdescr {
    my ($shopid, $catid, $itemid, $itemdescr) = @_;
    my $debug   = 0;
    my $syspath = "$MODULE_DATAPOOL/$shopid/levels/n-1/$catid";
    my $lastitemid = _inner_shopcat2_getlastitemid_byitemid($shopid, $itemid);
    $itemdescr =~ s/[\r\n]+/\<br\>/g;
    $itemdescr =~ s/[\s]+/ /g;
    if ($itemdescr ne '' && $itemdescr !~ /^[\r\n\s]+$/) {
        if ($debug) {
            print "<p>\$itemdescr is ok <$itemdescr></p><p>\$lastitemid:$lastitemid</p>";
        }
        if (!(-e $syspath)) {mkpath($syspath);}
        my $filepath = "$syspath/descriptions.txt";
        if (!(-e $filepath)) {
            if ($debug) {
                print "<p>$filepath is not found, creating...</p>";
            }
            open my $fh, ">", $filepath;
            flock $fh, LOCK_EX;
            print $fh "$lastitemid|$itemdescr\n";
            flock $fh, LOCK_UN;
            close $fh;
        } else {
            if ($debug) {
                print "<p>$filepath ok!</p>";
            }
            open my $fh, "<", $filepath;
            my @descrs = <$fh>;
            close $fh;
            my $isupdates = 0;
            for my $i (0..$#descrs) {
                my $record = $descrs[$i];
                $record =~ s/[\r\n]+//g;
                my @tmp_ = split(/\|/,$record);
                $tmp_[0] =~ s/[\D]+//g;
                if ($tmp_[0] eq $lastitemid) {
                    $isupdates = 1;
                    $descrs[$i] = "$lastitemid|$itemdescr\n";
                    last;
                }
            }
            if ($debug) {
                print "<p>\$isupdates=$isupdates</p>";
            }
            if ($isupdates == 0) {
                push(@descrs,"$lastitemid|$itemdescr\n");
            }
            open my $fh, ">", $filepath;
            flock $fh, LOCK_EX;
            print $fh @descrs;
            flock $fh, LOCK_UN;
            close $fh;
        }
    }
    else {
        if ($debug) {
            print "<p>\$itemdescr is blank</p>";
        }
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_shopcat2_get_itemdescr_from_list ()
#------------------------------------------------------------------------------
#
# Получение описания позиции из файла descriptions.txt для блока Варианты.
#
#------------------------------------------------------------------------------

sub _inner_shopcat2_get_itemdescr_from_list {
    my ($shopid, $catid, $itemid) = @_;
    my $filepath = "$MODULE_DATAPOOL/$shopid/levels/n-1/$catid/descriptions.txt";
    my $retval;
    if (-e $filepath) {
        open my $fh, "<", $filepath;
        my @descrs = <$fh>;
        close $fh;
        my $isupdates = 0;
        for my $i (0..$#descrs) {
            my $record = $descrs[$i];
            $record =~ s/[\r\n]//g;
            my @tmp_ = split(/\|/,$record);
            $tmp_[0] =~ s/[\D]+//g;
            if ($tmp_[0] eq $itemid) {
                $retval = $tmp_[1];
                last;
            }
        }
    }
    return $retval;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getPriceFile ()
#------------------------------------------------------------------------------
#
# Получить название файла прайс-листа.
#
#------------------------------------------------------------------------------

sub getPriceFile {
   my ($priceId) = @_;
   my $filepath     = $MODULE_DB_FILE;
   if ( -e $filepath ) {
        open my $fh, "<", $filepath;
        my @data = <$fh>;
        close $fh;
        for(my $i=0; $i<=$#data; $i++ ) {
            my $record = $data[$i];
            $record =~ s/[\r\n]//g;
            my @record_arr = split(/\|/,$record);
            if ($record_arr[0] eq $priceId) {
                my $pricefilepath = $record_arr[$#record_arr];
                $pricefilepath =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                return $pricefilepath;
            }
        }
   }
   return -1;
}

#------------------------------------------------------------------------------
# delCategory ()
#------------------------------------------------------------------------------
#
# Удалить категории из каталога.
#
#------------------------------------------------------------------------------

sub delCategory {
    my ( $shopid, $cattodelid, $processpricefile ) = @_;
    my $DISP_IN_RECORD = 1;
    my $retcontent;
    my $retcode         = 0;
    my $rejectedrecords = 0;
    my $catfile         = $MODULE_DB_FILE;
    if ( -e $catfile ) {
        my @newcat;
        open my $fh, "<", $catfile;
        my @data = <$fh>;
        close $fh;
        for my $i ( 0 .. $#data ) {
            my @indexdetails = split( /\|/, $data[$i] );
            $indexdetails[0] =~ s/[\D]+//g;
            $indexdetails[$#indexdetails] =~ s/[\r\n]//g;
            my $foundflag = 0;
            for ( my $j = $DISP_IN_RECORD ;
                $j <= ( $#indexdetails - 1 ) ; $j++ )
            {
                if ( $indexdetails[$j] =~ /\[$cattodelid\]/ ) {
                    $foundflag = 1;
                }
            }
            if ( $foundflag == 0 ) {
                push( @newcat, $data[$i] );
            }
            else {
                if ( $processpricefile == 1 ) {
                    my $currrejectedrecords = 0;
                    $indexdetails[$#indexdetails] =~
                      s/(\[[\d]+\])|(\[[\*]+\])//g;
                    my $pricefilepath =
                      $MODULE_PRICEPOOL . $indexdetails[$#indexdetails];
                    ( $retcontent, $retcode, $currrejectedrecords ) =
                      &_inner_shopcat2_delete_cat_from_catalog( $pricefilepath,
                        $cattodelid );
                    $rejectedrecords += $currrejectedrecords;
                    $retcontent =~ s/%rejectedrecords%/$rejectedrecords/g;
                }
            }
        }
        if ( $#newcat > -1 && $#newcat < $#data ) {
            open my $ofh, ">", $catfile;
            flock $ofh, LOCK_EX;
            print $ofh @newcat;
            flock $ofh, LOCK_UN;
            close $ofh;
        }
        else {
            if ( $#newcat == $#data ) {
                $retcontent = qq~<p class=hintcont>Категория <font class=hintspan>$cattodelid</font> не найдена!</p>\n~;
            }
            else {
                if ( $#newcat == -1 ) {
                    if ( -e $catfile ) {
                        unlink($catfile);
                        unlink glob $MODULE_PRICEPOOL . "*.txt";
                        $retcode    = 1;
                        $retcontent = qq~
                          <p class=hintcont>Категория <font class=hintspan>$cattodelid</font> успешно удалена!
                          Это последняя категория - удаляем <font class=hintspan>$catfile</font></p>
                        ~;
                    }
                }
            }
        }
    }
    else {
        $retcontent = qq~
          <p class=hintcont>Невозможно удалить категорию <font class=hintspan>$cattodelid</font>
          Файл $catfile не найден!</p>\n
        ~;
    }
    return ( $retcontent, $retcode );
}


#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# setCatItemName ( $shopid, $catid, $catname )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор фалйа прайс-листа.
# $catid - идентификатор категории.
# $catname - наименование категории.
# Сохранить название для категории или позиции.
#
#------------------------------------------------------------------------------

sub setCatItemName {
    my ($shopid, $catid, $catname) = @_;
    my $manageflag = 0;
    my @resourses = ('Категория','Позиция','категорию','позицию');
    my $cntnt_;
    my $editedrecords = 0;
    my $retcode = 0;
    my $shopfile = $MODULE_DB_FILE;
    if (-e $shopfile) {
        open my $fh, "<", $shopfile
            or die 'unable to open for read ' . $shopfile . ': ' . $!;
        my @data = <$fh>;
        close $fh or die 'unable to close ' . $shopfile . ': ' . $!;
        for my $i ( 0..$#data ) {
            my  $record = $data[$i];
            if ($record =~ /\[$catid\]/) {
                $record =~ s/[\r\n]+//g;
                my @arr = split(/\|/,$record);
                $arr[0] =~ s/[\D]//g;
                for my $j ( 1.. ($#arr-1) )  {
                    if ($arr[$j] =~ /\[$catid\]/) {
                        $arr[$j] =~ s/(\[[\d]+\])(.+)$/$1$catname/g;
                        my $lst_ = $arr[-1];
                        $lst_ =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                        my $pricefilepath = $MODULE_PRICEPOOL.$lst_;
                        my $editeditems = 0;
                        ($cntnt_, $retcode, $editeditems) =
                            _inner_shopcat2_rename_cat_in_catalog(
                                $pricefilepath, $catid, $catname, $manageflag);
                        $editedrecords += $editeditems;
                        $cntnt_ =~ s/%editeditems%/$editedrecords/g;
                    }
                }
                $data[$i] = join("|",@arr)."\n";
            }
        }
        if ( $editedrecords > 0 ) {
            open my $fh, ">", $shopfile
                or die 'unable to open for write ' . $shopfile . ': ' . $!;
            flock( $fh, LOCK_EX ) or die 'unable to lock mailbox: ' . $!;
            print $fh @data;
            truncate $fh, tell($fh);
            flock( $fh, LOCK_UN ) or die 'unable to unlock mailbox: ' . $!;
            close $fh or die 'unable to close ' . $shopfile . ': ' . $!;
        } else {
            $cntnt_ = qq~
                <p class=cmstext>$resourses[$manageflag] id=$catid не найдена в каталоге!</p>\n~;
        }
  } else {
      $cntnt_ = qq~<p class=cmstext>Невозможно редактировать $resourses[$manageflag+2] id=$catid - $shopfile не найден!</p>\n~;
  }
  return ($cntnt_, $retcode);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getCatTreeTab ( $sesion_id )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод страницы c таблицей категорий уровня < N-1.
# Функция возвращает таблицу категорий в HTML формате.
#
#------------------------------------------------------------------------------

sub getCatTreeTab {
    my ( $sid, $expand_cat_id ) = @_;
    my $DISP_IN_RECORD = 1;
    my $deletemess = 'Вы уверены, что хотите удалить выбранную категорию безвозвратно?';
    my $cattreetable;
    my $trtdcontent;
    my $count = 0;

    if ( -e $MODULE_DB_FILE ) {
        open my $ifh, "<", $MODULE_DB_FILE;
        my @data = <$ifh>;
        close $ifh;

        my @curr_ids_stamp = ();
        for ( my $i = 0 ; $i <= $#data ; $i++ ) {
            my $record = $data[$i];
            my @arr = split( /\|/, $record );
            for ( my $j = $DISP_IN_RECORD ; $j <= $#arr - 1 ; $j++ ) {
                my $id = 0;
                if ( $arr[$j] =~ /\[([\d]+)\]/ ) {
                    $id = $1;
                    my $index = $j - $DISP_IN_RECORD;
                    if ( $curr_ids_stamp[$index] != $id ) {
                        my $catname = $arr[$j];
                        if ( $catname =~ /\[\*\*\]/ ) {
                            next;
                        }
                        $curr_ids_stamp[$index] = $id;
                        $catname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                        my $trbg = qq~class="tr01"~;

                        if ( $count % 2 == 0 ) {
                            $trbg = qq~class="tr02"~;
                        }
                        my $fntwght = 'normal';
                        my $otstup  = 50 * ($index) . "px";
                        my $fntsize = 16 - ( 2 * $index );

                        if ( $fntsize < 10 ) {
                            $fntsize = "10pt";
                        } else {
                            $fntsize = $fntsize . "pt";
                        }

                        if ( $index == 0 ) {
                            $fntwght = 'bold';
                        }
                        my $expand_cat_content;

                        my $expand_link;
                        if ( $expand_cat_id == $id || $#data < $MODULE_EXPAND_VALUE ) {
                            $expand_cat_content =
                              doCatsLoad( $sid, '', $arr[0], $id );
                            $expand_link =
                              ( $#data < $MODULE_EXPAND_VALUE )
                              ? "javascript:void(0);"
                              : "javascript:doDataLoad('$id', '$arr[0]')";
                        }
                        else {
                            $expand_link =
                              "javascript:doDataLoad('$id', '$arr[0]')";
                        }

                        my $linkclass = "cattreetabload";
                        if ( $index == 0 ) { $linkclass = "cattreetabnoload"; }
                        $trtdcontent .= qq~
                             <tr $trbg>
                                 <td width="85%" valign="top">
                                     <a name="anchor$id"></a>
                                     <a href="$expand_link"
                                   class="$linkclass" style="margin-left:$otstup; font-size:$fntsize; font-weight:$fntwght">$catname</a>
                                   <div id="loaddiv$id" style="margin-left:$otstup;">$expand_cat_content</div>
                                 </td>
                                 <td width="15%" align=right valign="top">
                                     <a href="javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}', '&amp;action=asc2renamecatform&amp;shopid=$arr[0]&amp;catid=$id')"
                                        class="fa-action-link">
                                        <i class="fa fa-pencil-square-o" title="Переименовать"></i></a></a>
                                     <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2delcat&amp;shopid=$arr[0]&amp;catid=$id','$deletemess')"
                                        class="fa-action-link"><i class="fa fa-trash" title="Удалить"></i></a>
                                 </td>
                                 </tr>
                             ~;
                        $count++;
                    }
                }
            }
        }
    }
    else {
        $trtdcontent .= qq~<tr><td><p class=error>Невозможно отобразить структуру каталога - $MODULE_DB_FILE не найден!</p></td></tr>\n~;
    }
    $cattreetable = qq~
    <script>
    function doDataLoad(into, catid) {
        if (jQuery("#loaddiv"+into).contents().length == 0) {
            jQuery("#loaddiv"+into).load("$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2loadcats&amp;shopid="+catid+"&amp;catid="+into+"&amp;rand="+Math.random(), function(responseTxt, statusTxt, xhr){
                if(statusTxt == "error") {
                    alert("Error: " + xhr.status + ": " + xhr.statusText);
                }
            });
        } else {
            jQuery("#loaddiv"+into).empty();
            jQuery("#loaddiv"+into).css('height',0);
        }
    }
    </script> <table cellspacing=0 cellpadding=0 border=0 class=cattreetab_ width="100%"><tr><td>
        <table class="tabselect" cellpadding="10" cellspacing="0" width="100%">
        $trtdcontent
        </table>
    </td></tr></table>
    ~;
    return $cattreetable;
}


#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doCatsLoad ( $sesion_id, $crypted_login )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_login - зашифрованный логин пользователя.
# Вывод таблицы категорий уровня == N-1.
# Функция возвращает таблицу категорий в HTML формате.
#
#------------------------------------------------------------------------------

sub doCatsLoad {
    my ($sid, $cryptlgn, $shopid, $catid) = @_;
    my $nm1_cats_table;
    my $trtdcontent;
    my $DISP_IN_RECORD = 1;
    my $deletemess   = 'Вы уверены, что хотите удалить выбранную категорию безвозвратно?';
    my $count        = 0;
    my $FOUND_CAT_INDEX = -1;
    if ( -e $MODULE_DB_FILE ) {
        open my $ifh, "<", $MODULE_DB_FILE;
        my @data = <$ifh>;
        close $ifh;

        my @curr_ids_stamp = ();
        for ( my $i = 0 ; $i <= $#data ; $i++ ) {
            my $record = $data[$i];

            # строим дерево категорий
            my @arr = split( /\|/, $record );
            for ( my $j = $DISP_IN_RECORD ; $j <= $#arr - 1 ; $j++ ) {
                if ( $arr[$j] =~ /\[($catid)\]/ && $arr[0] =~ $shopid) {
                    $FOUND_CAT_INDEX = $j;
                }
            }
            if ($FOUND_CAT_INDEX > -1) {
                if (  $arr[$FOUND_CAT_INDEX] =~ /\[($catid)\]/ && $arr[$FOUND_CAT_INDEX+1] =~ /\[\*\*\]/ ) {
                    my $catname = $arr[$FOUND_CAT_INDEX+1];
                         $catname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                         my $catid2 = $1;
                         $catid2 =~ s/[\[\]]+//g;
                         my $gallery_link = '';
                         eval { $gallery_link = Gallery::System::_inner_sysgallery_getlink_for_shopcat2($_[0],"images/shopcat/$arr[0]/levels/n-1",$arr[0],$catid2,$catname,'',1); };
                         $trtdcontent .= qq~
                        <tr class="nm1hover"><td width="80%" style="padding:10px;">
                        <a  href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2browseitems&amp;shopid=$arr[0]&amp;catid=$catid2"
                            class=simple_link>$catname</a></td>
                        <td width="20%" align="right" style="padding:10px;">
                        $gallery_link
                        &nbsp;<a  href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2editcatpage&amp;shopid=$arr[0]&amp;catid=$catid2"><img
                                                                    src="images/admin/cms/png/rename.png"
                                                                    title="Редактировать дополнительные описания категории"
                                                                    alt="" style="border:none;" border="0"></a>
                        &nbsp;<a href="javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}', '&amp;action=asc2renamecatform&amp;shopid=$arr[0]&amp;catid=$catid2')"><img src="images/admin/cms/png/edit2.png" title="Редактировать название категории" alt="" style="border:none;" border="0"></a>
                        &nbsp;<a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2delcat&amp;shopid=$arr[0]&amp;catid=$catid2#anchor$catid','$deletemess')"
                            class="simple_link"><img src="images/admin/cms/png/delete2.png" title="Удалить «$catname»" alt="" border="0"></a>
                        </td></tr>~;
                }
            }
            # строим дерево категорий
        }
    }
    else {
        $nm1_cats_table = qq~
        <p class=error>Невозможно отобразить структуру каталога - $MODULE_DB_FILE не найден!</p>\n~;
    }
    if ($trtdcontent ne '') {
        $nm1_cats_table = qq~
        <div id="cat\_$catid" style="position:absolute; width:950px;">
        <table class="nm1tab" style="margin:20px 0 20px 0;" width="100%" cellpadding="0" cellspacing="0">
          <tbody>
          $trtdcontent
          </tbody>
        </table>
        </div>
        <script>
            console.log('cat tab height: '+jQuery('#cat\_$catid').css('height'));
            jQuery('#loaddiv$catid').css('height',jQuery('#cat\_$catid').css('height'));
        </script>
        ~;
    }
    return $nm1_cats_table;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getParentCatId ( $shopid, $catid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор дочерней категории.
# Получить идентификатор родительской категории для заданной дочерней.
# Функция возвращает идентификатор родительской категории.
#
#------------------------------------------------------------------------------

sub getParentCatId {
    my ($shopid, $catid) = @_;
    my $FOUND_CAT_ID   = -1;
    my $DISP_IN_RECORD = 1;
    my $filepath = $MODULE_DB_FILE;
    if ( -e $MODULE_DB_FILE ) {
        open my $ifh, "<", $MODULE_DB_FILE;
        my @data = <$ifh>;
        close $ifh;
        for ( my $i = 0 ; $i <= $#data ; $i++ ) {
            my $record = $data[$i];

            my @arr = split( /\|/, $record );
            for ( my $j = $DISP_IN_RECORD ; $j <= $#arr - 1 ; $j++ ) {
                if ( $arr[$j] =~ /\[($catid)\]/ && $arr[0] =~ $shopid ) {
                    if ($arr[$j-1] =~ /\[([\d]+)\]/) {$FOUND_CAT_ID = $1;}
                }
            }
        }

    }
    return $FOUND_CAT_ID;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getEditCatPage ( $sesion_id, $crypted_userid, $shopid, $catid, $prinflag )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_userid - зашифрованный идентификатор пользователя.
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $prinflag - флаг управления печатью.
# Получить содержимое страницы редактирования доп. описаний категории.
# Функция выводит содержимое страницы редактирования доп. описаний категории
# в HTML формате.
#
#------------------------------------------------------------------------------

sub getEditCatPage {
    my $catnavlink;
    my $table;
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $sc_content_var = qq~<div class=admin_header>$module_descr</div>~
      . showV3UpdateHint( $_[6], $_[5], $_[4] );
    ( $catnavlink, $table ) = &getCatDetails(@_);
    $sc_content_var .= qq~
        <p class=standart_text>
         <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admshopcat2"
            class=simple_link><b>Управление каталогом</b></a> / $catnavlink
        </p>
        $table
        ~;
    return $sc_content_var;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getCatDetails ( $sesion_id, $crypted_userid, $shopid, $catid, $prinflag )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_userid - зашифрованный идентификатор пользователя.
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $prinflag - флаг управления печатью.
# Получить блок навигации и таблицу доп. описаний категории.
# Функция выводит массив (навигация, таблица доп. описаний категории)
# в HTML формате.
#
#------------------------------------------------------------------------------

sub getCatDetails {
    my ($sid, $cryptlgn, $shopid, $catid) = @_;
    my $itemcontent;
    my $navi = 'undefined';
    my $trtdcontent = "<p class=error>Категория id=$catid не найдена!</p>";
    if (-e $MODULE_DB_FILE) {
        open my $fh,"<", $MODULE_DB_FILE or die "unable to open $MODULE_DB_FILE: $!";
        my @data = <$fh>;
        close $fh or die "unable to close: $!";
        my @curr_ids_stamp;
        for my $i (0..$#data) {
            my $record = $data[$i];
            $record =~ s/[\r\n]+//g;

            # строим таблицу с описаниями
            my @arr = split(/\|/,$record);
            if ($arr[$#arr-1] =~ /\[($catid)\]/) {
                my @navi_arr;
                my $numofpositions;
                my $catname = $arr[$#arr-1];
                $catname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                for my $j (1..$#arr-1) {
                    my $catname_ = $arr[$j];
                    $catname_ =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                    push(@navi_arr,$catname_);
                }
                $navi = join("&nbsp;»&nbsp;",@navi_arr);

                # настройки показа
                my $cat_id    = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblockscatlev', 'action', 'shopcatshowblockscat', 'catid' );
                my $cat_pos   = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblockscatlev', 'action', 'shopcatshowblockscat', 'posnum' );
                my $cat_info  = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblockscatlev', 'action', 'shopcatshowblockscat', 'info' );
                my $cat_specs = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblockscatlev', 'action', 'shopcatshowblockscat', 'specs' );
                my $cat_video = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblockscatlev', 'action', 'shopcatshowblockscat', 'video' );
                my $cat_files = getSettingFromGroupByAttr( $MODULE_NAME, 0, 'hideblockscatlev', 'action', 'shopcatshowblockscat', 'files' );

                $trtdcontent = qq~<p class="details_header"><span class="greytext">Наименование категории: </span><b>$catname</b></p>~;

                if (!$cat_id) {
                    $trtdcontent .= qq~<p class="details_header"><span class="greytext">Идентификатор: </span>$catid</p>~;
                }

                if (!$cat_pos) {
                    my $positions = doCatPosCount($shopid, $catid);
                    $trtdcontent .= qq~<p class="details_header"><span class="greytext">Содержит позиций: </span> $positions</p>~;
                }

                $trtdcontent .= getV3EditTabs(0, $sid, $shopid, $catid, $cat_info, $cat_specs, $cat_video, $cat_files);

                if ( $cat_id || $cat_pos ||
                     $cat_info || $cat_specs ||
                     $cat_video || $cat_files ) {
                         $trtdcontent .= qq~<div class="hide-policy-block">Для вашего удобства мы скрыли некоторые объекты на этой странице. Изменить политики отображения можно в <a href="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=admsetup&amp;name=$MODULE_NAME">настройках модуля</a>.</div>~;
                }
                last;
            }
        }
    } else {
        $trtdcontent = qq~<p class=error>Невозможно детализировать категорию id=$catid - $MODULE_DB_FILE не найден!</p>\n~;
    }
    $itemcontent = qq~<div class="cmstext">$trtdcontent</div>~;
    return ($navi, $itemcontent);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showCatDescrEditor ( $sesion_id, $crypted_userid, $shopid, $catid, $prinflag )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_userid - зашифрованный идентификатор пользователя.
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $prinflag - флаг управления печатью.
# Вывод дополнительного описания категории в редакторе.
# Функция выводит дополнительное описание категории в редакторе CKEditor.
#
#------------------------------------------------------------------------------

sub showCatDescrEditor {
    my ($sid, $cryptlgn, $shopid, $catid, $todoaction) = @_;
    my $editorcontent;
    my $tabdata;
    if (
        $todoaction =~ /^[a-z]+$/
        && (   $todoaction eq 'information'
            || $todoaction eq 'primenenie'
            || $todoaction eq 'video'
            || $todoaction eq 'files' )
      )
    {
        my $filename =
          "$MODULE_DATAPOOL/$shopid/levels/n-1/$catid/" . $todoaction . ".txt";
        if ( -e $filename ) {
            open my $fh, "<", $filename;
            $tabdata = join( "", <$fh> );
            close $fh;
        }

        $editorcontent = qq~
        <div class="popup_header">Изменение описания позиции</div>
        <form method=post
             action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2catpostdescr&amp;shopid=$shopid&amp;catid=$catid&todo=$todoaction"
             enctype="multipart/form-data"
             style="margin:0">
        <div class="pheix-ckeditor-popupcontainer">
            <textarea id="shopcat2editor-cat-$todoaction" name="editorcontent" required style="width:805px; height:450px;">$tabdata</textarea>
        </div>
        <script type="text/javascript">
           CKEDITOR.replace( 'shopcat2editor-cat-$todoaction', { pheixScript: '$ENV{SCRIPT_NAME}', pheixSessionId: '$sid', pheixGalDivId: 'shct2-divgal-cat-$todoaction' });
        </script>
        <input type=hidden name="preview" value="0">
        </form>
        ~;
    }
    else {
        $editorcontent = qq~
         <p class=error>
         Неправильно задан параметр имени текстового файла - '$todoaction'
         </p>~;
    }
    return $editorcontent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# modfyCatDescr ( $shopid, $catid, $datatosave )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $datatosave - данные для сохранения.
# Сохранение дополнительного описания категории в БД.
# Функция сохраняет дополнительное описание категории в БД.
#
#------------------------------------------------------------------------------

sub modfyCatDescr {
    my ($shopid, $catid, $todoaction, $datatosave) = @_;
    my $retcontent;
    my $retcode    = 0;
    if (
        $todoaction =~ /^[a-z]+$/
        && (   $todoaction eq 'information'
            || $todoaction eq 'primenenie'
            || $todoaction eq 'video'
            || $todoaction eq 'files' )
      )
    {

        if ( $datatosave ne '' && $datatosave !~ /^[\s\t\r\n]+$/ ) {
            my $filename = "$MODULE_DATAPOOL/$shopid/levels/n-1/$catid/"
              . $todoaction . ".txt";
            if ( !( -e "$MODULE_DATAPOOL" ) ) { mkdir($MODULE_DATAPOOL); }
            if ( !( -e "$MODULE_DATAPOOL/$shopid" ) ) {
                mkdir("$MODULE_DATAPOOL/$shopid");
            }
            if ( !( -e "$MODULE_DATAPOOL/$shopid/levels" ) ) {
                mkdir("$MODULE_DATAPOOL/$shopid/levels");
            }
            if ( !( -e "$MODULE_DATAPOOL/$shopid/levels/n-1" ) ) {
                mkdir("$MODULE_DATAPOOL/$shopid/levels/n-1");
            }
            if ( !( -e "$MODULE_DATAPOOL/$shopid/levels/n-1/$catid" ) ) {
                mkdir("$MODULE_DATAPOOL/$shopid/levels/n-1/$catid");
            }
            open my $fh, ">", $filename;
            print $fh $datatosave;
            close $fh;
            $retcode = 1;
            $retcontent = qq~
             <p class=hintcont>
             Данные успешно сохранены в файл <font class=hintspan>$filename</font>
             </p>~;
        }
        else {
            $retcode = 0;
            $retcontent = qq~
                <p class=hintcont>
                Невозможно сохранить пустые данные</p>~;
        }
    }
    else {
        $retcode = 0;
        $retcontent = qq~
            <p class=hintcont>
            Неправильно задан параметр имени текстового файла - '$todoaction'
            </p>~;
    }
    return ( $retcontent, $retcode );
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# setSeoData ( $shopid, $catid, $chpudata, $titledata, $metad, $metak)
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $chpudata - ЧПУ страницы
# $titledata - Тайтл страницы
# $metad - Мета-дескрипшн страницы
# $metak - Мета-кейвордс страницы
# Сохранение SEO данных категории в БД.
# Функция сохраняет SEO данные категории в БД.
#
#------------------------------------------------------------------------------

sub setSeoData {
    my $rc         = -1;
    my ($shopid, $catid, $itemid, $cgi_p) = @_;

    my $prms = Shopcat::ShopParams->new;
    my $sav_p_rc =
        $prms->setParametrizedVals($shopid, $catid, $itemid, $cgi_p);

    my $chpudata   = $cgi_p->param("seochpu");
    my $titledata  = $cgi_p->param("seotitle");
    my $metad      = $cgi_p->param("seometadescr");
    my $metak      = $cgi_p->param("seometakeys");

    $chpudata   =~ s/[\r\n\t]+//g;
    $chpudata   =~ s/\"/\&quot;/g;
    $titledata  =~ s/[\r\n\t]+//g;
    $titledata  =~ s/\"/\&quot;/g;
    $metad      =~ s/[\r\n\t]+//g;
    $metad      =~ s/\"/\&quot;/g;
    $metak      =~ s/[\r\n\t]+//g;
    $metak      =~ s/\"/\&quot;/g;

    if (-e $MODULE_SEO_DB_FILE) {
        open my $fh, "<", $MODULE_SEO_DB_FILE;
        my @records = <$fh>;
        close $fh;
        for my $i (0..$#records) {
            my $record = $records[$i];
            $record =~ s/[\r\n\t]+//g;
            my @rec_data = split(/\|/,$record);
            if ($rec_data[1] == $shopid && $rec_data[2] == $catid && $rec_data[3] == $itemid) {
                $rec_data[4] = $chpudata;
                $rec_data[5] = $titledata;
                $rec_data[6] = $metad;
                $rec_data[7] = $metak;
                if ( $chpudata eq '' && $titledata eq '' && $metad eq '' &&  $metak eq '' ) {
                    $records[$i] = '';
                }
                if ($rec_data[4]) { $records[$i] = join("|", @rec_data)."\n";}
                $rc = 0;
                last;
            }
        }
        if ( $rc != 0 && $chpudata ne '') {
            my $seo_rec_id = unpack( 'L', md5( "$shopid\_$catid" ) );
            $seo_rec_id = sprintf("%012u",$seo_rec_id);
            my $record = "$seo_rec_id|$shopid|$catid|$itemid|$chpudata|$titledata|$metad|$metak\n";
            push(@records, $record);
        }
        my $db_record = join("",@records);
        if ($db_record) {
            open(MFHDL, ">", $MODULE_SEO_DB_FILE);
            print MFHDL $db_record;
            close(MFHDL);
        } else {
            unlink($MODULE_SEO_DB_FILE);
        }
    } else {
        if ( $chpudata ne '') {
            my $seo_rec_id = unpack( 'L', md5( "$shopid\_$catid" ) );
            $seo_rec_id = sprintf("%012u",$seo_rec_id);
            my $record = "$seo_rec_id|$shopid|$catid|$itemid|$chpudata|$titledata|$metad|$metak\n";
            open my $fh, ">", $MODULE_SEO_DB_FILE;
            print $fh $record;
            close $fh;
            $rc = 0;
        }
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSeoData ( $shopid, $catid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# Получение SEO данных категории из БД.
# Функция получает SEO данные категории из БД.
#
#------------------------------------------------------------------------------

sub getSeoData {
    my ($shopid, $catid, $itemid) = @_;
    my $rc = -1;
    my @seo_data;
    if (-e $MODULE_SEO_DB_FILE) {
        open my $fh, "<", $MODULE_SEO_DB_FILE;
        my @records = <$fh>;
        close $fh;
        for my $i (0..$#records) {
            my $record = $records[$i];
            $record =~ s/[\r\n\t]//g;
            my @rec_data = split(/\|/,$record);
            if ($rec_data[1] == $shopid && $rec_data[2] == $catid && $rec_data[3] == $itemid) {
                push(@seo_data,
                    $rec_data[0],
                    $rec_data[4],
                    $rec_data[5],
                    $rec_data[6],
                    $rec_data[7]);
                last;
            }
        }

    }
    return @seo_data;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getV3EditBlock ( $sid, $shopid, $itemid, $blockname )
#------------------------------------------------------------------------------
#
# Функция блок редактирования данных.
#
#------------------------------------------------------------------------------

sub getV3EditBlock {
    my ($sid, $shopid, $itemid, $blockname, $itemflag) = @_;
    my $head   = _inner_shopcat2_browse_get_data_for_tab($sid, $shopid, $itemflag ? 'n' : 'n-1', $itemid, $blockname);
    my $action = $itemflag ? 'asc2itemseditor' : 'asc2cateditor';
    my $idname = $itemflag ? 'itemid' : 'catid';
    my $block = qq~
         <div  class="detailsblock">
         <table class=nomargintable>
          <tr>
           <td><div class=tabcontent>$head</div></td>
          </tr>
         </table>
        </div>
        ~;
    return ( $head ne '' && $blockname ne '') ? $block : '';
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doCatPosCount ($shopid, $catid)
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# Функция подсчета количества позиций в категории n-1 уровня.
#
#------------------------------------------------------------------------------

sub doCatPosCount {
    my ( $shopid, $catid )= @_;
    my $counter;
    my $retcode = -1;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        open my $ifh,"<", $catfile or die "unable to open $catfile: $!";
        my @data = <$ifh>;
        close $ifh or die "unable to close $catfile";
        for my $i (0..$#data) {
            my @arr = split(/\|/,$data[$i]);
            if ($arr[$#arr-1] =~ /\[($catid)\]/) {
                $counter++;
            }
        }
    }
    return $counter;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getV3ItemsTable ()
#------------------------------------------------------------------------------
#
# Вывод таблицы позиций для категории.
#
#------------------------------------------------------------------------------

sub getV3ItemsTable {
    my ($sid, $cryptedlgn, $shopid, $catid, $page) = @_;
    my $catitemtable;
    my $trtdcontent;
    my $pagelinkx;
    my @data;
    my $navi = 'undefined';
    my $count = 0;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        ($pagelinkx, @data) = getV3ItemsTablePagesLinks($sid, $catfile, $shopid, $catid, $page);
        my @curr_ids_stamp;
        for my $i (0..$#data) {
            my $record = $data[$i];
            $record =~ s/[\r\n]+//g;

            # строим список позиций
            my @arr = split(/\|/,$record);
            if ($arr[$#arr-1] =~ /\[($catid)\]/) {
                my @navi_arr;
                my $itemname = $arr[-1];
                $itemname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                for my $j(3..$#arr-1) {
                    my $catname_ = $arr[$j];
                    $catname_ =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                    push(@navi_arr,$catname_);
                }

            $navi = join("&nbsp;»&nbsp;",@navi_arr);
            my $trbg = $count % 2 ? 'class="tr01"' : 'class="tr02"';
            my $gallery_link;
            my $last_catid = _inner_shopcat2_getlastitemid_byitemid($shopid, $arr[0]);
            eval { $gallery_link = Gallery::System::_inner_sysgallery_getlink_for_shopcat2($_[0],"images/shopcat/$shopid/levels/n",$shopid,$last_catid,$itemname,$arr[0],0); };

            my $renameicon_filename = 'renameicon_nf.png';
            my $descripton = &_inner_shopcat2_get_itemdescr_from_list($shopid,$catid,$last_catid);
            if ($descripton ne '' && $descripton !~ /^[\r\n\s]$/) {$renameicon_filename = 'renameicon.png';}
            my $warning = '<span class="pheix-badge-cont-data pheix-badge-warn"><i class="fa fa-warning"></i></span>';
            if ( -e "conf/pages/shopcat2/$shopid/levels/n/$arr[0]/information.txt" ||
                 -e "conf/pages/shopcat2/$shopid/levels/n/$arr[0]/parameters.txt" ||
                 -e "conf/pages/shopcat2/$shopid/levels/n/$arr[0]/analogs.txt" ||
                 -e "conf/pages/shopcat2/$shopid/levels/n/$arr[0]/files.txt" ) {
                 $warning = '';
            }
            my $s = ( $arr[2] < 0 ) ? 'slogan' : q{};
            $trtdcontent .= qq~
                <tr $trbg>
                    <td width="80%"><p class="cattreetabp $s">$itemname</p></td>
                    <td width="20%" align=right>
                         $gallery_link
                         <div class="pheix-badge-cont">
                         <a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2edititempage&amp;shopid=$shopid&amp;catid=$catid&amp;itemid=$arr[0]&amp;page=$page" class="fa-action-link"><i class="fa fa-paragraph" title="Редактировать описание позиции &quot;$itemname&quot;"></i></a>
                         $warning
                         </div>
                         <a href="javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}', '&amp;action=asc2renameitemform&amp;shopid=$shopid&amp;catid=$catid&amp;itemid=$arr[0]&amp;page=$page')" class="fa-action-link"><i class="fa fa-pencil-square-o" title="Изменить название позиции &quot;$itemname&quot;"></i></a>
                         <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=asc2delitem&amp;shopid=$shopid&amp;catid=$catid&amp;itemid=$arr[0]&amp;page=$page','Вы уверены, что хотите удалить выбранную позицию безвозвратно?')" class="fa-action-link"><i class="fa fa-trash" title="Удалить позицию &quot;$itemname&quot;"></i></a>
                    </td>
                </tr>~;
            $count++;
            }
        }
        #$trtdcontent .= $pagelinkx ? qq~<tr><td colspan=2><p align="center">$pagelinkx</p></td></tr>~ : '';
    } else {
        $trtdcontent .= qq~<tr><td colspan=2><p class=error>Невозможно отобразить список позиций - $catfile не найден!</p></td></tr>~;
    }
    $catitemtable = qq~
        <table cellspacing=0 cellpadding=0 border=0 class=cattreetab_ width="100%"><tr><td>
            <table class="tabselect" cellpadding="10" cellspacing="0" width="100%">$trtdcontent</table>
            <p align="right" class="shopcat-page-links">$pagelinkx</p>
        </td></tr></table>~;
    return ($navi,$catitemtable);
}

#------------------------------------------------------------------------------

sub getV3ItemsTablePagesLinks {
    my ($sid, $catfile, $shopid, $catid, $page) = @_;
    my $pix_on_page = 10;
    open my $ifh, "<", $catfile or die "unable to open $catfile: $!";
    my @data = <$ifh>;
    close $ifh or die "unable to close $catfile";
    my $page_linx ;
    my $pages = 0;

    $pages = floor( $#data / $pix_on_page );
    if ( ( $pages * $pix_on_page <= $#data )
           || ( $pages < 1 ) ) {
               $pages++;
           }
    if ( $page >= $pages ) {
        if ( $page == $pages ) {
            $page = $pages-1;
        } else {
            $page = 0;
        }
    }
    $page_linx = '&nbsp;-&nbsp;';
    for my $i (1..$pages) {
        if ( ( $i - 1 ) != $page ) {
            $page_linx .= "<a href=\"$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2browseitems&amp;shopid=$shopid&amp;catid=$catid&amp;page="
            . ( $i - 1 )
            . "\" class=admpageslink>"
            . $i
            . "</a>&nbsp;-&nbsp;";
        } else {
            $page_linx .=
            "<span class=lightadminpagelink>"
            . $i
            . "</span>&nbsp;-&nbsp;";
        }
    }
    if ( $page >= $pages ) { $page = $pages - 1; }
    my $end = ( ( $page + 1 ) * $pix_on_page ) - 1;
    if ( $end > $#data ) {
        $end = $#data;
    }
    @data = @data[ $page * $pix_on_page .. $end ];
    return ($page_linx , @data);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showItemAjaxForm ( $sesion_id,  $crypted_userid, $shopid, $catid, $prinflag )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод ajax формы добавления новой позиции в каталог
#
#------------------------------------------------------------------------------

sub showItemAjaxForm {
    my ($sid, $shopid, $catid) = @_;
    my $filters;
    my $f_num = getSettingChildNum($MODULE_NAME, 0, 'filters');
    if ($f_num > -1) {
        for my $i (0..$f_num) {
            my $f_name  = 'filter-'.(sprintf "%02d", $i+1);
            my $f_label = 'filters/'.$f_name;
            my $f_descr = encode("utf8", getSingleSetting($MODULE_NAME, 0, $f_label));
            $filters .= $f_descr ne '0' ? qq~<div class="frm-div-marg">
                <input id="$f_name" name="$f_name" value="1" type="checkbox">
                <label for="$f_name" class="checkbox_label"><span></span>$f_descr</label></div>~ : '';
        }
    }
    my $form = qq~
        <div class="popup_header">Добавление новой позиции</div>
        <div class="stxt"><p><a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admshopcat2" class=simple_link><b>Управление каталогом</b></a> / Добавление новой позиции</p></div>
        <form id="addnewitem" class="addnewitemform"
              method=post
              action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=asc2addnewitem&amp;shopid=$shopid&amp;catid=$catid">
        <table width="800px;"><tr><td valign=top width="520">
            <div class="stxt frm-div-marg">Название позиции: <font color="#A0A0A0">[обязательное поле]</font></div>
            <div class="frm-div-marg"><input  placeholder="Введите название позиции"
                 required type=text
                 name="itemname"
                 value="" class=input style="width:500px;"></div>
            <div class="stxt frm-div-marg">ЧПУ страницы <i>(без .html)</i>: <font color="#A0A0A0">[обязательное поле]</font></div>
            <div class="frm-div-marg"><input name="seochpu" value="" class="input" style="width:500px" type="text" placeholder="ЧПУ страницы"></div>
            <div class="stxt frm-div-marg">Текст для тега <i>&lt;TITLE&gt;&lt;/TITLE&gt;</i>:</div>
            <div class="frm-div-marg"><input name="seotitle" value="" class="input" style="width:500px" type="text" placeholder="Текст для тега TITLE"></div>
            <div class="stxt frm-div-marg">Текст для тега <i>&lt;META name="description" /&gt;</i>:</div>
            <div class="frm-div-marg"><input name="seometadescr" value="" class="input" style="width:500px" type="text" placeholder="Текст для META.description"></div>
            <div class="stxt frm-div-marg">Текст для тега <i>&lt;META name="keywords" /&gt;</i>:</div>
            <div class="frm-div-marg"><input name="seometakeys" value="" class="input" style="width:500px" type="text" placeholder="Текст для META.keywords"></div>
            <div class="stxt frm-div-marg">Краткое описание позиции: <font color="#A0A0A0">[обязательное поле]</font></div>
            <div class="frm-div-marg"><textarea name="itemdescr"
                                                placeholder="Введите краткое описание позиции"
                                                class=input required
                                                style="width:500px; height:200px;"></textarea></div>
            <div class="frm-div-marg"><input type=submit value="Сохранить изменения" class="button" style="width:150px"></div>
        </td><td valign=top class="td-filtr" width="280">
            <div class="stxt frm-div-filtrs">
            <div class="frm-div-marg frm-div-marg-header">Настройки фильтров для новой позиции:</div>
            $filters
            </div>
        </td></tr></table></form>
    ~;
    return $form;
}

#------------------------------------------------------------------------------

sub getFiltersMask {
    my @filters = @_;
    my $mask;
    for my $i (0..$#filters) {
        if ($filters[$i] == 1) {
            $mask |= 1 << $i;
        }
    }
    #my $d_1 = $mask & 15;
    #my $d_2 = $mask & 240;
    #my $d_3 = $mask & 3840;
    return $mask;
}

#------------------------------------------------------------------------------

sub getItemMaxUniqId {
    my ($shopid, $catid) = @_;
    my $rc;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        open my $ifh, "<", $catfile or die "unable to open $catfile: $!";
        my @data = <$ifh>;
        close $ifh or die "unable to close: $!";
        my @itemids;
        for my $i (0..$#data) {
            $data[$i]  =~ s/[\r\n\t]//gi;
            my @dbcol  = split( /\|/, $data[$i] );
            my $itemid;
            if ( $dbcol[-1] =~ /\[\*\]\[([\d]+)\]/ ) {
                $itemid = $1;
            }
            if ($itemid) {
                if ($itemid > 9999) {
                    $itemid = $itemid % 10000
                }
                push(@itemids, $itemid);
            }
        }
        if (@itemids) {
            my $max = (sort { $b <=> $a } @itemids)[0];
            if ($max > 0) {
                $rc = $max;
            }
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------

sub addNewItemToV3 {
    my ($shopid, $catid, $item_id_new, $i_name, $i_descr, $cgi_p) = @_;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        open my $ifh, "<", $catfile or die "unable to open $catfile: $!";
        my @data = <$ifh>;
        close $ifh or die "unable to close: $!";
        my $record = $data[-1];
        $record =~ s/[\n\r\t]//gis;
        my @dbrow  = split( /\|/, $record );
        $dbrow[-1] = "[*][$item_id_new]$i_name";

        my $str = substr( md5( $item_id_new.$i_name ), 4, 4 );
        my $hashed_id = unpack( 'L', $str );
        $hashed_id = sprintf( "%012u", $hashed_id );
        $dbrow[0] = $hashed_id;

        my @new_data = (join ("|", @dbrow)."\n", @data);
        open my $ofh, ">", $catfile or die "unable to open $catfile: $!";
        if (flock $ofh, LOCK_EX) {
            print $ofh @new_data;
            truncate( $ofh, tell($ofh) );
            flock $ofh, LOCK_UN or die "unable to unlock $catfile: $!";
            my $retcont = "Позиция <b>$i_name ($item_id_new)</b> успешно добавлена в каталог";

            # save description
            _inner_shopcat2_save_itemdescr($shopid, $catid, $dbrow[0], $i_descr);

            # save seodata
            my $ismodify = Shopcat::V3::setSeoData(
                               $shopid, $catid, $dbrow[0], $cgi_p);
            if ($ismodify < 0) {
                $retcont .= "<br><b>Ошибка при сохранении SEO-данных!</b>";
            } else { $retcont .= "<br>SEO-данные успешно сохранены!"; }
            return (qq~<p class=hintcont>$retcont<p>~, 1)
        }
        close $ofh or die "unable to close: $!";
    }
    return (qq~<p class=hintcont>Добавлени позиции не выполнено<p>~, 0)
}

#------------------------------------------------------------------------------

sub decodeFiltersMaskById {
    my ($shopid, $itemtid) = @_;
    my @decodedfilters;
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        open my $ifh, "<", $catfile or die "unable to open $catfile: $!";
        my @data = <$ifh>;
        close $ifh or die "unable to close: $!";
        for my $i (0..$#data) {
            $data[$i]  =~ s/[\r\n\t]//gi;
            my @dbcol  = split( /\|/, $data[$i] );
            if ($dbcol[0] eq $itemtid) {
                my $itemlastid;
                if ($dbcol[-1] =~ /\[\*\]\[([\d]+)\]/) {
                    $itemlastid = $1;
                    if ($itemlastid > 9999) {
                        my $decodesmask = ($itemlastid - ($itemlastid % 10000)) / 10000;
                        my $f_num = getSettingChildNum($MODULE_NAME, 0, 'filters');
                        for my $i (0..$f_num) {
                            push(@decodedfilters, $decodesmask & 1 || 0);
                            $decodesmask = $decodesmask >> 1;
                        }
                    }
                }
            }
        }
    }
    return @decodedfilters;
}

#------------------------------------------------------------------------------

sub getFiltersMaskFromCGI {
    my ($co) = @_;
    my @i_filters;
    my $filter_mask;
    my $f_num = Config::ModuleMngr::getSettingChildNum($MODULE_NAME, 0, 'filters');
    if ($f_num > -1) {
        for my $i (0..$f_num) {
            my $f_name = 'filter-'.(sprintf "%02d", $i+1);
            push(@i_filters, $co->param($f_name) || 0)
        }
    }
    if (@i_filters) {
        $filter_mask = getFiltersMask(@i_filters);
    }
    return $filter_mask;
}

#------------------------------------------------------------------------------

sub setItemFilters {
    my ($shopid, $itemid, $mask) = @_;
    my $pricefile = getPriceFileFullPath($shopid);
    if (-e $pricefile) {
        open my $ifh, "<", $pricefile or die "unable to open $pricefile: $!";
        my @data = <$ifh>;
        close $ifh or die "unable to close: $!";
        for my $i (0..$#data) {
            $data[$i]  =~ s/[\r\n\t]//gi;
            my @dbcol  = split( /\|/, $data[$i] );
            if ($itemid eq $dbcol[0]) {
                my $itemid;
                my $itemid_new;
                if ( $dbcol[-1] =~ /\[\*\]\[([\d]+)\]/ ) {
                    $itemid = $1;
                }
                my $itemname = $dbcol[-1];
                $itemname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                if ($itemid > 9999) {
                    my $unmasked_id = sprintf("%04d", $itemid % 10000);
                    $itemid_new = $mask . $unmasked_id;
                    #print "<h1>$mask + $unmasked_id = $itemid_new</h1>";
                    #return;

                } else {
                    $itemid_new = $mask . $itemid;
                }
                if ($itemid_new) {
                    $dbcol[-1] = "[*][$itemid_new]$itemname";
                    $data[$i] = (join "|", @dbcol)."\n";
                    open my $ofh, ">", $pricefile or die "unable to open $pricefile: $!";
                    flock($ofh, LOCK_EX);
                    print $ofh @data;
                    flock($ofh, LOCK_UN);
                    close $ofh or die "unable to close: $!";
                    last;
                }
            } else {
                $data[$i] .= "\n";
            }
        }
    }
}

#------------------------------------------------------------------------------

sub getV3EditTabs {
    my ($entity, $sid, $shopid, $id, $shw1, $shw2, $shw3, $shw4) = @_;
    my $_lc;
    my $_bc;
    my $_fsets = getSingleSetting( $MODULE_NAME, 1, 'filtersmode' );
    my $prms = Shopcat::ShopParams->new;
    my @filters = $prms->getParamDataArray();
    if ($_fsets !~ /^[\d]+$/ || $_fsets < 0 || $_fsets > 2) {
        $_fsets = 0;
    }
    if (!$shw1) {
        my $filter = 0;
        my $showfl = 0;
        my $legend = 'information';
        my $_blk = getV3EditBlock($sid, $shopid, $id, $legend, $entity);
        if ($_fsets == 1) {
            $filters[$filter] =~ s/\[([\d]+)\]//gi;
            if ($filters[$filter] ne '0') {
                $_blk = qq~<div id="tab_item-2">$_blk</div>~;
                $legend = $filters[$filter] || $legend;
                $showfl = 1;
            }
        }
        if (($_fsets != 1) || ($_fsets == 1 && $showfl == 1)) {
            $_lc .= qq~<li role="tab" aria-controls="tab_item\-$filter"
                      class="stxt resp-tab-item">$legend</li>~;
            $_bc .= $_blk;
        }
    }
    if (!$shw2) {
        my $filter = 1;
        my $showfl = 0;
        my $legend = $entity == 1 ? 'parameters' : 'primenenie';
        my $_blk = getV3EditBlock($sid, $shopid, $id, $legend, $entity);
        if ($_fsets == 1) {
            $filters[$filter] =~ s/\[([\d]+)\]//gi;
            if ($filters[$filter] ne '0') {
                $_blk = qq~<div id="tab_item-2">$_blk</div>~;
                $legend = $filters[$filter] || $legend;
                $showfl = 1;
            }
        }
        if (($_fsets != 1) || ($_fsets == 1 && $showfl == 1)) {
            $_lc .= qq~<li role="tab" aria-controls="tab_item\-$filter"
                      class="stxt resp-tab-item">$legend</li>~;
            $_bc .= $_blk;
        }
    }
    if (!$shw3) {
        my $filter = 2;
        my $showfl = 0;
        my $legend = $entity == 1 ? 'analogs' : 'video';
        my $_blk = getV3EditBlock($sid, $shopid, $id, $legend, $entity);
        if ($_fsets == 1) {
            $filters[$filter] =~ s/\[([\d]+)\]//gi;
            if ($filters[$filter] ne '0') {
                $_blk = qq~<div id="tab_item-2">$_blk</div>~;
                $legend = $filters[$filter] || $legend;
                $showfl = 1;
            }
        }
        if (($_fsets != 1) || ($_fsets == 1 && $showfl == 1)) {
            $_lc .= qq~<li role="tab" aria-controls="tab_item\-$filter"
                      class="stxt resp-tab-item">$legend</li>~;
            $_bc .= $_blk;
        }
    }
    if (!$shw4) {
        my $filter = 3;
        my $showfl = 0;
        my $legend = 'files';
        my $_blk = getV3EditBlock($sid, $shopid, $id, $legend, $entity);
        if ($_fsets == 1) {
            $filters[$filter] =~ s/\[([\d]+)\]//gi;
            if ($filters[$filter] ne '0') {
                $_blk = qq~<div id="tab_item-2">$_blk</div>~;
                $legend = $filters[$filter] || $legend;
                $showfl = 1;
            }
        }
        if (($_fsets != 1) || ($_fsets == 1 && $showfl == 1)) {
            $_lc .= qq~<li role="tab" aria-controls="tab_item\-$filter"
                      class="stxt resp-tab-item">$legend</li>~;
            $_bc .= $_blk;
        }
    }
    my $cntnt_ .= qq~
    <script type="text/javascript" src="js/easytabs-init.js"></script>
    <div class="pheix-params-tabs-container">
     <div id="horizontalTab">
      <ul class="resp-tabs-list">$_lc</ul>
      <div class="resp-tabs-container">$_bc</div>
     </div>
    </div>
    <div class="shopcat-margin"></div>
    ~;
    return $cntnt_;
}

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
# getShopCatV3Sitemap()
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов
# Получить массив страниц каталога для публикации в карте сайта.
#
#------------------------------------------------------------------------------

sub getShopCatV3Sitemap {
    my @pages;
    my $_off  = getV3PowerStatus();
    my $_set  = getSingleSetting($MODULE_NAME, 1, 'exportsitemap');
    if ( -e $MODULE_DB_FILE  && ( $_off == 0 ) && ( $_set == 1 ) )  {
        my @db    = getLinesFromFile($MODULE_DB_FILE);
        my $proto = getProtoSrvName();
        use Cwd qw(cwd);
        my $dbfts = strftime(
            '%Y-%m-%d',
            localtime(${stat($MODULE_DB_FILE)}[9])
        );
        foreach my $rec (@db) {
            chomp($rec);
            my @cols = split( /\|/, $rec );
            my $shopid = $cols[0];
            for my $col (@cols)  {
                if ( $col =~ /\[([\d]+)\]/ ) {
                    my $id = $1;
                    if ( $col =~ /\[\*\]/ ) {
                        my $fname     = getPriceFileFullPath($shopid);
                        if (-e $fname) {
                            my $ts = strftime(
                                '%Y-%m-%d',
                                localtime(${stat($fname)}[9])
                            );
                            open my $fh, "<", $fname;
                            my @rows = <$fh>;
                            close $fh;
                            for my $row (@rows)  {
                                my @c = split( /\|/, $row );
                                if (
                                    $c[-1] =~ /\[\*\]\[([\d]+)\]/ &&
                                    $c[2] ne '-1' &&
                                    $#c == $PRICEFILE_DB_COLUMS
                                ) {
                                    my $cid  = $1;
                                    #my $actn =
                                    #    'shopcatalog_'. $cid . '_' . $shopid;
                                    my $actn = get4PUrl($shopid, $cid);
                                    my $url  = $proto.'/'.$actn;
                                    if (
                                        !(map {
                                            $url eq $_->{loc} ? ($_) : ()
                                        } @pages)
                                    ) {
                                        my %rec  = (
                                            'loc'     => $url,
                                            'lastmod' => $ts
                                        );
                                        push @pages, \%rec;
                                    }
                                }
                            }
                        }
                    }
                    elsif ( $col =~ /\[\*\*\]/ && $id > 1) {
                        #my $actn =
                        #    'shopcatalog_' . $id . q{_} . $shopid . '.html';
                        my $actn = get4PUrl(0, $id);
                        my $url  = $proto.'/'.$actn;
                        if ( !(map { $url eq $_->{loc} ? ($_) : () } @pages) ) {
                            my %rec  = (
                                'loc'     => $url,
                                'lastmod' => $dbfts
                            );
                            push @pages, \%rec;
                        }
                    }
                    else {
                        # my $actn = 'shopcatalog_' . $id . '.html';
                        my $actn = get4PUrl(0, $id);
                        my $url  = $proto.'/'.$actn;
                        if ( !(map { $url eq $_->{loc} ? ($_) : () } @pages) ) {
                            my %rec  = (
                                'loc'     => $url,
                                'lastmod' => $dbfts
                            );
                            push @pages, \%rec;
                        }
                    }
                }
            }
        }
    }
    return @pages;
}

#------------------------------------------------------------------------------

END { }

1;
