if (   $access_modes[2] == 1
    || $access_modes[2] == 2 )    # actions compatible with access modes: r,rw
{

    # start of read-only actions
    if ( $env_params[1] eq 'admshopcat2' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Shopcat::V3::show_shopcat2_startpage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ), 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2loadcats' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Shopcat::V3::doCatsLoad(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2],
                $env_params[3],
                0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2renamecatform' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Shopcat::V3::_inner_shopcat2_rename_cat_form( $env_params[0],
                $env_params[2], $env_params[3] );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2editnm1levdescr' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Shopcat::V3::show_shopcat2_editcategory_page( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $env_params[3], 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2browseitems' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Shopcat::V3::showV3ItemsList( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $env_params[3], $env_params[4] =~ /^[\d]+$/ ? $env_params[4] : 0, 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2renameitemform' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Shopcat::V3::_inner_shopcat2_rename_cat_form( $env_params[0],
                $env_params[2], $env_params[3], $env_params[4], $env_params[5] =~ /^[\d]+$/ ? $env_params[5] : 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2edititempage' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Shopcat::V3::show_shopcat2_edititem_page( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $env_params[3], $env_params[4], $env_params[5] =~ /^[\d]+$/ ? $env_params[5] : 0, 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2edititempriceform' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Shopcat::V3::_inner_shopcat2_cat_item_price_form(
                $env_params[0], $env_params[2], $env_params[3] );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2itemseditor' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Shopcat::V3::show_shopcat2_item_in_editor( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $env_params[3], $env_params[4], 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2categoryeditor' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Shopcat::V3::show_shopcat2_categorydescr_in_editor(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $env_params[3], $env_params[4], 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2editcatpage' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Shopcat::V3::showEditCatPage( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $env_params[3], 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2cateditor' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Shopcat::V3::showCatDescrEditor( $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2], $env_params[3], $env_params[4], 0 );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    #v3.0
    if ( $env_params[1] eq 'asc2additemform' ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Shopcat::V3::showItemAjaxForm( $env_params[0], $env_params[2] || 0, $env_params[3] || 0);
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    # end of read-only actions

    if ( $access_modes[2] == 2 )  # actions compatible just with access mode: rw
    {

        # start of read-write actions

        if ( $env_params[1] eq 'asc2downloadcat' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @upload_result =
                  Shopcat::V3::_inner_shopcat2_upload_catalog_to_server(
                    $co->param('uploadcatalog') );
                Shopcat::V3::show_shopcat2_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $upload_result[0], $upload_result[1], 1 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        #v3.0
        if ( $env_params[1] eq 'asc2delcat' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @returnstatus = ( '', 0 );
                my $parent_cat = -1;
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    $parent_cat = Shopcat::V3::getParentCatId(
                        $env_params[2], $env_params[3] );
                    @returnstatus =
                      Shopcat::V3::delCategory(
                        $env_params[2], $env_params[3], 1 );
                }
                Shopcat::V3::show_shopcat2_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $returnstatus[0], $returnstatus[1], 1, $parent_cat );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        #v3.0
        if ( $env_params[1] eq 'asc2renamecat' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @returnstatus = ( '', 0 );
                my $parent_cat = -1;
                my $newname = $co->param("categoryname");
                $newname =~ s/\"/\&quot;/g;
                if (   $env_params[2] =~ /^[\d]+$/
                    && $env_params[3] =~ /^[\d]+$/
                    && $newname ne ''
                    && $newname !~ /^[\r\n\s]+$/ )
                {
                    @returnstatus =
                      Shopcat::V3::setCatItemName(
                        $env_params[2], $env_params[3], $newname );
                    $parent_cat = Shopcat::V3::getParentCatId(
                        $env_params[2], $env_params[3] );
                    my $ismodify = Shopcat::V3::setSeoData(
                        $env_params[2], $env_params[3], -1, $co);
                }
                Shopcat::V3::show_shopcat2_startpage( $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $returnstatus[0], $returnstatus[1], 1, $parent_cat );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        #v3.0
        if ( $env_params[1] eq 'asc2renameitem' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @returnstatus = ( "", 0 );
                my $newname      = $co->param("categoryname");
                my $newdescr     = $co->param("itemdescr");
                $newname =~ s/\"/\&quot;/g;
                $newdescr =~ s/\"/\&quot;/g;
                if (   $env_params[2] =~ /^[\d]+$/
                    && $env_params[3] =~ /^[\d]+$/
                    && $env_params[4] =~ /^[\d]+$/
                    && $newname ne ''
                    && $newname !~ /^[\r\n\s]+$/ )
                {
                    Shopcat::V3::_inner_shopcat2_save_itemdescr( $env_params[2],
                        $env_params[3], $env_params[4], $newdescr );
                    @returnstatus =
                      Shopcat::V3::_inner_shopcat2_rename_cat_in_catalog(
                        Shopcat::V3::getPriceFileFullPath($env_params[2]),
                        $env_params[4], $newname, 1 );
                    my $ismodify = Shopcat::V3::setSeoData(
                        $env_params[2], $env_params[3], $env_params[4], $co );
                    $ismodify = Shopcat::V3::setItemFilters($env_params[2], $env_params[4], Shopcat::V3::getFiltersMaskFromCGI($co));
                }
                Shopcat::V3::showV3ItemsList(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2], $env_params[3],
                    $env_params[5] =~ /^[\d]+$/ ? $env_params[5] : 0,
                    $returnstatus[0],
                    $returnstatus[1],
                    $SHOW_MESS
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        #v3.0
        if ( $env_params[1] eq 'asc2delitem' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @returnstatus = ( '', 0 );
                if (   $env_params[2] =~ /^[\d]+$/
                    && $env_params[3] =~ /^[\d]+$/
                    && $env_params[4] =~ /^[\d]+$/ )
                {
                    @returnstatus =
                      Shopcat::V3::_inner_shopcat2_delete_item_from_catalog(
                        $env_params[2], $env_params[3], $env_params[4] );
                }
                my $check_cat_id =
                  Shopcat::V3::_inner_shopcat2_existed_id(
                        $env_params[2],
                        $env_params[3] );
                if ( $check_cat_id == 1 ) {
                    Shopcat::V3::showV3ItemsList(
                        $env_params[0],
                        Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                        $env_params[2],
                        $env_params[3],
                        $env_params[5] =~ /^[\d]+$/ ? $env_params[5] : 0,
                        $returnstatus[0],
                        $returnstatus[1],
                        $SHOW_MESS
                    );
                }
                else {
                    Shopcat::V3::delCategory(
                        $env_params[2], $env_params[3], 0 );
                    Shopcat::V3::show_shopcat2_startpage(
                        $env_params[0],
                        Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                        $returnstatus[0],
                        $returnstatus[1],
                        1
                    );
                }
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        #v3.0
        if ( $env_params[1] eq 'asc2catpostdescr' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @returnstatus = ( '', 0 );
                if (   $env_params[2] =~ /^[\d]+$/
                    && $env_params[3] =~ /^[\d]+$/
                    && $env_params[4] =~ /^[a-z]+$/ )
                {
                    my $datatosave = $co->param('editorcontent');
                    @returnstatus =
                      Shopcat::V3::modfyCatDescr(
                        $env_params[2],
                        $env_params[3],
                        $env_params[4], $datatosave );
                }
                Shopcat::V3::showEditCatPage(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2],
                    $env_params[3],
                    $returnstatus[0],
                    $returnstatus[1],
                    1
                );
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        #v3.0
        if ( $env_params[1] eq 'asc2itemspostdescr' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @returnstatus = ( '', 0 );
                if (   $env_params[2] =~ /^[\d]+$/
                    && $env_params[3] =~ /^[\d]+$/
                    && $env_params[4] =~ /^[a-z]+$/ )
                {
                    my $datatosave = $co->param('editorcontent');
                    @returnstatus =
                      Shopcat::V3::_inner_shopcat2_update_item_description(
                        $env_params[2],
                        $env_params[3],
                        $env_params[4], $datatosave );
                }
                my $catid = Shopcat::V3::_inner_shopcat2_getcatid_byitemid(
                    $env_params[2], $env_params[3] );
                Shopcat::V3::show_shopcat2_edititem_page(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2],
                    $catid,
                    $env_params[3],
                    0,
                    $returnstatus[0],
                    $returnstatus[1],
                    1
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        #v3.0
        if ( $env_params[1] eq 'asc2itemssaveprice' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @returnstatus = ( '', 0 );
                if ( $env_params[2] =~ /^[\d]+$/ &&
                     $env_params[3] =~ /^[\d]+$/) {
                    my $newprice = $co->param('newprice');
                    @returnstatus =
                      Shopcat::V3::_inner_shopcat2_update_item_price(
                        $env_params[2], $env_params[3], $newprice );
                }
                my $catid = Shopcat::V3::_inner_shopcat2_getcatid_byitemid(
                    $env_params[2], $env_params[3]);
                Shopcat::V3::show_shopcat2_edititem_page(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2],
                    $catid,
                    $env_params[3],
                    $returnstatus[0],
                    $returnstatus[1],
                    1
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        #v3.0
        if ( $env_params[1] eq 'asc2categorypostdescr' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @returnstatus = ( '', 0 );
                if (   $env_params[2] =~ /^[\d]+$/
                    && $env_params[3] =~ /^[\d]+$/
                    && $env_params[4] =~ /^[a-z]+$/ )
                {
                    my $datatosave = $co->param('editorcontent');
                    @returnstatus =
                      Shopcat::V3::_inner_shopcat2_update_categorynm1_description(
                        $env_params[2], $env_params[3], $env_params[4], $datatosave );
                }
                Shopcat::V3::show_shopcat2_editcategory_page(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2],
                    $env_params[3],
                    $returnstatus[0],
                    $returnstatus[1],
                    1 );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        #v3.0
        if ( $env_params[1] eq 'asc2addnewitem' ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @returnstatus = ( '', 0 );
                if ( defined $env_params[2] && $env_params[2] =~ /^[\d]+$/ ) {
                    my $shopid = $env_params[2];
                    my $catid  = defined $env_params[3] && $env_params[3] =~ /^[\d]+$/ ? $env_params[3] : 0;
                    my $i_name = $co->param("itemname");
                    my $i_desc = $co->param("itemdescr");
                    my $filter_mask = Shopcat::V3::getFiltersMaskFromCGI($co);
                    my $maxid = Shopcat::V3::getItemMaxUniqId($shopid, $catid);
                    if ($maxid) {
                        my $id = sprintf("%04d", ($maxid + 1));
                        if ($filter_mask > 0) {
                            $id = $filter_mask . 
                                sprintf("%04d", ($maxid + 1) % 10000);
                        }
                        if ($i_desc ne q{}) {
                            @returnstatus = Shopcat::V3::addNewItemToV3(
                                $shopid, $catid, $id, $i_name, $i_desc, $co );
                        }
                    }
                }
                Shopcat::V3::showV3ItemsList(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $env_params[2],
                    $env_params[3],
                    0,
                    @returnstatus,
                    $SHOW_MESS
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        # end of read-write actions

    }    # Access modes: rw
}    # Access modes: r
