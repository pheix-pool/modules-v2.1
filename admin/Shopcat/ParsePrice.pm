package Shopcat::ParsePrice;

use strict;
use warnings;

##############################################################################
#
# File   :  ParsePrice.pm
#
##############################################################################
#
# Главный пакет разбора загружаемого на сервер прайс-листа (*.txt)
#
##############################################################################

our ( @ISA, @EXPORT );

BEGIN {
    #for all servers
    use lib '../';
    use lib '../../extlibs/';
    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';

    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
      doFileParse
      saveFileToPricePoll
      createPriceRecord
      delFiles
      getFilesArray
    );
}

#---------------------------------------------------------------------------------

my @debug_data = ();

#---------------------------------------------------------------------------------

use Cwd;
use Lingua::Translit;
use Digest::MD5 qw(md5);
use File::Path;
use Archive::Tar;
use File::Find;
use Encode;
use Config::ModuleMngr qw(getSingleSetting getSettingChildNum);

#---------------------------------------------------------------------------------

my $GLOBAL_FILENAME;
my $ROOT_CATS_STR;
my $DEBUG_FILENAME          = getcwd.'/parse-price-debug.txt';
my $GLOBAL_PATH             = 'conf/system/pricepool';
my $GLOBAL_SHOPCAT_DBFILE   = 'conf/system/shopcat2.tnk';
my $GLOBAL_IDLEGENDA_DBFILE = 'conf/system/.sc2idlegenda.tnk';
my $GLOBAL_DATAPOOL         = 'conf/pages/shopcat2';
my $GLOBAL_BACKUP_PATH      = 'conf/backup/shopcat';
my $GLOBAL_MAINCATS_FILE    = 'conf/system/.sc2maincats.tnk';
my @GLOBAL_PRICE_LIST       = ();
my @GLOBAL_ID_LEGENDA       = ();
my @ROOT_CATS_IDS           = ();
my $GLOBAL_ID               = 0;
my $GLOBAL_TOTAL            = 0;
my $DEBUG                   = 0;
my $GLOBAL_START_ID         = 1982;
my $backup                  = Archive::Tar->new;
my $FNUM                    = getSettingChildNum('Shopcat', 0, 'filters');
my $FILTERS                 = getFiltersHash();

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# ($stat, $mess) = doFileParse ( $catfilename, $settings)
#---------------------------------------------------------------------------------
#
# $catfilename - путь к разбираемому файлу прайс-листа.
# $settings - дополнительные настройки, используемые при разборе
# Получение keytag по зашифрованному идентификатору пользователя.
# Функция разбора загружаемого на сервер прайс-листа (*.txt)
#
#---------------------------------------------------------------------------------

sub doFileParse {
    my ($catfilename, $settings) = @_;
    my $retcontent;
    my $retcode = 0;

    if ( !( -e $catfilename ) ) {
        $retcontent = qq~
            <p class=hintcont>Не найден файла для разбора -
            <font class=hintspan>$catfilename</font>!</p>
        ~;

    } else {
        @ROOT_CATS_IDS = ();
        my @maincat    = ();
        my $tr = new Lingua::Translit("GOST 7.79 RUS");
        doBackUp();
        doConvertToUtf($catfilename);
        delFiles($GLOBAL_PATH);
        unlink($GLOBAL_SHOPCAT_DBFILE);

        if ( !( -e $GLOBAL_PATH ) ) { mkpath($GLOBAL_PATH); }

        my @db_index_records = ();
        my $GLOBAL_CAT;

        #my $catfilename = "citilink.txt";

        my $counter_max = 0;
        my $counter     = 0;
        my $id_counter  = 0;
        my $initial_id  = $GLOBAL_START_ID;
        my $level_a_cnt = 0;
        my $level_b_cnt = 1000;

        open my $fh, '<', $catfilename or die 'unable to open '.$catfilename.': '.$!;
        @maincat = <$fh>;
        close $fh;

        $maincat[0] =~ s/^[\S]+//;
        my @current_levels_str;

        for ( my $i = 0 ; $i <= $#maincat ; $i++ ) {

            #for(my $i=0; $i<=100; $i++) {
            my $record = $maincat[$i];
            $record =~ s/[\n\r]+//g;

            #print $record;
            if ( $record =~ /^\t\t\t\t\t/ ) {
                if ( $counter_max < $counter ) { $counter_max = $counter; }
                $counter = 0;
                if ( $maincat[ $i + 1 ] !~ /^\t\t\t\t\t/ ) {
                    $record =~ s/[\t]+//g;
                    $record =~ s/[\s]+$//g;
                    $record =~ s/^[\s]+//g;
                    $record =~ s/[\s]+/ /g;
                    if ( $tr->can_reverse() && $record ne '' ) {
                        @db_index_records = saveFileToPricePoll(@db_index_records);
                        # $GLOBAL_ID = $initial_id + $id_counter;
                        my $filename = lc( $tr->translit("$record") );
                        $filename =~ s/^[\s]+//g;
                        $filename =~ s/[\s]+$//g;
                        $filename =~ s/[\;\~\!\?\*\'\`\(\)\"\\\/\-\+\=\$\#\@\r\n\t\.\,\&]+//ig;
                        $filename =~ s/[\s]+/_/g;
                        my $file_prefix = sprintf( "%04d", $id_counter + 1 );
                        $GLOBAL_FILENAME =
                          $file_prefix . "_" . $filename . ".txt";

                        my $glob_fn_hash = substr( md5( "$GLOBAL_CAT -> $record -> $filename\.txt" ), 0, 4 ); # Do not use prefix dueto prefix can be changed
                        $GLOBAL_ID = unpack( 'L', $glob_fn_hash );               # Convert to 4-byte integer (long)
                        $GLOBAL_ID = sprintf("%012u",$GLOBAL_ID); # Zero-fill to align to 10-digits length

                        #print "$filename\n";
                        my $chck_rc   = doRootCatSearch($GLOBAL_CAT);
                        my $chck_rcid = -1;
                        if ($chck_rc =~ /^\[([\d]+)\]/) {
                            $chck_rcid = $1;
                            if (isUniqRootId($chck_rcid)) {
                                $ROOT_CATS_STR = $chck_rc;
                            }
                        }

                        my @levelcols;
                        if  ( $ROOT_CATS_STR ne '' && $GLOBAL_CAT ne '' ) {
                            @levelcols = ( $ROOT_CATS_STR, "[$level_a_cnt]" . $GLOBAL_CAT );
                        } elsif ( $ROOT_CATS_STR ne '' || $GLOBAL_CAT ne '' ) {
                            @levelcols = ( $ROOT_CATS_STR ne '' ? $ROOT_CATS_STR : "[$level_a_cnt]" . $GLOBAL_CAT);
                        }

                        push(

                            @db_index_records,
                            join( "|",
                                $GLOBAL_ID,
                                #&doRootCatSearch($GLOBAL_CAT),
                                @levelcols ,
                                "[**][$level_b_cnt]" . $record,
                                "[*][$file_prefix]" . $GLOBAL_FILENAME )
                        );

                        $id_counter++;
                        $level_b_cnt++;
                    }
                    else {
                        # error: $tr->can_reverse() && $record ne '' fails
                        ;
                    }
                }
                else {
                    $record =~ s/[\t]+//g;
                    $record =~ s/[\s]+$//g;
                    $record =~ s/[\s]+/ /g;
                    $record =~ s/^[\s]+//g;
                    if ($record) {
                        $GLOBAL_CAT = $record;
                        $level_a_cnt++;
                    }
                    #doDebugPrint("\$record=$record; \$GLOBAL_CAT=$GLOBAL_CAT; \$maincat[$i]=<$maincat[$i]>; \$maincat[".($i+1)."]=<$maincat[$i+1]>", 1);
                }
            }
            else {
                # parse positions
                my $rc =
                  createPriceRecord( $db_index_records[$#db_index_records],
                    $record, $settings );
                $counter++;
            }
        }

        if (@db_index_records) {
            @db_index_records = saveFileToPricePoll(@db_index_records);
            open my $fh, '>', $GLOBAL_SHOPCAT_DBFILE or die 'unable to open '.$GLOBAL_SHOPCAT_DBFILE.': '.$!;
            print $fh join( "\n", @db_index_records );
            close $fh;
            open $fh, '>', $GLOBAL_IDLEGENDA_DBFILE or die 'unable to open '.$GLOBAL_IDLEGENDA_DBFILE.': '.$!;
            print $fh join( "\n", @GLOBAL_ID_LEGENDA );
            close $fh;
            my $numcats = $#db_index_records + 1;
            $retcode    = 1;
            $retcontent = qq~
            <p class=hintcont>Успешно создан индексный файл каталога -
            <font class=hintspan>$GLOBAL_SHOPCAT_DBFILE</font>!<br>
            Всего категорий: <font class=hintspan>$numcats</font><br>
            Всего позиций: <font class=hintspan>$GLOBAL_TOTAL</font></p>
 	    ~;
        }
        else {
            $retcontent = qq~
            <p class=hintcont>Невозможно создать индексный файл -
            <font class=hintspan>$GLOBAL_SHOPCAT_DBFILE</font>!<br>
            Длина массива (\@db_index_records): <font class=hintspan>$#db_index_records</font></p>
        ~;
        }
    }
    return ( $retcontent, $retcode );
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# @rc = saveFileToPricePoll ( @indexdb )
#---------------------------------------------------------------------------------
#
# @indexdb - массив строк БД shopcat2.tnk.
# Функция сохраняет файл конечного прайс-листа и обновляет массив строк БД
#
#---------------------------------------------------------------------------------

sub saveFileToPricePoll {
    my @indexdb = @_;
    if (@GLOBAL_PRICE_LIST) {
        if ($DEBUG) {
            print("try to save price to $GLOBAL_PATH/$GLOBAL_FILENAME...");
        }
        open my $fh, '>', $GLOBAL_PATH.'/'.$GLOBAL_FILENAME or die 'unable to open '.$GLOBAL_PATH.'/'.$GLOBAL_FILENAME.': '.$!;
        print $fh join( "\n", @GLOBAL_PRICE_LIST );
        close $fh;
        @GLOBAL_PRICE_LIST = ();
        if ($DEBUG) {
            print("ok\n");
        }
    }
    else {
        if ( $GLOBAL_ID > 0 ) {
            my $lastel = pop(@indexdb);
        }
    }
    return @indexdb;
}


#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# $rc = createPriceRecord ( $db_index_record )
#---------------------------------------------------------------------------------
#
# $db_index_record - строка из загруженного файла прайс-листа.
# Функция разбирает строку с данными по позиции и формирует масссив для
# сохранения в базу данных (файл): 0 - ошибка, 1 - успешное завершение.
#
#---------------------------------------------------------------------------------

sub createPriceRecord {
    my ($db_index_record, $rawstr, $parcefilters) = @_;
    $rawstr =~ s/[ ]+/ /g;
    $rawstr =~ s/[\r\n]+//g;
    $rawstr =~ s/[ ]+$//g;
    $rawstr =~ s/^[ ]+//g;
    if ( $rawstr ne '' && $rawstr !~ /^Бусиново/ ) {
        my @arr = split( /\t/, $rawstr );
        if ( $arr[$#arr] ) {

            for ( my $i = 0 ; $i <= $#arr ; $i++ ) {
                if (   $arr[$i] =~ /^[\s]+$/
                    || $arr[$i] eq ''
                    || $arr[$i] =~ /^[\r\n]+$/ )
                {
                    $arr[$i] = 0;
                }
                $arr[$i] =~ s/[\s]+/ /g;
                $arr[$i] =~ s/^[\s]+//g;
                $arr[$i] =~ s/[\s]+$//g;
                $arr[$i] =~ s/^\"//g;
                $arr[$i] =~ s/\"$//g;
                $arr[$i] =~ s/\"\"/\"/g;
            }

            my $str = substr( md5( $arr[5] ), 4, 4 );
            my $hashed_id =
              unpack( 'L', $str );    # Convert to 4-byte integer (long)
            $hashed_id = sprintf( "%012u", $hashed_id )
              ;                       # Zero-fill to align to 10-digits length

            my @parce_db_record = split( /\|/, $db_index_record );

# 1417805614|reserved|1700|[1]СКЛАДСКАЯ ТЕХНИКА|[**][3001]Гидравлические тележки Noblelift|[*][4001]СТАНДАРТНЫЕ

            my $item_price     = 0;
            my @avail          = ( '0', '0', '0' );
            my $item_logicalid = 1000 + $#GLOBAL_PRICE_LIST + 1;
            if ( $arr[0] eq '+' || $arr[1] eq '+' || $arr[2] eq '+' ) {
                if ( $arr[0] eq '+' ) { $avail[0] = 1; }
                if ( $arr[1] eq '+' ) { $avail[1] = 1; }
                if ( $arr[2] eq '+' ) { $avail[2] = 1; }
                $item_price = $arr[8];
                $item_price =~ s/\,/\./g;
                # my $konvert_fee = ( ( $item_price / 100 ) * 3.5 );
                my $konvert_fee = 0;
                $item_price = sprintf( "%.2f", $item_price + $konvert_fee );
                if ( $item_price >= -1 ) {
                    $arr[4] =~ s/[\D]+//gi;
                    my $item_output_id = $item_logicalid;

                    # NOTABENE: Проверка на 10К, так как мы ожидаем, что этот id < 9999, при разборе маски фильтров
                    if ($arr[4] =~ /^[0-9]+$/ && $arr[4] < 10000) {
                        $item_output_id = $arr[4];
                    }

                    my $f_mask;
                    if ($parcefilters) {
                        $f_mask = getFiltersMaskFromExcel($arr[10]);
                    }
                    push(
                        @GLOBAL_PRICE_LIST,
                        join( "|",
                            $hashed_id,
                            "[" . join( "", @avail ) . "]" . $GLOBAL_ID,
                            $item_price,
                            @parce_db_record[1 .. $#parce_db_record-1] ,
                            "[*][$f_mask$item_output_id]" . $arr[5] )
                    );
                    push( @GLOBAL_ID_LEGENDA,
                        join( "|", $arr[4], $hashed_id, $GLOBAL_ID ) );
                    $GLOBAL_TOTAL++;
                }
            }
        }
        else {
            if ($DEBUG) {
                print "***error: manufacturer part number is null ($arr[4])\n";
            }
            return 0;
        }
    }
    else {
        #print "***error: invalid raw record\n";
        #exit();
    }
    return 1;
}


#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# $rc = delFiles ( $from_dir )
#---------------------------------------------------------------------------------
#
# $from_dir - каталог, в котором будут удалены все файлы.
# Функция удаляет все файлы из заданного каталога: 0 - ошибка,
# 1 - успешное завершение.
#
#---------------------------------------------------------------------------------

sub delFiles {
    my ($from_dir) = @_;
    my $mainreturn = 1;
    my @files_     = getFilesArray($from_dir);
    for ( my $fileinx = 0 ; $fileinx <= $#files_ ; $fileinx++ ) {
        my $res = unlink("$from_dir/$files_[$fileinx]");
        if ( !$res ) { $mainreturn = 0; }
    }
    return $mainreturn;
}

#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
# getFilesArray ( $dir )
#--------------------------------------------------------------------------------------
#
# $dir - каталог для поиска.
# Найти файлы в каталоге. Функция возвращает массив c найденными файлами.
#
#--------------------------------------------------------------------------------------

sub getFilesArray {
    my ($dir) = @_;
    my @files;
    $dir =~ s/\\/\//g;
    if (-e $dir) {
        opendir my $dhndl, $dir or die 'unable to open directory '.$dir.' :'.$!;
        @files = grep { ( !/^\.+$/ ) and !( -d "$dir/$_" ) } readdir $dhndl;
        closedir $dhndl;
    }
    return @files;
}

#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
# doBackUp ( )
#--------------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Создание backup-архива текущей БД. Функция не возвращает какое-либо значение.
#
#--------------------------------------------------------------------------------------

sub doBackUp {
    my $filename = localtime;
    $filename =~ s/[\:\.\s\*\\\/\,]+/_/g;
    if (!(-e $GLOBAL_BACKUP_PATH)) {
        mkpath($GLOBAL_BACKUP_PATH);
    }
    my %backup = (
        prpool => $GLOBAL_PATH,
        dtpool => $GLOBAL_DATAPOOL,
        legend => $GLOBAL_IDLEGENDA_DBFILE,
        mcatsf => $GLOBAL_MAINCATS_FILE,
        images => 'images/shopcat',
        modcfg => 'conf/system/shopcat2*'
    );

    my $destination = "$GLOBAL_BACKUP_PATH/$filename.tar.gz";
    my $source      = join ' ', (values %backup);
    my $cmd         = `tar -czf $destination $source`;
}

#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
# doConvertToUtf ( $pricefile )
#--------------------------------------------------------------------------------------
#
# $pricefile - путь к файлу прайс-листа, который требуется конвертировать.
# Создание backup-архива текущей БД. Функция не возвращает какое-либо значение.
#
#--------------------------------------------------------------------------------------

sub doConvertToUtf {
    my ($filename) = @_;
    my $cmd_tr         = `tr -d '\r' < $filename >a; mv a $filename`;
    my $cmd_iconv      = `iconv -t UTF-8 -f WINDOWS-1251 -c $filename >a; mv a $filename`;
}

#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
# doRootCatSearch ( $rootcatname )
#--------------------------------------------------------------------------------------
#
# $rootcatname - название корневой категории.
# Получить имя корневой категории для категории первого уровня.
#
#--------------------------------------------------------------------------------------

sub doRootCatSearch {
    my ($rootcatname)  = @_;
    my $currrootcat  = -1;
    my $rootcatindex = 50000;
    if (-e $GLOBAL_MAINCATS_FILE) {
        open my $fh, '<', $GLOBAL_MAINCATS_FILE or die 'unable to open '.$GLOBAL_MAINCATS_FILE.': '.$!;
        my @rootcats = <$fh>;
        close $fh;
        for (my $i = 0; $i <= $#rootcats; $i++) {
            if ($rootcats[$i] =~ /^>/) {
                $currrootcat = $rootcats[$i];
                $currrootcat =~ s/[\>\r\n\t]+//gi;
                $rootcatindex++;
            } else {
                if ($rootcats[$i] !~ /^[\r\n]+$/) {
                    my $cat = $rootcats[$i];
                    $cat =~ s/[\r\n\t]+//gi;
                    if ($cat eq $rootcatname) {
                        last;
                    }
                }
            }
        }
    }
    if ($currrootcat == -1) {
        # $currrootcat = $ENV{SERVER_NAME};
    }
    return $currrootcat == -1 ? '' : "[$rootcatindex]".$currrootcat;
}

#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
# doDebugPrint ( $debugstr, $append )
#--------------------------------------------------------------------------------------
#
# $debugstr - стока для сохранения в файл отладки.
# $append - флаг управления добавлением в файл (0 - новый файл, 1 - добавление в конец)
# Сохранение информации в файл отладки.
#
#--------------------------------------------------------------------------------------

sub doDebugPrint {
    my ($debugstr, $append) = @_;
    if ($append && (-e $DEBUG_FILENAME)) {
        open my $fh, '+<', $DEBUG_FILENAME or die 'unable to open '.$DEBUG_FILENAME.': '.$!;
        flock( $fh, 2 );
        seek( $fh, 0, 2 );
        print $fh $debugstr."\n";
        flock( $fh, 8 );
        close($fh);
    } else {
        open my $fh,  '>', $DEBUG_FILENAME or die 'unable to open '.$DEBUG_FILENAME.': '.$!;
        print $fh $debugstr."\n";
        close $fh;
    }
}

#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
# isUniqRootId ( $id )
#--------------------------------------------------------------------------------------
#
# $id - идентификатор категории.
# Проверяет идентификатор категории на уникальность в массиве @ROOT_CATS_IDS.
#
#--------------------------------------------------------------------------------------

sub isUniqRootId {
    my ($id) = @_;
    my $rc = 1;
    for my $i (0..$#ROOT_CATS_IDS) {
        if ($ROOT_CATS_IDS[$i] == $id) {
            $rc = 0;
        }
    }
    if ($rc) {
        push(@ROOT_CATS_IDS, $id);
    }
    return $rc;
}

#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
# getFiltersMaskFromExcel ( $excel_filters )
#--------------------------------------------------------------------------------------
#
# $excel_filters - строка с перечислением фильтров и прайс-листа.
# Функция возвращает маску фильтров для входной строки из прайс-листа.
#
#--------------------------------------------------------------------------------------

sub getFiltersMaskFromExcel {
    my ($excel_filters) = @_;
    my @f_narr = split /\;/, $excel_filters;
    my @filters;
    my $mask;
    if (@f_narr) {
        if ($FNUM > -1) {
            foreach my $hashref (@$FILTERS) {
                my $f_name  = $hashref->{name};
                my $f_label = $hashref->{label};
                my $f_descr = $hashref->{descr};
                my $use_filter = 0;
                for my $j (0..$#f_narr) {
                    my $f_descr_decoded = decode("utf8", $f_descr);
                    my $f_excel_decoded = decode("utf8", $f_narr[$j]);
                    if ($f_excel_decoded) {
                        if ($f_descr_decoded =~ m/\Q$f_excel_decoded/gsi || $f_excel_decoded eq '*') {
                            # $use_filter = "{$f_descr}";
                            $use_filter = 1;
                        }
                    }
                }
                push @filters, $use_filter;
            }
        }
    }
    if (@filters) {
        $mask = Shopcat::V3::getFiltersMask(@filters);;
    }
    return $mask;
}

#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
# getFiltersHash ( )
#--------------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Функция возвращает указатель на массив хешей для всех фильтров, установленных
# в конфигурации модуля.
#
#--------------------------------------------------------------------------------------

sub getFiltersHash {
    my @_arr;
    if ($FNUM > -1) {
        for my $i (0..$FNUM) {
            my $f_name  = 'filter-'.(sprintf "%02d", $i+1);
            my $f_label = 'filters/'.$f_name;
            my $f_descr = encode("utf8", getSingleSetting('Shopcat', 0, $f_label));
            my %_hash = (
                'name' => $f_name,
                'label' => $f_label,
                'descr' => $f_descr,
            );
            push @_arr, \%_hash;
        }
    }
    return \@_arr;
}

#--------------------------------------------------------------------------------------

END { }

1;
