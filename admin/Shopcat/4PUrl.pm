package Shopcat::4PUrl;

use strict;
use warnings;

##############################################################################
#
# File   :  4PUrl.pm
#
##############################################################################

our ( @ISA, @EXPORT );

BEGIN {
    # for all servers
    use lib '../';
    use lib '../../extlibs/';
    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';
    # export subroutines
    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
        get4PUrl
        getLinesFromFile
        getPriceFileFullPath
    );
}

#---------------------------------------------------------------------------------

use Encode;

#---------------------------------------------------------------------------------

my $MODULE_SEO_DB_FILE  = 'conf/system/shopcat2.seo.tnk';
my $MODULE_DB_FILE      = 'conf/system/shopcat2.tnk';
my $MODULE_PRICEPOOL    = 'conf/system/pricepool/'; # WARNING!!! / is needed at the end
my $PRICEFILE_DB_COLUMS = 5;

#------------------------------------------------------------------------------
# get4PUrl( $id1, $id2 )
#------------------------------------------------------------------------------
#
# $id1 - идентификатор прайс-листа (или идентификатор текущей категории).
# $id2 - идентификатор текущей категории (или идентификатор позиции).
# Получить ЧПУ из БД модуля Shopcat::V3.
#
#------------------------------------------------------------------------------

sub get4PUrl {
    my $id1     = $_[0];
    my $id2     = $_[1];
    my $chpuurl = q{};
    my $found   = 0;
    $id1   =~ s/[\D]//g;
    $id2   =~ s/[\D]//g;
    my @db = getLinesFromFile($MODULE_SEO_DB_FILE);
    if (@db) {

        foreach my $record (@db) {
            $record =~ s/[\r\n\t]+//g;
            my @rec_data = split(/\|/,$record);
            if ($id1 && $id2) {
                if (    $rec_data[1] == $id1 &&
                        $rec_data[2] == $id2 &&
                        $rec_data[3] == -1
                ) {
                    my $u = passThroughCats($id2, @rec_data);
                    if ( defined $u ) {
                        $chpuurl = $u;
                        $found = 1;
                        last;
                    }
                }
                if ($rec_data[2] == $id1 && $rec_data[3] == $id2) {
                    $chpuurl = $rec_data[4];
                    $found = 1;
                    last;
                }
                if (
                    $rec_data[1] == $id1 &&
                    $rec_data[3] == getArtikulByCatId($id1, $id2)
                ) {
                    $chpuurl = $rec_data[4];
                    $found = 1;
                    #last;
                }
            } elsif ($id2 && !$id1) {
                if ($rec_data[2] == $id2 && $rec_data[3] == -1) {
                    my $u = passThroughCats($id2, @rec_data);
                    if ( defined $u ) {
                        $chpuurl = $u;
                        $found = 1;
                        last;
                    }
                }
                elsif ($rec_data[2] == $id2 && $rec_data[3] > -1) {
                    my $pricefile = getPriceFileFullPath($rec_data[1]);
                    my $itemid = getItemIdInPriceFile($pricefile, $rec_data[3]);
                    my $itemprice =
                        getItemCostByNLevID( $rec_data[1], $itemid );
                    if ( $itemprice > -1 ) {
                        $chpuurl = $rec_data[4];
                        $found = 1;
                        last;
                    }
                }
            }
        }
    }
    if ($found == 0) {
        if ($id1 && $id2) {
            $chpuurl = "shopcatalog\_$id2\_$id1";
            my $is_item = isCatIsItem($id2, $id1);
            if ($is_item) {
                $chpuurl = "shopcatalog\_$id1\_$id2";
            } else {
                $chpuurl = "shopcatalog\_$id2\_$id1";
            }
        }
        if ($id1 && !$id2) {$chpuurl = "shopcatalog\_$id1";}
        if ($id2 && !$id1) {$chpuurl = "shopcatalog\_$id2";}
    }
    return $chpuurl.".html";
}

#------------------------------------------------------------------------------
# passThroughCats( $id, @rowdata )
#------------------------------------------------------------------------------
#
# $id - идентификатор текущей категории (или идентификатор позиции).
# @rowdata - массив значений для заданного идентификатор позиции.
# Получить ЧПУ с пробросом ссылки на единственную позицию в категории.
#
#------------------------------------------------------------------------------

sub passThroughCats {
    my ($id, @rowdata) = @_;
    my $url;
    my $shopid = $rowdata[1];
    my @items  = getPriceFileItemsWithPosCost($shopid);
    if ( $#items == 1 ) {
        my @db = getLinesFromFile($MODULE_SEO_DB_FILE);
        foreach my $record (@db) {
            $record =~ s/[\r\n\t]+//g;
            my @rec_data = split( /\|/, $record );
            if (
                $rec_data[1] == $shopid &&
                $rec_data[2] == $id &&
                $rec_data[3] == $items[0]
            ) {
                $url = $rec_data[4];
                last;
            }
        }
    }
    else {
        $url = $rowdata[4];
    }
    return $url;
}

#------------------------------------------------------------------------------
# getPriceFileItemsWithPosCost( $shopid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# Получить массив id позиций с положительной ценой.
#
#------------------------------------------------------------------------------

sub getPriceFileItemsWithPosCost {
    my ($shopid)    = @_;
    my @uniqueitems = 0;
    if ($shopid > 0 && $shopid =~ /^[\d]+$/) {
        my $fname = getPriceFileFullPath($shopid);
        my @rows  = getLinesFromFile($fname);
        foreach my $row (@rows) {
            my @cols  = split( /\|/, $row );
            my $recid = $cols[0];
            if ( $recid =~ /^[\d]+$/ && $#cols == $PRICEFILE_DB_COLUMS) {
                my $itemid = getItemIdInPriceFile($fname, $recid);
                my $itemprice = getItemCostByNLevID( $shopid, $itemid );
                if ( $itemprice > -1 ) {
                    if ( !(map { $recid eq $_ ? ($_) : () } @uniqueitems ) ) {
                        push @uniqueitems, $recid;
                    }
                }
            }
        }
    }
    return @uniqueitems;
}

#------------------------------------------------------------------------------
# getPriceFileFullPath( $shopid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# Получить полный путь до файла прай-листа (pricepool/*) по идентификатору
# прайс-листа ($shopid).
#
#------------------------------------------------------------------------------

sub getPriceFileFullPath {
    my $shopid = $_[0] || return "no \$shopid specified";;
    my $errmes;
    my @db = getLinesFromFile($MODULE_DB_FILE);
    if (@db) {
        for my $record (@db) {
            chomp $record;
            my @record_arr = split( /\|/, $record );
            if ( $record_arr[0] eq $shopid ) {
                my $pricefilepath = $record_arr[-1];
                $pricefilepath =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                return $MODULE_PRICEPOOL . $pricefilepath;
            }
        }
        $errmes = "\$#db=$#db, no \$shopid=$shopid record";
    } else {
        $errmes = "\@db is empty";
    }
    return $errmes
}

#------------------------------------------------------------------------------
# getItemIdInPriceFile( $fname, $recid )
#------------------------------------------------------------------------------
#
# $filepath - путь до прайс-листа.
# $recId - идентификатор записи позиции [0].
# Получить идентификатора позиции [*][xxxx] из БД модуля Shopcat::V3.
#
#------------------------------------------------------------------------------

sub getItemIdInPriceFile {
    my ($fname, $recid) = @_;
    my $itemid = 'undef';
    my @db     = getLinesFromFile($fname);
    if (@db) {
        for my $record (@db) {
            my @rec_data = split(/\|/,$record);
            if ($rec_data[0] == $recid) {
                if ($record =~ /\[\*\]\[([\d]+)\]/) {
                    $itemid = $1;
                }
                last;
            }
        }
    }
    return $itemid;
}

#------------------------------------------------------------------------------
# getItemCostByNLevID( $shopid, $cat_id )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $cat_id - идентификатор категории уровня N.
# Обрезать строки (названия позиции).
#
#------------------------------------------------------------------------------

sub getItemCostByNLevID {
    my $fname  = getPriceFileFullPath($_[0]);
    my $cat_id = $_[1] || -1;
    my $itemocost = 0;
    my @rc = getLinesFromFile($fname);
    if (@rc) {
        for my $record (@rc) {
            if ( $record =~ /\[($cat_id)\]/ ) {
                my @cols = split(/\|/,$record);
                if ( @cols ) {
                    my $price = $cols[2];
                    if (
                        $price =~ /^[\d]+$/ ||
                        $price =~ /^[\d]+\.[\d]+$/ ||
                        $price eq '-1'
                    ) {
                        $itemocost = $price;
                    }
                }
            }
        }
    }
    return $itemocost;
}

#------------------------------------------------------------------------------
# isCatIsItem( $shopid, $catid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории N-1.
# Проверить заданный $catid на то, что он является категорией уровня < N
#
#------------------------------------------------------------------------------

sub isCatIsItem {
    my ($shopid, $catid) = @_;
    my $found = 0;
    my $fname = getPriceFileFullPath( $shopid );
    my @db    = getLinesFromFile($fname);
    if (@db) {
        foreach my $record (@db) {
            my @tmp = split ( /\|/, $record );
            if ($tmp[-1] =~ /\[$catid\]/) {
                $found = 1;
            }
        }
    }
    return $found;
}

#------------------------------------------------------------------------------
# getArtikulByCatId ( )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $cat_id - идентификатор категории.
# Получить глобальный артикул по id записи n-уровня.
#
#------------------------------------------------------------------------------

sub getArtikulByCatId {
    my ($shopid, $cat_id) = @_;
    my $artikul = -1;
    my $fname;
    if ($shopid > -1) {
        $fname = getPriceFileFullPath($shopid);
    } else {
        $fname = $MODULE_DB_FILE;
    }
    if ($cat_id) {
        if (-e $fname && $artikul == -1 ) {
            my @db = getLinesFromFile($fname);
            foreach my $rec (@db) {
                if ($rec =~ /\[($cat_id)\]/) {
                    my @cols = split(/\|/, $rec);
                    if ($shopid == -1) {
                        $artikul = $cols[0];

                        last;
                    }
                    else {
                        my $itemprice = $cols[2];
                        if ( $itemprice > -1 ) {
                            $artikul = $cols[0];
                        }
                    }
                }
            }
        }
    }
    return $artikul;
}

#------------------------------------------------------------------------------
# getLinesFromFile ( $fname )
#------------------------------------------------------------------------------
#
# $fname - имя файла.
# Получить массив строк из файла.
#
#------------------------------------------------------------------------------

sub getLinesFromFile {
    my ($f) = @_;
    my @lines;
    if (-e $f) {
        open my $fh, "<:encoding(UTF-8)", $f or die "unable to open $f: $!";
        @lines = <$fh>;
        close $fh or die "unable to close $f: $!";
    }
    return @lines;
}

#------------------------------------------------------------------------------

END { }

1;
