package Shopcat::ShopParams;

use strict;
use warnings;

##############################################################################
#
# File   :  ShopParams.pm
#
##############################################################################
#
# Пакет обработки параметрических полей БД модуля Shopcat::V3
#
##############################################################################

BEGIN {
    #for all servers
    use lib '../';    # lib path for work mode

    # avoid broken dependence for development boundle
    use lib '../.sys_libs/';
    use lib '../.sys_extlibs/';
}

use XML::LibXML;
use Encode;
use Fcntl qw(:DEFAULT :flock :seek);

#------------------------------------------------------------------------------
# new ()
#------------------------------------------------------------------------------
#
# Конструктор класса Shopcat::ShopParams.
#
#------------------------------------------------------------------------------

sub new {
    my $MODULE_NAME = "Shopcat";
    my ($class)     = @_;
    my $self        = {
        name     => 'Shopcat::ShopParams',
        version  => '2.2',
        mod_name => $MODULE_NAME,
        mod_dscr =>
'Пакет обработки параметрических полей модуля Shopcat::V3',
        mod_fldr => 'admin/libs/modules/' . $MODULE_NAME,
        mod_tmpl => 'admin/skins/classic_.txt',
        mod_dbfn => 'conf/system/shopcat2.parametrized.tnk',
        mod_attr => 'parametrizedindex',
    };
    bless $self, $class;
    return $self;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getParamDataArray ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить массив параметризуемых фильтров.
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------

sub getParamDataArray {
    my ($self)  = @_;
    my @paramsarr;
    my $xmlfile = Config::ModuleMngr::getModXml( $self->{mod_fldr} );
    my $xmlpath = $self->{mod_fldr} . "/" . $xmlfile;
    if ( -e $xmlpath && defined $xmlfile ) {
        my $doc = XML::LibXML->load_xml( location => $xmlpath );
        foreach my $filters (
            $doc->findnodes('/module/configuration/settings/filters/*') )
        {
            if ( $filters->textContent() !~ /^[\r\n\s]+$/sx ) {
                my $fi = $filters->getAttribute($self->{mod_attr});
                if ( $fi > 0 ) {
                    push @paramsarr, encode("utf8", '['.$fi.']'.$filters->textContent());
                }
            }
        }
    }
    return @paramsarr;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getParamTabTmpl ( $sid )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии доступа к административной части.
# $tabcont - содержимое вкладки.
# $type - тип содержимого (для разбора).
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $itemid - идентификатор позиции.
# Получение шаблона параметров на закладках.
#
#------------------------------------------------------------------------------

sub getParamTabTmpl {
    my ($self, $tabcont, $type, $shopid, $catid, $itemid ) = @_;
    my @parr = $self->getParamDataArray;
    my $legend;
    my $tabs;
    for my $i (0..$#parr) {
        if ($parr[$i] =~ /\[([\d]+)\]/) {
            my $label = $parr[$i];
            my $tab_id = $1;
            $label =~ s/\[([\d]+)\]//gi;
            if ($label ne '0') {
                $legend .= qq~
                <li role="tab"
                    aria-controls="tab_item-$i"
                    class="stxt resp-tab-item">$label</li>
                ~;
                my $tabcont_ =
                    $self->parseTabContent(
                        $tabcont, $type, $i, $tab_id, $shopid, $catid, $itemid );
                $tabs .= qq~<div id="tab_item-$i">$tabcont_</div>~;
            }
        }
    }
    my $cntnt_ .= qq~
    <script type="text/javascript" src="js/easytabs-init.js"></script>
    <div class="pheix-params-tabs-container">
     <div id="horizontalTab">
      <ul class="resp-tabs-list">$legend</ul>
      <div class="resp-tabs-container">$tabs</div>
     </div>
    </div>
    ~;
    return $cntnt_;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# parseTabContent ( $sid )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии доступа к административной части.
# $tabcont - содержимое вкладки.
# $type - тип содержимого (для разбора).
# $tab_num - номер вкладки.
# $tab_id - идентификатор вкладки.
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $itemid - идентификатор позиции.
# Получение шаблона параметров на закладках.
#
#------------------------------------------------------------------------------

sub parseTabContent {
    my ( $self, $tabcont, $type, $tab_num,
        $tab_id, $shopid, $catid, $itemid ) = @_;
    if ($type eq 'modal-1') {
        my @details = $self->getParametrizedVals( $shopid, $catid, $itemid );
        if ($tab_num > 0) {
            $tabcont =~ s/name=\"([a-z0-9]+)\"/name=\"$1\-param\-$tab_id\"/gi;
            $tabcont =~ s/value=\"(.*)\"/value=\"\"/gi;
            my @tmpl_ = split /\n/, $tabcont;
            for my $j (0..$#details) {
                if ($details[$j] =~ /\[$tab_id\](.{0,})/) {
                    my $value = $1;
                    for my $i (0..$#tmpl_) {
                        if ( $tmpl_[$i] =~ /name=\"[a-z0-9]+\-param\-$tab_id\"/i &&
                             $tmpl_[$i] =~ /value=\"\"/ ) {
                            if ($value) {
                                $tmpl_[$i] =~ s/value=\"\"/value=\"$value\"/gi;
                            } else {
                                $tmpl_[$i] =~ s/value=\"\"//gi;
                            }
                            last;
                        } elsif ($tmpl_[$i] =~ /name=\"[a-z0-9]+\-param\-$tab_id\"/i &&
                                 $tmpl_[$i] =~ /\<textarea/) {
                            if ($value) {
                                $value =~ s/\<br\>/\n/gi;
                                $tmpl_[$i] =~ s/\>\<\/textarea>/\>$value\<\/textarea>/gi;
                            }
                        }
                    }
                }
            }
            $tabcont = join "\n", @tmpl_;
        }
        if ($tab_num == 0) {
            my @tmpl_ = split /\n/, $tabcont;
            my @tabcont_;
            for my $i (0..$#tmpl_) {
                if ($tmpl_[$i] =~ /\<textarea/) {
                    $tmpl_[$i] =~ s/name=\"([a-z0-9]+)\"/name=\"$1\-param\-1\"/gi;
                    for my $j (0..$#details) {
                        if ($details[$j] =~ /\[1\](.{0,})/) {
                            my $value = $1;
                            if ( $tmpl_[$i] =~
                                 /name=\"[a-z0-9]+\-param\-1\"/i ) {
                                if ($value) {
                                    $value =~ s/\<br\>/\n/gi;
                                    $tmpl_[$i] =~ s/\>\<\/textarea>/\>$value\<\/textarea>/gi;
                                }
                            }
                        }
                    }
                }
                if ($tmpl_[$i] !~ /(f\-s\-labl)|(f\-s\-inpt)/) {
                    push @tabcont_,$tmpl_[$i];
                }
            }
            $tabcont = join "\n", @tabcont_;
        }
    }
    return $tabcont;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# setParametrizedVals ( $sid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $itemid - идентификатор позиции.
# $cgi_p - указатель CGI объекта.
# Сохранение параметризованных значений.
#
#------------------------------------------------------------------------------

sub setParametrizedVals {
    my ($self, $shopid, $catid, $itemid, $cgi_p) = @_;
    my @db;
    my $fh;
    my $newrecord = 1;
    if (-e $self->{mod_dbfn}) {
        open $fh, "<", $self->{mod_dbfn} or
            die 'unable to open for read ' . $self->{mod_dbfn} . ': ' . $!;
        @db = <$fh>;
        close $fh or die 'unable to close ' . $self->{mod_logf} . ': ' . $!;
    }
    my @param_names = $cgi_p->param;
    my @psave;
    for my $i (0..$#param_names) {
        if ($param_names[$i] =~ /\-param\-([\d]+)/) {
            my $f_attr_id = $1;
            my $param = $cgi_p->param($param_names[$i]);
            $param =~ s/\n/\<br\>/g;
            $param =~ s/[\r\n\t]+//g;
            $param =~ s/\"/\&quot;/g;
            $param =~ s/\|/\&brvbar\;/g;
            push @psave, '['.$f_attr_id.']'.$param;
        }
    }
    if (@db) {
        my @db_;
        for my $i (0..$#db) {
            $db[$i] =~ s/[\r\n]+//gix;
            my @cols = split /\|/, $db[$i];
            if ($cols[0] == $shopid &&
                $cols[1] == $catid && $cols[2] == $itemid ) {
                my $e_ = 0;
                for my $j (0..$#psave) {
                    if ( !$psave[$j] ||
                         $psave[$j] eq '' ||
                         $psave[$j] =~ /^\[[\d]+\]$/ ) {
                        $e_++;
                    }
                }
                if ($e_ <= $#psave) {
                    push @db_, join("|", $shopid, $catid, $itemid, @psave);
                    $newrecord = 0;
                }
            } else {
                push @db_, $db[$i];
            }
        }
        @db = @db_;
    }
    if ($newrecord) {
        my $e_ = 0;
        for my $j (0..$#psave) {
            if ( !$psave[$j] ||
                 $psave[$j] eq '' ||
                 $psave[$j] =~ /^\[[\d]+\]$/ ) {
                $e_++;
            }
        }
        if ($e_ <= $#psave) {
            push @db, join("|", $shopid, $catid, $itemid, @psave) . "\n";
        }
    }
    if (@db) {
        open $fh, ">", $self->{mod_dbfn} or
            die 'unable to open for write ' . $self->{mod_dbfn} . ': ' . $!;
        flock( $fh, LOCK_EX ) or die 'unable to lock mailbox: ' . $!;
        print $fh join "\n", @db;
        truncate $fh, tell($fh);
        flock( $fh, LOCK_UN ) or die 'unable to unlock mailbox: ' . $!;
        close $fh or die 'unable to close ' . $self->{mod_logf} . ': ' . $!;
    } else {
        if (-e $self->{mod_dbfn}) {
            unlink $self->{mod_dbfn};
        }
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getParametrizedVals ( $sid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $itemid - идентификатор позиции.
# Получение параметризованных значений.
#
#------------------------------------------------------------------------------

sub getParametrizedVals {
    my ($self, $shopid, $catid, $itemid) = @_;
    my @db;
    my @params;
    if (-e $self->{mod_dbfn}) {
        open my $fh, "<", $self->{mod_dbfn} or
            die 'unable to open for read ' . $self->{mod_dbfn} . ': ' . $!;
        my @db = <$fh>;
        close $fh or die 'unable to close ' . $self->{mod_logf} . ': ' . $!;
        for my $i (0..$#db) {
            $db[$i] =~ s/[\r\n]+//gix;
            my @cols = split /\|/, $db[$i];
            if ($cols[0] == $shopid &&
                $cols[1] == $catid && $cols[2] == $itemid ) {
                @params = @cols[3..$#cols];
                last;
            }
        }
    }
    return @params;
}

#------------------------------------------------------------------------------

END { }

1;
