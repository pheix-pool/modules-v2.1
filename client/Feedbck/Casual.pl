my $feedback_is_switchedoff = Feedbck::Casual::getModPwrStatus();
if ( $feedback_is_switchedoff == 0 ) {
    if ( $env_params[0] eq 'feedback' ) {
        if ($co->request_method() eq 'GET') {
            print $co->header(
                -type            => 'text/html; charset=UTF-8',
                -Last_Modified   => $global_last_modified,
                '-Expires'       => $global_last_modified,
                '-Cache-Control' => 'max-age=0, must-revalidate'
            );
            Feedbck::Casual::showFBHomePage();
        } else {
            my $rc = Feedbck::Casual::saveFeedBack($co, $env_params[1]);
            my $form;
            my $fio       = $co->param('fio');
            my $email     = $co->param('email');
            my $subj      = $co->param('subject');
            my $mess      = $co->param('message');
            if ( $env_params[1] > 0 ) {
                $form = $env_params[1];
            } else {
                $form = 0;
            }
            if ($rc == 1) {
                print $co->header(
                    -type            => 'text/html; charset=UTF-8',
                    -Last_Modified   => $global_last_modified,
                    '-Expires'       => $global_last_modified,
                    '-Cache-Control' => 'max-age=0, must-revalidate'
                );
                Feedbck::Casual::showFBHomePage( $fio, $email, $subj, $mess, $form, 1 );
            } elsif ($rc == 2) {
                print $co->header(
                    -type            => 'text/html; charset=UTF-8',
                    -Last_Modified   => $global_last_modified,
                    '-Expires'       => $global_last_modified,
                    '-Cache-Control' => 'max-age=0, must-revalidate'
                );
                Feedbck::Casual::showFBHomePage( $fio, $email, $subj, $mess, $form, 2 );
            } elsif ($rc == 3) {
                print $co->header(
                    -type            => 'text/html; charset=UTF-8',
                    -Last_Modified   => $global_last_modified,
                    '-Expires'       => $global_last_modified,
                    '-Cache-Control' => 'max-age=0, must-revalidate'
                );
                Feedbck::Casual::showFBHomePage( $fio, $email, $subj, $mess, $form, 3 );
            } else {
                print $co->redirect( -uri => $proto.$ENV{SERVER_NAME} );
            }
        }
        exit;
    }

    if ( $env_params[0] eq 'feedbackform' ) {
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        print Feedbck::Casual::getFBForm();
        exit;
    }
}
