package Feedbck::Casual;

use strict;
use warnings;

##############################################################################
#
# File   :  Casual.pm
#
##############################################################################
#
# Главный пакет управления обратной связью в пользовательской части CMS Pheix
#
##############################################################################

BEGIN {
    use lib '../';
    use lib 'libs/modules';
    use lib 'admin/libs/extlibs/';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs';
}

use WWW::Telegram::BotAPI;
use Email::SendGrid::V3;
use Crypt::CBC;
use Pheix::Tools qw(getSingleSetting);
use MIME::Base64;
use Encode qw(encode decode);
use utf8;
use open qw(:std :utf8);
use CGI;

use constant {
    ADMIN_AREA      => 0,
    USER_AREA       => 1,
    TARGET_EMAIL    => 0,
    TARGET_TELEGRAM => 1,
    TARGET_BOTH     => 2,
};

#------------------------------------------------------------------------------

my $MODULE_NAME           = 'Feedbck';
my $MODULE_DESCR          = 'Обратная связь';
my $MODULE_DB_FILE        = 'conf/system/feedback.tnk';
my $INSTALL_DB_PATH       = 'conf/system/install.tnk';
my $CAPTCHA_DB_PATH       = 'conf/system/captchas.tnk';
my $MODULE_USER_TEMPLATE  = 'conf/config/template.html';
my $FEEDBACK_TARGET       = getSingleSetting($MODULE_NAME, USER_AREA, 'telegramfeedback');
my $GLOBAL_CONTACT_EMAIL  = getSingleSetting($MODULE_NAME, USER_AREA, 'contactemail');
my $PROTO                 = getSingleSetting('Config', ADMIN_AREA, 'workviaproto') == '1' ? 'https://' : 'http://';
my $ENABLE_CAPTCHA        = getSingleSetting($MODULE_NAME, USER_AREA, 'enablecaptcha');

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModPwrStatus ()
#------------------------------------------------------------------------------
#
# Получить статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub getModPwrStatus {
    my $file_to_parse = "$INSTALL_DB_PATH";
    if ( -e $file_to_parse ) {
        open( CFGFH, "<$file_to_parse" );
        my @filecontent = <CFGFH>;
        close(CFGFH);
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my @tmp = split( /\|/, $filecontent[$admplinx] );
            if ( $tmp[2] eq $MODULE_NAME ) { return $tmp[4]; }
        }
    }
    return;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showFBHomePage ()
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода записей из CMS.
#
#------------------------------------------------------------------------------

sub showFBHomePage {
    my $pageaction = $ENV{REQUEST_URI};

    if ( -e $MODULE_USER_TEMPLATE ) {
        open my $ifh, "<", $MODULE_USER_TEMPLATE;

        my @scheme = <$ifh>;

        close $ifh;

        for my $cnt (0..$#scheme) {
            $scheme[$cnt] = Pheix::Pages::fillCommonTags( $scheme[$cnt], "feedback", $MODULE_NAME );

            if ( $scheme[$cnt] =~ /\%content\%/ ) {
                my $content = getFBForm(@_);

                $scheme[$cnt] =~ s/\%content\%/$content/;
            }

            print $scheme[$cnt];
        }
    }
    else {
        print qq~
            <b>$MODULE_NAME:</b>&nbsp;
            in function show_feedback_clientpage() -&nbsp;
            $MODULE_USER_TEMPLATE not found!
        ~;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# saveFBData ( $mess, $email, $fio, $subj )
#------------------------------------------------------------------------------
#
# Добавление сообщения в БД.
#
#------------------------------------------------------------------------------

sub saveFBData {
    my $fh;
    my $messID = time;
    my $mess_  = decode("utf8", $_[0]);
    my $email_ = decode("utf8", $_[1]);
    my $fio_   = decode("utf8", $_[2]);
    my $subj_  = decode("utf8", $_[3]);
    my $type_  = $_[4];

    $mess_  =~ s/[\r\n]+/<br>/g;
    $email_ =~ s/[\r\n]+/<br>/g;
    $fio_   =~ s/[\r\n]+/<br>/g;
    $subj_  =~ s/[\r\n]+/<br>/g;

    if ( -e $MODULE_DB_FILE ) {
        open($fh, "+<", $MODULE_DB_FILE);
    }
    else {
        open($fh, ">", $MODULE_DB_FILE);
    }

    my @db    = <$fh>;
    my $date_ = Pheix::Tools::getDateUpdate();

    push(@db, sprintf("%s|%s|%s|%s|%s|%s|%s|%s\n", $messID, $messID, $date_, $type_, $fio_, $email_, $subj_, $mess_));

    seek($fh, 0, 0);
    flock($fh, 2);

    print $fh @db;

    truncate($fh, tell($fh));
    flock($fh, 8);
    close($fh);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# sendGridEmail ()
#------------------------------------------------------------------------------
#
# Функция отправки email через сервис sendGrid.
#
#------------------------------------------------------------------------------

sub sendGridEmail {
    my ($target_email, $message, $subject, $fio, $consumer_email) = @_;

    my $compiled_mess = doCompileEmail($fio, $message, $consumer_email, $subject);
    my $compiled_subj = doCompileSubj($fio);

    (my $subj = sprintf("=?UTF-8?B?%s?=", encode_base64(encode("utf8", $compiled_subj)))) =~ s/[\r\n]+//g;

    return -1 unless $target_email && $target_email ne q{} && $target_email =~ /\@/;

    my $sendgrid_token = getSingleSetting($MODULE_NAME, USER_AREA, 'sendgridtoken');

    return -1 unless $sendgrid_token;

    my $authorized_sender = getSingleSetting($MODULE_NAME, USER_AREA, 'sendgridsender');

    return -1 unless $authorized_sender;

    my $sg = Email::SendGrid::V3->new(api_key => $sendgrid_token);

    my $result = $sg->from($authorized_sender)
                    ->subject($subj)
                    ->add_content('text/plain', $compiled_mess)
                    ->add_envelope( to => [ $target_email ] )
                    ->send;

    # die $result->{reason} unless $result->{success};

    return -1 unless $result->{success};

    return 0;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getFBForm ()
#------------------------------------------------------------------------------
#
# Локальная форма обратной связи - быстрая заявка.
#
#------------------------------------------------------------------------------

sub getFBForm {
    my ($_f, $_e, $_s, $_m, $type, $_fl, $req_uri, @_flabels) = @_;

    my $header;
    my $FB_Content;
    my $crypted_captcha_value;

    my $fio     = decode("utf8", $_f);
    my $email   = decode("utf8", $_e);
    my $subject = decode("utf8", $_s);
    my $message = decode("utf8", $_m);

    my @field_title = (
        $_flabels[0] || 'Фамилия, имя, отчество:',
        $_flabels[1] || 'Ваш e-mail:',
        $_flabels[2] || 'Телефон:',
        $_flabels[3] || 'Сообщение:',
        $_flabels[4] || 'Код защиты от спама:',
        $_flabels[5] || 'Отправить',
        $_flabels[6] || 'Сообщение успешно отправлено! Мы свяжемся с Вами в ближайшее время.'
    );

    if (!@_flabels) {
        $header = qq~<p class="pageheader">$MODULE_DESCR</p>~;
    }

    if ($_fl != 1) {
        my $random = sprintf("%06d", rand(1_000_000));

        if (scalar(@_) > 0 && $_fl != 4) {
            my @field_data = @_[ 0 ... 3 ];

            for ( my $i = 0 ; $i < $#field_title-1 ; $i++ ) {
                if ( $field_data[$i] eq '' ) {
                    $field_title[$i] =
                      qq~<font color="#ff0000">$field_title[$i]</font>~;
                }
            }

            if ( $_fl == 2 ) {
                $_flabels[4] = qq~<font color="#ff0000">$field_title[4]</font>~;
            }
        }

        my $encrypt_ret = eval {
            $crypted_captcha_value = Pheix::Tools::getHexStrFromString(Pheix::Tools::doBlowfishEncrypt($random));

            die "can not store code" unless storeCaptchaCode($random);

            1;
        } or do {
            my $err = $@;

            $crypted_captcha_value = '000000';
        };

        $FB_Content = qq~
            $header
            <form id="pheix_fb_module_form" method=post action="$req_uri" style="margin:0">
            <div class="feedback-form">
            <input type=hidden name=formtype value="supportrequest">
            <div class=standart_text>$field_title[0]</div>
            <div class=qr_standart_text><input id="pheix_fb_module_form_fio_field" type=text name=fio value="$fio" class=fbinput></div>
            <div class=standart_text>$field_title[1]</div>
            <div class=qr_standart_text><input id="pheix_fb_module_form_email_field" type=text name=email value="$email" class=fbinput></div>
            <div class=standart_text>$field_title[2]</div>
            <div class=qr_standart_text><input id="pheix_fb_module_form_phone_field" type=text name=subject value="$subject" class=fbinput></div>
            <div class=standart_text>$field_title[3]</div>
            <div class=qr_standart_text><textarea id="pheix_fb_module_form_message_field" rows=8 cols=5 name=message class=fbinput style="width:100%">$message</textarea></div>
        ~;

        if ($ENABLE_CAPTCHA) {
            $FB_Content .= qq~
                <div class=standart_text>$field_title[4]</div>
                <div class=qr_standart_text><img src="$ENV{SCRIPT_NAME}?action=showcaptcha&amp;val=$crypted_captcha_value" width=102 height=32 border=0 alt="" class="captcha"></div>
                <div class=qr_standart_text>
                   <input  type="hidden"
                           name="code1"
                           value="$crypted_captcha_value">
                   <input  type="text"
                           id="pheix_fb_module_form_code_field"
                           class=fbinput
                           name="code2"
                           size="26" value="">
                </div>
            ~;
        }

        $FB_Content .= qq~
            <div class=qr_standart_text>
               <input  type=submit
                       id="pheix_fb_module_form_btn_submit"
                       value="$field_title[5]"
                       class=fbbtn>&nbsp;
            </div></div></form>
        ~;
    }
    else {
        $FB_Content = qq~
            $header
            <div id="pheix_fb_module_status_message">
            <p class=standart_text align=left>$field_title[6]</p></div>
        ~;
    }

    return $FB_Content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getFBGlobEmail ()
#------------------------------------------------------------------------------
#
# Получить глобальный email.
#
#------------------------------------------------------------------------------

sub getFBGlobEmail {
    return $GLOBAL_CONTACT_EMAIL;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doCompileEmail ()
#------------------------------------------------------------------------------
#
# Создать письмо по заданному шаблону.
#
#------------------------------------------------------------------------------

sub doCompileEmail {
    my $date       = localtime(time);
    my $pheix_ver  = Pheix::Tools::getPheixVersion();
    my $usrfio     = decode("utf8", $_[0]);
    my $usrmessage = decode("utf8", $_[1]);
    my $usremail   = decode("utf8", $_[2]);
    my $usrphone   = decode("utf8", $_[3]);

    my $email_template = qq~Доброго времени суток!
Пользователь $usrfio отправил с сервера $PROTO$ENV{SERVER_NAME} следующее сообщение:

+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
$usrmessage
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Контактные данные отправителя:

   ФИО     : $usrfio
   email   : $usremail
   телефон : $usrphone

=========================================================
Письмо сгенерировано автоматически, не отвечайте на него!
date   : $date
system : Pheix $pheix_ver
~;
    return $email_template;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doCompileSubj ()
#------------------------------------------------------------------------------
#
# Создать тему к письму по заданному шаблону.
#
#------------------------------------------------------------------------------

sub doCompileSubj {
    (my $user = decode("utf8", $_[0])) =~ s/[\r\n\t]+//gi;

    return sprintf("Пишет %s с сервера %s%s", $user, $PROTO, $ENV{SERVER_NAME});
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# saveFeedBack ()
#------------------------------------------------------------------------------
#
# Сохранить данные обратносй связи.
#
#------------------------------------------------------------------------------

sub saveFeedBack {
    my ($co, $form_t) = @_;

    my $rc;

    (my $referer = $ENV{HTTP_REFERER}) =~ s/https?\:\/\///g;

    my @referer_arr = split( "\/", $referer );

    if ( $referer_arr[0] eq $ENV{HTTP_HOST} ) {
        my $fio       = $co->param('fio');
        my $email     = $co->param('email');
        my $subj      = $co->param('subject');
        my $mess      = $co->param('message');
        my $spam      = $co->param('code1');
        my $spam_conf = $co->param('code2');
        my $formtype  = $co->param('formtype');
        my $form      = 0;

        $form = $form_t if $form_t > 0;

        if ($ENABLE_CAPTCHA) {
            my $captchavalue = $spam;

            eval {
                $spam =
                    Pheix::Tools::doBlowfishDecrypt(
                        Pheix::Tools::getStringFromHexStr($captchavalue));
            };

            if ($spam =~ /^[\d]+$/) {
                $spam   = sprintf("%.6f", "0.$spam");
                my @tmp = split(/\./, $spam);

                $spam   = $tmp[1];
            }
        }

        if ($fio ne '' && $email ne '' && $subj ne '' && $mess ne '') {
            $fio  =~ s/\@/\\\@/g;
            $subj =~ s/\@/\\\@/g;
            $mess = $co->param('message');

            my $active_code = fetchActiveCaptchaCode();

            if (!$ENABLE_CAPTCHA || ($spam && $spam_conf && $spam == $spam_conf && $spam ne q{} && $spam_conf ne q{} && $active_code == $spam_conf)) {
                if ($FEEDBACK_TARGET == TARGET_EMAIL || $FEEDBACK_TARGET == TARGET_BOTH) {
                    Feedbck::Casual::sendGridEmail(Feedbck::Casual::getFBGlobEmail(), $mess, $subj, $fio, $email);
                }

                if ($FEEDBACK_TARGET == TARGET_TELEGRAM || $FEEDBACK_TARGET == TARGET_BOTH) {
                    my $token  = getSingleSetting($MODULE_NAME, USER_AREA, 'telegramtoken');
                    my $chatid = getSingleSetting($MODULE_NAME, USER_AREA, 'telegramchatid');

                    die "no telegram bot is setup" unless $token && $chatid && $token ne q{} && $chatid ne q{};

                    my $telegram_api = WWW::Telegram::BotAPI->new(token => $token);

                    my $api_res = eval { $telegram_api->getMe; 1; }
                       or do {
                           die sprintf("telegram API failure: %s", $telegram_api->parse_error->{msg});
                       };

                    $telegram_api->sendMessage({
                        chat_id => $chatid,
                        text    => sprintf("%s <%s> %s: %s", decode("utf8", $fio), decode("utf8", $email), decode("utf8", $subj), decode("utf8", $mess)),
                    });
                }

                Feedbck::Casual::saveFBData($mess, $email, $fio, $subj, $formtype);

                $rc = 1;
            } else {
                $rc = 2;
            }
        } else {
            $rc = 3;
        }
    } else {
        $rc = -1;
    }

    return $rc;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# parseFeedBackVars ()
#------------------------------------------------------------------------------
#
# Распарсить специальные переменные модуля.
#
#------------------------------------------------------------------------------

sub parseFeedBackVars {
    my ($data) = @_;
    my $_cntnt;
    my $_swoff = getModPwrStatus();

    if ($data =~ /%FEEDBACK_FORM\((.{0,})\)%/ && $_swoff == 0) {
        my @fields;

        my $_ft = 1;
        my $_ds = $1;

        $_ds =~ s/(\&\#39;)/\'/gi;
        $_ds =~ s/\./\&\#46;/gi;

        while ($_ds =~ s/\'([^\'^\,.]*)\'//) {
            push @fields, $1;
        }

        my $cgi = CGI->new();

        if ($cgi->request_method() eq 'POST') {
            my $fio   = $cgi->param('fio');
            my $email = $cgi->param('email');
            my $subj  = $cgi->param('subject');
            my $mess  = $cgi->param('message');

            my $rc = eval { saveFeedBack($cgi, $_ft); }
    	        or do {
                    my $err = $@;
                    my $msg = sprintf("<p>Parse %%FEEDBACK_FORM()%% failure: %s...</p>", substr($err, 0, 128));

                    $data =~ s/%FEEDBACK_FORM\((.{0,})\)%/$msg/gi;

                    return $data;
    	        };

            if ($rc == 1) {
                $_cntnt = getFBForm( '', '', '', '', $_ft, 1, $ENV{REQUEST_URI}, @fields);
            } elsif ($rc == 2) {
                $_cntnt = getFBForm( $fio, $email, $subj, $mess, $_ft, 2, $ENV{REQUEST_URI}, @fields );
            } elsif ($rc == 3) {
                $_cntnt = getFBForm( $fio, $email, $subj, $mess, $_ft, 3, $ENV{REQUEST_URI}, @fields );
            } else {
                $_cntnt = getFBForm( '', '', '', '', $_ft, 4, $ENV{REQUEST_URI}, @fields );
            }
        } else {
            $_cntnt = getFBForm( '', '', '', '', $_ft, 4, $ENV{REQUEST_URI}, @fields );
        }

        if (!$_cntnt) {
            $_cntnt = qq~<p>Parse %FEEDBACK_FORM()% error: no content</p>~;
        }

        $data =~ s/%FEEDBACK_FORM\((.{0,})\)%/$_cntnt/gi;
    } else {
        if ($_swoff == 1) {
            $_cntnt = qq~<p>Module <b>$MODULE_NAME</b> is switched off!</p>~;
            $data  =~ s/%FEEDBACK_FORM\((.{0,})\)%/$_cntnt/gi;
        }
    }

    $_cntnt = $data;

    return $_cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getTrackerCookie ()
#------------------------------------------------------------------------------
#
# Получить tracking куки.
#
#------------------------------------------------------------------------------

sub getTrackerCookie {
    my $cgi = CGI->new();

    return $cgi->cookie('_pheix_stats_tracker');
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# storeCaptchaCode ()
#------------------------------------------------------------------------------
#
# Сохранить код каптчи.
#
#------------------------------------------------------------------------------

sub storeCaptchaCode {
    my ($code) = @_;

    my $fh;

    return unless $code && $code > 0;

    my $tracker = getTrackerCookie();

    return unless $tracker && $tracker ne q{};

    if (-e -f $CAPTCHA_DB_PATH) {
        open($fh, "+<", $CAPTCHA_DB_PATH) or return;
    }
    else {
        open($fh, ">", $CAPTCHA_DB_PATH) or return;
    }

    my @db  = <$fh>;
    my $now = time;

    push(@db, sprintf("%d|%s|%s|%d\n", $now, $tracker, $code, ($now + 120)));

    seek($fh, 0, 0);
    flock($fh, 2);

    print $fh @db;

    truncate($fh, tell($fh));
    flock($fh, 8);

    close($fh) or die sprintf("can not close file %s", $CAPTCHA_DB_PATH);

    return 1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# fetchActiveCaptchaCode ()
#------------------------------------------------------------------------------
#
# Получить самый свежий код каптчи для tracking куки.
#
#------------------------------------------------------------------------------

sub fetchActiveCaptchaCode {
    my $tracker = getTrackerCookie();

    return unless $tracker && $tracker ne q{};

    open(my $fh, "<", $CAPTCHA_DB_PATH) or return;

    my @db  = <$fh>;
    my $now = time;

    close($fh) or die sprintf("can not close file %s", $CAPTCHA_DB_PATH);

    for my $row (reverse(@db)) {
        chomp($row);

        my @cols = split(/\|/, $row);

        if ($cols[1] eq $tracker && $cols[3] > 0 && $now < $cols[3]) {
            return $cols[2];
        }
    }

    return;
}

#------------------------------------------------------------------------------

END { }

1;
