my $rssya_is_switchedoff =
  Rss::Yandex::getModPwrStatus();
if ( $rssya_is_switchedoff == 0 ) {
    if ( defined $env_params[0] && $env_params[0] eq 'rssfeed' ) {
        if (defined $co) { 
            print $co->header(
                -type            => 'text/html; charset=UTF-8',
                -Last_Modified   => $global_last_modified,
                '-Expires'       => $global_last_modified,
                '-Cache-Control' => 'max-age=0, must-revalidate'
            );
            my $got_page = $env_params[1] || 0;
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            Rss::Yandex::RSSYshowHomePage($got_page);
        } else {
            die "CGI object is missing";
        }
        exit;
    }
}