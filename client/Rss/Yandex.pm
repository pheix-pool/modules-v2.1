package Rss::Yandex;

##############################################################################
#
# File   :  Yandex.pm
#
##############################################################################
#
# Главный пакет парсинга фида новостей Яндекса в польз. части CMS Pheix
#
##############################################################################

BEGIN { 
    use lib '../';
    use lib 'libs/modules';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs';
}

use POSIX;
use Pheix::Tools qw(getSingleSetting);
use Pheix::Pages qw(fillCommonTags);
use Encode;
use utf8;
use open qw(:std :utf8);

#------------------------------------------------------------------------------

my $MODULE_NAME  = "Rss";
my $MODULE_DESCR = "Парсинг фида новостей Яндекса";
my $INSTALL_FILE_PATH    = "conf/system/install.tnk";
my $MODULE_USER_TEMPLATE = "conf/config/template.html";
my $MODULE_DB_FILE = getSingleSetting( $MODULE_NAME, 1, 'tankfile' );
my $TOTAL_RECORDS_ONPAGE = getSingleSetting( $MODULE_NAME, 1, 'recordsonpage' );

if ($TOTAL_RECORDS_ONPAGE <= 0) {
    $TOTAL_RECORDS_ONPAGE = 1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModPwrStatus ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить статус модуля (включен или выключен).
# Функция возвращает статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub getModPwrStatus {
    my $file_to_parse = $INSTALL_FILE_PATH;
    if ( -e $file_to_parse ) {
        open( CFGFH, "<$file_to_parse" );
        my @filecontent = <CFGFH>;
        close(CFGFH);
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my @tmp = split( /\|/, $filecontent[$admplinx] );
            if ( $tmp[2] eq $MODULE_NAME ) { return $tmp[4]; }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# RSSYshowHomePage ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Вывод стартовой страницы пользовательской части.
#
#------------------------------------------------------------------------------

sub RSSYshowHomePage {
    my $sid = $_[0];
    if ( -e $MODULE_USER_TEMPLATE ) {
        open my $ifh, "<", $MODULE_USER_TEMPLATE;
        my @tmpl = <$ifh>;
        close $ifh;
        for my $i ( 0 .. $#tmpl ) {
            $tmpl[$i] = fillCommonTags( $tmpl[$i], "rssfeed", $MODULE_NAME );
            if ( $tmpl[$i] =~ /\%content\%/ ) {
                my $contentblock = showRssYandex(@_);
                $tmpl[$i] =~ s/\%content\%/$contentblock/;
            }
            print $tmpl[$i];
        }
    } else {
        print qq~<div><b>$MODULE_NAME:</b> $MODULE_USER_TEMPLATE missed!</div>~;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showRssYandex ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Вывод контента стартовой страницы пользовательской части.
#
#------------------------------------------------------------------------------

sub showRssYandex {
    my ($page) = @_;
    my $header = getSingleSetting( $MODULE_NAME, 1, 'userareaheader' );
    my $rssnews;
    my $found = 0;
    if (-e $MODULE_DB_FILE) {
        open my $ifh, "<", $MODULE_DB_FILE;
        my @dbnews = sort { $b cmp $a } <$ifh>;
        close $ifh;
        if (@dbnews) {
            my ($navi, @news) = getPageLinksNavi($page, @dbnews);
            if (@news) {
                for my $i ( 0 .. $#news ) {
                    my @db_col = split( /\|/, $news[$i] );
                    $rssnews .= qq~
                        <div class="rss-news-block">
                        <div class="rss-news-head">$db_col[3] — 
                           <span class="rss-silver-fnt">$db_col[2]</span></div>
                        $db_col[4]</div>
                    ~;
                    $found++;
                }
            }
            if ($found > 0) {
                $rssnews .= "<p align=center>$navi</p>";
            }
        }
    }
    if ($found == 0) {
        $rssnews .= qq~<p>Раздел в процессе наполнения.<p>~;
    }
    my $rsscontent = qq~<h3>$header</h3>
        <div class=shopcatcontainer>$rssnews</div>"
    ~;
    return $rsscontent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getPageLinksNavi ( @args )
#------------------------------------------------------------------------------
#
# @args - массив входных аргументов.
# Вывод постраничный навигации.
#
#------------------------------------------------------------------------------

sub getPageLinksNavi {
    my $page_linx ;
    my $pages     = 0;
    my $total_rec = $TOTAL_RECORDS_ONPAGE;
    my $curr_page = $_[0];
    my @data = @_[1..$#_];

    $pages = floor( $#data / $total_rec );

    if (   ( $pages * $total_rec <= $#data )
        || ( $pages < 1 ) )
    {
        $pages++;
    }

    if ( $curr_page >= $pages ) { $curr_page = 0; }
    $page_linx = '&nbsp;-&nbsp;';
    for ( my $i = 1 ; $i <= $pages ; $i++ ) {
        if ( ( $i - 1 ) != $curr_page ) {
            $page_linx .= '<a href="rssfeed_'
              . ( $i - 1 )
              . '.html" class="rss-pages-link">'
              . $i
              . '</a>&nbsp;-&nbsp;';
        }
        else {
            $page_linx .=
              '<span class="rss-disabled-link">' . $i . '</span>&nbsp;-&nbsp;';
        }
    }

    if ( $curr_page >= $pages ) { $curr_page = $pages - 1; }
    my $end = ( ( $curr_page + 1 ) * $total_rec ) - 1;
    if ( $end > $#data ) {
        $end = $#data;
    }
    @data =
      @data[ $curr_page * $total_rec .. $end ];
    return ($page_linx, @data);
}

#------------------------------------------------------------------------------

END { }

1;
