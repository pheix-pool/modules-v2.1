package Weblog::Static;

use strict;
use warnings;

##############################################################################
#
# File   :  Static.pm
#
##############################################################################
#
# Главный пакет управления блогом в пользовательской части CMS Pheix
#
##############################################################################

BEGIN {
    use lib '../';
    use lib 'libs/modules';

    # avoid broken dependence for development boundle
    use lib '../.sys_libs';
}

use POSIX;
use Encode;
use utf8;
use open qw(:std :utf8);
use Pheix::Tools qw(
    getSingleSetting
    getSettingFromGroupByAttr
    getProtoSrvName
);

#------------------------------------------------------------------------------

my $MODULE_NAME             = 'Weblog';
my $MODULE_DESCR            = 'Блог';
my $MODULE_DB_FILE          = 'conf/system/weblog.tnk';
my $INSTALL_FILE_PATH       = 'conf/system/install.tnk';
my $MODULE_INDX_TEMPLATE    = 'conf/config/index.html';
my $MODULE_USER_TEMPLATE    = 'conf/config/template.html';
my $PROTO                   = ( getSingleSetting('Config', 0, 'workviaproto' ) == 1) ? 'https://' : 'http://';

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModJsCssCode ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить дополнительный код js/css для данного модуля.
# Функция возвращает дополнительный код js/css для данного модуля.
#
#------------------------------------------------------------------------------

sub getModJsCssCode {
    my $modulecode = qq~
    <script type="text/javascript">
    function doWeblogRecordsLoad(pagenum) {
        jQuery("#loadweblogrecords")
                .load(
                        "/user.pl\?action=wlloadrecords\&amp;pagenum="
                                + pagenum
                                + "\&amp;rand="
                                + Math.random(),
                        function(responseTxt, statusTxt, xhr) {
                            if (statusTxt == "error")
                                alert("Error: " + xhr.status + ": "
                                        + xhr.statusText);
                        });
        top.location.href="#wl-records-anchor";
    }
    </script>~;
    return $modulecode;
}

#------------------------------------------------------------------------------
# get_power_status_for_module ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить статус модуля (включен или выключен).
# Функция возвращает статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub get_power_status_for_module {
    if ( -e $INSTALL_FILE_PATH ) {
        open my $fh, "<", $INSTALL_FILE_PATH ;
        my @_rows = <$fh>;
        close $fh;
        for my $i (0..$#_rows) {
            my @_cols = split /\|/, $_rows[$i];
            if ( $_cols[2] eq $MODULE_NAME ) { return $_cols[4]; }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# check_action_for_static_page ( $pageaction )
#------------------------------------------------------------------------------
#
# $pageaction - название action действия обрабатываемой страницы.
# Проверка существования action для страницы.
#
#------------------------------------------------------------------------------

sub check_action_for_static_page {
    my ($pageaction) = @_;
    my $pagefileid = 0;
    if ( -e $MODULE_DB_FILE ) {
        open my $fh, "<", $MODULE_DB_FILE;
        my @_rows = <$fh>;
        close $fh;
        for my $i (0..$#_rows) {
            my @tmp_sr = split /\|/, $_rows[$i];
            my @sub_sr = split /\,{1}\s{1,}?|\,{1}/, $tmp_sr[3];
            for ( my $j = 0 ; $j <= $#sub_sr ; $j++ ) {
                if ( $sub_sr[$j] eq $pageaction ) {
                    $pagefileid = $tmp_sr[1];
                    last;
                }
            }
        }
        if ( $pagefileid > 0 ) {
            if   ( -e "conf/pages/$pagefileid.txt" ) { ; }
            else {
                $pagefileid = 0;
            }
        } else {
            if ($pageaction eq 'index') {
                my $contenttype = getSingleSetting($MODULE_NAME, 1, 'indexas');
                my $records     = getRecordsNum();
                if ($contenttype == 1 && $records > 0) {
                    $pagefileid = 1;
                }
            }
        }
    }
    return $pagefileid;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_weblog_clientpage ( @_ )
#------------------------------------------------------------------------------
#
# @_ - массив входных аргументов.
# Промежуточная функция вывода записей из CMS.
#
#------------------------------------------------------------------------------

sub show_weblog_clientpage {
    my ($pageaction) = @_;
    my $tmpl_fname   = $MODULE_USER_TEMPLATE;
    if ( $pageaction eq 'index' &&
         ( getSingleSetting( $MODULE_NAME, 1, 'indextemplate' ) == 1 )
    ) {
        $tmpl_fname = $MODULE_INDX_TEMPLATE;
    }
#   if ($pageaction eq 'foo') {
#       $tmpl_fname = 'conf/config/template-foo.html';
#   } elsif ($pageaction eq 'bar') {
#       $tmpl_fname = 'conf/config/template-bar.html';
#   }
    if ( -e $tmpl_fname ) {
        open my $fh, "<", $tmpl_fname;
        my @scheme = <$fh>;
        close $fh;
        for my $cnt (0..$#scheme) {
            $scheme[$cnt] =
              _inner_weblog_fill_seotags( $scheme[$cnt], $pageaction );
            $scheme[$cnt] =
              Pheix::Pages::fillCommonTags( $scheme[$cnt],
                    "weblog", $MODULE_NAME );

            if ( $scheme[$cnt] =~ /\%content\%/ ) {
                my $content;
                if ($pageaction eq 'index') {
                    my $contenttype = getSingleSetting($MODULE_NAME, 1, 'indexas');
                        if ($contenttype == 1) {
                            $content = getWebLogIndexRecords(0);
                        } else {
                            $content = _inner_weblog_showpage(@_);
                        }
                } else {
                    $content = _inner_weblog_showpage(@_);
                }
                $scheme[$cnt] =~ s/\%content\%/$content/;
            }
            print $scheme[$cnt];
        }
    }
    else {
        print qq~
            <b>$MODULE_NAME:</b>&nbsp;
            in fuction show_weblog_clientpage() -&nbsp;
            $tmpl_fname not found!
        ~;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_showpage ( $pageaction )
#------------------------------------------------------------------------------
#
# $pageaction - название action действия обрабатываемой страницы.
# Функция непосредственного вывода страницы из CMS.
#
#------------------------------------------------------------------------------

sub _inner_weblog_showpage {
    my ($pageaction) = @_;
    my $pagefileid   = 0;
    my $weblog_content_variable;
    my $weblog_status = get_power_status_for_module($MODULE_NAME);
    if ( $weblog_status == 0 ) {
        if ( -e $MODULE_DB_FILE ) {
            open my $fh, "<", $MODULE_DB_FILE;
            my @sub_razdels = <$fh>;
            close $fh;
            for my $i (0..$#sub_razdels) {
                my @tmp_sr = split( /\|/, $sub_razdels[$i] );
                my @sub_sr = split( /\,{1}\s{1,}?|\,{1}/, $tmp_sr[3] );
                for ( my $j = 0 ; $j <= $#sub_sr ; $j++ ) {
                    if ( $sub_sr[$j] eq $pageaction ) {
                        $pagefileid = $tmp_sr[1];
                        last;
                    }
                }
            }
            if ( $pagefileid > 0 ) {
                if ( -e "conf/pages/$pagefileid.txt" ) {
                    $weblog_content_variable =
                      _inner_weblog_getcontent($pagefileid);
                }
                else {
                    $weblog_content_variable = qq~
                        <p class=error><b>$MODULE_NAME:</b>&nbsp;
                        файл conf/pages/$pagefileid.txt не найден!</p>
                    ~;
                }
            }
            else {
                $weblog_content_variable = qq~
                    <p class=error><b>$MODULE_NAME:</b>&nbsp;
                    $pageaction - страница не найдена!</p>
                ~;
            }
        }
        else {
            $weblog_content_variable = qq~
                <p class="error"><b>$MODULE_NAME:</b>&nbsp;
                файл $MODULE_DB_FILE не найден!</p>
            ~;
        }
    }
    else {
        $weblog_content_variable = qq~
            <p class="error"><b>$MODULE_NAME:</b> модуль выключен -&nbsp;
            содержимое страницы недоступно!</p>
        ~;
    }

    return $weblog_content_variable;

}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_getcontent ( $_rid )
#------------------------------------------------------------------------------
#
# $_rid - идентификатор записи в Блоге.
# Формирование содежимого страницы с обработкой внешних включений.
#
#------------------------------------------------------------------------------

sub _inner_weblog_getcontent {
    my ($_rid) = @_;
    my $_cntnt;
    open my $fh, '<', "conf/pages/$_rid\.txt";
    my $_data = join '', <$fh>;
    close $fh;
    $_data = parseWeblogVars( $_data );
    eval {
        $_data = Feedbck::Casual::parseFeedBackVars( $_data );
    };
    eval {
        my $fileup = Fileupld::Generic->new;
        $_data = $fileup->parseVariables( $_data );
    };
#   if ($@) {
#       $_cntnt = $_data;
#   }
    return $_data;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_fill_seotags ( $input, $pageaction )
#------------------------------------------------------------------------------
#
# $input - строка шаблона template.html.
# $pageaction - название action действия обрабатываемой страницы.
# Заполнение SEO тегов.
#
#------------------------------------------------------------------------------

sub _inner_weblog_fill_seotags {
    my ($input, $pageaction) = @_;
    if ( $input =~ /\%metakeys\%/ ||
         $input =~ /\%metadesc\%/ ||
         $input =~ /\%page_title\%/ ) {
        my $titletagtext;
        my $metadescrtagtext;
        my $metakeystagtext;

        if ( -e $MODULE_DB_FILE ) {
            open my $fh, "<", $MODULE_DB_FILE;
            my @sub_razdels = <$fh>;
            close $fh;
            for my $srinx (0..$#sub_razdels) {
                my @tmp_sr = split( /\|/, $sub_razdels[$srinx] );
                my @sub_sr = split( /\,{1}\s{1,}?|\,{1}/, $tmp_sr[3] );
                for ( my $j = 0 ; $j <= $#sub_sr ; $j++ ) {
                    if ( $sub_sr[$j] eq $pageaction ) {
                        $titletagtext     = $tmp_sr[4];
                        $metadescrtagtext = $tmp_sr[5];
                        $metakeystagtext  = $tmp_sr[6];
                        last;
                    }
                }
            }
        }

        if ( $input =~ /\%page_title\%/ && defined($titletagtext) ) {
            $input =~ s/\%page_title\%/$titletagtext/;
        }
        if ( $input =~ /\%metadesc\%/ && defined($metadescrtagtext) ) {
            $input =~ s/\%metadesc\%/$metadescrtagtext/;
        }
        if ( $input =~ /\%metakeys\%/ && defined($metakeystagtext) ) {
            $input =~ s/\%metakeys\%/$metakeystagtext/;
        }
    } elsif ($input =~ /\%metatags\%/) {
        $input = getWebLogMeta( $input, $pageaction );
    } elsif ($input =~ /\%weblog-page-header\%/ ) {
        $input = _inner_weblog_fill_pageheader( $input, $pageaction );
    } elsif ( $input =~ /\%weblog-pageaction\%/ && defined($pageaction) ) {
        $input =~ s/\%weblog-pageaction\%/$pageaction/gi;
    } else {
        ;
    }
    return $input;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _inner_weblog_fill_pageheader ( $input, $pageaction )
#------------------------------------------------------------------------------
#
# $input - строка шаблона template.html.
# $pageaction - название action действия обрабатываемой страницы.
# Вывод заголовка записи.
#
#------------------------------------------------------------------------------

sub _inner_weblog_fill_pageheader {
    my ($input, $pageaction) = @_;
    my $weblog_page_header;
    if (get_power_status_for_module == 0) {
        if ( -e $MODULE_DB_FILE ) {
            open my $fh, "<", $MODULE_DB_FILE;
            my @sub_razdels = <$fh>;
            close $fh;
            for my $srinx (0..$#sub_razdels) {
                my @tmp_sr = split( /\|/, $sub_razdels[$srinx] );
                my @sub_sr = split( /\,{1}\s{1,}?|\,{1}/, $tmp_sr[3] );
                for ( my $j = 0 ; $j <= $#sub_sr ; $j++ ) {
                    if ( $sub_sr[$j] eq $pageaction ) {
                        $weblog_page_header = $tmp_sr[7];
                        last;
                    }
                }
            }
        }
    }
    if ( $input =~ /\%weblog-page-header\%/ ) {
        $input =~ s/\%weblog-page-header\%/$weblog_page_header/;
    }
    return $input;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getWebLogMeta ( $input, $pageaction )
#------------------------------------------------------------------------------
#
# $input - строка шаблона template.html.
# $pageaction - название action действия обрабатываемой страницы.
# Вывод meta тегов для заданной страницы.
# Функция возвращает содержимое meta тегов для заданной страницы в
# HTML формате.
#
#------------------------------------------------------------------------------

sub getWebLogMeta {
    my ($input, $pageact) = @_;
    my $metaContent;
    if ( -e $MODULE_DB_FILE ) {
        open my $fh, "<", $MODULE_DB_FILE;
        my @records = <$fh>;
        close $fh;
        my @metadata;
        for my $srinx (0..$#records) {
            my @tmp_sr = split( /\|/, $records[$srinx] );
            my @sub_sr = split( /\,{1}\s{1,}?|\,{1}/, $tmp_sr[3] );
            for ( my $j = 0 ; $j <= $#sub_sr ; $j++ ) {
                if ( ($sub_sr[$j] eq $pageact) && ($tmp_sr[13] == 1) ) {
                    $tmp_sr[8]  =~ s/\<br\>//gi;
                    $tmp_sr[12] =~ s/^[\/]+//gi;
                    push(@metadata,
                         $tmp_sr[9],
                         $tmp_sr[7],
                         $tmp_sr[8],
                         $PROTO.$ENV{SERVER_NAME}."/".$tmp_sr[12],
                    );
                    last;
                }
            }
        }
        if ($#metadata > -1) {
            $metaContent = qq~
            <meta name="twitter:card" content="summary_large_image">
            <meta name="twitter:site" content="$metadata[0]">
            <meta name="twitter:title" content="$metadata[1]">
            <meta name="twitter:description" content="$metadata[2]">
            <meta name="twitter:image" content="$metadata[3]">~;
        }
    }
    $input =~ s/\%metatags\%/$metaContent/;
    return $input;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getWebLogIndexRecords ( $num, $pageaction )
#------------------------------------------------------------------------------
#
# $input - количество записей.
# Вывод списка записей на главную страницу.
# Функция возвращает записей для главной страницы в HTML формате.
#
#------------------------------------------------------------------------------

sub getWebLogIndexRecords {
    my ($show_page_num) = @_;
    my $weblog_cntnt;
    my $records_num   = getSingleSetting($MODULE_NAME, 1, 'indexnum');
    if ($records_num !~ /^[\d]+$/ || $records_num < 0) {
        $records_num = 10;
    }
    my $records_val   = getRecordsNum();
    my $page_header   = getSettingFromGroupByAttr($MODULE_NAME, 1, 'pageseotags', 'action', 'weblog', 'title');
    my $records_on_page = getIndxRecsList($show_page_num, $records_num);
    $weblog_cntnt .= qq~
        <div id="loadweblogrecords" class="wl-load-block">
        <h2>$page_header</h2>
        <a name="wl-records-anchor"></a>
        $records_on_page
        </div> <!-- loadweblogrecords -->~;
    return $weblog_cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getRecordsNum ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить количество записей в блоге.
# Функция возвращает количество записей в блоге.
#
#------------------------------------------------------------------------------

sub getRecordsNum {
    my $records = 0;
    if ( -e $MODULE_DB_FILE ) {
        open my $fh, "<", $MODULE_DB_FILE;
        my @sub_razdels = <$fh>;
        close $fh;
        my $found_valid_records = 0;
        for my $i (0..$#sub_razdels) {
            my $record = $sub_razdels[$i];
            $record =~ s/[\n\r\t]+//gi;
            my @cols = split(/\|/,$record);
            if ( $#cols == 14 &&
                 $cols[0] =~ /^[\d]+$/ &&
                 $cols[1] =~ /^[\d]+$/ &&
                 $cols[3] =~ /^[a-z0-9\-\_\.]+$/i &&
                 $cols[$#cols] == 0) {
                $records++;
            }
        }
    }
    return $records;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getIndxRecsList ( $page , $num_on_page )
#------------------------------------------------------------------------------
#
# $page - индекс страницы.
# $num_on_page - количество записей на странице.
# Получить таблицу с записями из БД модуля.
# Функция возвращает таблицу с записями из БД модуля в HTML формате.
#
#------------------------------------------------------------------------------

sub getIndxRecsList {
    my ($page, $num_on_page) = @_;
    my $records_tab;
    my @dataarr     = getArrayPage($page, $num_on_page);
    my $contenttype = getSingleSetting($MODULE_NAME, 1, 'indexas');
    if (@dataarr && $#dataarr > 0) {
        for (my $i = 1; $i <= $#dataarr; $i++) {
            my $link;
            my $record = $dataarr[$i];
            my @cols = split(/\|/, $record);
            my @sub_sr = split( /\,{1}\s{1,}?|\,{1}/, $cols[3] );
            if ($#sub_sr > 0) {
                for (my $k=0; $k<=$#sub_sr; $k++) {
                    if ($sub_sr[$k] eq 'index' && $contenttype == 1) {
                        if (defined($sub_sr[$k+1])) {
                            $link = $sub_sr[$k+1];
                        } elsif (defined($sub_sr[$k-1])) {
                            $link = $sub_sr[$k-1];
                        } else {
                            $link = 'index';
                        }
                        last;
                    }
                }
                if (!defined($link)) {
                    $link = $sub_sr[0];
                }
            } else {
                $link = $cols[3];
            }
            $records_tab .= qq~
            <div class="weblog-indx-contents-block">
                <a class="weblog-indx-contents-header" href="$link.html">$cols[7]</a>
                <p class="weblog-indx-contents-data">$cols[8]</p>
            </div>
            ~;
        }
    }
    if (!defined($records_tab)) {
        $records_tab = qq~<p>getIndxRecsList(): записи не найдены!</p>~;
    } else {
        $records_tab .= qq~<div class="weblog-indx-contents-pages-block">$dataarr[0]</div>~
    }
    return $records_tab;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getAnnonceRecsList ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить таблицу с записями из БД модуля для блока анонсов.
# Функция возвращает таблицу с записями из БД модуля для блока анонсов
# в HTML формате.
#
#------------------------------------------------------------------------------

sub getAnnonceRecsList {
    my $records_tab;
    if (get_power_status_for_module == 0) {
        my $page        = 0;
        my $num_on_page = getSingleSetting($MODULE_NAME, 1, 'annoncenum');
        my $contenttype = getSingleSetting($MODULE_NAME, 1, 'indexas');
        if ($num_on_page !~ /^[\d]+$/ || $num_on_page < 0) {
            $num_on_page = 5;
        }
        my @dataarr = getArrayPage($page, $num_on_page);
        if (@dataarr && $#dataarr > 0) {
            for (my $i = 1; $i <= $#dataarr; $i++) {
                my $link;
                my $record = $dataarr[$i];
                my @cols = split(/\|/, $record);
                my @sub_sr = split( /\,{1}\s{1,}?|\,{1}/, $cols[3] );
                if ($#sub_sr > 0) {
                for (my $k=0; $k<=$#sub_sr; $k++) {
                    if ($sub_sr[$k] eq 'index' && $contenttype == 1) {
                        if (defined($sub_sr[$k+1])) {
                            $link = $sub_sr[$k+1];
                        } elsif (defined($sub_sr[$k-1])) {
                            $link = $sub_sr[$k-1];
                        } else {
                            $link = 'index';
                        }
                        last;
                    }
                }
                if (!defined($link)) {
                    $link = $sub_sr[0];
                }
            } else {
                $link = $cols[3];
            }
                $records_tab .= qq~
                    <div class="weblog-annonce-contents-block">
                        <a class="weblog-annonce-contents-header" href="$link.html">$cols[7]</a>
                        <p class="weblog-annonce-contents-data">$cols[8]</p>
                    </div>
                ~;
            }
        }
        if (!defined($records_tab)) {
            $records_tab = qq~<!-- getAnnonceRecsList(): no records found! -->~;
        }
    }
    return $records_tab;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getArrayPage ( $shopid, $catid, $curr_page )
#------------------------------------------------------------------------------
#
# $curr_page - номер страницы.
# $num_on_page - количество записей на странице.
# Получение нужного количества элементов для отображения на заданной странице,
# а также блок ссылок для постраничной навигации.
# Функция возвращает HTML код блока ссылок для постраничной навигации, а также
# массив для отображения на заданной странице.
#
#------------------------------------------------------------------------------

sub getArrayPage {
    my ($curr_page, $num_on_page) = @_;
    my @array;
    my $shopid      = "\$shopid";
    my $catid       = "\$catid";
    if ($num_on_page < 0 || $num_on_page !~ /^[\d]+$/) {
        $num_on_page = 10;
    }
    if ( -e $MODULE_DB_FILE ) {
        open my $fh, "<", $MODULE_DB_FILE;
        @array = sort { $b cmp $a } <$fh>;
        close $fh;
    } else {
        return;
    }

    # Формирование навигации по страницам

    my $page_linx = '';
    my $pages     = 0;

    $pages = floor( $#array  / $num_on_page );

    if (( $pages * $num_on_page <= $#array ) || ( $pages < 1 ) ) {
        $pages++;
    }

    if ( $curr_page >= $pages ) { $curr_page = 0; }
    $page_linx = '&nbsp;-&nbsp;';
    for ( my $i = 1 ; $i <= $pages ; $i++ ) {
        if ( ( $i - 1 ) != $curr_page ) {
            $page_linx .= '<a href="javascript:doWeblogRecordsLoad('
                          . ( $i - 1 )
                          . ')" class=admpageslink>'
                          . $i
                          . '</a>&nbsp;-&nbsp;';
        }
        else {
            $page_linx .= '<span class=lightadminpagelink>'
                          . $i
                          . '</span>&nbsp;-&nbsp;';
        }
    }

    if ($pages > 1) {
        if ($curr_page >= 0) {
            $page_linx .= qq~<a href="javascript:doWeblogRecordsLoad(-1)">Все</a>&nbsp;-&nbsp;~;
        } else {
            $page_linx .= qq~<span class=lightadminpagelink>Все</span>&nbsp;-&nbsp;~;
        }
    }
    if ($curr_page >= 0) {
        if ( $curr_page >= $pages ) { $curr_page = $pages - 1; }
        my $end = ( ( $curr_page + 1 ) * $num_on_page ) - 1;
        if ( $end > $#array ) {
            $end = $#array;
        }
        @array = @array[ $curr_page * $num_on_page .. $end ];
    }

    # Формирование навигации по страницам

    return ($page_linx, @array);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getWlSitemap ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов
# Функция возвращает массив хэшей url-date.
#
#------------------------------------------------------------------------------

sub getWlSitemap {
    my @hashlist;
    my $_off = get_power_status_for_module();
    my $_set = getSingleSetting($MODULE_NAME, 1, 'exportsitemap');
    if ( -e $MODULE_DB_FILE  && ( $_off == 0 ) && ( $_set == 1))  {
        open my $fh, '<', $MODULE_DB_FILE;
        my @_rows = <$fh>;
        close $fh;
        for my $i (0..$#_rows) {
            my ($_sort, $_id, $_date, $_actn, @_othr) = split /\|/, $_rows[$i];
            my $timestamp = strftime '%Y-%m-%d', localtime($_id);
            my $_fname = 'conf/pages/'.$_id.'.txt';
            if (-e $_fname) {
                eval {
                    $timestamp = strftime '%Y-%m-%d',
                        localtime((stat($_fname))[9]);
                };
            }
            my $_url = getProtoSrvName().'/'.$_actn.'.html';
            my %_rec = ( 'loc' => $_url, 'lastmod' => $timestamp );
            push @hashlist, \%_rec;
        }
    }
    return @hashlist;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# parseWeblogVars ( $data )
#------------------------------------------------------------------------------
#
# $data - контент страницы.
# Функция подставляет заданные значения вместо контент-перемененных и
# возвращает модифицированный контент страницы.
#
#------------------------------------------------------------------------------

sub parseWeblogVars {
    my ($data) = @_;

    while ( $data =~ /%WEBLOG_INSERT_FILE\((.*?)\)%/s ) {
        my $fcntnt;
        my $fname = $1;
        if ( -e $fname ) {
            open my $fh, '<', $fname;
            $fcntnt = join( '', <$fh> );
            close $fh;
        }
        $data =~ s/%WEBLOG_INSERT_FILE\((.*?)\)%/$fcntnt/s;
    }

    while ( $data =~ /%WEBLOG_FILE_SIZE\((.*?)\)%/s ) {
        my $fsz   = 0;
        my $fname = $1;
        if ( -e $fname ) {
            my $_fs = ( -s $fname );
            if ( $_fs > 10000 ) {
                $fsz = sprintf "%.2f", $_fs / 1000000;
            }
            else {
                $fsz = sprintf "%.4f", $_fs / 1000000;
            }
        }
        $data =~ s/%WEBLOG_FILE_SIZE\((.*?)\)%/$fsz/s;
    }

    while ( $data =~ /%WEBLOG_FILE_MODDATE\((.*?)\)%/s ) {
        my $fdt   = 'undef';
        my $fname = $1;
        if ( -e $fname ) {
            # $fdt = ( strftime '%d, %B %Y5 &ndash; %T', localtime((stat($fname))[9]) );
            $fdt = Pheix::Tools::getDateUpdate((stat($fname))[9]);
        }
        $data =~ s/%WEBLOG_FILE_MODDATE\((.*?)\)%/$fdt/s;
    }

    return $data;
}

#------------------------------------------------------------------------------

END { }

1;
