my $weblog_is_switchedoff = Weblog::Static::get_power_status_for_module();
if ( $weblog_is_switchedoff == 0 ) {
    my $WebLogPage =
      Weblog::Static::check_action_for_static_page( $env_params[0] );
    if ( $WebLogPage > 0 && defined($co) ) {
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        Weblog::Static::show_weblog_clientpage( $env_params[0] );
        exit;
    }

    if ( ( defined( $env_params[0] ) ) && ( $env_params[0] eq 'wlloadrecords' ) ) {
        my $got_page = $env_params[1];
        if ( $got_page !~ /^[\d]+$/ ||
             !defined($got_page) ) {
                 if ($got_page < 0) {
                     $got_page = -1;
                 } else {
                     $got_page = 0;
                 }
             }
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        print Weblog::Static::getWebLogIndexRecords($got_page);
        exit;
    }
}