my $xeeg = Xaoslab::EEG->new;

if ( ( $xeeg->getModPwrStatus() == 0 ) && ( $xeeg->{mod_user} == 1 ) ) {
    if ( defined $env_params[0] && $env_params[0] eq 'xaoslabjson' ) {
        if (defined $co) {
            print $co->header(
                -type            => 'application/json; charset=UTF-8',
                -Last_Modified   => $global_last_modified,
                '-Expires'       => $global_last_modified,
                '-Cache-Control' => 'max-age=0, must-revalidate',
                '-Access-Control-Allow-Origin' => '*'
            );
            # $xeeg->showXEEGHomePage();
            print $xeeg->getJSONAudioFilesList();
        } else {
            die "CGI object is missing";
        }
        exit;
    }
    if ( defined $env_params[0] && $env_params[0] eq 'xaoslabplot' ) {
        if (defined $co) {
            print $co->header(
                -type            => 'text/html; charset=UTF-8',
                -Last_Modified   => $global_last_modified,
                '-Expires'       => $global_last_modified,
                '-Cache-Control' => 'max-age=0, must-revalidate',
            );
            my $objid  = $env_params[1] || 0;
            print $xeeg->drawChart( $objid );
        } else {
            die "CGI object is missing";
        }
        exit;
    }
}
