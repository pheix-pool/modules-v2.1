package Xaoslab::EEG;

use strict;
use warnings;

BEGIN {
    use lib '../';
    use lib 'libs/modules';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs';
}

##############################################################################
#
# File   :  EEG.pm
#
##############################################################################

use JSON;
use POSIX;
use Encode qw(decode encode);
use Pheix::Tools qw(getConfigValue getModXml getSingleSetting);
use Pheix::Pages qw(fillCommonTags);

#------------------------------------------------------------------------------
# new ()
#------------------------------------------------------------------------------

sub new {
    my $MODULE_NAME   = "Xaoslab";
    my $MODULE_FOLDER = 'admin/libs/modules/' . $MODULE_NAME;
    my ($class)     = @_;
    my $self        = {
        name     => 'Xaoslab::EEG',
        version  => '2.2',
        mod_name => $MODULE_NAME,
        mod_dscr => 'EEG collect/analyze module for P5 Pheix CMS',
        mod_fldr => $MODULE_FOLDER,
        mod_inst => 'conf/system/install.tnk',
        mod_tmpl => 'conf/config/template.html',
        mod_dbfn => getConfigValue( $MODULE_FOLDER, getModXml($MODULE_FOLDER), 'tankfile' ),
        mod_scrt => getStatsJsCss(),
        mod_user => getSingleSetting( $MODULE_NAME, 1, 'soffuserarea' ),
    };
    bless $self, $class;
    return $self;
}

#------------------------------------------------------------------------------
# getStatsJsCss ( )
#------------------------------------------------------------------------------

sub getStatsJsCss {
    return '<!-- no code available -->';
}

#------------------------------------------------------------------------------
# getModPwrStatus ( )
#------------------------------------------------------------------------------

sub getModPwrStatus {
    my ($self) = @_;
    my $fn     = $self->{mod_inst};
    if ( -e $fn ) {
        open my $fd, '<', $fn or die "unable to open " . $self->{mod_inst} . ": $!";
        my @recs = <$fd>;
        close $fd or die "unable to close " . $self->{mod_inst} . ": $!";
        for my $i (0..$#recs) {
            my @tmp = split( /\|/, $recs[$i] );
            if ( $tmp[2] eq $self->{mod_name} ) { return $tmp[4]; }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------
# getXaoslabData( $self, $recid, ... )
#------------------------------------------------------------------------------

sub getXaoslabData {
    my ($self, $recid ) = @_;
    my @recs;
    my %data;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            $recs[$i] =~ s/[\r\n\t]+//gi;
            my @cols  = split(/\|/,$recs[$i]);
            %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4],
                'reslt' => ( $cols[5] == 0 ? 'not processed' : 'processed' ),
                'jsonp' => ( $cols[6] eq 'undefined' ? 'none' : 'plot' ),
                'csvpt' => ( $cols[6] eq 'undefined' ? 'none' : 'data' ),
            );
            if ( $data{recid} == $recid ) {
                last;
            }
        }
    }
    return %data;
}

#------------------------------------------------------------------------------
# showXEEGHomePage ( )
#------------------------------------------------------------------------------

sub showXEEGHomePage {
    my ($self)           = @_;
    my $pageaction       = $ENV{REQUEST_URI};

    if ( -e $self->{mod_tmpl} ) {
        open my $fh, '<', $self->{mod_tmpl}
            or die 'unable to open ' . $self->{mod_tmpl} . ": $!";;
        my @tmpl = <$fh>;
        close $fh or die 'unable to close ' . $self->{mod_tmpl} . ": $!";
        for my $cnt (0..$#tmpl) {
            $tmpl[$cnt] = fillCommonTags( $tmpl[$cnt], $pageaction );
            if ( $tmpl[$cnt] =~ /\%content\%/ ) {
                my $content = $self->getAudioFilesList();
                $tmpl[$cnt] =~ s/\%content\%/$content/;
            }
            print $tmpl[$cnt];
        }
    }
    else {
        print qq~
            <b>$self->{mod_name}:</b>&nbsp;
            in fuction showXEEGHomePage() -&nbsp;
            $self->{mod_tmpl} not found!
        ~;
    }
}

#------------------------------------------------------------------------------
# getAudioFilesList ( )
#------------------------------------------------------------------------------

sub getJSONAudioFilesList {
    my ($self) = @_;
    my @tojson;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or die "unable to open " . $self->{mod_dbfn} . ": $!";
        my @recs = reverse <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            $recs[$i] =~ s/[\r\n\t]+//gi;
            my $cntr  = ($i+1);
            my @cols  = split(/\|/,$recs[$i]);
            my $date  = localtime($cols[1]);
            $date     =~ s/\"/\&quot;/gi;
            my %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => decode("UTF-8", $cols[4]),
                'reslt' => ( $cols[5] == 0 ? 'not processed' : 'processed' ),
                'jsonp' => ( $cols[6] eq 'undefined' ? 'none' : $cols[6] ),
                'csvpt' => ( $cols[7] eq 'undefined' ? 'none' : $cols[7] ),
                'udate' => $date
            );
            if ( ( $data{reslt} eq 'processed' ) &&
                 ( $data{jsonp} ne 'none' )) {
                push(@tojson, \%data);
            }
        }
    }
    return JSON->new->encode(\@tojson);
}

#------------------------------------------------------------------------------
# drawChart( $self, $sid, $recid )
#------------------------------------------------------------------------------

sub drawChart {
    my ($self,$recid) = @_;
    my %data  = $self->getXaoslabData($recid);
    my $chart = qq~
    <html>
    <head>
        <meta charset="utf-8">
        <title>EEG plot for «$data{hminm}»</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
        <script src="https://unpkg.com/wavesurfer.js"></script>
        <style>
            body { margin:0px; padding:0px; background-color:#000; }
            canvas#myChart { position: absolute; z-index: 1; height:100vh; background-color:#fff; }
            .container { position: absolute; width:100%; height:100vh; margin:0px; padding:0px; }
            .table { display:table; width:100%; height:100%; margin:0px; padding:0px; }
            .table_cell { display:table-cell; width:100%; height:100%; vertical-align:bottom; margin:0px; padding:0px; }
            .player { margin:0px; padding:0px; display: inline-block; position: relative; width:100%; height:250px; background: linear-gradient(to top, rgba(0,0,0,1), rgba(0,0,0,.5)); z-index: 2; }
            .waveform { margin:0px; padding:0px; }
            .hint { font-size:10pt; color: #fff; font-family: 'Roboto', sans-serif; position: absolute; z-index: 3; width:100%; text-align:center; top: calc( 100vh - 40px ); }
            .playaxe { position: absolute; left: 0px; top: 0px; width: 2px; height: 100vh; margin: 0px; padding: 0px; background-color: rgba(255,0,0,0.6); z-index: 2;  }
    		</style>
        <script>
        var fftjson = null;
        function ffteeg( jsondata ) {
            if ( typeof jsondata === 'undefined' || jsondata == null ) {
                throw new Error("ffteeg(): JSON data load is failed: no data to store!");
            } else {
                fftjson = jsondata;
                console.log('ffteeg(): JSON data is loaded');
            }
            return 1;
        }
        </script>
    </head>
    <body id="body">
        <canvas id="myChart" style="margin-left:0px;"></canvas>
        <div class="container">
            <div class="table">
                <div class="table_cell">
                    <div class="player">
                        <div class="waveform"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="playaxe"></div>
        <div class="hint">Double-click on plot to stop playing</div>
        <script type="text/javascript" src="media/upload/$recid\_data.js"></script>
        <script type="text/javascript">var iters=[],data_delta=[],data_theta=[],data_alpha=[],data_beta=[],data_gamma=[],data_eeg_6=[],data_eeg_7=[],data_eeg_8=[];if("undefined"==typeof fftjson||null==fftjson)throw new Error("JSON data is not available!");for(var i=0;i<fftjson.eeg.length;i++)data_delta.push(fftjson.eeg[i][0]),data_theta.push(fftjson.eeg[i][1]),data_alpha.push(fftjson.eeg[i][2]),data_beta.push(fftjson.eeg[i][3]),data_gamma.push(fftjson.eeg[i][4]),data_eeg_6.push(fftjson.eeg[i][5]),data_eeg_7.push(fftjson.eeg[i][6]),data_eeg_8.push(fftjson.eeg[i][7]),iters.push(i);var ctx=document.getElementById("myChart").getContext("2d"),myChart=new Chart(ctx,{type:"line",data:{labels:iters,datasets:[{label:"δ-ритм",data:data_delta,fill:!1,backgroundColor:"rgba(173, 173, 170, 0.8)",borderColor:"rgba(173, 173, 170, 0.5)"},{label:"θ-ритм",data:data_theta,fill:!1,backgroundColor:"rgba(54, 162, 235, 0.8)",borderColor:"rgba(54, 162, 235, 0.5)"},{label:"α-ритм",data:data_alpha,fill:!1,backgroundColor:"rgba(4, 207, 14, 0.8)",borderColor:"rgba(4, 207, 14, 0.5)"},{label:"β-ритм",data:data_beta,fill:!1,backgroundColor:"rgba(217, 22, 191, 0.8)",borderColor:"rgba(217, 22, 191, 0.5)"},{label:"γ-ритм",data:data_gamma,fill:!1,backgroundColor:"rgba(245, 220, 0, 0.8)",borderColor:"rgba(245, 220, 0, 0.5)"},{label:"ритм №5",data:data_eeg_6,fill:!1,backgroundColor:"rgba(57, 250, 192, 0.8)",borderColor:"rgba(57, 250, 192, 0.5)",hidden:!0},{label:"ритм №6",data:data_eeg_7,fill:!1,backgroundColor:"rgba(242, 174, 170, 0.8)",borderColor:"rgba(242, 174, 170, 0.5)",hidden:!0},{label:"ритм №7",data:data_eeg_8,fill:!1,backgroundColor:"rgba(171, 171, 245, 0.8)",borderColor:"rgba(171, 171, 245, 0.5)",hidden:!0}]},options:{layout:{padding:{left:0,right:0,top:0,bottom:0}},scales:{yAxes:[{ticks:{min:-100},display:!1}],xAxes:[{ticks:{display:!1}}]}}});</script>
        <script type="text/javascript">jQuery(document).ready(function(){var e=jQuery("#myChart").css("width"),r=jQuery("#myChart").css("height");console.log("canvas width : "+e),console.log("canvas height: "+r),jQuery(".container").css("height",parseInt(r)+10+"px")});var wavesurfer=WaveSurfer.create({container:".waveform",waveColor:"white",progressColor:"#63ffac",height:250});wavesurfer.load("media/upload/$recid\.mp3"),wavesurfer.on("seek",function(e){var r=e*wavesurfer.getDuration();console.log(r),wavesurfer.play(r)}),jQuery("#body").dblclick(function(){wavesurfer.stop(),wavesurfer.pause()});wavesurfer.on('audioprocess',function(){if(wavesurfer.isPlaying()){var axe=parseInt(jQuery(".waveform wave wave").css("width"));jQuery(".playaxe").css("left",(axe-2)+"px")}});wavesurfer.on('pause',function(){jQuery(".playaxe").css("left","0px")})</script>
    </body>
    </html>
    ~;
    return decode("UTF-8", $chart);
}

#------------------------------------------------------------------------------

END { }

1;
