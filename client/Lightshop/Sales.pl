use lib '../';
use Lightshop::Session;

my $lshop = Lightshop::Sales->new();

my $lightshop_is_switchedoff = Lightshop::Sales::get_power_status_for_module();
if ( $lightshop_is_switchedoff == 0 ) {

    if (    defined ($env_params[0]) &&
            defined ($env_params[1]) &&
            defined ($env_params[2]) &&
            $env_params[0] eq 'addtocart' && 
            $env_params[1] =~ /^[\d]+$/ && 
            $env_params[2] =~ /^[\d]+$/) {

        Lightshop::Sales::delete_outdated_cart_entries();
        my $shopid    = $env_params[1];
        my $product   = $env_params[2];
        my $user_id   = Lightshop::Sales::get_cookie('alfacms_lightshop') || -1;
        my $cur_items = Lightshop::Sales::get_from_cart($user_id) || '';
        my $cookie    = "";

        if ( ( $user_id !~ /^[\d]+$/ ) || ( $user_id eq '' ) ) {
            my $unique_id = Lightshop::Sales::get_unique_id();
            $cookie =
              Lightshop::Sales::write_cookie( 'alfacms_lightshop', $unique_id ) || 'undefined';
            Lightshop::Sales::add_to_cart( $unique_id, "$shopid-$product-1" );
        }
        else {
            if ( $cur_items ne '' ) {
                $cur_items =~ s/[\r\n]+//g;
                my @tmp = split( /\./, $cur_items );
                my $found = 0;
                for ( my $i = 0 ; $i <= $#tmp ; $i++ ) {
                    my @tmp2 = split( /\-/, $tmp[$i] );
                    if ( $tmp2[0] eq $shopid && $tmp2[1] eq $product ) {
                        my $nq = ( $tmp2[2] + 1 );
                        $tmp[$i] = join("-", $shopid, $product ,$nq);
                        $cur_items = join( "\.", @tmp );
                        $found = 1;
                        last;
                    }
                }
                if ( $found == 0 ) { $cur_items = "$cur_items.$shopid-$product-1"; }
                Lightshop::Sales::modify_cart( $user_id, $cur_items );
            }
            else { Lightshop::Sales::add_to_cart( $user_id, "$shopid-$product-1" ); }
        }

        #print $co->redirect(-cookie => $cookie, -uri => "$ENV{HTTP_REFERER}");
        print $co->redirect(
            -cookie => $cookie,
            -uri    => "$ENV{SCRIPT_NAME}?action=cart&ref=$ENV{HTTP_REFERER}"
        );
        exit;
    }

    if ( defined ($env_params[0]) && $env_params[0] eq 'cart' ) {
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        Lightshop::Sales::show_cart_lightshopmodule();
        exit;
    }

    if ( defined ($env_params[0]) && defined ($env_params[2]) && $env_params[0] eq 'getorderform' ) {
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        my $formtype = 0;
        if ( $env_params[1] == 1 ) {
            $formtype = 1;
        }
        print Lightshop::Sales::getOrderForm($formtype, $env_params[2]);
        exit;
    }

    if ( defined ($env_params[0]) && $env_params[0] eq 'delfromcart' ) {
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        if ( $env_params[1] =~ /^[\d]+$/ && $env_params[2] =~ /^[\d]+$/ && $env_params[3] =~ /^[\d]+$/) {
            Lightshop::Sales::delete_from_cart( $env_params[1],
                $env_params[2], $env_params[3] );
        }
        Lightshop::Sales::show_cart_lightshopmodule();
        exit;
    }

    if ( defined ($env_params[0]) && $env_params[0] eq 'recountcost' ) {

        my $dostavka = 0;
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        if ( $env_params[1] =~ /^[\d]+$/ ) {
            my $user_id   = $env_params[1];
            my $carttable = Lightshop::Sales::get_from_cart($user_id);
            my @items_in_cart;
            if ( $carttable ne '' ) {
                @items_in_cart = split( /\./, $carttable );
                if ( $#items_in_cart > -1 ) {
                    for ( my $i = 0 ; $i <= $#items_in_cart ; $i++ ) {
                        my @tmp = split( /\-/, $items_in_cart[$i] );
                        my $item_new_value = $co->param("$tmp[0]-$tmp[1]");
                        if (   ( $item_new_value =~ /^[\d]+$/ )
                            && ( $item_new_value > 0 ) )
                        {
                            $tmp[2] = $item_new_value;
                            $items_in_cart[$i] = join( "-", @tmp );
                        }
                    }
                    $carttable = join( ".", @items_in_cart );
                    Lightshop::Sales::modify_cart( $user_id, $carttable,
                        $dostavka );
                    print "cart is updated";
                } else {
                    print "ignore update - no items in cart {\$#items_in_cart == -1}";
                }
            } else {
                print "ignore update - empty carttable {\$carttable eq ''}";
            }
        } else {
            print "ignore update - invalid user id {$env_params[1]}";
        }
        #Lightshop::Sales::show_cart_lightshopmodule($dostavka);
        exit;
    }

    if ( defined ($env_params[0]) && $env_params[0] eq 'submitorder' ) {

        my $paytype      = $co->param('paytype');
        if ($paytype eq "1") {
            $paytype = 1;
        } else {
            $paytype = 0
        }

        # process rekvizity

        my $rekviz_check = 1;
        my @rekvizarr = ();
        my $rekvizlen    = $co->param('orgrekvizlen');
        if ($rekvizlen !~ /^[\d]+$/ || $rekvizlen < 0) {$rekvizlen = 0;}
        for (my $rinx = 0; $rinx <= $rekvizlen; $rinx++) {
            my $cur_rekviz = Encode::decode("utf8", $co->param('orgrekviz'.$rinx));
            push(@rekvizarr, $cur_rekviz);
            if (!defined($cur_rekviz) || $cur_rekviz eq '') {
                $rekviz_check = 0;
            }
        }

        # process rekvizity

        my $fio          = $co->param('userfio');
        my $email        = $co->param('useremail');
        my $phone        = $co->param('userphone');
        my $address      = $co->param('useraddress');
        my $usercomments = '-'; #$co->param('usercomments');
        $address =~ s/[\r\n]+/\<br\>/g;

        my $spam      = $co->param('code1');
        my $spam_conf = $co->param('code2');

        my $decrypted_spam = Pheix::Tools::doBlowfishDecrypt( 
            Pheix::Tools::getStringFromHexStr( $spam ) );

        $spam = sprintf( "%.6f", "0.$decrypted_spam" );
        my @tmp = split( /\./, $spam );
        $spam = $tmp[1];

        if ( $paytype == 1 ) { $rekviz_check = 1; }

        if (   $fio ne ''
            && $email ne ''
            && $phone ne ''
            && $address ne ''
            && $usercomments ne ''
            #&& $rekvizity ne '' )
            && $rekviz_check == 1)
        {
            if ($spam_conf eq '' || $spam_conf !~ /^[\d]+$/) {
                $spam_conf = 0;
            }
            if ($spam eq '' || $spam !~ /^[\d]+$/) {
                $spam = 0;
            }
            if ( $spam == $spam_conf 
                 && $spam_conf > 0
                 && $spam > 0 )
            {
                my $saverc = 0;
                my $_userflags = 0;
                my $_userid = Lightshop::Sales::get_cookie('alfacms_lightshop');
                my $login   = Lightshop::Sales::get_uniq_login($email);

                my @arr_param_values = (
                    $_userid,
                    Lightshop::Sales::LIGHTSHOP_date_format(),
                    Encode::decode("utf8", $login),
                    Lightshop::Sales::LIGHTSHOP_generate_passw(),
                    Encode::decode("utf8", $email),
                    Encode::decode("utf8", $fio),
                    Encode::decode("utf8", $address),
                    Encode::decode("utf8", $phone),
                    $usercomments,
                    0
                );
                for ( my $i = 0 ; $i < $#arr_param_values ; $i++ ) {
                    $arr_param_values[$i] =~ s/[\r\n]+//g;
                    if ( $arr_param_values[$i] eq '' ) {
                        $arr_param_values[$i] = 'undefined';
                    }
                }

                my $_deal_id   = time;
                my $_dial_tab  = Lightshop::Sales::get_from_cart($_userid);
                my $_dial_cost = Lightshop::Sales::get_cart_cost($_userid);
                my $_dostavka  = 0;

                my @arr_deal_values = (
                    $_deal_id, Lightshop::Sales::LIGHTSHOP_date_format(),
                    $_userid, $_dial_tab, $_dial_cost, 0, $_dostavka, 0
                );

                if ( $paytype == 0 && $rekviz_check == 1) {

                    # save rekviz to file #

                    my $filename = lc($_userid.'.xml'); 
                    $saverc = Lightshop::Sales::doRekvizitySave( $_userid, $filename, @rekvizarr );

                    # save rekviz to file #

                }

                Lightshop::Sales::LIGHTSHOP_add_to_dealsDB(@arr_deal_values);
                Lightshop::Sales::LIGHTSHOP_add_to_userDB(@arr_param_values);

                my $blank_cookie =
                  Lightshop::Sales::write_blank_cookie('alfacms_lightshop');
                print $co->header(
                    -cookie          => $blank_cookie,
                    -type            => 'text/html; charset=UTF-8',
                    -Last_Modified   => $global_last_modified,
                    '-Expires'       => $global_last_modified,
                    '-Cache-Control' => 'max-age=0, must-revalidate'
                );

                Lightshop::Sales::delete_cart_entries_through_order(
                    $env_params[1] );
                Lightshop::Sales::show_submitorderpage_lightshopmodule(Encode::decode("utf8", $fio));
                my $compile_email_message =
                  $lshop->getCompiledEmail( Encode::decode("utf8", $fio), $_deal_id, $_userid );
                if ( $compile_email_message ne '' ) {
                    my $rc = Lightshop::Sales::simple_send_message_uni( $email,
                        $compile_email_message, $_deal_id );
                } 
            }
            else {
                $lshop->printHttpHeader();
                Lightshop::Sales::show_cart_lightshopmodule(
                    $paytype, '', $fio, $email, $phone, $address,
                    $usercomments, @rekvizarr, 2 );
            }
        }
        else {
            $lshop->printHttpHeader();
            Lightshop::Sales::show_cart_lightshopmodule(
                $paytype, '', $fio, $email, $phone, $address, $usercomments,
                @rekvizarr, 3 );
        }
        exit;
    }

    # Lightshop LC

    if ( defined ($env_params[0]) && $env_params[0] eq 'login' ) {
        $lshop->printHttpHeader();
        $lshop->showLightShopUsrLoginPage();
        exit;
    }

    if ( defined ($env_params[0]) && $env_params[0] eq 'loginto' ) {
        $lshop->printHttpHeader();
        my $userid;
        my $usrlgn;
        my @usrdat;
        my $dealid = $co->param('dealid');
        my $passwd = $co->param('userpassw');
        if ($dealid !~ /^[\d]+$/) {
            $usrlgn = $dealid;
            @usrdat = $lshop->getUsrDetailByLogin( $usrlgn );
            $dealid = $lshop->getDealIdByUid($usrdat[0]);
        } else {
            $userid = $lshop->getUidByDealId( $dealid );
            $usrlgn = $lshop->getUserDataById( $userid, 2 );
            @usrdat = $lshop->getUsrDetailByLogin( $usrlgn );
        }
        if ( (($usrdat[0] > -1) && ( $usrdat[3] eq $passwd )) || 
             (( $usrlgn eq $dealid) && ( $usrdat[3] eq $passwd )) ) {
            Lightshop::Session::delSessionByTimeOut();
            Lightshop::Session::createUserSession( $usrdat[0] );
            my $sesid = Lightshop::Session::getSessionId( $usrdat[0]);
            $lshop->showLightShopUsrHomePage(
                $sesid, 
                Lightshop::Session::getCryptLoginBySid( $sesid ), $dealid, 0 );
        } else { 
            $lshop->showLightShopUsrLoginPage();
        }
        exit;
    }

    if ( defined ($env_params[1]) && $env_params[1] eq 'lightshoplc' ) {
        $lshop->printHttpHeader();
        my $sesid = $env_params[0];
        Lightshop::Session::delSessionByTimeOut();
        my $check_auth = Lightshop::Session::isValidUser( $sesid );
        if ( $check_auth == 1 ) {
            my $crypted_userid =
              Lightshop::Session::getCryptLoginBySid( $sesid );
            my $userid = Pheix::Tools::doBlowfishDecrypt(
                Pheix::Tools::getStringFromHexStr( $crypted_userid ) );
            my $usrlgn = $lshop->getUserDataById( $userid, 2 );
            my @usrdat = $lshop->getUsrDetailByLogin( $usrlgn );
            my $dealid = $lshop->getDealIdByUid($usrdat[0]);
            $lshop->showLightShopUsrHomePage( 
                $sesid,
                $crypted_userid,
                $dealid, 0 );
        } else {
            $lshop->showLightShopUsrLoginPage();
        }
        exit;
    }

    if ( defined ($env_params[1]) && $env_params[1] eq 'lightshopchcklgn' ) {
        $lshop->printHttpHeader();
        my $rc    = 0;
        my $sesid = $env_params[0];
        Lightshop::Session::delSessionByTimeOut();
        my $check_auth = Lightshop::Session::isValidUser( $sesid );
        if ( $check_auth == 1 ) {
            my $logintochck = $co->param('loginchck');
            my $crypted_userid =
              Lightshop::Session::getCryptLoginBySid( $sesid );
            my $userid = Pheix::Tools::doBlowfishDecrypt(
                Pheix::Tools::getStringFromHexStr( $crypted_userid ) );
            my $usrlgn = $lshop->getUserDataById( $userid, 2 );
            if ($usrlgn ne $logintochck) {
                $rc = $lshop->isLoginUnique($logintochck);   
            } else {
                $rc = 2;
            }
        }
        print $rc;
        exit;
    }

    if ( defined ($env_params[1]) && $env_params[1] eq 'lightshopusermodify' ) {
        $lshop->printHttpHeader();
        my $sesid = $env_params[0];
        Lightshop::Session::delSessionByTimeOut();
        my $check_auth = Lightshop::Session::isValidUser( $sesid );
        if ( $check_auth == 1 ) {
            my $crypted_userid =
              Lightshop::Session::getCryptLoginBySid( $sesid );
            my $userid = Pheix::Tools::doBlowfishDecrypt(
                Pheix::Tools::getStringFromHexStr( $crypted_userid ) );
            my $rekviz_check = 1;
            my $uploadmess;
            my @rekvizarr;
            my $dealid      = $lshop->getDealIdByUid($userid);
            my $paytype     = $co->param('paytype');
            my $rekvizity   = $co->param('rekvizity');
            my $userfio     = $co->param("userfio");
            my $userregdate = $co->param("userregdate");
            my $userlogin   = $co->param("userlogin");
            my $userpassw   = $co->param("userpassw");
            my $useremail   = $co->param("useremail");
            my $useraddr    = $co->param("useraddress");
            my $userphone   = $co->param("userphone");
            my $usercomments= '-';
            $useraddr =~ s/[\r\n]+/\<br\>/g;

            if ($paytype eq "1") {
                $paytype = 1;
            } else {
                $paytype = 0;
                my $rekvizlen    = $co->param('orgrekvizlen');
                if ($rekvizlen !~ /^[\d]+$/ || $rekvizlen < 0) {$rekvizlen = 0;}
                for my $rinx (0..$rekvizlen) {
                    my $cur_rekviz = Encode::decode("utf8", $co->param('orgrekviz'.$rinx));
                    push(@rekvizarr, $cur_rekviz);
                    if (!defined($cur_rekviz) || $cur_rekviz eq '') {
                        $rekviz_check = 0;
                    }
                }
            }

            my $spam      = $co->param('code1');
            my $spam_conf = $co->param('code2');

            my $decrypted_spam = Pheix::Tools::doBlowfishDecrypt( 
                Pheix::Tools::getStringFromHexStr( $spam ) );

            $spam = sprintf( "%.6f", "0.$decrypted_spam" );
            my @tmp = split( /\./, $spam );
            $spam = $tmp[1];

            if ( 
                $userlogin ne '' &&
                $userpassw ne '' &&
                $userfio ne '' && 
                $useremail ne '' && 
                $userphone ne '' &&
                $useraddr ne '' &&
                $usercomments ne '' &&
                $rekviz_check == 1) {
                    if ($spam_conf eq '' || $spam_conf !~ /^[\d]+$/) {
                        $spam_conf = 0;
                    }
                    if ($spam eq '' || $spam !~ /^[\d]+$/) {
                        $spam = 0;
                    }
                    if ( $spam == $spam_conf 
                         && $spam_conf > 0
                         && $spam > 0 ) {
                             if ($paytype == 0) {
                                 if ( $rekviz_check == 1) {
                                     my $filename = lc($userid.'.xml'); 
                                     my $saverc = Lightshop::Sales::doRekvizitySave( $userid, $filename, @rekvizarr );
                                 }
                             }
                             $lshop->modifyUserData(
                                 $userid,   $userregdate, $userlogin,
                                 $userpassw, $useremail,   $userfio,
                                 $useraddr,  $userphone,   $usercomments );
                             $lshop->showLightShopUsrHomePage( 
                                 $sesid,
                                 $crypted_userid, 
                                 $dealid, 3 );
                    } else {
                        $lshop->showLightShopUsrHomePage( 
                            $sesid,
                            $crypted_userid, 
                            $dealid, 2, 
                            $userfio,
                            $userlogin,
                            $userpassw,
                            $useremail,
                            $userphone,
                            $useraddr,
                            $usercomments, @rekvizarr);
                        } 
            } else {
                $lshop->showLightShopUsrHomePage( 
                    $sesid,
                    $crypted_userid, 
                    $dealid, 1, 
                    $userfio,
                    $userlogin,
                    $userpassw,
                    $useremail,
                    $userphone,
                    $useraddr,
                    $usercomments, @rekvizarr);
            }
        }
        else { $lshop->showLightShopUsrLoginPage(); }
        exit;
    }

    if ( defined ($env_params[0]) && defined ($env_params[1]) && $env_params[1] eq 'lightshopusrexit' ) {
        my $outer_ses_id = $env_params[0];
        Lightshop::Session::doUserExit($outer_ses_id);
        print $co->redirect( -uri => "/login.html" );
        exit;
    }
}
;    # module is switched off