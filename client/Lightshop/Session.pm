package Lightshop::Session;

use strict;
use warnings;

##########################################################################
#
# File   :  Session.pm
#
##########################################################################
#
# Пакет управления сессиями пользовательской части модуля Lightshop
#
##########################################################################

our ( @ISA, @EXPORT );

BEGIN {

    use lib '../';
    use lib 'libs/modules';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs';

}

use Pheix::Tools qw(
    doBlowfishDecrypt
    doBlowfishEncrypt
    getHexStrFromString
    getStringFromHexStr
    generateKeyTag
);
use Digest::MD5 qw(md5_hex);
use Crypt::CBC;

# Поддержка UTF-8 на уровне переменных, regexp и строковых функций
use Encode;
use utf8;
use open qw(:std :utf8);

#---------------------------------------------------------------------------------

my $CURR_LOGIN_PATH = 'conf/system/cur_login.tnk';
my $CLIENT_PWD_PATH = 'conf/system/cur_login.tnk';

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# delSessionByTimeOut ()
#---------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Удаление устаревших записей из файла cur_login.tnk.
# Функция не возвращает какое-либо значение.
#
#---------------------------------------------------------------------------------

sub delSessionByTimeOut {
    my @alive_entries = ();
    open( FHCLEAR, "<$CURR_LOGIN_PATH" );
    my @f_strings = <FHCLEAR>;
    close(FHCLEAR);

    for ( my $clearLogInx = 0 ; $clearLogInx <= $#f_strings ; $clearLogInx++ ) {
        my @tmp = split( /\|/, $f_strings[$clearLogInx] );
        $tmp[$#tmp] =~ s/[\r\n]+//g;
        if ( time - $tmp[3] < 900 ) {
            push( @alive_entries, $f_strings[$clearLogInx] );
        }
    }

    open( FHCLEAR, ">$CURR_LOGIN_PATH" );
    flock FHCLEAR, 2;
    print FHCLEAR @alive_entries;
    truncate( FHCLEAR, tell(FHCLEAR) );
    flock FHCLEAR, 8;
    close(FHCLEAR);
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# $rc = createUserSession ( $userid )
#---------------------------------------------------------------------------------
#
# $userid - незашифрованный идентификатор пользователя.
# Добавление записи в файл cur_login.tnk.
# Функция не возвращает какое-либо значение.
#
#---------------------------------------------------------------------------------

sub createUserSession {
    my $userid    = $_[0];
    my $keytag    = generateKeyTag();
    my $hash_ssid = '';
    my $rnd_num   = 0;
    my $user_ip   = $ENV{REMOTE_ADDR};
    my $found     = 1;

    while ( $found == 1 ) {
        $found   = 0;
        $rnd_num = 100000000;
        while ( $rnd_num <= 100000000 ) {
            my $r_temp = rand 1000000000;
            $rnd_num = 100000000;
            $rnd_num = sprintf "%u", $r_temp;
        }
        $hash_ssid = md5_hex( $rnd_num . $keytag );
        if ( -e "$CURR_LOGIN_PATH" ) {
            open( FHANDLE, "<$CURR_LOGIN_PATH" );
            my @loggedusers = <FHANDLE>;
            for ( my $finx = 0 ; $finx <= $#loggedusers ; $finx++ ) {
                if ( $loggedusers[$finx] =~ /$hash_ssid/ ) {
                    $found = 1;
                    last;
                }
            }
            close FHANDLE;
        }
    }
    my $hash_login =
      getHexStrFromString( doBlowfishEncrypt( $userid, $keytag ) );
    open( CURRTNK, "+<$CURR_LOGIN_PATH" );
    flock( CURRTNK, 2 );
    seek( CURRTNK, 0, 2 );
    print CURRTNK join( "|", $hash_login, $user_ip, $hash_ssid, time ) . "\n";
    flock( CURRTNK, 8 );
    close(CURRTNK);
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# $rc = doKeyTagGenerate ( )
#---------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Генерация keytag (в модуле Lightshop используется облегченная версия механизма
# шифрования пользовательских данных).
# Функция возвращает keytag.
#
#---------------------------------------------------------------------------------

sub doKeyTagGenerate {
    my $feedin    = '2757148ebf939bed6aa50dd0e7a7775e';
    my $feedindex = $feedin;
    $feedindex =~ s/[\D]+//ig;
    my @indexes = split( //, $feedindex );
    my @feeinar = split( //, $feedin );
    my @feedout = ();
    for ( my $feedInx = 0 ; $feedInx <= $#indexes ; $feedInx++ ) {
        push( @feedout, $feeinar[ $indexes[$feedInx] ] );
    }
    return join( "", @feedout );
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# $rc = getSessionId ( $userid )
#---------------------------------------------------------------------------------
#
# $userid - незашифрованный идентификатор пользователя.
# Получение идентификатору сессии по идентификатору пользователя.
# Функция возвращает идентификатор сессии.
#
#---------------------------------------------------------------------------------

sub getSessionId {
    my $login         = '';
    my $previous      = 0;
    my $sess_id       = 0;
    my $userid        = $_[0];
    my $userid_logged = '';
    my $keytag        = generateKeyTag();
    open( FLOGHANDLE, "<$CURR_LOGIN_PATH" );
    my @currlogins = <FLOGHANDLE>;
    close(FLOGHANDLE);
    for ( my $curLogInx = 0 ; $curLogInx <= $#currlogins ; $curLogInx++ ) {
        $currlogins[$curLogInx] =~ s/[\r\n]+//g;
        my @tmp = split( /\|/, $currlogins[$curLogInx] );
        eval {
            $userid_logged =
              doBlowfishDecrypt(
                getStringFromHexStr( $tmp[0] ), $keytag );
        };
        if ( $userid_logged eq $userid ) {
            if ( $previous < $tmp[3] ) {
                $sess_id  = $tmp[2];
                $previous = $tmp[3];
            }
        }
    }
    return $sess_id;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# $rc = getCryptLoginBySid ( $sesion_id )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Получение зашифрованного идентификатора пользователя по идентификатору сессии
# из файла активных пользовательских сессий cur_login.tnk.
# Функция возвращает зашифрованный идентификатор пользователя.
#
#---------------------------------------------------------------------------------

sub getCryptLoginBySid {
    my $param = -1;
    my $found = 0;
    open( FHANDLE, "<$CURR_LOGIN_PATH" );
    my @entries = <FHANDLE>;
    close(FHANDLE);

    for ( my $clearLogInx = 0 ; $clearLogInx <= $#entries ; $clearLogInx++ ) {
        my @tmp = split( /\|/, $entries[$clearLogInx] );
        $tmp[$#tmp] =~ s/[\r\n]+//g;
        if ( $tmp[2] eq $_[0] && $found == 0 ) {
            $param = $tmp[0];
            last;
        }
    }
    return $param;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# $rc = isValidUser ( $sesion_id )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Проверка $sesion_id на корректность с данными из cur_login.tnk.
# Функция возвращает 1, если данные корректны и -1 в противном случае.
#
#---------------------------------------------------------------------------------

sub isValidUser {
    my $session_id = $_[0];
    if ( !( defined($session_id) ) ) { $session_id = 0; }
    my $valid   = -1;
    my $user_ip = $ENV{REMOTE_ADDR};

    open( FHCLEAR, "<$CURR_LOGIN_PATH" );
    my @f_strings = <FHCLEAR>;
    close(FHCLEAR);

    for ( my $clearLogInx = 0 ; $clearLogInx <= $#f_strings ; $clearLogInx++ ) {
        my @tmp = split( /\|/, $f_strings[$clearLogInx] );
        $tmp[$#tmp] =~ s/[\r\n]+//g;
        if ( $tmp[2] eq $session_id && $tmp[1] eq $user_ip && ( ( time - $tmp[3] ) < 900 ) )
        {
            $valid = 1;
            $tmp[3] = time;
            $f_strings[$clearLogInx] = join( "\|", @tmp ) . "\n";
        }
    }
    open( FHCLEAR, ">$CURR_LOGIN_PATH" );
    flock( FHCLEAR, 2 );
    print FHCLEAR @f_strings;
    truncate( FHCLEAR, tell(FHCLEAR) );
    flock( FHCLEAR, 8 );
    close(FHCLEAR);
    return $valid;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# doUserExit ( $sesion_id )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Выход из административной панели, удаление сессии из cur_login.tnk.
# Функция не возвращает какое-либо значение.
#
#---------------------------------------------------------------------------------

sub doUserExit {
    if ( -e "$CURR_LOGIN_PATH" ) {
        my @alive_entries = ();
        open( FHCLEAR, "<$CURR_LOGIN_PATH" );
        my @f_strings = <FHCLEAR>;
        close(FHCLEAR);

        for ( my $clrLogInx = 0 ; $clrLogInx <= $#f_strings ; $clrLogInx++ )
        {
            my @tmp = split( /\|/, $f_strings[$clrLogInx] );
            $tmp[$#tmp] =~ s/[\r\n]+//g;
            if ( ( $tmp[2] ne $_[0] ) && ( $tmp[2] != $_[0] ) ) {
                push( @alive_entries, $f_strings[$clrLogInx] );
            } else {
                print ", push FAIL";
            }
        }
        open( FHCLEAR, ">$CURR_LOGIN_PATH" );
        flock( FHCLEAR, 2 );
        print FHCLEAR @alive_entries;
        truncate( FHCLEAR, tell(FHCLEAR) );
        flock( FHCLEAR, 8 );
        close(FHCLEAR);
    }
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getDecryptedLogin ( $cryptedlogin )
#---------------------------------------------------------------------------------
#
# $cryptedlogin - зашифрованный логин пользователя.
# Получить расшифрованный логин пользователя.
# Функция возвращает расшифрованный логин пользователя.
#
#---------------------------------------------------------------------------------

sub getDecryptedLogin {
    my $cryptedlogin = $_[0];
    my $uncryptedlogin = '';
    if ($cryptedlogin ne '') {
        $uncryptedlogin = doBlowfishDecrypt (
           getStringFromHexStr($cryptedlogin),
           generateKeyTag());
    }
    return $uncryptedlogin; 
}

#---------------------------------------------------------------------------------

END { }

1;
