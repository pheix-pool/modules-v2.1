package Lightshop::Sales;

use strict;
use warnings;

BEGIN {
    use lib '../';
    use lib 'libs/modules';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs';
}

##############################################################################
#
# File   :  Sales.pm
#
##############################################################################
#
# Главный пакет управления online-покупками в польз. части CMS Pheix
#
##############################################################################

use CGI;
use Pheix::Tools;
use File::Path;
use MIME::Base64;
use Encode;
use utf8;
use open qw(:std :utf8);

#------------------------------------------------------------------------------

my $GLOBAL_DOSTAVKA      = 0;
my $MODULE_NAME          = "Lightshop";
my $MODULE_DESCR         = "Заказы и покупки";
my $MODULE_FOLDER        = "admin/libs/modules/$MODULE_NAME/";
my $MODULE_CART_FILE     = "conf/system/lightshopcart.tnk";
my $MODULE_USER_TEMPLATE = "conf/config/template.html";
my $INSTALL_FILE_PATH    = "conf/system/install.tnk";
my $MODULE_DOWNLOAD_PATH = "upload/rekvizity";
my $MODULE_CLIENT_DB     = "conf/system/lightshopclientdb.tnk";
my $MODULE_DEAL_DB       = "conf/system/lightshopdealsdb.tnk";
my $GLOBAL_CONTACT_EMAIL = getSingleSetting( $MODULE_NAME, 1, 'contactemail' );
my $GLOBAL_CONTACT_PHONE = getSingleSetting( $MODULE_NAME, 1, 'contactphone' );
my $GLOBAL_CURRENCY      = getSingleSetting( $MODULE_NAME, 1, 'currency' );
my $PROTO                = getSingleSetting('Config', 0, 'workviaproto') == '1' ? 'https://' : 'http://';

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# new ()
#------------------------------------------------------------------------------
#
# Конструктор класса Lightshop::Sales.
#
#------------------------------------------------------------------------------

sub new {
    my $MODULE_NAME = "Lightshop";
    my ( $class )   = @_;
    my $self = {
        name        => 'Lightshop::Sales',
        version     => '1.0',
        instpath    => 'conf/system/install.tnk',
        mod_name    => $MODULE_NAME,
        mod_dscr    => 'Заказы и покупки',
        mod_fldr    => 'admin/libs/modules/'.$MODULE_NAME,
        mod_cart    => 'conf/system/lightshopcart.tnk',
        mod_tmpl    => 'conf/config/template.html',
        mod_upld    => 'upload/rekvizity',
        mod_usrs    => 'conf/system/lightshopclientdb.tnk',
        mod_deal    => 'conf/system/lightshopdealsdb.tnk',
        mod_mail    => getSingleSetting( $MODULE_NAME, 1, 'contactemail' ),
        mod_phon    => getSingleSetting( $MODULE_NAME, 1, 'contactphone' ),
        mod_curr    => getSingleSetting( $MODULE_NAME, 1, 'currency' ),
    };
    bless $self, $class;
    return $self;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# printHttpHeader ( $self )
#------------------------------------------------------------------------------
#
# $self - ссылка на объект.
# Напечатать HTTP заголовок.
#
#------------------------------------------------------------------------------

sub printHttpHeader {
    my ( $self ) = @_;
    my $last_modified = localtime(time - 86400);
    my $cgi = CGI->new();
    print $cgi->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $last_modified,
            '-Expires'       => $last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
            );
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_power_status_for_module ()
#------------------------------------------------------------------------------
#
# Получить статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub get_power_status_for_module {
    if ( -e $INSTALL_FILE_PATH ) {
        open( my $inputfh, "<", $INSTALL_FILE_PATH );
        my @filecontent = <$inputfh>;
        close($inputfh);
        for my $admplinx ( 0 .. $#filecontent ) {
            my @tmp = split /\|/, $filecontent[$admplinx];
            if ( $tmp[2] eq $MODULE_NAME ) { return $tmp[4]; }
        }
    }
    return;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_cart_lightshopmodule ()
#------------------------------------------------------------------------------
#
# Функция вывода страницы корзины.
#
#------------------------------------------------------------------------------
sub show_cart_lightshopmodule {
    if ( -e $MODULE_USER_TEMPLATE ) {
        open( my $inputfh, "<", $MODULE_USER_TEMPLATE );
        my @scheme = <$inputfh>;
        close($inputfh);
        my $cnt = 0;
        for ( my $cnt = 0 ; $cnt <= $#scheme ; $cnt++ ) {

            $scheme[$cnt] =
              Pheix::Pages::fillCommonTags( $scheme[$cnt], "lightshopcart",
                $MODULE_NAME );

            if ( $scheme[$cnt] =~ /\%content\%/ ) {
                my $content = "";
                $content = showUserCart(@_);
                $scheme[$cnt] =~ s/\%content\%/$content/;
            }
            print $scheme[$cnt];
        }
    }
    else {
        print "<p class=error>$MODULE_USER_TEMPLATE - not found!</p>";
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# show_submitorderpage_lightshopmodule ()
#------------------------------------------------------------------------------
#
# Функция вывода страницы отправки заказа.
#
#------------------------------------------------------------------------------

sub show_submitorderpage_lightshopmodule {
    if ( -e $MODULE_USER_TEMPLATE ) {
        open( my $inputfh, "<", $MODULE_USER_TEMPLATE );
        my @scheme = <$inputfh>;
        close($inputfh);
        my $cnt = 0;
        for ( my $cnt = 0 ; $cnt <= $#scheme ; $cnt++ ) {

            $scheme[$cnt] = Pheix::Pages::fillCommonTags( $scheme[$cnt],
                "lightshopsubmitorder", $MODULE_NAME );

            if ( $scheme[$cnt] =~ /\%content\%/ ) {
                my $content = "";
                $content = &LIGHTSHOP_show_submitorder(@_);
                $scheme[$cnt] =~ s/\%content\%/$content/;
            }
            print $scheme[$cnt];
        }
    }
    else {
        print "<p class=error>$MODULE_USER_TEMPLATE - not found!</p>";
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_cart_cost ()
#------------------------------------------------------------------------------
#
# Функция подсчета общей стоимость заказа.
#
#------------------------------------------------------------------------------

sub get_cart_cost {
    my $totalcost = 0;
    my $user_id   = &get_cookie('alfacms_lightshop') || -1;
    my $carttable = &get_from_cart($user_id);
    my @items_in_cart;
    if ( defined($carttable) && defined($user_id) && $user_id =~ /^[\d]+$/ ) {
        if ( $carttable ne '' ) {
            @items_in_cart = split( /\./, $carttable );
            if ( $#items_in_cart > -1 ) {
                for ( my $i = 0 ; $i <= $#items_in_cart ; $i++ ) {
                    my @tmp = split( /\-/, $items_in_cart[$i] );
                    my @item_details = &get_item_details( $tmp[0], $tmp[1] );

                    #$item_details[2] =~ s/[\.\,]+[\d]+$//g;
                    if (   $item_details[2] =~ /^[\d]+\.[\d]+$/
                        && $tmp[1] =~ /^[\d]+$/ )
                    {
                        eval {
                            $item_details[2] =
                              Shopcat::V3::getCorrPrice( $item_details[2] );
                        };
                        $totalcost = $totalcost + $item_details[2] * $tmp[2];
                    }
                }
            }
        }
    }
    return $totalcost;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LIGHTSHOP_show_submitorder ()
#------------------------------------------------------------------------------
#
# Функция формирование страницы принятия заказа.
#
#------------------------------------------------------------------------------

sub LIGHTSHOP_show_submitorder {
    (my $userfio) = @_;
    my $IRET_content = qq~
        <p class="pageheader">Заказ принят!</p>
        <p class="standart_text"><b>$userfio</b>, ваш заказ отправлен на обработку. Менеджер свяжется с вами в ближайшее время. Подробности и дополнительная информация по заказу отправлены на контактный электронный адрес.</p>
        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
    ~;
    return $IRET_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doRekvizitySave ()
#------------------------------------------------------------------------------
#
# Сохранить реквизиты.
#
#------------------------------------------------------------------------------

sub doRekvizitySave {
    my $rc = -1;
    my $rekviz_item = qq~
        <rekvizit>
            <name>\%1</name>
            <value>\%2</value>
        </rekvizit>~;

    my $file_template = qq~<?xml version="1.0" encoding="UTF-8"?>\n<?xml-stylesheet type="text/xsl" href="/css/xsl/lightshop.xsl"?>
    <organization>
        <clientid>$_[0]</clientid>\%1
    </organization>\n~;

    if ( !( -e $MODULE_DOWNLOAD_PATH ) ) { mkpath($MODULE_DOWNLOAD_PATH); }
    my @rekvizit_titles = &getOrgRekvizitTitles();
    my @rekvizit_values = @_[ 2 .. $#_ ];
    if ( $#rekvizit_values == $#rekvizit_titles ) {
        my $data_to_save = '';
        for ( my $inx = 0 ; $inx <= $#rekvizit_values ; $inx++ ) {
            my $item  = $rekviz_item;
            my $value = $rekvizit_values[$inx];
            my $title = $rekvizit_titles[$inx];
            $item =~ s/\%1/$title/;
            $item =~ s/\%2/$value/;
            $data_to_save .= $item;
        }
        if ( $data_to_save ne '' ) {
            $file_template =~ s/\%1/$data_to_save/;
            open( SAVREKFH, ">$MODULE_DOWNLOAD_PATH/$_[1]" );
            print SAVREKFH $file_template;
            close(SAVREKFH);
            $rc = 0;
        }
        else {
            $rc = -2;
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getOrgRekvizitTitles ()
#------------------------------------------------------------------------------
#
# Получить список названий реквизитов.
#
#------------------------------------------------------------------------------

sub getOrgRekvizitTitles {
    my @rekvizit_titles = (
        'Название организации',
        'Юридический адрес',
        'ИНН',
        'КПП',
        'ОКПО',
        'Расчетный счет',
        'Корреспондентский счет',
        'Наименование банка',
        'БИК',
    );
    return @rekvizit_titles;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# write_cookie ()
#------------------------------------------------------------------------------
#
# Записать заданное значение COOKIE.
#
#------------------------------------------------------------------------------

sub write_cookie {
    my $copy_co = new CGI;
    my $result =
      $copy_co->cookie( -name => $_[0], -value => $_[1], -path => "/" );
    return $result;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# write_cookie_for_along_time ()
#------------------------------------------------------------------------------
#
# Записать значение COOKIE на 1 год.
#
#------------------------------------------------------------------------------

sub write_cookie_for_along_time {
    my $copy_co = new CGI;
    my $result  = $copy_co->cookie(
        -name    => $_[0],
        -value   => $_[1],
        -expires => "+365d",
        -path    => "/"
    );
    return $result;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# write_blank_cookie ()
#------------------------------------------------------------------------------
#
# Записать пустое значение COOKIE.
#
#------------------------------------------------------------------------------

sub write_blank_cookie {
    my $copy_co = new CGI;
    my $result = $copy_co->cookie( -name => $_[0], -value => "", -path => "/" );
    return $result;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_cookie ()
#------------------------------------------------------------------------------
#
# Прочитать значение COOKIE.
#
#------------------------------------------------------------------------------

sub get_cookie {
    my $copy_co = new CGI;
    my @result  = $copy_co->cookie("$_[0]");
    return $result[0];
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_unique_id ()
#------------------------------------------------------------------------------
#
# Сгенерировать уникальный ID.
#
#------------------------------------------------------------------------------

sub get_unique_id {
    my $found   = 1;
    my $rnd_num = 0;
    while ( $found == 1 ) {
        $found   = 0;
        $rnd_num = 100000000;
        while ( $rnd_num <= 100000000 ) {
            my $r_temp = rand 1000000000;
            $rnd_num = 100000000;
            $rnd_num = sprintf "%u", $r_temp;
        }
        open( FHANDLE, "<conf/system/lightshopcart.tnk" );
        while ( my $str = readline(*FHANDLE) && $found == 0 ) {
            if ( $str =~ $rnd_num ) { $found = 1; }
        }
        close FHANDLE;
    }
    return $rnd_num;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# add_to_cart ()
#------------------------------------------------------------------------------
#
# Добавить новую позицию в корзину.
#
#------------------------------------------------------------------------------

sub add_to_cart {
    my $timeid = time;
    if ( -e "conf/system/lightshopcart.tnk" ) {
        open( CURRTNK, "+<conf/system/lightshopcart.tnk" );
    }
    else { open( CURRTNK, ">conf/system/lightshopcart.tnk" ); }
    flock CURRTNK, 2;
    seek( CURRTNK, 0, 2 );
    print CURRTNK "$_[0]|$timeid|$_[1]|500\n";
    flock CURRTNK, 8;
    close CURRTNK;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# modify_cart ()
#------------------------------------------------------------------------------
#
# Изменить количество товаров в корзине.
#
#------------------------------------------------------------------------------

sub modify_cart {
    my $out_index = 0;
    open( FH, "+<conf/system/lightshopcart.tnk" );
    my @f_strings = <FH>;
    while ( $out_index <= $#f_strings ) {
        my @params = split( /\|/, $f_strings[$out_index] );
        if ( $params[0] eq $_[0] ) {
            $params[1] = time;
            $params[2] = $_[1];
            if ( $_[2] ne '' ) { $params[3] = $_[2] . "\n"; }
            $f_strings[$out_index] = join( "\|", @params );
            $out_index = $#f_strings;
        }
        $out_index++;
    }
    seek( FH, 0, 0 );
    flock FH, 2;
    print FH @f_strings;
    truncate( FH, tell(FH) );
    flock FH, 8;
    close(FH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_field_from_cart ()
#------------------------------------------------------------------------------
#
# Получить стоимость доставки.
#
#------------------------------------------------------------------------------

sub get_field_from_cart {
    my $out_index = 0;
    open( FH, "<conf/system/lightshopcart.tnk" );
    my @f_strings = <FH>;
    close(FH);
    while ( $out_index <= $#f_strings ) {
        my @params = split( /\|/, $f_strings[$out_index] );
        if ( $params[0] eq $_[0] ) {
            $params[$#params] =~ s/[\r\n]+//g;
            return $params[ $_[1] ];
        }
        $out_index++;
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_from_cart ()
#------------------------------------------------------------------------------
#
# Получить список товаров в корзине.
#
#------------------------------------------------------------------------------

sub get_from_cart {
    my $out_index = 0;
    open( FH, "<conf/system/lightshopcart.tnk" );
    my @f_strings = <FH>;
    close(FH);
    while ( $out_index <= $#f_strings ) {
        my @params = split( /\|/, $f_strings[$out_index] );
        if ( $params[0] eq $_[0] ) {
            $params[2] =~ s/[\r\n]+//g;
            return $params[2];
        }
        $out_index++;
    }
    return;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_quantity_byid ()
#------------------------------------------------------------------------------
#
# Получить количество товаров по ID.
#
#------------------------------------------------------------------------------

sub get_quantity_byid {
    my $out_index = 0;
    open( FH, "+<conf/system/lightshopcart.tnk" );
    my @f_strings = <FH>;
    close(FH);
    my $qtyty = 0;
    while ( $out_index <= $#f_strings ) {
        my @params = split( /\|/, $f_strings[$out_index] );
        if ( $params[0] eq $_[0] ) {
            my @tmp = split( /\./, $params[2] );
            for ( my $i = 0 ; $i <= $#tmp ; $i++ ) {
                my @tmp2 = split( /\-/, $tmp[$i] );
                $qtyty = $qtyty + $tmp2[1];
            }
            $out_index = $#f_strings;
        }
        $out_index++;
    }
    return $qtyty;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_cart_menu_status ()
#------------------------------------------------------------------------------
#
# Отобразить сколько товаров в корзине в меню.
#
#------------------------------------------------------------------------------

sub get_cart_menu_status {
    my $user_id          = &get_cookie('alfacms_lightshop');
    my $quantity_in_cart = 0;
    open( FH, "<conf/system/lightshopcart.tnk" );
    my @f_strings = <FH>;
    close(FH);
    for ( my $i = 0 ; $i <= $#f_strings ; $i++ ) {
        $f_strings[$i] =~ s/[\r\n]+//g;
        my @params = split( /\|/, $f_strings[$i] );
        if ( $params[0] eq $user_id ) {
            my @quantity = split( /\./, $params[2] );
            $quantity_in_cart = $#quantity + 1;
        }
    }
    if ( $quantity_in_cart > 0 ) {
        return "КОРЗИНА ($quantity_in_cart)";
    }
    else { return "КОРЗИНА"; }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_cart_block_goods ()
#------------------------------------------------------------------------------
#
# Отобразить сколько товаров в блоке корзины.
#
#------------------------------------------------------------------------------

sub get_cart_block_goods {
    my $user_id = &get_cookie('alfacms_lightshop') || -1;
    my $quantity_in_cart = 0;
    open( FH, "<conf/system/lightshopcart.tnk" );
    my @f_strings = <FH>;
    close(FH);
    for ( my $i = 0 ; $i <= $#f_strings ; $i++ ) {
        $f_strings[$i] =~ s/[\r\n]+//g;
        my @params = split( /\|/, $f_strings[$i] );
        if ( defined( $params[0] ) ) {
            if ( $params[0] == $user_id ) {
                my @quantity = split( /\./, $params[2] );
                for ( my $j = 0 ; $j <= $#quantity ; $j++ ) {
                    my @cart_arr = split( /\-/, $quantity[$j] );
                    if ( $cart_arr[2] =~ /^[\d]+$/ ) {
                        $quantity_in_cart += $cart_arr[2];
                    }
                }

                #$quantity_in_cart = $#quantity+1;
            }
        }
    }
    if   ( $quantity_in_cart > 0 ) { return "$quantity_in_cart"; }
    else                           { return "0"; }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# delete_from_cart ()
#------------------------------------------------------------------------------
#
# Удалить позицию из корзины.
#
#------------------------------------------------------------------------------

sub delete_from_cart {
    my $user_id = $_[0];
    my $shop_id = $_[1];
    my $item_id = $_[2];

    open( FH, "<conf/system/lightshopcart.tnk" );
    my @f_strings = <FH>;
    close(FH);
    for ( my $i = 0 ; $i <= $#f_strings ; $i++ ) {
        my @params = split( /\|/, $f_strings[$i] );
        if ( $params[0] eq $user_id ) {
            $params[$#params] =~ s/[\r\n]+//g;
            my @updated_quantity = ();
            my @quantity = split( /\./, $params[2] );
            for ( my $j = 0 ; $j <= $#quantity ; $j++ ) {
                my @tmp_parsed = split( /\-/, $quantity[$j] );
                if (   $tmp_parsed[0] == $shop_id
                    && $tmp_parsed[1] == $item_id
                    && $tmp_parsed[0] =~ /^[\d]+$/
                    && $tmp_parsed[1] =~ /^[\d]+$/ )
                {
                    # found position to delete
                    ;
                }
                else {
                    push( @updated_quantity, $quantity[$j] );
                }
            }
            if ( $#updated_quantity == -1 ) {
                @f_strings = (
                    @f_strings[ 0 .. $i - 1 ],
                    @f_strings[ $i + 1 .. $#f_strings ]
                );
            }
            else {
                $params[2] = join( ".", @updated_quantity );
                $f_strings[$i] = join( "|", @params ) . "\n";
            }
        }

    }
    open( FH, ">conf/system/lightshopcart.tnk" );
    seek( FH, 0, 0 );
    flock FH, 2;
    print FH @f_strings;
    truncate( FH, tell(FH) );
    flock FH, 8;
    close(FH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# delete_outdated_cart_entries ()
#------------------------------------------------------------------------------
#
# Удалить устаревшие позиции из корзины.
#
#------------------------------------------------------------------------------

sub delete_outdated_cart_entries {
    open( FH, "<conf/system/lightshopcart.tnk" );
    my @f_strings = <FH>;
    close(FH);
    for ( my $i = 0 ; $i <= $#f_strings ; $i++ ) {
        my @params = split( /\|/, $f_strings[$i] );
        my $delta = time - $params[1];
        if ( $delta > 259200 ) {
            @f_strings = ( @f_strings[ 0 .. $i - 1 ],
                @f_strings[ $i + 1 .. $#f_strings ] );
        }
    }
    open( FH, ">conf/system/lightshopcart.tnk" );
    seek( FH, 0, 0 );
    flock FH, 2;
    print FH @f_strings;
    truncate( FH, tell(FH) );
    flock FH, 8;
    close(FH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# delete_cart_entries_through_order ()
#------------------------------------------------------------------------------
#
# Удалить позицию из корзины после оформления заказа.
#
#------------------------------------------------------------------------------

sub delete_cart_entries_through_order {
    my $user_id = $_[0];
    open( FH, "<conf/system/lightshopcart.tnk" );
    my @f_strings = <FH>;
    close(FH);
    for ( my $i = 0 ; $i <= $#f_strings ; $i++ ) {
        my @params = split( /\|/, $f_strings[$i] );
        if ( ( $user_id =~ /^[\d]+$/ ) && ( $params[0] == $user_id ) ) {
            @f_strings = ( @f_strings[ 0 .. $i - 1 ],
                @f_strings[ $i + 1 .. $#f_strings ] );
        }
    }
    open( FH, ">conf/system/lightshopcart.tnk" );
    seek( FH, 0, 0 );
    flock FH, 2;
    print FH @f_strings;
    truncate( FH, tell(FH) );
    flock FH, 8;
    close(FH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LIGHTSHOP_date_format ()
#------------------------------------------------------------------------------
#
# Форматированная дата.
#
#------------------------------------------------------------------------------

sub LIGHTSHOP_date_format {
    my $date = localtime;
    $date =~ s/[\.\:\,]//g;
    $date =~ s/[\s]+/ /g;
    my @date_array = split( / /, $date );
    @date_array = ( $date_array[2], $date_array[1], $date_array[4] );
    if ( $date_array[1] eq 'Jan' ) { $date_array[1] = 'Январь' }
    if ( $date_array[1] eq 'Feb' ) { $date_array[1] = 'Февраль' }
    if ( $date_array[1] eq 'Mar' ) { $date_array[1] = 'Март' }
    if ( $date_array[1] eq 'Apr' ) { $date_array[1] = 'Апрель' }
    if ( $date_array[1] eq 'May' ) { $date_array[1] = 'Май' }
    if ( $date_array[1] eq 'Jun' ) { $date_array[1] = 'Июнь' }
    if ( $date_array[1] eq 'Jul' ) { $date_array[1] = 'Июль' }
    if ( $date_array[1] eq 'Aug' ) { $date_array[1] = 'Август' }
    if ( $date_array[1] eq 'Sep' ) { $date_array[1] = 'Сентябрь' }
    if ( $date_array[1] eq 'Oct' ) { $date_array[1] = 'Октябрь' }
    if ( $date_array[1] eq 'Nov' ) { $date_array[1] = 'Ноябрь' }
    if ( $date_array[1] eq 'Dec' ) { $date_array[1] = 'Декабрь' }

    return "$date_array[0] $date_array[1], $date_array[2]";
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LIGHTSHOP_generate_passw ()
#------------------------------------------------------------------------------
#
# Сгенерировать пароль.
#
#------------------------------------------------------------------------------

sub LIGHTSHOP_generate_passw {
    my @pharses = (
        'Kp1', 'Uiq', 'PzD', '1Ms', 'OU9', 'Nfs', 'IwB', '4X6', 'P1a', 'Jw3',
        '92D', 'Ie5', 'nhY', 'ksO', '7T6', 'mYh', 'OHJ', 'LKq', 'NB6', 'Ofd',
        'Iac', 'PEr', 'Kal', '9U1', '15F', 'KI4', 'nlO', 'Qw3', 'io9', '6Vf'
    );
    my @ost1 = ('0');
    my @ost2 = ('0');
    my @ost3 = ('0');
    my $flag = 1;
    while ( $flag == 1 ) {
        my $rnd1 = rand($#pharses);
        my $rnd2 = rand($#pharses);
        my $rnd3 = rand($#pharses);
        @ost1 = split( /\./, $rnd1 );
        @ost2 = split( /\./, $rnd2 );
        @ost3 = split( /\./, $rnd3 );
        if   ( $ost1[0] == $ost2[0] && $ost2[0] == $ost3[0] ) { $flag = 1; }
        else                                                  { $flag = 0; }
    }
    return $pharses[ $ost1[0] ] . $pharses[ $ost2[0] ] . $pharses[ $ost3[0] ];
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LIGHTSHOP_add_to_userDB ()
#------------------------------------------------------------------------------
#
# Добавление данных в файл базы данных пользователей.
#
#------------------------------------------------------------------------------

sub LIGHTSHOP_add_to_userDB {
    my @inputs = @_;
    if ( -e "conf/system/lightshopclientdb.tnk" ) {
        open( USERDBFH, "+<conf/system/lightshopclientdb.tnk" );
    }
    else { open( USERDBFH, ">conf/system/lightshopclientdb.tnk" ); }
    my @indexes = <USERDBFH>;

    push( @indexes, join( "|", @inputs, ) . "\n" );
    seek( USERDBFH, 0, 0 );
    flock USERDBFH, 2;
    print USERDBFH @indexes;
    truncate( USERDBFH, tell(USERDBFH) );
    flock USERDBFH, 8;
    close(USERDBFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LIGHTSHOP_add_to_dealsDB ()
#------------------------------------------------------------------------------
#
# Добавление данных в файл базы данных сделок.
#
#------------------------------------------------------------------------------

sub LIGHTSHOP_add_to_dealsDB {
    if ( -e "conf/system/lightshopdealsdb.tnk" ) {
        open( DEALSDBFH, "+<conf/system/lightshopdealsdb.tnk" );
    }
    else { open( DEALSDBFH, ">conf/system/lightshopdealsdb.tnk" ); }
    my @indexes = <DEALSDBFH>;

    push( @indexes, join( "|", @_, ) . "\n" );
    seek( DEALSDBFH, 0, 0 );
    flock DEALSDBFH, 2;
    print DEALSDBFH @indexes;
    truncate( DEALSDBFH, tell(DEALSDBFH) );
    flock DEALSDBFH, 8;
    close(DEALSDBFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_uniq_login ()
#------------------------------------------------------------------------------
#
# Генерируем уникальный логин на основании email.
#
#------------------------------------------------------------------------------

sub get_uniq_login {
    my $e_mail         = $_[0];
    my @login          = split( /\@/, $e_mail );
    my $stopflag       = 0;
    my $stopflag_tries = 0;
    my $parsed_email   = $e_mail;
    $parsed_email =~ s/\W//gi;
    my $generated_login = $login[0];
    if ( $login[0] eq "" ) { @login = ($parsed_email); }

    if ( -e "conf/system/lightshopclientdb.tnk" ) {
        open( USERDBFH, "<conf/system/lightshopclientdb.tnk" );
        my @indexes = <USERDBFH>;
        close(USERDBFH);

        while ( $stopflag == 0 ) {
            $stopflag = 1;
            for ( my $i = 0 ; $i <= $#indexes ; $i++ ) {
                my @udata = split( /\|/, $indexes[$i] );
                if ( $udata[2] eq $generated_login ) {
                    $stopflag = 0;
                    $stopflag_tries++;
                    $generated_login = $login[0] . $stopflag_tries;
                }
            }
        }
    }
    return $generated_login;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# simple_send_message_uni ( $useremail, $mess, $orderid )
#------------------------------------------------------------------------------
#
# $useremail - email покупателя.
# $mess - текст электронного письма.
# $orderid - номер заказа.
# Универсальная функция отправки email.
# Функция возвращает 0 в случае успешной отправки email или -1 в случае ошибки.
#
#------------------------------------------------------------------------------

sub simple_send_message_uni {
    my $error = '';
    my $FIXED_SUBJECT =
        "Заказ №"
      . $_[2]
      . ' принят! Спасибо за использование сервера ' . $PROTO
      . $ENV{SERVER_NAME} . "!";

    if ( $_[$#_] eq 'remind' ) {
        $FIXED_SUBJECT = "Пользователь направил напоминание о заказе №"
          . $_[2];
    }

    my $FIXED_FROM = 'Интернет-магазин '.$PROTO.$ENV{SERVER_NAME};
    my $FIXED_MODERATOR_EMAIL = &getContactEmail();
    my $sendaddr              = $_[0];
    my $subj                  = "=?UTF-8?B?"
      . encode_base64( Encode::encode( "utf8", $FIXED_SUBJECT ) ) . "?=";
    my $from =
        "=?UTF-8?B?"
      . encode_base64( Encode::encode( "utf8", $FIXED_FROM ) )
      . "?= <$FIXED_MODERATOR_EMAIL>";
    $from =~ s/[\r\n]+//g;
    $subj =~ s/[\r\n]+//g;

    if ( $FIXED_MODERATOR_EMAIL ne '' && $FIXED_MODERATOR_EMAIL =~ /\@/ ) {
        unless ( open( MAIL, "|/usr/sbin/sendmail -t" ) ) {
            $error = "Sendmail openning error!, $!";
            return (-1);
        }
        print MAIL "To: $sendaddr\n";
        print MAIL "CC: $FIXED_MODERATOR_EMAIL\n";
        print MAIL "From: $from\n";
        print MAIL "Subject: $subj\n";
        print MAIL
          "MIME-Version: 1.0\nContent-Type: text/plain; charset=\"UTF-8\"\n\n";
        print MAIL $_[1];

        unless ( close(MAIL) ) {
            $error = "Could not close Sendmail!, $!";
            return (-1);
        }
    }
    else {
        return -1;
    }
    return 0;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getCompiledEmail ()
#------------------------------------------------------------------------------
#
# Функция создания электронного письма.
#
#------------------------------------------------------------------------------

sub getCompiledEmail {
    my ( $self, $fio, $dealid, $userid ) = @_;
    my @items_in_cart;
    my $IRET_content = qq~Здравствуйте, $fio!\nНа сервере $PROTO$ENV{SERVER_NAME} сделан следующий заказ:\n\n ~;
    my $carttable = $self->getDealDetailsByDealId($dealid);
    if ( defined($carttable) && defined($userid) && $userid =~ /^[\d]+$/ ) {
        if ( $carttable ne '' ) {
            my $user_login    = $self->getUserDataById( $userid, 2 );
            my $user_password = $self->getUserDataById( $userid, 3 );
            @items_in_cart = split( /\./, $carttable );
            if ( $#items_in_cart > -1 ) {
                my $totalcost = 0;
                for my $i (0..$#items_in_cart) {
                    my $costperitem  = 0;
                    my $pp_num       = $i + 1;
                    my @tmp          = split( /\-/, $items_in_cart[$i] );
                    my @item_details = $self->getItemDetailsV3( $tmp[0], $tmp[1] );

                    if (   $item_details[2] =~ /^[\d]+\.[\d]+$/
                        && $tmp[2] =~ /^[\d]+$/ )
                    {
                        eval {
                            $item_details[2] =
                              Shopcat::V3::getCorrPrice( $item_details[2] );
                        };
                        $totalcost   = $totalcost + $item_details[2] * $tmp[2];
                        $costperitem = $item_details[2] * $tmp[2];
                    }
                    $IRET_content .= qq~\t$pp_num. $item_details[5] (цена за штуку: $item_details[2] $GLOBAL_CURRENCY) - $tmp[2] шт., стоимость $costperitem $GLOBAL_CURRENCY\n~;
                }
                my $dostavka_descr = qq~условия доставки по РФ: $PROTO$ENV{SERVER_NAME}/dostavka.html~;
                $IRET_content .= qq~\tИтого к оплате: $totalcost $GLOBAL_CURRENCY ($dostavka_descr)\nДля уточнения сроков выполнения заказа обращайтесь по следующим телефонам: $GLOBAL_CONTACT_PHONE\n\nПроверить статус заказа можно на следующей странице: $PROTO$ENV{SERVER_NAME}/login.html\nНомер заказа: $dealid\nПароль: $user_password\n\nС уважением, интернет-магазин $PROTO$ENV{SERVER_NAME}.\n\n~;
            }
            else { $IRET_content = ""; }
        }
        else { $IRET_content = ""; }
    }
    else { $IRET_content = ""; }
    return $IRET_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showLightShopUsrLoginPage ( $self )
#------------------------------------------------------------------------------
#
# $self - ссылка на объект.
# Функция вывода страницы авторизации.
#
#------------------------------------------------------------------------------

sub showLightShopUsrLoginPage {
    my ($self) = @_;
    if ( -e $self->{mod_tmpl} ) {
        open( my $inputfh, "<", $self->{mod_tmpl} );
        my @tmpl = <$inputfh>;
        close($inputfh);
        for my $i (0..$#tmpl) {
            $tmpl[$i] =
              Pheix::Pages::fillCommonTags( $tmpl[$i], "lightshopusrlogin",
                $self->{mod_name} );
            if ( $tmpl[$i] =~ /\%content\%/ ) {
                my $content = "";
                $content = $self->getLightShpUsrLgnFrm();
                $tmpl[$i] =~ s/\%content\%/$content/;
            }
            print $tmpl[$i];
        }
    } else {
        print __FILE__.":".__LINE__." - ".$self->{mod_tmpl}." not found";
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showLightShopUsrHomePage ( $self )
#------------------------------------------------------------------------------
#
# $self - ссылка на объект.
# Функция вывода личной страницы авторизованного пользователя.
#
#------------------------------------------------------------------------------

sub showLightShopUsrHomePage {
    my ( $self,
         $sessid,
         $cryptl,
         $dealid,
         $manageflag,
         $userfio,
         $userlogin,
         $userpassw,
         $useremail,
         $userphone,
         $useraddr,
         $usercomments,
         @rekvizity ) = @_;
    if ( -e $self->{mod_tmpl} ) {
        open( my $inputfh, "<", $self->{mod_tmpl} );
        my @tmpl = <$inputfh>;
        close($inputfh);
        for my $i (0..$#tmpl) {
            $tmpl[$i] =
              Pheix::Pages::fillCommonTags( $tmpl[$i], "lightshopusrarea",
                $self->{mod_name} );
            if ( $tmpl[$i] =~ /\%content\%/ ) {
                my $content =
                    $self->getUsrOrderDetails(
                        $sessid,    $cryptl,    $dealid,       $manageflag,
                        $userfio,   $userlogin, $userpassw,    $useremail,
                        $userphone, $useraddr,  $usercomments, @rekvizity );
                $tmpl[$i] =~ s/\%content\%/$content/;
            }
            print $tmpl[$i];
        }
    }
    else {
        print __FILE__.":".__LINE__." - ".$self->{mod_tmpl}." not found";
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getLightShpUsrLgnFrm ( $self )
#------------------------------------------------------------------------------
#
# $self - ссылка на объект.
# Получить форму ввода номера заказа и пароля.
# Функция возвращает форму ввода номера заказа и пароля в HTML-формате.
#
#------------------------------------------------------------------------------

sub getLightShpUsrLgnFrm {
    my ($self) = @_;
    my $IRET_content = qq~
        <p class="pageheader">Проверка статуса заказа</p>
        <div class="cart-checkout-from-block">
            <form method="post" action="$ENV{SCRIPT_NAME}?action=loginto" style="margin:0px;" id="lightshop-check-order-form" name="lightshop-check-order-form">
            <p><label>Номер заказа или логин: </label><br><input required="" name="dealid" type="text"></p>
            <p><label>Пароль: </label><br><input required="" name="userpassw" type="password"></p>
            <p><a href="javascript:top.document.forms['lightshop-check-order-form'].submit();" class="more-about-button">Проверить заказ</a></p></form></div>
    ~;
    return $IRET_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getUserDataById ( $self, $userid, $col_num )
#------------------------------------------------------------------------------
#
# $self - ссылка на объект.
# $userid - идентификатор пользователя.
# $col_num - номер столбца с данными, которые требуется получить.
# Получение информации о пользователе из базы данных (конкретный столбец).
# Функция возвращает информацию о пользователе из базы данных.
#
#------------------------------------------------------------------------------

sub getUserDataById {
    my ($self, $userid, $col_num) = @_;
    my $return_value;
    if (-e $MODULE_CLIENT_DB) {
        open( my $inputfh, "<", $MODULE_CLIENT_DB );
        my @f_strings = <$inputfh>;
        close($inputfh);
        for my $i (0..$#f_strings) {
            my $client_record = $f_strings[$i];
            $client_record =~ s/[\r\n]+//g;
            my @tmp_arr = split( /\|/, $client_record );
            if ( $tmp_arr[0] eq $userid ) { $return_value = $tmp_arr[ $col_num ]; last }
        }
    }
    return $return_value;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getUidByDealId ( $self, $dealid )
#------------------------------------------------------------------------------
#
# $self - ссылка на объект.
# $dealid - идентификтор сделки.
# Получение идентификатор пользователя по идентификатору сделки.
# Функция возвращает идентификатор пользователя.
#
#------------------------------------------------------------------------------

sub getUidByDealId {
    my ($self, $dealid) = @_;
    my $return_value;
    if (-e $MODULE_DEAL_DB) {
        open( my $inputfh, "<", $MODULE_DEAL_DB );
        my @f_strings = <$inputfh>;
        close($inputfh);
        for my $i (0..$#f_strings) {
            my $deal_record = $f_strings[$i];
            $deal_record =~ s/[\r\n]+//g;
            my @tmp_arr = split( /\|/, $deal_record );
            if ( $tmp_arr[0] eq $dealid ) { $return_value = $tmp_arr[2]; last }
        }
    }
    return $return_value;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDealIdByUid ( $self, $userid )
#------------------------------------------------------------------------------
#
# $self - ссылка на объект.
# $userid - идентификатор пользователя.
# Получение идентификатора сделки по идентификатору пользователя.
# Функция возвращает идентификатора сделки.
#
#------------------------------------------------------------------------------

sub getDealIdByUid {
    my ($self, $userid) = @_;
    my $return_value;
    if (-e $MODULE_DEAL_DB) {
        open( my $inputfh, "<", $MODULE_DEAL_DB );
        my @f_strings = <$inputfh>;
        close($inputfh);
        for my $i (0..$#f_strings) {
            my $deal_record = $f_strings[$i];
            $deal_record =~ s/[\r\n]+//g;
            my @tmp_arr = split( /\|/, $deal_record );
            if ( $tmp_arr[2] eq $userid ) { $return_value = $tmp_arr[0]; last }
        }
    }
    return $return_value;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDealDetailsByDealId ( $self, $dealid )
#------------------------------------------------------------------------------
#
# $self - ссылка на объект.
# $dealid - идентификтор сделки.
# Получение данных по сделки из БД по идентификатору сделки.
# Функция возвращает данные по сделке.
#
#------------------------------------------------------------------------------

sub getDealDetailsByDealId {
    my ($self, $dealid) = @_;
    my $return_value;
    if (-e $MODULE_DEAL_DB) {
        open( my $inputfh, "<", $MODULE_DEAL_DB );
        my @f_strings = <$inputfh>;
        close($inputfh);
        for my $i (0..$#f_strings) {
            my $deal_record = $f_strings[$i];
            $deal_record =~ s/[\r\n]+//g;
            my @tmp_arr = split( /\|/, $deal_record );
            $tmp_arr[0] =~ s/[\D]+//gi;
            if ( $tmp_arr[0] eq $dealid ) { $return_value = $tmp_arr[3]; last }
        }
    }
    return $return_value;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_item_details ()
#------------------------------------------------------------------------------
#
# Получение информации из базы данных.
#
#------------------------------------------------------------------------------

sub get_item_details {
    my $out_index = 0;
    my $shopid    = $_[0];
    my $itemid    = $_[1];
    my $pricefile = '';
    my $error     = '';
    eval { $pricefile = Shopcat::V3::getPriceFileFullPath($shopid); };
    if ( -e $pricefile ) {
        open( FH, "<$pricefile" );
        my @f_strings = <FH>;
        close(FH);

        #Configuring new array
        #print "<h1>$shopid, $itemid</h1>";
        while ( $out_index <= $#f_strings ) {
            $f_strings[$out_index] =~ s/[\r\n]+//g;
            my @params = split( /\|/, $f_strings[$out_index] );
            $params[0] =~ s/[\W]+//g;
            if ( $params[0] eq $itemid ) {

                return @params;
            }
            $out_index++;
        }
    }
    return (-1);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LIGHTSHOP_user_data_fromdb ()
#------------------------------------------------------------------------------
#
# Получение данных из файла базы данных пользователей.
#
#------------------------------------------------------------------------------

sub LIGHTSHOP_user_data_fromdb {
    my $userid_to_get = $_[0];
    my @user_data_arr = ();
    open( USERDBFH, "<conf/system/lightshopclientdb.tnk" );
    my @indexes = <USERDBFH>;
    close USERDBFH;
    for ( my $i = 0 ; $i <= $#indexes ; $i++ ) {
        my @split_arr = split( /\|/, $indexes[$i] );
        if ( $split_arr[0] eq $userid_to_get ) {
            my $tmp_val = $indexes[$i];
            $tmp_val =~ s/[\r\n]+//g;
            @user_data_arr = split( /\|/, $tmp_val );
            last;
        }
    }
    if ( $#user_data_arr > -1 ) {
        return @user_data_arr;
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getUsrDetailByLogin ()
#------------------------------------------------------------------------------
#
# Получение данных из файла базы данных пользователей по логину.
#
#------------------------------------------------------------------------------

sub getUsrDetailByLogin {
    my ( $self, $login ) = @_;
    my @user_data_arr;
    if (-e $self->{mod_usrs}) {
        open( my $inputfh, "<", $self->{mod_usrs} );
        my @indexes = <$inputfh>;
        close($inputfh);
        for my $i (0..$#indexes) {
            my @split_arr = split( /\|/, $indexes[$i] );
            if ( $split_arr[2] eq $login ) {
                my $tmp_val = $indexes[$i];
                $tmp_val =~ s/[\r\n]+//g;
                @user_data_arr = split( /\|/, $tmp_val );
                last;
            }
        }
        if ( $#user_data_arr > -1 ) {
            return @user_data_arr;
        }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getUsrOrderDetails ()
#------------------------------------------------------------------------------
#
# Вывод главной страницы личного кабинета.
#
#------------------------------------------------------------------------------

sub getUsrOrderDetails {
    my ( $self,
         $sesid,
         $crypted_login,
         $dealid,
         $manageflag,
         $userfio,
         $userlogin,
         $userpassw,
         $useremail,
         $userphone,
         $useraddr,
         $usercomments,
         @rekvizity ) = @_;
    my $tabcount       = 3;
    my $content_in_tab = '';
    my $userid = Lightshop::Session::doBlowfishDecrypt(
        Lightshop::Session::getStringFromHexStr( $crypted_login ),
        Lightshop::Session::doKeyTagGenerate()
    );

    $content_in_tab = $self->getOrderByDealId(
                        $sesid, $dealid, $manageflag, $userfio,
                        $userlogin, $userpassw, $useremail,
                        $userphone, $useraddr, $usercomments, @rekvizity );
    $userfio = $self->getUserDataById($userid, 5);
    my $tab_content = qq~
    <div class="lightshop-usr-topblock">
        <div class="lightshop-usr-pageheader">Детализация заказа №$dealid</div>
        <div class="lightshop-usr-profile"><i class="fa fa-user" style="font-size:19px;"></i>&nbsp;&nbsp;
        <ul class="dropdown-menu">
            <li class=dropdown-menu-li>
                <li class=dropdown-menu-li>
                    <a href="#">
                    <span class="divmenucell" style="margin-left:1px;">
                    <span class="topline"></span><span class="item">$userfio</span></span></a>
                    <ul style="margin-top:5px; margin-left:1px;">
                        <li class=dropdown-menu-submenu>
                            <a href="$ENV{SCRIPT_NAME}?id=$sesid&action=lightshoplc#userprofile" style="width:120px;">Профиль</a></li>
                        <li class=dropdown-menu-submenu>
                            <a href="$ENV{SCRIPT_NAME}?id=$sesid&action=lightshopusrexit" style="width:120px;">Выход</a></li>
                    </ul>
                </li>
            </li>
        </ul>
        <script type="text/javascript">
        \$(function(){
            \$('.dropdown-menu').dropdown_menu();
        });
        </script>
        </div>
    </div>
    <div style="margin:0px;">$content_in_tab</div>
    ~;
    return $tab_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getOrderByDealId ()
#------------------------------------------------------------------------------
#
# Вывод заказа и формы с регистрационными данными в личном кабинете.
#
#------------------------------------------------------------------------------

sub getOrderByDealId {
    my ( $self,
         $sesid,
         $dealid,
         $manageflag,
         $_userfio,
         $_userlogin,
         $_userpassw,
         $_useremail,
         $_userphone,
         $_useraddr,
         $_usercomments,
         @_rekvizity ) = @_;
    my $staticpages_counter = 0;
    my $dealcontent;
    my $userarea = Shopcat::V3::getV3UserareaStatus();
    my $hiddenstyle = 'style="display:none;"';
    if ( -e $self->{mod_deal} ) {
        open( my $inputfh, "<", $self->{mod_deal} );
        my @deals = <$inputfh>;
        close($inputfh);
        if ( $#deals > -1 ) {
            my $information;
            if ( $manageflag == 3 || $manageflag == 4) {
                my $ipdatetime;
                if ( localtime =~ /([\d]+:[\d]+:[\d]+)/ ) {
                    $ipdatetime = "$1:";
                }
                if ($manageflag == 3) {
                    $information .= qq~<div class="light-shop-usrstatus">$ipdatetime данные успешно обновлены!</div>\n~;
                }
                if ($manageflag == 4) {
                    $information .= qq~<div class="light-shop-usrstatus">$ipdatetime напоминание отправлено!</div>\n~;
                }
            }
            $dealcontent = qq~$information
                <p class="standart_text">Для уточнения сроков выполнения заказов обращайтесь по следующим телефонам:
                <b>$self->{mod_phon}</b></p><div class="cart-canvas"><table class="goods-in-cart">
            ~;
            my $totalordercost = 0;
            for my $i (0..$#deals) {
                my @cols = split( /\|/, $deals[$i] );
                if ( $cols[0] == $dealid ) {

                    $cols[-1] =~ s/[\n]+//g;

                    my $status      = "";
                    my @links_array = ( 0, 0, 0 );
                    my $links_str   = '';
                    my $user_login = $self->getUserDataById( $cols[2], 2 );
                    my $user_fio   = $self->getUserDataById( $cols[2], 5 );
                    my $user_addr  = $self->getUserDataById( $cols[2], 6 );
                    my $user_email = $self->getUserDataById( $cols[2], 4 );
                    my @bought   = split( /\./, $cols[3] );
                    my $deal_content;

                    if ( $cols[7] == -1 ) {
                        $status     = "Сделка не состоялась";
                    }
                    if ( $cols[7] == 0 ) {
                        $status     = "Сделка в работе";
                    }
                    if ( $cols[7] == 1 ) {
                        $status =
                          "Сделка успешно состоялась";
                    }
                    for my $j (0..$#bought) {
                        my $navi;
                        my $cntr = $j+1;
                        my ($shopid, $itemid, $quantity) = split( /\-/, $bought[$j] );
                        my @item_name = $self->getItemDetailsV3 ( $shopid, $itemid );
                        if ( $item_name[0] != -1 ) {
                            my $cat_descr;
                            my $tcost    = 0;
                            my $level_id = 0;
                            my $cat_id   = 0;
                            eval {
                                $item_name[2] =
                                  Shopcat::V3::getCorrPrice( $item_name[2] );
                            };
                            if ($item_name[2] > 0) {
                                $tcost = sprintf "%.2f", $quantity * $item_name[2];
                            }
                            my $img_path_artikul = "images/shopcat/$shopid/levels/n/$itemid";
                            my $img_path = "images/no_photo.png";
                            my $title_img;
                            my $itemlink = "href=\"shopcatalog_$item_name[0]\_$shopid.html\"";
                            eval {
                                $title_img = Shopcat::V3::getTitleImgFromDir($img_path_artikul);
                            };
                            if ( -e "$img_path_artikul/$title_img" ) {
                                $img_path = "$img_path_artikul/$title_img";
                            }
                            my $itemname = $item_name[5];
                            if ($userarea) {
                                $hiddenstyle = '';
                                $navi = $item_name[1];
                                $navi =~ s/shopcat-textlink/shopcat-textlink-navi/gi;
                            } else {
                                $itemlink = '';
                            }
                            my @item_details = &get_item_details( $shopid, $itemid );
                            if ( $item_details[-1] =~ /\[([\d]+)\]/ ) {
                                $level_id = $1;
                            }
                            if ( $item_details[ $#item_details - 1 ] =~ /\[([\d]+)\]/ ) {
                                $cat_id = $1;
                            }
                            eval {
                                $cat_descr =
                                Shopcat::V3::getDescrNM1(
                                    $shopid, $cat_id, $level_id );
                            };
                            if (   $cat_descr =~ 'No description record'
                                   || $cat_descr =~ 'Descriptions file not found'
                                   || $cat_descr eq '' ) {
                                       $cat_descr = 'Описание отсутсвует';
                                    }
                            $dealcontent .= qq~
                                <tr><td class="goods-in-cart-left">
                                <p class="cart-item-header">$cntr. $itemname</p>
                                <div class="cart-link-path" $hiddenstyle>$navi</div>
                                <table><tr>
                                    <td valign=top><div class="cart-item-img"><a $itemlink title="$itemname">
                                    <img src="images/admin/cms/spacer.gif" alt="$itemname" style="background: url('$img_path') no-repeat; background-size: auto 150px;" height="150" width="150">
                                    </a></div></td>
                                    <td><div class="cart-item-descr">Цена за штуку: <b>$item_name[2] $GLOBAL_CURRENCY</b><br>Статус товара: <font style="color:rgb(15, 163, 44);"><b>в наличии</b></font><br>Артикул: <b>$item_name[8]</b>
                                    <div class="cart-item-text">$cat_descr</div>
                                    <br>Количество товаров:&nbsp;<b>$quantity шт.</b>
                                    <p class="cart-item-descr-price"><span>$tcost</span> $GLOBAL_CURRENCY</p></div></td></tr></table>
                                    </td>
                                    <td class="goods-in-cart-right" valign=top>&nbsp;</td>
                                </tr>
                            ~;
                        }
                        else {
                            $dealcontent .=
                              "<li><font color=red>Товар удален из базы данных</font></li>\n";
                        }
                    }
                    my $totalcost = sprintf "%.2f", ($cols[4] + $cols[6]);
                    $totalordercost += $totalcost;
                    $staticpages_counter++;
                }
            }
            my $form;
            my $paytype = 1;
            my $userid = $self->getUidByDealId($dealid);
            my @rekvizity = get_objects_by_id( $self->{mod_upld}, $userid );
            my $useregdat = $self->getUserDataById($userid, 1);
            my $userpassw = $manageflag == 1 || $manageflag == 2 ? Encode::decode("utf8", $_userpassw) : $self->getUserDataById($userid, 3);
            my $userlogin = $manageflag == 1 || $manageflag == 2 ? Encode::decode("utf8", $_userlogin) : $self->getUserDataById($userid, 2);
            my $userfio   = $manageflag == 1 || $manageflag == 2 ? Encode::decode("utf8", $_userfio) : $self->getUserDataById($userid, 5);
            my $usermail  = $manageflag == 1 || $manageflag == 2 ? Encode::decode("utf8", $_useremail) : $self->getUserDataById($userid, 4);
            my $userphon  = $manageflag == 1 || $manageflag == 2 ? Encode::decode("utf8", $_userphone) : $self->getUserDataById($userid, 7);
            my $useraddr  = $manageflag == 1 || $manageflag == 2 ? Encode::decode("utf8", $_useraddr) : $self->getUserDataById($userid, 6);

            if (defined $rekvizity[0] && $rekvizity[0] ne '') {
                my @rekxml;
                if ($manageflag == 1 || $manageflag == 2) {
                    @rekxml = @_rekvizity;
                } else {
                    @rekxml = $self->getRekviziryFromFile("$self->{mod_upld}/$rekvizity[0]");
                }
                $form = getOrderFormWithErrors(0, 0,
                    Encode::encode("utf8", $userfio),
                    Encode::encode("utf8", $usermail),
                    Encode::encode("utf8", $userphon),
                    Encode::encode("utf8", $useraddr), '-',
                    @rekxml, $manageflag == 2 ? $manageflag : 0);
                $paytype = 0;
            } else {
                $form = getOrderFormWithErrors(1, 1,
                    Encode::encode("utf8", $userfio),
                    Encode::encode("utf8", $usermail),
                    Encode::encode("utf8", $userphon),
                    Encode::encode("utf8", $useraddr),
                    $manageflag == 2 ? $manageflag : 0);
            }
            $form =~ s/Всё верно, оформляем!/Сохранить/;

            my $loginwarn;
            my $passwarn;
            if ($userlogin eq '' || $userlogin =~ /^[\r\n\t\s]+$/) { $loginwarn = qq~style="color:#ff0000;"~; }
            if ($userpassw eq '' || $userpassw =~ /^[\r\n\t\s]+$/) { $passwarn = qq~style="color:#ff0000;"~; }
            my $rdatefield = qq~<input type="hidden" required name="userregdate" value="$useregdat"></p>~;
            my $loginfield = qq~<p><label $loginwarn>Логин </label><br>
                <input type="text" required id="userlogin" name="userlogin" value="$userlogin" onchange="checkLightShopLogin('$sesid', '$userlogin')">
                <span class="lightshop-login-hint" id="lightshop-login-hint"></span></p>~;
            my $passwfield = qq~<p><label $passwarn>Пароль </label><br><input type="text" required name="userpassw" value="$userpassw"></p>~;
            $form =~ s/<!-- %rdate_field% -->/$rdatefield/;
            $form =~ s/<!-- %login_field% -->/$loginfield/;
            $form =~ s/<!-- %passw_field% -->/$passwfield/;
            $dealcontent .= qq~</table>
                     <div class="cart-checkout-block">
                        <p class="cart-item-descr-price"><span class="cart-item-lighted-font">Стоимость заказа:</span> <span>~.sprintf("%.2f", $totalordercost).qq~</span> $GLOBAL_CURRENCY</p>
                    </div>
                    <div class="cart-checkout-from-block">
                        <a name="cart-form-anchor"></a>
                        <p class="cart-checkout-from-block-header">Заказ оформлен на ~.($paytype == 0 ? 'юридическое лицо' : 'физическое лицо').qq~: </p>
                        <a name="userprofile"></a>
                        <div>
                            <form method="post" action="$ENV{SCRIPT_NAME}?id=$sesid&amp;action=lightshopusermodify" id="loadcartform" name="loadcartform">
                            $form
                            </form>
                        </div>
                    </div>
                </div>
            ~;
        }
        else {
            $dealcontent .= qq~<div class="light-shop-usrerror">Список записей пуст</div>~;
        }
    }
    else {
        $dealcontent .= qq~<div class="light-shop-usrerror">Файл $self->{mod_deal} не найден</div>~;
    }
    return $dealcontent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# LIGHTSHOP_usermodify ()
#------------------------------------------------------------------------------
#
# Редактирование учетной записи пользователя.
#
#------------------------------------------------------------------------------

sub modifyUserData {
    my (
        $self,
        $userid,
        $userregdate,
        $userlogin,
        $userpassw,
        $useremail,
        $userfio,
        $useraddr,
        $userphone,
        $usercomment
    ) = @_;
    open( my $inputfh, "<", $self->{mod_usrs} );
    my @userdb = <$inputfh>;
    close($inputfh);
    for my $i (0..$#userdb) {
        my $record = $userdb[$i];
        $record =~ s/[\r\n]+//g;
        my @tarr = split( /\|/, $record );
        $tarr[0] =~ s/\W//g;
        if ( $tarr[0] eq $userid ) {
            if ($self->isLoginUnique($userlogin) == 1) {
                $userlogin = $tarr[2] ;
            }
            my @savencdata = (
                $userid,
                $userregdate,
                $userlogin,
                $userpassw,
                $useremail,
                $userfio,
                $useraddr,
                $userphone,
                $usercomment,
                0
            );
            for my $j (0..$#savencdata) {
                eval {
                    $savencdata[$j] = Encode::decode("utf8", $savencdata[$j]);
                };
            }
            $userdb[$i] = join( "|", @savencdata, 0 ) . "\n";
        }
    }
    open( my $outputfh, ">", $self->{mod_usrs} );
    flock $outputfh, 2;
    seek( $outputfh, 0, 2 );
    print $outputfh @userdb;
    flock $outputfh, 8;
    close $outputfh;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_objects_by_id ()
#------------------------------------------------------------------------------
#
# Получение списка объектов по ID и каталогу.
#
#------------------------------------------------------------------------------

sub get_objects_by_id {
    my @object_images = ();
    my $dir           = $_[0];
    if ( $dir eq '' ) { $dir = $MODULE_DOWNLOAD_PATH; }
    my $id    = $_[1];
    my @files = &get_files_array($dir);
    foreach (@files) {
        if ( $_ =~ /^$_[1]/ ) { push( @object_images, $_ ) }
    }
    @object_images = sort { $a <=> $b } @object_images;
    return @object_images;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_files_array ()
#------------------------------------------------------------------------------
#
# Получение списка файлов некоторого каталога.
#
#------------------------------------------------------------------------------

sub get_files_array {
    my @files = ();
    my $dir   = $_[0];
    $dir =~ s/\\/\//g;
    if ( -e $dir ) {
        opendir DIR, $dir;
        @files = grep { ( !/^\.+$/ ) && !( -d "$dir/$_" ) } readdir DIR;
        closedir DIR;
    }
    return @files;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getItemDetailsV3 ()
#------------------------------------------------------------------------------
#
# Портированная для V3 функция получения детализации заказа.
#
#------------------------------------------------------------------------------

sub getItemDetailsV3 {
    my ($self, $shopid, $itemid) = @_;
    my $out_index = 0;
    my $pricefile = '';
    eval { $pricefile = Shopcat::V3::getPriceFileFullPath($shopid); };
    if ( -e $pricefile ) {
        open( FH, "<$pricefile" );
        my @f_strings = <FH>;
        close(FH);

        #Configuring new array
        while ( $out_index <= $#f_strings ) {
            $f_strings[$out_index] =~ s/[\r\n]+//g;
            my @params = split( /\|/, $f_strings[$out_index] );
            $params[0] =~ s/[\W]+//g;
            if ( $params[0] eq $itemid ) {
                my $navi_block;
                my $cat_descr;
                my $id     = 0;
                my $cat_id = 0;

                if ( $params[$#params] =~ /\[([\d]+)\]/ ) { $id = $1; }
                if ( $params[ $#params - 1 ] =~ /\[([\d]+)\]/ ) {
                    $cat_id = $1;
                }

                #Link to Shopcat
                eval {
                    $cat_descr = Shopcat::V3::getDescrNM1(
                        $shopid, $cat_id, $id );
                };

                if (   $cat_descr =~ 'No description record'
                    || $cat_descr =~ 'Descriptions file not found'
                    || $cat_descr eq '')
                {
                    $cat_descr = qq~
                        <span style="color:#C0C0C0">Описание отсутсвует</span>
                    ~;
                } else { $cat_descr = qq~$cat_descr~ }

                #Link to Shopcat
                eval {
                    $navi_block = Shopcat::V3::getNaviHeader( $id, $shopid );
                };
                $navi_block =~ s/textlink\-navi/textlink/g;

                #$params[2] =~ s/[\.\,]+[\d]+$//g;
                my $itemname = $params[$#params];
                $itemname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                my @new_params = (
                    $id, $navi_block, $params[2], '',
                    '',  $itemname,   '',         $cat_descr,
                    $params[0]
                );

                return @new_params;
            }
            $out_index++;
        }
    }
    return (-1);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getContactEmail ()
#------------------------------------------------------------------------------
#
# Получить глобальный email.
#
#------------------------------------------------------------------------------

sub getContactEmail {
    my $contact_email = $GLOBAL_CONTACT_EMAIL;
    eval {
        $contact_email = Feedbck::Casual::getFBGlobEmail();
    };
    return $contact_email;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getBuyItLink ()
#------------------------------------------------------------------------------
#
# Получить ссылку Купить Сейчас в HTML.
#
#------------------------------------------------------------------------------

sub getBuyItLink {
    my $lightshop_is_switchedoff =
      Lightshop::Sales::get_power_status_for_module();
    my $buyitlink;
    if ( $lightshop_is_switchedoff == 0 ) {
        my $shopid   = $_[0];
        my $itemid   = $_[1];
        my $price    = $_[2];
        my $itemname = $_[3];
        if ( $price > 0 ) {
            $buyitlink = qq~
              <a href="$ENV{SCRIPT_NAME}?action=addtocart&amp;shopid=$shopid&amp;item=$itemid"
                 title="Купить сейчас!"
                 class="lightshopbuyitnowlink">Купить сейчас!</a>
              ~;
        }
        else {
            $buyitlink = 'недоступно';
            my $feedback_is_off = 1;
            eval {
                $feedback_is_off =
                  Feedbck::Casual::getModPwrStatus();
            };
            if ( $feedback_is_off == 0 ) {
                $itemname =~ s/\"/&quot;/g;
                $buyitlink = qq~
                 <a href="$ENV{SCRIPT_NAME}?action=sendmess&amp;form=1&amp;message=Заказ позиции \'$itemname\'"
                    title="Заказать!"
                    class="lightshopbuyitnowlink">Заказать!</a>
                 ~;
            }
        }
    }
    return $buyitlink;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getCartLink ()
#------------------------------------------------------------------------------
#
# Получить ссылку на корзину в HTML.
#
#------------------------------------------------------------------------------

sub getCartLink {
    my $lightshop_is_switchedoff =
      Lightshop::Sales::get_power_status_for_module();
    my $cartlink = '';
    if ( $lightshop_is_switchedoff == 0 ) {
        my $goodsincart = &get_cart_block_goods();
        my $cartcosts   = &get_cart_cost();
        if ( $goodsincart > 0 ) {
            $cartlink = qq~
                <a  href="cart.html"
                    title="В корзине $goodsincart товар(ов) на сумму $cartcosts $GLOBAL_CURRENCY"
                    class="lightshopcartlink">Корзина</a>
                ~;
        }
    }
    return $cartlink;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getFullFilePathById ()
#------------------------------------------------------------------------------
#
# Получить полный путь к файлу по идентификетору.
#
#------------------------------------------------------------------------------

sub getFullFilePathById {
    my $filepath  = '';
    my $foundfile = '';
    my $searchdir = $MODULE_DOWNLOAD_PATH;
    my $id        = $_[0];
    my @files     = &get_files_array($searchdir);
    foreach (@files) {
        if ( $_ =~ /^$id/ ) {
            $foundfile = $_;
            last;
        }
    }
    if ($foundfile) {
        $filepath = $searchdir . "/" . $foundfile;
    }
    return $filepath;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showUserCart ()
#------------------------------------------------------------------------------
#
# Функция показа содержимого корзины покупателя.
#
#------------------------------------------------------------------------------

sub showUserCart {
    my $IRET_content =
      qq~<p class="pageheader">Корзина покупателя</p>\n~;
    my $user_id = get_cookie('alfacms_lightshop') || -1;
    my $carttable = get_from_cart($user_id);
    my @items_in_cart;
    my $dostavka = $GLOBAL_DOSTAVKA;
    my $userarea = Shopcat::V3::getV3UserareaStatus();
    my $hiddenstyle = 'style="display:none;"';

    if ( defined($carttable) && $user_id =~ /^[\d]+$/ ) {
        if ( $carttable ne '' ) {
            @items_in_cart = split( /\./, $carttable );
            if ( $#items_in_cart > -1 ) {
                my $totalcost = 0;
                $IRET_content .= qq~<div class="cart-canvas">
                    <form   id="cartform"
                            name="cartform"
                            method="POST"
                            action="$ENV{SCRIPT_NAME}?action=recountcost&amp;uid=$user_id"
                            style="margin:0px;">
                                <input type="hidden" id="cartlength" name="cartlength" value="$#items_in_cart">
                                <table class="goods-in-cart">
                ~;
                for ( my $i = 0 ; $i <= $#items_in_cart ; $i++ ) {
                    my $cat_descr;
                    my $pp_num       = $i + 1;
                    my $level_id     = 0;
                    my $cat_id       = 0;
                    my @tmp          = split( /\-/, $items_in_cart[$i] );
                    my $shopid       = $tmp[0];
                    my $itemid       = $tmp[1];
                    my @item_details = get_item_details( $shopid, $itemid );

                    #$item_details[2] =~ s/[\.\,]+[\d]+$//g;
                    if ( $item_details[$#item_details] =~ /\[([\d]+)\]/ ) {
                        $level_id = $1;
                    }
                    if ( $item_details[ $#item_details - 1 ] =~ /\[([\d]+)\]/ )
                    {
                        $cat_id = $1;
                    }

                    my $itemname = $item_details[$#item_details];
                    $itemname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;

                    #Link to Shopcat
                    eval {
                        $cat_descr = Shopcat::V3::getDescrNM1(
                            $shopid, $cat_id, $level_id );
                    };
                    if (  $cat_descr =~ 'No description record'
                          || $cat_descr =~ 'Descriptions file not found'
                          || $cat_descr eq '' )
                    {
                        $cat_descr = 'Описание отсутсвует';
                    }
                    if (   $item_details[2] =~ /^[\d]+\.[\d]+$/
                        && $itemid =~ /^[\d]+$/ )
                    {
                        eval {
                            $item_details[2] =
                              Shopcat::V3::getCorrPrice( $item_details[2] );
                        };
                        $totalcost = $totalcost + $item_details[2] * $tmp[2];
                    }

                    my $item_price = sprintf("%.2f", $item_details[2]);
                    my @header_n_navi;
                    my $overquantity;
                    my $title_img;
                    my $item_navi;
                    my $itemid_2;
                    my $quantity = $tmp[2];
                    my $total_for_item = sprintf("%.2f",$item_price * $tmp[2]);
                    my @quantity_select;
                    if ($quantity !~ /^[\d]+$/ || $quantity < 1) {
                        $quantity = 1;
                    }
                    if ($quantity > 10) {
                        $overquantity = qq~<option value="$quantity" selected>$quantity</option>~;
                    } else {
                        $quantity_select[$quantity] = 'selected';
                    }

                    #$cat_descr =~ s/(<[\/a-zA-Z\s\:0-9\"\;\#\-\:\=]+>)//gi;
                    my $img_path_artikul = "images/shopcat/$shopid/levels/n/$itemid";
                    my $img_path = "images/no_photo.png";
                    my $itemlink = "href=\"shopcatalog_$level_id\_$shopid.html\"";
                    eval {
                         $title_img = Shopcat::V3::getTitleImgFromDir($img_path_artikul);
                    };
                    if ( -e "$img_path_artikul/$title_img" ) {
                        $img_path = "$img_path_artikul/$title_img";
                    }
                    eval {
                        my $pricefile = Shopcat::V3::getPriceFileFullPath( $shopid );
                        $itemid_2 = Shopcat::V3::getItemIdInPriceFile($pricefile, $itemid);
                        @header_n_navi = Shopcat::V3::getNaviHeader($itemid_2, $shopid);
                    };
                    if ($userarea) {
                        $hiddenstyle = '';
                        if (@header_n_navi) {
                            $item_navi = $header_n_navi[1];
                        } else {
                            $item_navi = qq~<a href="/" class="textlink-navi">Главная</a> » <a href="shopcatalog.html" class="textlink-navi">Каталог</a> ~;
                        }
                    } else {
                        $itemlink = '';
                    }
                    $IRET_content .= qq~
                        <tr><td class="goods-in-cart-left">
                        <p class="cart-item-header">$pp_num. $itemname</p>
                        <div class="cart-link-path" $hiddenstyle>$item_navi</div>
                        <table><tr>
                            <td valign=top><div class="cart-item-img"><a $itemlink title="$itemname">
                            <img src="images/admin/cms/spacer.gif" alt="$itemname" style="background: url('$img_path') no-repeat; background-size: auto 150px;" height="150" width="150">
                            </a></div></td>
                            <td><div class="cart-item-descr">Цена за штуку: <b>$item_details[2] $GLOBAL_CURRENCY</b><br>Статус товара: <font style="color:rgb(15, 163, 44);"><b>в наличии</b></font><br>Артикул: <b>$itemid</b>
                            <div class="cart-item-text">$cat_descr</div>
                            <br>Выберите количество товаров:&nbsp;
                            <input type="hidden" autocomplete="off" id="cart-item-$pp_num-price" value="$item_price">
                            <input type="hidden" autocomplete="off" id="cart-item-$pp_num-currentprice" value="$total_for_item">
                            <input type="hidden" autocomplete="off" id="cart-item-$pp_num-select-name" value="$shopid-$itemid">
                            <select name="$shopid-$itemid" id="item-$pp_num-quantity-select" autocomplete="off" onchange="javascript:ModifyPrice($pp_num, $user_id);">
                                <option value="1" $quantity_select[1]>1</option>
                                <option value="2" $quantity_select[2]>2</option>
                                <option value="3" $quantity_select[3]>3</option>
                                <option value="4" $quantity_select[4]>4</option>
                                <option value="5" $quantity_select[5]>5</option>
                                <option value="6" $quantity_select[6]>6</option>
                                <option value="7" $quantity_select[7]>7</option>
                                <option value="8" $quantity_select[8]>8</option>
                                <option value="9" $quantity_select[9]>9</option>
                                <option value="10" $quantity_select[10]>10</option>
                                $overquantity
                            </select>
                        <p class="cart-item-descr-price"><span id="item-$pp_num-price-container">$total_for_item</span> $GLOBAL_CURRENCY</p></div></td></tr></table>
                        </td>
                        <td class="goods-in-cart-right" valign=top><a href="$ENV{SCRIPT_NAME}?action=delfromcart&amp;uid=$user_id&amp;shopid=$shopid&amp;item=$itemid" class="cart-item-delete" title="Удалить товар из корзины">&times;</a></td>
                        </tr>
                    ~;
                }

                my $form;
                my @selected = ('selected','');
                if (!@_) {
                    $form = getOrderForm(1, $user_id);
                } else {
                    $form = getOrderFormWithErrors(@_);
                    if ($_[0] == 0) {
                        @selected = ('','selected');
                    }
                }
                $IRET_content .= qq~
                    </table></form>
                    <div class="cart-checkout-block">
                        <p class="cart-item-descr-price"><span class="cart-item-lighted-font">Стоимость заказа:</span> <span id="item-total-price-container">$totalcost</span> $GLOBAL_CURRENCY</p>
                    </div>
                    <div class="cart-checkout-from-block">
                        <a name="cart-form-anchor"></a>
                        <p class="cart-checkout-from-block-header">Моментальное оформление заказа</p>
                        <p><label>Оформляем на:</label>
                        <select id="prefetch-select" autocomplete="off" onchange="javascript:doLightShopFormLoad($user_id)">
                            <option value="1" $selected[0]>Физическое лицо</option>
                            <option value="0" $selected[1]>Юридическое лицо</option>
                        </select></p>
                        <div>
                            <form method="post" action="$ENV{SCRIPT_NAME}?action=submitorder&amp;uid=$user_id" id="loadcartform" name="loadcartform">
                            $form
                            </form>
                        </div>
                    </div></div>
                    ~;
            }
            else {
                $IRET_content .= qq~
                    <p class="standart_text">В корзине нет товаров. <span $hiddenstyle>Перейдите в раздел
                    <a class="lightshopbuyitnowlink" href="shopcatalog.html">«Каталог»</a> и выберите интересующие товары из соответствующих подразделов.</span></p>
                    <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                ~;
            }
        }
        else {
            $IRET_content .= qq~
                <p class="standart_text">В корзине нет товаров. <span $hiddenstyle>Перейдите в раздел
                <a class="lightshopbuyitnowlink" href="shopcatalog.html">«Каталог»</a> и выберите интересующие товары из соответствующих подразделов.</span></p>
                <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
            ~;
        }
    }
    else {
        $IRET_content .= qq~
            <p class="standart_text">В корзине нет товаров. <span $hiddenstyle>Перейдите в раздел
            <a class="lightshopbuyitnowlink" href="shopcatalog.html">«Каталог»</a> и выберите интересующие товары из соответствующих подразделов.</span></p>
            <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
        ~;
    }
    return $IRET_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModJsCssCode ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить дополнительный код js/css для данного модуля.
# Функция возвращает дополнительный код js/css для данного модуля.
#
#------------------------------------------------------------------------------

sub getModJsCssCode {
    my $modulecode = qq~
    <!-- DROP DOWN MENU -->

    <link rel="stylesheet" type="text/css" href="css/dropdownmenu/dropdown-menu.css" />
    <link rel="stylesheet" type="text/css" href="css/dropdownmenu/dropdown-menu-skin.css" />
    <script type="text/javascript" src="/js/dropdownmenu/dropdown-menu.min.js"></script>

    <script type="text/javascript">
    function ModifyPrice(item, uid) {
        console.log("Recalculate price for "+item+" item ["+uid+"]; price field: "+"#cart-item-"+item+"-price");
        var priceforone = parseFloat(jQuery("#cart-item-"+item+"-price").val());
        console.log("Price for 1 item: "+priceforone);
        var quantity = parseFloat(jQuery("#item-"+item+"-quantity-select option:selected").val());
        var total = (priceforone*quantity);
        jQuery("#cart-item-"+item+"-currentprice").val(total.toFixed(2).toString());
        jQuery("#item-"+item+"-price-container").text(total.toFixed(2).toString());
        ModifyTotal();
        var total_items = parseFloat(jQuery("#cartlength").val());
        var msg   = jQuery('#cartform').serialize();

        if (typeof msg === 'undefined' || msg === null) {
            ;
        } else {
            jQuery.post( "$ENV{SCRIPT_NAME}?action=recountcost&amp;uid="+uid, msg)
                .fail(function( data ) {
                    console.log( "ModifyPrice(): post to $ENV{SERVER_NAME} failed: " + data );
                })
                .done(function( data ) {
                    console.log( "ModifyPrice(): post to $ENV{SERVER_NAME} done: " + data );
                });
        }
    }

    function ModifyTotal() {
        var total = 0;
        for (i = 0; i < 100; i++) {
            if (jQuery("input").is("#cart-item-"+i+"-currentprice")) {
                total += parseFloat(jQuery("#cart-item-"+i+"-currentprice").val());

            }
        }
        jQuery("#item-total-price-container").text(total.toFixed(2).toString());
    }
    function doLightShopFormLoad(userid) {
        var paytype = jQuery("#prefetch-select option:selected").val();
        jQuery("#loadcartform")
                .load(
                        "/user.pl?action=getorderform&amp;formtype="
                                + paytype
                                + "&amp;userid=" + userid + "?"
                                + Math.random(),
                        function(responseTxt, statusTxt, xhr) {
                            if (statusTxt == "error")
                                alert("Error: " + xhr.status + ": "
                                        + xhr.statusText);
                        });
    }

    function checkLightShopLogin(sesid, login) {
        var changedlgn = jQuery("#userlogin").val();
        if (typeof changedlgn === 'undefined' || changedlgn === null) {
            console.log( "checkLightShopLogin(): null value of #userlogin");
        } else {
            jQuery.get( "$ENV{SCRIPT_NAME}?id="+sesid+"&amp;action=lightshopchcklgn&amp;loginchck="+changedlgn)
                .fail(function( data ) {
                    console.log( "checkLightShopLogin(): get data from $ENV{SERVER_NAME} failed: " + data );
                })
                .done(function( data ) {
                    var message;
                    if (parseInt(data) == 1) {
                        message = '<i class="fa fa-times-circle-o" style="color:#ff6666; font-size:16px;"></i>&nbsp;&nbsp;Логин <font style="color:#909090">'+changedlgn+'</font> занят';
                        jQuery("#userlogin").val('')
                        console.log( "checkLightShopLogin(): get data from $ENV{SERVER_NAME} - login is busy");
                    } else {
                        if (parseInt(data) == 2) {
                            message = '<i class="fa fa-exclamation-circle" style="color:#ffd11a; font-size:16px;"></i>&nbsp;&nbsp;Текущий логин';
                        } else {
                            message = '<i class="fa fa-check-circle-o" style="color:#4CAF50; font-size:16px;"></i>&nbsp;&nbsp;Логин свободен';
                        }
                        console.log( "checkLightShopLogin(): get data from $ENV{SERVER_NAME} - login is free");
                    }
                    jQuery("#lightshop-login-hint").html(message);
                });
        }
    }
    </script>~;
    return $modulecode;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getOrderForm ( $formtype )
#------------------------------------------------------------------------------
#
# $formtype - тип формы (0 - обычная, 1 - c реквизитами).
# Получить форму для оформления заказа.
# Функция возвращает форму для оформления заказа в HTML формате.
#
#------------------------------------------------------------------------------

sub getOrderForm {
    (my $paytype, my $user_id) = @_;
    my $rekvizit_fields;
    if ($paytype == 0) {
        my @field_title   = &getOrgRekvizitTitles();
        for my $i (0..$#field_title) {
            my $orgrekvizlen =
                qq~<input name="orgrekvizlen" value="$#field_title" type="hidden">~;
            if ( $i != 0 ) { $orgrekvizlen = ''; }
            $rekvizit_fields .= qq~
                $orgrekvizlen
                <p><label>$field_title[$i]: </label><br><input name="orgrekviz$i" required value="" type="text"></p>
            ~;
        }
    }
    my $random      = rand();
    my @tmp         = split( /\./, $random );
    my $crypted_captcha_value = Pheix::Tools::getHexStrFromString(
       Pheix::Tools::doBlowfishEncrypt( $tmp[1] ) );

    my $make_order_form = qq~
        $rekvizit_fields
        <input type="hidden" name="paytype" value="$paytype">
        <p><label>Фамилия и имя: </label><br><input type="text" required name="userfio"></p>
        <p><label>Email: </label><br><input type="text" required name="useremail"></p>
        <p><label>Телефон: </label><br><input type="text" required name="userphone"></p>
        <p><label>Адрес доставки: </label><br><textarea type="text" required name="useraddress"></textarea></p>
        <input type="hidden" name="code1" value="$crypted_captcha_value">
        <p><label>Код защиты от спама:</label><br>
        <img src="$ENV{SCRIPT_NAME}?action=showcaptcha&amp;val=$crypted_captcha_value?$random" class="captcha-new-form">&nbsp;<input class="anti-span-code" type="text" name="code2" value=""></p>
        <p><a href="javascript:top.document.forms['loadcartform'].submit();" class="more-about-button">Всё верно, оформляем!</a></p>
    ~;
    return $make_order_form;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getOrgRekvizitFieldsLabels ()
#------------------------------------------------------------------------------
#
# Получить список полей формы с реквизитами (новый формат формы).
#
#------------------------------------------------------------------------------

sub getOrgRekvizitFieldsLabels {
    my $orgrekvizHtml = '';
    my @field_data    = @_;
    my @field_title   = &getOrgRekvizitTitles();
    for ( my $i = 0 ; $i <= $#field_title ; $i++ ) {
        if ( !defined( $field_data[$i] ) || $field_data[$i] eq '' ) {
            $field_title[$i] =
              qq~<font color="#ff0000">$field_title[$i]</font>~;
        }
        my $orgrekvizlen =
          qq~<input name="orgrekvizlen" value="$#field_title" type="hidden">~;
        if ( $i != 0 ) { $orgrekvizlen = ''; }
        my $fielddata = $field_data[$i];
        $orgrekvizHtml .= qq~
        $orgrekvizlen<p><label>$field_title[$i]: </label><br><input name="orgrekviz$i" value="$fielddata" type="text"></p>~;
    }
    return $orgrekvizHtml;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getOrderFormWithErrors ()
#------------------------------------------------------------------------------
#
# Получить форму оформления заказа для шага #2.
#
#------------------------------------------------------------------------------

sub getOrderFormWithErrors {
    my $paytype     = $_[0];
    my $user_id     = &get_cookie('alfacms_lightshop');
    my $random      = rand();
    my @tmp         = split( /\./, $random );
    my $codetitle   = 'Код защиты от спама:';
    my @field_title = (
        'Способ оплаты',
        'Реквизиты юридического лица:',
        'Фамилия и имя:',
        'Email:',
        'Телефон:',
        'Адрес доставки:',
        'Комментарии к заказу:'
    );
    my @field_data = ();
    my $sposoboplaty =
      'Оплата наличными - физическое лицо';
    my $rekvizityfield = '';

    if ( $#_ >= 0 ) {
        for my $i (0..$#field_title) {
            if ( defined( $_[$i] ) ) {
                push( @field_data, Encode::decode("utf8", $_[$i]) );
            }
            else {
                push( @field_data, '' );
            }
            if ( $field_data[$i] eq '' ) {
                $field_title[$i] =
                  qq~<font color="#ff0000">$field_title[$i]</font>~;
            }
        }
        if ( $_[$#_] == 2 ) {
            $codetitle = qq~<font color="#ff0000">$codetitle</font>~;
        }
    }

    my $crypted_captcha_value = Pheix::Tools::getHexStrFromString(
       Pheix::Tools::doBlowfishEncrypt( $tmp[1] ) );

    my $comments = $_[6] || '';
    $comments =~ s/<br>/\n/g;

    if ( $paytype == 0 ) {
        # $sposoboplaty = 'Безналичный расчет - юридическое лицо';
        $rekvizityfield = getOrgRekvizitFieldsLabels( @_[ 7 .. $#_ - 1 ] );
    }

    my $delivery_addr = $field_data[5];
    $delivery_addr    =~ s/<br>/\n/gi;

    my $form_with_errors = qq~
        $rekvizityfield
        <input type="hidden" name="paytype" value="$paytype">
        <p><label>$field_title[2] </label><br><input type="text" required name="userfio" value="$field_data[2]"></p>
        <!-- %rdate_field% -->
        <!-- %login_field% -->
        <!-- %passw_field% -->
        <p><label>$field_title[3] </label><br><input type="text" required name="useremail" value="$field_data[3]"></p>
        <p><label>$field_title[4] </label><br><input type="text" required name="userphone" value="$field_data[4]"></p>
        <p><label>$field_title[5] </label><br><textarea type="text" required name="useraddress">$delivery_addr</textarea></p>
        <input type="hidden" name="code1" value="$crypted_captcha_value">
        <p><label>$codetitle</label><br>
        <img src="$ENV{SCRIPT_NAME}?action=showcaptcha&amp;val=$crypted_captcha_value?$random" class="captcha-new-form">&nbsp;<input class="anti-span-code" type="text" name="code2" value=""></p>
        <p><a href="javascript:top.document.forms['loadcartform'].submit();" class="more-about-button">Всё верно, оформляем!</a></p>
        ~;
    return $form_with_errors;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getRekviziryFromFile ( $self, $filepath)
#------------------------------------------------------------------------------
#
# Получить массив реквизитов из заданного файла.
#
#------------------------------------------------------------------------------

sub getRekviziryFromFile {
    my ($self, $filepath) = @_;
    my @rekvizity;
    if (-e $filepath) {
        my $doc = XML::LibXML->load_xml( location => $filepath );
        my @rekxml = $doc->findnodes('/organization/rekvizit/value');
        for my $i (0..8) {
            my $rekvalue = $rekxml[$i]->textContent();
            if ($rekvalue !~ /^[\r\n\s]+$/sx && $rekvalue ne '') {
                eval {
                    #$rekvalue = Encode::encode("utf8", $rekvalue);
                };
                push(@rekvizity, $rekvalue);
            } else {
                push(@rekvizity, '')
            }
        }
        return @rekvizity;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# isLoginUnique ()
#------------------------------------------------------------------------------
#
# Проверка логина на уникальность.
#
#------------------------------------------------------------------------------

sub isLoginUnique {
    my ($self, $login) = @_;
    my $rc = 0;
    if ( -e $self->{mod_usrs} ) {
        open( my $inputfh, $self->{mod_usrs} );
        my @recs = <$inputfh>;
        close($inputfh);
        for my $i (0..$#recs) {
            my @cols = split( /\|/, $recs[$i] );
            if ( $cols[2] =~ /^$login$/) {
                    $rc = 1;
                    last;
                }
            }
    }
    return $rc;
}

#------------------------------------------------------------------------------

END { }

1;
