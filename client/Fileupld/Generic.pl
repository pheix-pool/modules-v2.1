my $fileup = Fileupld::Generic->new;

if ( ( $fileup->getModPwrStatus() == 0 ) && ( $fileup->{mod_user} == 1 ) ) {
    if ( defined $env_params[0] && $env_params[0] eq 'fileuploader' ) {
        if (defined $co) {
            print $co->header(
                -type            => 'text/html; charset=UTF-8',
                -Last_Modified   => $global_last_modified,
                '-Expires'       => $global_last_modified,
                '-Cache-Control' => 'max-age=0, must-revalidate',
                '-Access-Control-Allow-Origin' => '*'
            );
            print $fileup->showHomePage;
        } else {
            die "CGI object is missing";
        }
        exit;
    }
}
