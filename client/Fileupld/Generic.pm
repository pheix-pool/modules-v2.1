package Fileupld::Generic;

use strict;
use warnings;

BEGIN {
    use lib '../';
    use lib 'libs/modules';

    # avoid broken dependence for development boundle of MODULES_2.1
    use lib '../.sys_libs';
}

##############################################################################
#
# File   :  Generic.pm
#
##############################################################################

use utf8;
use POSIX;
use Encode qw(decode encode);
use Pheix::Tools qw(getConfigValue getModXml getSingleSetting getDateUpdate);
use Pheix::Pages qw(fillCommonTags);

#------------------------------------------------------------------------------
# new ()
#------------------------------------------------------------------------------

sub new {
    my $MODULE_NAME   = "Fileupld";
    my $MODULE_FOLDER = 'admin/libs/modules/' . $MODULE_NAME;
    my ($class)     = @_;
    my $self        = {
        name     => 'Fileupld::Generic',
        version  => '2.2',
        mod_name => $MODULE_NAME,
        mod_dscr => 'Универсальный загрузчик файлов',
        mod_fldr => $MODULE_FOLDER,
        mod_inst => 'conf/system/install.tnk',
        mod_tmpl => 'conf/config/template.html',
        mod_dbfn => getConfigValue(
            $MODULE_FOLDER, getModXml($MODULE_FOLDER), 'tankfile'
        ),
        mod_scrt => getStatsJsCss(),
        mod_user => getSingleSetting( $MODULE_NAME, 1, 'soffuserarea' ),
    };
    bless $self, $class;
    return $self;
}

#------------------------------------------------------------------------------
# getStatsJsCss ( )
#------------------------------------------------------------------------------

sub getStatsJsCss {
    return '<!-- no code available -->';
}

#------------------------------------------------------------------------------
# getModPwrStatus ( )
#------------------------------------------------------------------------------

sub getModPwrStatus {
    my ($self) = @_;
    my $fn     = $self->{mod_inst};
    if ( -e $fn ) {
        open my $fd, '<', $fn or die "unable to open ".$self->{mod_inst}.": $!";
        my @recs = <$fd>;
        close $fd or die "unable to close ".$self->{mod_inst}.": $!";
        for my $i (0..$#recs) {
            my @tmp = split( /\|/, $recs[$i] );
            if ( $tmp[2] eq $self->{mod_name} ) { return $tmp[4]; }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------
# printModDebug ( )
#------------------------------------------------------------------------------

sub showHomePage {
    my ($self) = @_;
    my $pageaction  = $ENV{REQUEST_URI};
    if ( -e $self->{mod_tmpl} ) {
        open my $fh, "<", $self->{mod_tmpl}
            or die "unable to open " . $self->{mod_tmpl} . ": $!";
        my @tmpl = <$fh>;
        close $fh or die "unable to close " . $self->{mod_tmpl} . ": $!";
        for my $cnt (0..$#tmpl) {
            $tmpl[$cnt] = Pheix::Pages::fillCommonTags(
                $tmpl[$cnt],
                "fileuploader",
                $self->{name}
            );
            if ( $tmpl[$cnt] =~ /\%content\%/ ) {
                my $content = $self->printModDebug;
                $tmpl[$cnt] =~ s/\%content\%/$content/;
            }
            print $tmpl[$cnt];
        }
    }
    else {
        print qq~
            <b>$self->{name}:</b>&nbsp;
            in function show_feedback_clientpage()&nbsp;-&nbsp;
            $self->{mod_tmpl} not found!
        ~;
    }
}

#------------------------------------------------------------------------------
# printModDebug ( )
#------------------------------------------------------------------------------

sub printModDebug {
    my ($self) = @_;
    my $md = decode("utf8", $self->{mod_dscr});
    my $debug = qq~
        <p><b>$self->{name}</b> at $self->{mod_fldr}.</p>
        <p>
            $md<br>
            version: $self->{version}<br>
            db: $self->{mod_dbfn}
        </p>
    ~;
    return $debug;
}

#------------------------------------------------------------------------------
# printModDebug ( $data )
#------------------------------------------------------------------------------

sub parseVariables {
    my ( $self, $data ) = @_;
    my $cntnt;
    my $swoff = $self->getModPwrStatus();
    if ( $data =~ /%FILE_DETAILS\((.{0,})\)%/ && $swoff == 0 ) {
        my @fields;
        my $ds = $1;
        $ds =~ s/(\&\#39;)/\'/gi;
        while ( $ds =~ s/\'([^\'.]*)\'// ) {
            push @fields, $1;
        }
        if ( @fields ) {
            my @recs;
            if ( $fields[0] eq '*' ) {
                @recs = $self->getAllFilesIds;
            }
            else {
                push(@recs, $fields[0]);
            }
            foreach (@recs) {
                my %details = $self->getFileData($_);
                if ( %details ) {
                    my $fpath = $details{fpath};
                    if ( -e $fpath ) {
                        my @cntnr = ( "div", "span" );
                        my $fname = decode('utf-8', $details{hminm});
                        my $ftime = getDateUpdate((stat($fpath))[9]);
                        my $fskb  = ( -s $fpath );
                        my $fsize = ( $fskb > 10000 ) ?
                            sprintf "%.2f", $fskb / 1000000 :
                                sprintf "%.4f", $fskb / 1000000;
                        my $tag = getSingleSetting( $self->{mod_name}, 1, 'containertag' );
                        $cntnt .= qq~
                            <$cntnr[$tag] class="pheix-upload-file-block">
                                <$cntnr[$tag] class="_phx-inblck">
                                    <a href="$details{fpath}">$fname</a>
                                </$cntnr[$tag]>
                                <$cntnr[$tag] class="_phx-inblck">
                                    ($fsize Мб., обновлен: $ftime)
                                </$cntnr[$tag]>
                            </$cntnr[$tag]>
                        ~;
                    }
                }
            }
        }
        if ( !$cntnt || !(defined $cntnt) || $cntnt eq '' ) {
            $cntnt = qq~<p>Parse %FILE_DETAILS()% error $fields[0]</p>~;
        }
        $data =~ s/%FILE_DETAILS\((.{0,})\)%/$cntnt/gi;
    } else {
        if ($swoff == 1) {
            $cntnt = qq~<p>Module <b>$self->{name}</b> is switched off!</p>~;
            $data =~ s/%FILE_DETAILS\((.{0,})\)%/$cntnt/gi;
        }
    }
    $cntnt = $data;
    return $cntnt;
}

#------------------------------------------------------------------------------
# getFileData( $recid )
#------------------------------------------------------------------------------

sub getFileData {
    my ($self, $recid ) = @_;
    my @recs;
    my %data;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open " . $self->{mod_dbfn} . ": $!";
        @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
        for my $i (0..$#recs) {
            $recs[$i] =~ s/[\r\n\t]+//gi;
            my @cols  = split(/\|/,$recs[$i]);
            %data  = (
                'srtid' => $cols[0],
                'recid' => $cols[1],
                'fpath' => $cols[2],
                'usrid' => $cols[3],
                'hminm' => $cols[4]
            );
            if ( $data{recid} == $recid ) {
                last;
            }
        }
    }
    return %data;
}

#------------------------------------------------------------------------------
# getAllFilesIds( )
#------------------------------------------------------------------------------

sub getAllFilesIds {
    my ( $self ) = @_;
    my @records;
    open my $fh, "<", $self->{mod_dbfn} or die "unable to open " . $self->{mod_dbfn} . ": $!";
    my @recs = <$fh>;
    close $fh or die "unable to close " . $self->{mod_dbfn} . ": $!";
    for my $i (0..$#recs) {
        $recs[$i] =~ s/[\r\n\t]+//gi;
        my @cols  = split(/\|/,$recs[$i]);
        if ( @cols ) {
            push(@records, $cols[1]);
        }
    }
    return @records;
}

#------------------------------------------------------------------------------

END { }

1;
