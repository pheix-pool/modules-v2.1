my $remotefs_is_switchedoff =
  Remote::FS::getModPwrStatus();
if ( $remotefs_is_switchedoff == 0 ) {
    if ( defined $env_params[0] && $env_params[0] eq 'remotefs' ) {
        if (defined $co) { 
            print $co->redirect( -uri => "/" );
        } else {
            die "CGI object is missing";
        }
        exit;
    }
}
