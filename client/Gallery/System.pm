package Gallery::System;

use strict;
use warnings;

BEGIN {
    use lib '../';
    use lib '../.sys_libs';
    use lib 'libs/modules';
}

##############################################################################
#
# File   :  System.pm
#
##############################################################################
#
# Главный пакет управления системными галереями в польз. части CMS Pheix
#
##############################################################################

use Pheix::Tools qw(getFilesArray);
use Encode;
use utf8;
use open qw(:std :utf8);

#------------------------------------------------------------------------------
# new ()
#------------------------------------------------------------------------------
#
# Конструктор класса Gallery::System.
#
#------------------------------------------------------------------------------

sub new {
    my $MODULE_NAME = "Gallery";
    my ($class)     = @_;
    my $self        = {
        name     => 'Gallery::System',
        version  => '2.2',
        mod_name => $MODULE_NAME,
        mod_dscr => 'Системная галерея',
        mod_fldr => 'libs/modules/' . $MODULE_NAME,
        mod_inst => 'conf/system/install.tnk',
        mod_dbfn => 'conf/system/sysgallery.tnk',
        mod_ipth => 'images/systemgallery/upload',
        mod_extn => '.galconcat',
    };
    bless $self, $class;
    return $self;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# get_power_status_for_module ()
#------------------------------------------------------------------------------
#
# Получить статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub getGalSysPowerStatus {
    my ( $self ) = @_;
    if ( -e $self->{mod_inst} ) {
        open my $fh, "<", $self->{mod_inst};
        my @_db = <$fh>;
        close $fh;
        for my $i ( 0..$#_db ) {
            my @_t = split  /\|/, $_db[$i];
            if ( $_t[2] eq $self->{mod_name} ) {
                return $_t[4];
            }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSysGalGrpIdByPath ()
#------------------------------------------------------------------------------
#
# Функция получения идентификатора группы по корневому пути.
#
#------------------------------------------------------------------------------

sub getSysGalGrpIdByPath {
    my ( $self, $path) = @_;
    my $rc;
    if (-e $self->{mod_dbfn}) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open $self->{mod_dbfn}: ".$!;
        my $_qr = quotemeta($path);
        my @_db = grep { $_ =~ /$_qr/gi } <$fh>;
        close $fh or die "unable to close $self->{mod_dbfn}: ".$!;
        if (@_db) {
            chomp($_db[0]);
            my @_col = split  /\|/, $_db[0];
            if ($_col[6] > 0) {
                $rc = $_col[1];
            }
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSysGalIdByPath ()
#------------------------------------------------------------------------------
#
# Функция получения идентификатора галереи по корневому пути.
#
#------------------------------------------------------------------------------

sub getSysGalIdByPath {
    my ( $self, $path) = @_;
    my $rc;
    if (-e $self->{mod_dbfn}) {
        open my $fh, "<", $self->{mod_dbfn} or
            die "unable to open $self->{mod_dbfn}: ".$!;
        my $_qr = quotemeta($path);
        my @_db = grep { $_ =~ /$_qr/gi } <$fh>;
        close $fh or die "unable to close $self->{mod_dbfn}: ".$!;
        if (@_db) {
            chomp($_db[0]);
            my @_col = split  /\|/, $_db[0];
            $rc = $_col[1];
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getGroupIndex ()
#------------------------------------------------------------------------------
#
# Функция получения блока группы с плитками входящих в группу галерей.
#
#------------------------------------------------------------------------------

sub getGroupIndex {
    my ( $self, $galid, $path, $shopid ) = @_;
    my $_type = $self->getSysGalType($galid);
    my $cntnt;
    if ($_type == 1) {
        my $fname =  $path.q{/}.$galid.$self->{mod_extn};
        if (-e $fname) {
            open my $fh, "<", $fname or die "unable to open $fname: ".$!;
            my @_db = <$fh>;
            close $fh or die "unable to close $fname: ".$!;
            if (@_db) {
                for my $i ( 0..$#_db ) {
                    chomp($_db[$i]);
                    my @_col = split  /\|/, $_db[$i];
                    my $_gid = $_col[0];
                    my $_pth = $self->getSysGalPath($_gid);
                    my @_imgs = $self->getSysGalImgs($_pth, $_gid);
                    my $_indeximg = $_pth.q{/}.$_imgs[0];
                    if (-e $_indeximg) {
                        $cntnt .= qq~<div class="sysgal-gr-block">
                          <a href="javascript:doAjaxV3SliderLoad('$_gid');"
                             style="background:url('$_indeximg') no-repeat;"
                             class="sysgal-gr-block-link"><img src="images/spacer.gif" alt=""></a>
                          <div class="sysgal-gr-block-descr">$_col[1]</div>
                        </div>~;
                    }
                }
            }
        }
        if ($cntnt) {
            $cntnt = qq~<div id="v3-slider-load">$cntnt</div>~;
        }
    } elsif ($_type == 2) {
        my $prfile;
        eval {
            if ($shopid) {
                $prfile = Shopcat::V3::getPriceFileFullPath($shopid);
            }
        };
        if (-e $prfile) {
            open my $fh, "<", $prfile or die "unable to open $prfile: ".$!;
            my @_db = <$fh>;
            close $fh or die "unable to close $prfile: ".$!;
            foreach my $_rec (@_db) {
                my @f_vals;
                my $f_inx = 1;
                chomp($_rec);
                my @_col = split /\|/, $_rec;
                my $imgpth = "images/shopcat/$shopid/levels/n/$_col[0]";
                if (-e $imgpth) {
                    my $_gid = $self->getSysGalIdByPath($imgpth);
                    my @_imgs = $self->getSysGalImgs($imgpth, $_gid);
                    my $_type_gal = $self->getSysGalType($_gid);
                    my $_indeximg = $imgpth.q{/}.$_imgs[0];
                    $_col[-1] =~ s/(\[([\d]+)\])|(\[[\*]+\])//g;
                    my $_id = $2;
                    eval {
                        $f_inx = Shopcat::V3::getFilterIndex();
                        if ($f_inx > 1 && $f_inx < 13) {
                            @f_vals = Shopcat::V3::getFilterData(
                                $shopid > 0 ? $shopid : -1, $_id, $f_inx );
                        }
                    };
                    my $_descr = $f_vals[0] ne '' ? $f_vals[0] : $_col[-1];
                    if ((-e $_indeximg) && $_type_gal == 0) {
                        $cntnt .= qq~<div class="sysgal-gr-block">
                          <a href="shopcatalog\_$_id\_$shopid.html"
                             style="background:url('$_indeximg') no-repeat;"
                             class="sysgal-gr-block-link"><img src="images/spacer.gif" alt=""></a>
                          <div class="sysgal-gr-block-descr">$_descr</div>
                        </div>~;
                    }
                }
            }
        }
    }
    return $cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSysGalImgs ()
#------------------------------------------------------------------------------
#
# Получение списка изображений по идентификатору и каталогу.
#
#------------------------------------------------------------------------------

sub getSysGalImgs {
    my ( $self, $dir, $galid ) = @_;
    my @imgs;
    my @files = getFilesArray($dir);
    for my $kinx ( 0 .. $#files ) {
        my @tmp = split( /\_/, $files[$kinx] );
        if ( $tmp[0] eq $galid ) {
            push( @imgs, $files[$kinx] );
        }
    }
    @imgs = sort { $a cmp $b } @imgs;
    return @imgs;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSysGalPath ()
#------------------------------------------------------------------------------
#
# Функция получения пути к корневому каталогу галереии по идентификатору.
#
#------------------------------------------------------------------------------

sub getSysGalPath {
    my ( $self, $galid ) = @_;
    my $_rootpath = $self->{mod_ipth};
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn}  or
            die "unable to open $self->{mod_dbfn}: ".$!;
        my @_db = <$fh>;
        close $fh or die "unable to close $self->{mod_dbfn}: ".$!;
        for my $j ( 0 .. $#_db ) {
            my @_t = split /\|/, $_db[$j];
            $_t[1] =~ s/[\D]+//g;
            if ( $_t[1] eq $galid) {
                if ( $_t[3] =~ /^images\// ) {
                    $_rootpath = $_t[3];
                }
                last;
            }
        }
    }
    return $_rootpath;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSysGalType ()
#------------------------------------------------------------------------------
#
# Функция получения типа галереии по идентификатору.
#
#------------------------------------------------------------------------------

sub getSysGalType {
    my ( $self, $galid ) = @_;
    my $_type;
    if ( -e $self->{mod_dbfn} ) {
        open my $fh, "<", $self->{mod_dbfn}  or
            die "unable to open $self->{mod_dbfn}: ".$!;
        my @_db = <$fh>;
        close $fh or die "unable to close $self->{mod_dbfn}: ".$!;
        for my $j ( 0 .. $#_db ) {
            my @_t = split /\|/, $_db[$j];
            $_t[1] =~ s/[\D]+//g;
            if ( $_t[1] eq $galid) {
                $_type = $_t[6];
                last;
            }
        }
    }
    return $_type;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSysGallerySlider ()
#------------------------------------------------------------------------------
#
# Функция получения слайдера для заданной галереи.
#
#------------------------------------------------------------------------------

sub getSysGallerySlider {
    my ($self, $cgi_p) = @_;
    my $rc;
    my $_gid = $cgi_p->param('galid');
    if ($_gid > 0) {
        my $_gtype = $self->getSysGalType($_gid);
        if ($_gtype == 0) {
            my $_gpath = $self->getSysGalPath($_gid);
            #if ($_gpath eq $self->{mod_ipth}) {
            #} else {
                my @images = getFilesArray($_gpath);
                @images = reverse(sort {$a cmp $b} @images);
                my @imgchck;
                for my $j (0..$#images) {
                    if ($images[$j] =~ /(\.jpg)|(\.png)|(\.gif)$/) {
                        push(@imgchck,$images[$j]);
                    }
                }
                if (@imgchck) {
                    eval {
                        $rc =
                            Shopcat::V3::fillSlider($_gpath, @imgchck).
                            Shopcat::V3::getSliderJs();
                    };
                }
            #}
        }
    }
    if (!$rc) {
        $rc = qq~<p class="error"><big><b>Ошибка при доступе к галереи «$_gid»!</b></big></p>~;
    }
    return $rc;
}

#------------------------------------------------------------------------------

END { }

1;
