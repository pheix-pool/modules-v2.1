my $_sysgal = Gallery::System->new;
my $gallery_is_switchedoff = $_sysgal->getGalSysPowerStatus();
if ( $gallery_is_switchedoff == 0 ) {
    if (   defined( $env_params[0] )
        && $env_params[0] eq 'systemgallery'
        && defined($co) )
    {
        print $co->redirect( -uri => "/" );
        exit;
    }

    if (   defined( $env_params[0] )
        && $env_params[0] eq 'sysgalslider'
        && defined($co) )
    {
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        print $_sysgal->getSysGallerySlider($co);
        exit;
    }
}