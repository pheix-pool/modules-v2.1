package Shopcat::V3;

use strict;
use warnings;

##########################################################################
#
# File   :  V3.pm
#
##########################################################################

our ( @ISA, @EXPORT );

BEGIN {
    use lib '../';
    use lib '../.sys_libs';
    use lib 'libs/modules';
    # exports
    require Exporter;
    @ISA = qw(Exporter);
    # exports are shared for user.pl
    our @EXPORT = qw(
        getV3PowerStatus
        getV3UserareaStatus
        isThisShopCatAction
        getActionParams
        checkCatIsItem
        showV3HomePage
        getVarntsPage
        doCatsLoad
        getSearchResults
        modifyFilter
    );
}

#------------------------------------------------------------------------------

use Redis;
use Pheix::Tools;
use Shopcat::Templts;
use Shopcat::Util;
use Shopcat::4PUrl;
use POSIX;
use Time::HiRes qw( gettimeofday tv_interval );
use Encode;
use utf8;
use open qw(:std :utf8);
use CGI qw/:standard/;
use CGI::Cookie;
use Time::HiRes qw( gettimeofday tv_interval );
use List::Util qw( shuffle );
use File::stat;

#------------------------------------------------------------------------------

my $MODULE_NAME             = "Shopcat";
my $MODULE_DESCR            = "Каталог";
my $MODULE_FOLDER           = "admin/libs/modules/$MODULE_NAME/";
my $MODULE_DB_FILE          = "conf/system/shopcat2.tnk";
my $MODULE_SEO_DB_FILE      = "conf/system/shopcat2.seo.tnk";
my $MODULE_PARAMETRIZED     = 'conf/system/shopcat2.parametrized.tnk',
my $MODULE_PRICEPOOL        = "conf/system/pricepool/";       # WARNING!!! / is needed at the end
my $MODULE_DATAPOOL         = "conf/pages/shopcat2";          # WARNING!!! / is NOT needed at the end
my $MODULE_LEFT_NAV         = "conf/pages/right-nav.txt";
my $GLOBAL_CURRENCY_CONVERT_PATH = 'conf/system/currencydump.tnk';
my $MODULE_VARIANTS_LIM     = 5;
my $PROCESS_SORT_TITLES     = 0;
my $PRICEFILE_DB_COLUMS     = 5;

#------------------------------------------------------------------------------

my $FILTERS_MODE    = getSingleSetting( $MODULE_NAME, 1, 'filtersmode' );
my $FORCE_FALL_BACK = getSingleSetting( $MODULE_NAME, 1, 'fallbackmode' );
my $MODULE_DEBUG    = getSingleSetting( $MODULE_NAME, 1, 'debugmode' );
my $GLOBAL_CURRENCY = getSingleSetting( $MODULE_NAME, 1, 'currency' );
my $BACKLINK_ANCHOR = getSingleSetting( $MODULE_NAME, 1, 'backlink' );
#------------------------------------------------------------------------------

my $DEFAULT_FILTERS =
    $FORCE_FALL_BACK && ($FILTERS_MODE == 1 || $FILTERS_MODE == 2) ?
        Shopcat::Templts::getFiltersBlock(getFiltersArray()) : '';

my $BACK_LINK = qq~
   <a href="javascript:top.history.back()" class="shopcat-textlink-navi">$BACKLINK_ANCHOR</a>
~;

my $DEFAULT_TMPL_BLOCK = qq~
   <li class="shopcat-margined-list-item" data-type="default-block">
      <a href="%blk_uri%" class="shopcat-simplelink">%blk_t%</a>%blk_p%</li>
~;

my $DEFAULT_SLIDER_JS = getSliderJs();
my $DEFAULT_SEARCH_FORM_TEMPLATE = getSrchPanel();

my $ROOT_CATEGORY_LIST = qq~
  $DEFAULT_SEARCH_FORM_TEMPLATE
  <div id="search-container">
    <div class="shopcat-header-div"><h3>%header%</h3></div>
    <div class="shopcat-catlist-div">
      %root_categories_list%
    </div>
  </div>
~;
my $DEFAULT_DYNAMIC_TEMPLATE = qq~
  $DEFAULT_SEARCH_FORM_TEMPLATE
  <div class="shopcat-navi">
    <div class="shopcat-home-div"><a href="shopcatalog.html" class="shopcat-textlink-navi"><i class="fa fa-home"></i></a></div>
    <div class="shopcat-navi-div">%navi%</div>
  </div>
  <div id="search-container">
    <div class="shopcat-header-div"><h3>%header%</h3></div>
    <div class="shopcat-content-div">%rows_n_columns%</div>
  </div>
  <div class="shopcat-navi">
    <div class="shopcat-navi-div">%navi%</div>
  </div>
~;
my $DEFAULT_VARIANTS_BLOCK = qq~
  <div style="margin:20px 0 20px 0;">
   <div class="divs-in-inline-block-variants">
    <!--<a href="%linkurl%" title="%cat_name%">
    <img  src="images/admin/cms/spacer.gif"
          width="150" height="150"
          alt="%cat_name%"
          class="shopcat-variants-img"
          style="background: url('%img_path%') no-repeat; background-size: cover;"></a>-->
    <div class="shopcat-fa-img-variants">
        <div class="shopcat-variant-img">
            <a href="%linkurl%" title="%cat_name%">
            %img_content%
            </a>
        </div>
    </div>
    <div class="inline-block-description">
      <span class="shopcat-description-anchor-text">Артикул:</span> %artikul%<br>
      <span class="shopcat-description-anchor-text">Цена:</span> %itemprice%</br>
                <span class="shopcat-description-anchor-text">Идентификатор категории:</span> %cat_id%</br>
                <span class="shopcat-description-anchor-text">Название категории:</span> %cat_name%</br>
                <span class="shopcat-description-anchor-text">Описание позиции в файле описаний категории:</span> %cat_descr%<br>
    </div>
   </div>
  </div>
~;

#------------------------------------------------------------------------------
# getSliderJs ( )
#------------------------------------------------------------------------------
#
# Вывод JS кодя для слайдера.
#
#------------------------------------------------------------------------------

sub getSliderJs {
    return qq~
    <!--<script type="text/javascript">
        if (jQuery('.slick-slider-pheix-embedded').length > 0) {
            jQuery('.slick-slider-pheix-embedded').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                autoplay: true,
                autoplaySpeed: 5000,
                centerPadding: '70px',
                centerMode: true,
                arrows: false,
                dots:true,
            });
        }
    </script>-->
    <script type="text/javascript">
        if (jQuery('.slick-slider-pheix-embedded').length > 0) {
            jQuery('.slick-slider-pheix-embedded').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                autoplay: true,
                autoplaySpeed: 5000,
                /*centerPadding: '70px',*/
                centerMode: false,
                arrows: false,
                dots:true,
                mobileFirst:true,
                responsive: [
                    {
                      breakpoint: 1200,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                      }
                    },
                    {
                      breakpoint: 992,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                      }
                    },
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                      }
                    },
                    {
                      breakpoint: 576,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                      }
                    },
                    {
                      breakpoint: 450,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 1,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                ]
            });
        }
    </script>
    ~;
}

#------------------------------------------------------------------------------
# getSrchPanel ( )
#------------------------------------------------------------------------------
#
# Вывод шаблона блока поиска.
#
#------------------------------------------------------------------------------

sub getSrchPanel {
    my $_cntnt;
    if ($FILTERS_MODE > 0) {
        $_cntnt = qq~
        <form id="search-catalog" class="topsearch" method="post" action="javascript:doAjaxSearch();">
         <div class="shopcat-search-div">
          <div class="shopcat-search-div-td1a">$DEFAULT_FILTERS</div>
          <div class="shopcat-search-div-td2a">
           <input id="search-catalog-query" required="" placeholder="hey, Pheix..." value="" name="searchquery" class="shopcat-search-input" type="text">
          </div>
          <div class="shopcat-search-div-td3a">
           <input value="&#xf002;" class="shopcat-search-button" type="submit">
          </div>
        </div></form>~;
    } else {
        $_cntnt = qq~
        <form id="search-catalog" class="topsearch" method="post" action="javascript:doAjaxSearch();">
         <div class="shopcat-search-div">
          <div class="shopcat-search-div-td1">
           <input required="" placeholder="hey, Pheix..." value="" name="searchquery" class="shopcat-search-input" type="text">
          </div>
          <div class="shopcat-search-div-td2">
           <input value="&#xf002;" class="shopcat-search-button" type="submit">
          </div>
        </div></form>~;
    }
}

#------------------------------------------------------------------------------
# getN1LvlTempl ( )
#------------------------------------------------------------------------------
#
# Получить шаблон уровня N-1 в соответствии с политикой отображения фильтров.
#
#------------------------------------------------------------------------------

sub getN1LvlTempl {
    my $_cntnt = qq~
    $DEFAULT_SEARCH_FORM_TEMPLATE
    <div class="shopcat-navi">
      <div class="shopcat-home-div"><a href="shopcatalog.html" class="shopcat-textlink-navi"><i class="fa fa-home"></i></a></div>
      <div class="shopcat-navi-div">%navi%</div>
    </div>
    <div id="search-container">
      <div class="shopcat-header-div"><h3>%header%</h3></div>
      <div class="shopcat-content-div shopcat-n1-lvl-content">%rows_n_columns%</div>
      <div class="shopcat-template-sliderblock shopcat-n1-lvl-sider">%slider%</div>
    ~;
    if ($FILTERS_MODE != 1) {
        $_cntnt .= qq~
        <div id="horizontalTab" class="shopcat-n1-lvl-tabs">
         <ul class="resp-tabs-list">
          <li>Информация</li>
          <li>Дополнительно</li>
          <li>Видео</li>
          <li>Похожие товары</li>
          <li>Комментарии</li>
         </ul>
         <div class="resp-tabs-container">
          <div class="shopcat-template-block">%information%</div>
          <div class="shopcat-template-block">%primenenie%</div>
          <div class="shopcat-template-block">%video%</div>
          <div class="shopcat-template-block">%variants%</div>
          <div class="shopcat-template-block">%files%</div>
         </div>
        </div>
        ~;
    } else {
        $_cntnt .= qq~
            <div class="shopcat-template-block shopcat-n1-lvl-tmplblock">%filteredtab%</div>
        ~;
    }
    $_cntnt .= qq~
    </div>
    <div class="shopcat-navi">
      <div class="shopcat-home-div"><span class="shopcat-textlink-span"><i class="fa fa-undo"></i></span></div>
      <div class="shopcat-navi-div">%navi_bottom%</div>
    </div>
    $DEFAULT_SLIDER_JS
    <script type="text/javascript">
        if (jQuery('#horizontalTab').length > 0) {
            jQuery('#horizontalTab').easyResponsiveTabs();
        }
    </script>
    ~;
}

#------------------------------------------------------------------------------
# getNLvlTempl ( )
#------------------------------------------------------------------------------
#
# Получить шаблон уровня N в соответствии с политикой отображения фильтров.
#
#------------------------------------------------------------------------------

sub getNLvlTempl {
    my $_cntnt = qq~
    $DEFAULT_SEARCH_FORM_TEMPLATE
    <div class="shopcat-navi">
        <div class="shopcat-home-div"><a href="shopcatalog.html" class="shopcat-textlink-navi"><i class="fa fa-home"></i></a></div>
        <div class="shopcat-navi-div">%navi%</div>
    </div>
    <div id="search-container">
      <div class="shopcat-header-div"><h3>%header%</h3></div>
    ~;
    if ($GLOBAL_CURRENCY ne '0') {
        $_cntnt .= qq~
        <div class="shopcat-content-div">
         <span class="shopcat-description-anchor-text">Артикул: </span>
         %artikul%, <span class="shopcat-description-anchor-text">цена: </span>%price%,
         <span class="shopcat-description-anchor-text">действие: </span> %byuitnow%</div>
        ~;
    }
    $_cntnt .= qq~<div class="shopcat-template-sliderblock shopcat-n-lvl-sider">%slider%</div>~;
    if ($FILTERS_MODE != 1) {
        $_cntnt .= qq~
        <div id="horizontalTab" class="shopcat-n-lvl-tabs">
            <ul class="resp-tabs-list">
                <li>Информация</li>
                <li>Дополнительно</li>
                <li>Комментарии</li>
                <li>Похожие товары</li>
            </ul>
            <div class="resp-tabs-container">
                <div class="shopcat-template-block">%information%</div>
                <div class="shopcat-template-block">%parameters%</div>
                <div class="shopcat-template-block">%files%</div>
                <div class="shopcat-template-block">%othermodels%</div>
            </div>
        </div>
        ~;
    } else {
        $_cntnt .= qq~
            <div class="shopcat-template-block shopcat-n-lvl-tmplblock">%filteredtab%</div>
        ~;
    }
    $_cntnt .= qq~
    </div>
    <div class="shopcat-navi">
        <div class="shopcat-home-div"><span class="shopcat-textlink-span"><i class="fa fa-undo"></i></span></div>
        <div class="shopcat-navi-div">%navi_bottom%</div>
    </div>
    $DEFAULT_SLIDER_JS
    <script type="text/javascript">
        if (jQuery('#horizontalTab').length > 0) {
            jQuery('#horizontalTab').easyResponsiveTabs();
        }
    </script>
    ~;
    return $_cntnt;
}

#------------------------------------------------------------------------------
# isThisShopCatAction ( $action )
#------------------------------------------------------------------------------
#
# $action - значение action.
# Проверка того, что заданный action присутствует в БД модуля Shopcat::V3.
# Функция проверяет факт присутствия заданного action в БД модуля Shopcat::V3.
#
#------------------------------------------------------------------------------

sub isThisShopCatAction {
    my $rc = 0;
    my $action = $_[0];
    my @seo_data = getLinesFromFile($MODULE_SEO_DB_FILE);
    if (@seo_data) {
        foreach my $record (@seo_data) {
            my @tmp = split (/\|/,$record);
            if ($tmp[4] eq $action) {
                $rc = 1;
            }
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------
# getActionParams ( $action )
#------------------------------------------------------------------------------
#
# $action - значение action.
# Получение параметров заданного action из БД модуля Shopcat::V3.
# Функция получает параметры заданного action из БД модуля Shopcat::V3.
#
#------------------------------------------------------------------------------

sub getActionParams {
    my ($action) = @_;
    my @actionParams = ();
    my @seo_data = getLinesFromFile($MODULE_SEO_DB_FILE);
    if (@seo_data) {
        foreach my $record (@seo_data) {
            my @tmp = split( /\|/,$record );
            if ($tmp[4] eq $action) {
                if ($tmp[3] == -1) {
                    my $shopid = checkNN1Level($tmp[1], $tmp[2]);
                    push(@actionParams, $shopid, $tmp[2]);
                } else {
                    my $pricefile = getPriceFileFullPath($tmp[1]);
                    my $id = getItemIdInPriceFile($pricefile, $tmp[3]);
                    push(@actionParams, $tmp[1], $id);
                }
            }
        }
    }
    return @actionParams;
}

#------------------------------------------------------------------------------
# getV3PowerStatus ( )
#------------------------------------------------------------------------------
#
# Получить статус модуля (включен или выключен).
#
#------------------------------------------------------------------------------

sub getV3PowerStatus {
    my @db = getLinesFromFile('conf/system/install.tnk');
    if (@db) {
        foreach my $record (@db) {
            my @tmp = split( /\|/, $record );
            if ($tmp[2] eq $MODULE_NAME) {
                return $tmp[4];
            }
        }
    }
    return -1;
}

#------------------------------------------------------------------------------
# getV3UserareaStatus ( )
#------------------------------------------------------------------------------
#
# Получить статус пользовательской части (включена или выключена).
#
#------------------------------------------------------------------------------

sub getV3UserareaStatus {
    my $status = getSingleSetting( $MODULE_NAME, 0, 'userarea' );
    return $status =~ /^[01]?$/ ? $status : 0;

}

#------------------------------------------------------------------------------
# showV3HomePage ( )
#------------------------------------------------------------------------------
#
# Промежуточная функция вывода записей из CMS.
#
#------------------------------------------------------------------------------

sub showV3HomePage {
    my ($shopid, $catid, $pageaction, $showflag, $filter) = @_;
    my $t0    = [gettimeofday];
    my @templ = getPageTemplate();

    if (@templ) {
        for my $i (0..scalar(@templ)) {
            $templ[$i] = fillSeoTags($templ[$i], $shopid, $catid, $pageaction);

            if ($templ[$i] =~ /\%filters\%/) {
                my $filters = Shopcat::Templts::getFiltersBlock(getFiltersArray());
                $templ[$i] =~ s/\%filters\%/$filters/;
            }

            if ($templ[$i] =~ /\%content\%/) {
                my $content = getV3HomePage(@_);
                $templ[$i] =~ s/\%content\%/$content/;
            }

            if ($templ[$i] =~ /\%render\%/) {
                my $render = 'Время рендеринга страницы: <b>'.tv_interval( $t0, [gettimeofday]).'</b> сек.' ;
                $templ[$i] =~ s/\%render\%/$render/;
            }

            if ($templ[$i] =~ /\%shopcat_rnd_items\%/) {
                my $rnditems = q{};
                my $shopid_rnd_items =
                    getSingleSetting( $MODULE_NAME, 1, 'rnditemsshopid' );
                if (
                    $shopid_rnd_items &&
                    $shopid_rnd_items =~ /^[\d]+$/ &&
                    $shopid_rnd_items > 0
                ) {
                    $rnditems = getRandomItems($shopid_rnd_items, 2, $filter);
                }
                $templ[$i] =~ s/\%shopcat_rnd_items\%/$rnditems/g;
            }

            if ($templ[$i] =~ /\%shopcat_itemid\%/) {
                my $itemid = 0;
                if (checkCatIsItem($catid,$shopid) == 1) {
                    my $pf = getPriceFileFullPath($catid);
                    if (-e $pf) {
                        my @r = getRecIdInPriceFile($pf,$shopid);
                        if (@r) {
                            $itemid = $r[-1];
                        }
                    }
                }
                $templ[$i] =~ s/\%shopcat_itemid\%/$itemid/g;
            }

            $templ[$i] =
                Pheix::Pages::fillCommonTags($templ[$i], "shopcatalog",
                    $MODULE_NAME, $filter);

            print $templ[$i];
        }
    }
}

#------------------------------------------------------------------------------
# getV3HomePage ( )
#------------------------------------------------------------------------------
#
# Вывод контента стартовой страницы каталога.
#
#------------------------------------------------------------------------------

sub getV3HomePage {
    my ($shopid, $catid, $pageaction, $showflag, $filter) = @_;
    my $t0 = [gettimeofday];
    my $SH2_content = '';
    $SH2_content .= browseShopCat($shopid, $catid, $pageaction, $showflag, $filter);
    my $elapsed = tv_interval ( $t0, [gettimeofday]);
    $SH2_content .=
        $MODULE_DEBUG == 1 ?
        qq~<div class="shopcat-content-generated" align="center">content generated in: $elapsed sec</div>~ : '';
    return $SH2_content;
}

#------------------------------------------------------------------------------
# getDebugInfo ( )
#------------------------------------------------------------------------------
#
# Формирование отслодочного сообщения.
#
#------------------------------------------------------------------------------

sub getDebugInfo {
    my ($lid, $c_lev, $p_lev, $next_lev, $c_l_col,
        $ts, $d_l, $shid, ) = @_;
    my $cntnt_ = qq~
        level_id = $lid<br>
        current_level = $c_lev<br>
        prev_level_id = $p_lev<br>
        first_next_level_id = $next_lev<br>
        current_level_db_column = $c_l_col<br>
        template_block_status = $ts<br>
        default_level = $d_l<br>
        shopid = $shid
    ~;
    return $cntnt_;
}

#------------------------------------------------------------------------------
# browseShopCat ( )
#------------------------------------------------------------------------------
#
# Функция браузеринга по уровням каталога.
#
#------------------------------------------------------------------------------

sub browseShopCat {
    my ($shopid_, $catid_, $pageaction_, $showflag_, $filter_) = @_;
    my $cntnt_;
    my $debug_;
    my $level_id;
    my $shopid;
    my $showall_flag = $showflag_ || 0;
    my $pricefile = $MODULE_DB_FILE;

    if ($shopid_ =~ /^[\d]+$/) {
        $level_id = $shopid_;
    } else {
        $level_id = -1;
    }
    if ($catid_ =~ /^[\d]+$/ && $catid_ > 0) {
        $shopid = $catid_;
    } else {
        $shopid = -1;
    }

    if ($shopid > 0) {
        $pricefile = getPriceFileFullPath($shopid);
    }

    if (-e $pricefile) {
        my @maincat = getLinesFromFile($pricefile);
        my @level_of_maincat = ();
        for my $i (0..$#maincat)  {
            if ($maincat[$i] =~ /(\[$level_id\])/ ) {
                push(@level_of_maincat,$maincat[$i]);
            }
        }

        if (@level_of_maincat) {
            my $default_level = 1;
            my $current_level = -1;
            my $current_level_db_column = -1;
            my $first_next_level_id = -1;
            my $prev_level_id = -1;
            my $template_block;
            my $template_block_status;
            my @next_unique_levels;
            my $record = $level_of_maincat[0];
            $record =~ s/[\r\n]//g;
            my @record_levels_arr = split /\|/, $record;
            for my $i ( 0..$#record_levels_arr ) {
                my $dbcol_ = $record_levels_arr[$i];
                if ($dbcol_  =~ /(\[$level_id\])/) {
                    $current_level = $i;
                    $current_level_db_column = $i;
                    if ($record_levels_arr[$i-1] =~ /\[([\d]+)\]/) {
                        $prev_level_id = $1;
                    }
                    if ( defined($record_levels_arr[$i+1]) ) {
                        if ($record_levels_arr[$i+1] =~ /\[([\d]+)\]/) {
                            $first_next_level_id = $1;
                        }
                    }
                }
            }

            if ($current_level > 1) {
                $default_level = $current_level;
            }

            $debug_ = getDebugInfo(
                   $level_id,
                   $current_level,
                   $prev_level_id,
                   $first_next_level_id,
                   $current_level_db_column,
                   $template_block_status || 0,
                   $default_level,
                   $shopid,
            );

            for my $i ( 0..$#level_of_maincat ) {
                $record = $level_of_maincat[$i];
                $record =~ s/[\r\n]//g;
                my @db_row = split(/\|/, $record);
                if ( defined( $db_row[$current_level_db_column+1] ) ) {
                    my $test_column = $db_row[$current_level_db_column+1];
                    my $search_flag;
                    for my $j (0..$#next_unique_levels) {
                        if ($next_unique_levels[$j] eq $test_column) {
                            $search_flag++;
                        }
                    }
                    if (!$search_flag) {
                        push @next_unique_levels, $test_column;
                    }
                }
            }

            if (@next_unique_levels) {
                my @variants;
                my $list;
                for my $i (0..$#next_unique_levels) {
                    my $id;
                    if ($next_unique_levels[$i] =~ /\[([\d]+)\]/) {
                        $id = $1;
                    }
                    if ($next_unique_levels[$i] =~ /\[\*\]/) {
                        my $itemprice  = getItemCostByNLevID($shopid, $id);
                        $default_level = 'n-1';
                        $debug_ = getDebugInfo(
                           $level_id,
                           $current_level,
                           $prev_level_id,
                           $first_next_level_id,
                           $current_level_db_column,
                           $template_block_status || 0,
                           $default_level,
                           $shopid,
                        );
                        #if ( $itemprice > -1 ) {
                            push @variants, $next_unique_levels[$i];
                        #}
                        $next_unique_levels[$i] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                        $next_unique_levels[$i] =~ s/[\s]+/ /g;

                        # FIX ME: Обрезка названия
                        $next_unique_levels[$i] = adjustStringLen($next_unique_levels[$i]);

                        my @pricepoolRecIds;
                        my $linkurl;
                        if ($shopid > 0) {
                            @pricepoolRecIds = getRecIdInPriceFile($pricefile, $id);
                        }
                        if (@pricepoolRecIds) {
                            $linkurl = get4PUrl(@pricepoolRecIds);
                        }
                        if ($linkurl =~ /shopcatalog/) {
                            $linkurl = get4PUrl($id, $shopid);
                        }

                        if ( $itemprice > -1 ) {
                            $list .= getTemplateBlock(
                                $id,
                                $shopid,
                                $default_level,
                                $linkurl,
                                $next_unique_levels[$i],
                                $level_id
                            );
                        }
                    } else {
                        my $reftoprice = 0;
                        if ($next_unique_levels[$i] =~ /\[\**\]/) {
                            $reftoprice = getShopIdByCatId($id);
                        }
                        my $linkurl = get4PUrl($reftoprice, $id);
                        $next_unique_levels[$i] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                        $list .= getTemplateBlock($id, $shopid, $default_level, $linkurl, $next_unique_levels[$i], $level_id);
                    }
                }

                if (!@variants) {
                    $cntnt_ .= getDynLvlTmpl($shopid, $default_level, $list, $level_id);
                } else {
                    #$cntnt_ .= browseN1Lvl($shopid,$default_level,$list,$level_id,$showall_flag,@variants);
                    $cntnt_ .= browseN1Lvl($shopid,$default_level,$list,$level_id,$showall_flag);
                }
            } else {
                my $item_price = $record_levels_arr[2];
                my $js_module  = getSingleSetting($MODULE_NAME, 1, 'defaultnlevhidejs');

                $debug_ .= sprintf("artikul = %s", $record_levels_arr[0]);

                $cntnt_ .= $item_price > 0 ?
                    browseNLvl($shopid,$level_id,'n',$prev_level_id,$filter_,@record_levels_arr) :
                        (-e './'.$js_module ? sprintf("<script type=\"text/javascript\" src=\"%s\"></script>", $js_module) : q{});
            }
        } else {
            my $error_mess = qq~
                <div class="shopcat-fallback-mess-div"><b>$MODULE_NAME:</b>
                    Level $level_id not found: skip to top level</div>
            ~;
            if ($level_id == 0 || $MODULE_DEBUG != 1) {
                $error_mess = q{};
            }
            $cntnt_ .= $error_mess.getStaticTmpl(0);
        }
    } else {
        $cntnt_ = $MODULE_DEBUG == 1 ? "<div class=\"shopcat-fallback-mess-div\"><b>$MODULE_NAME:</b> in function browseShopCat() - pricefile {shopid=$shopid, $pricefile} not found!</div>" : '';
    }
    if ($MODULE_DEBUG != 1) {
        $debug_ = q{};
    }
    return $cntnt_.qq~<div class="shopcat-debug-info-div">$debug_</div>~;
}

#------------------------------------------------------------------------------
# getStaticTmpl ( )
#------------------------------------------------------------------------------
#
# Функция вывода дополнительного статического шаблона для конкретного уровня.
#
#------------------------------------------------------------------------------

sub getStaticTmpl {
    my ($lvl) = @_;
    my $template;
    if ($lvl !~ /^[\d]+$/) { $lvl = 0; }
    my $path = "$MODULE_DATAPOOL/levels/$lvl/template.html";
    if (-e $path) {
        my $_cntnt = getRootCatsList();
        if ($_cntnt =~ /default-block/) {
            my @_list = split( /\<\/li\>/, $_cntnt);
            for my $i (0..$#_list) {
                chomp($_list[$i]);
                $_list[$i] =~ s/(<a.{1,}<\/a>)//gi;
                if ($1 && $_list[$i]) {
                    $_list[$i] = $1;
                }
            }
            $_cntnt = join "<br>", @_list;
        }
        else {

        }
        $template = join(q{}, getLinesFromFile($path));
        if ($_cntnt) {
            $template =~ s/\%rows_n_columns\%/$_cntnt/;
        }
    } else {
        if ($FORCE_FALL_BACK != 1) {
            eval {
                $template = Shopcat::Templts::getCatTreeTable($MODULE_DB_FILE);
            };
        } else {
            my $root_cats = getRootCatsList();
            my $_hdr = $MODULE_DESCR;
            if ($FILTERS_MODE == 1) {
                my $f_inx = getFilterIndex();
                if ($f_inx > 1) {
                    $_hdr = q{};
                }
            }
            $ROOT_CATEGORY_LIST =~ s/\%root_categories_list\%/$root_cats/g;
            $ROOT_CATEGORY_LIST =~ s/\%header\%/$_hdr/g;
            $template = $MODULE_DEBUG == 1 ?
            qq~<div class="shopcat-fallback-mess-div">
                <b>$MODULE_NAME:</b> in function getStaticTmpl() -&nbsp;
                $path not found!<br>Use \$ROOT_CATEGORY_LIST:</div>
                ~ : '';
            $template .= $ROOT_CATEGORY_LIST;
        }
    }
    return $template;
}

#------------------------------------------------------------------------------
# getDynLvlTmpl ( )
#------------------------------------------------------------------------------
#
# Функция вывода дополнительного динамического шаблона для конкретного уровня.
#
#------------------------------------------------------------------------------

sub getDynLvlTmpl {
    my ($shopid, $lvl, $rows, $catid) = @_;
    my $template;
    if ($lvl !~ /^[\d]+$/ && $lvl ne 'n-1') { $lvl = 0; }
    if ($catid !~ /^[\d]+$/) { $catid = 0; }
    if ($rows eq '') { $catid = 0; }
    if ($lvl == 0 && $lvl ne 'n-1') {
        $template = getStaticTmpl(0);
    } else {
        my $path =
            $shopid == -1 ? "$MODULE_DATAPOOL/levels/$lvl/template\_$catid\.html" :
            "$MODULE_DATAPOOL/$shopid/levels/$lvl/template.html";
        if (-e $path ) {
            $template = join(q{}, getLinesFromFile($path));
        } else {
            if ($FORCE_FALL_BACK != 1) {
                eval {
                    $template = Shopcat::Templts::getDynoTemplate();
                };
            } else {
                $template = $MODULE_DEBUG == 1 ?  qq~
                    <div class="shopcat-fallback-mess-div">
                    <b>$MODULE_NAME:</b> in function getDynLvlTmpl() -
                        $path not found!<br>
                    Use \$DEFAULT_DYNAMIC_TEMPLATE:</div>
                ~ : '';
                $template .= $DEFAULT_DYNAMIC_TEMPLATE;
            }
        }
        my @header_n_navi = getNaviHeader($catid);
        $template =~ s/%rows_n_columns%/$rows/g;
        $template =~ s/%header%/$header_n_navi[0]/g;
        $template =~ s/%navi%/$header_n_navi[1]/g;
    }
    return $template;
}

#------------------------------------------------------------------------------
# getNaviHeader ( )
#------------------------------------------------------------------------------
#
# Функция вывода блока навигации для n уровня по id категории.
#
#------------------------------------------------------------------------------

sub getNaviHeader {
    my $catid  = $_[0] || -1;
    my $shopid = $_[1] || -1;
    my $filter = $_[2] || 0;
    my $DB_RECORD_DISP = 1;
    my $navi_block = -1;
    my @navi_links;
    my $header;
    my $dbfile = $MODULE_DB_FILE;
    if ($shopid > 0) {
        $dbfile = getPriceFileFullPath($shopid);
        $DB_RECORD_DISP = 3;
    }
    my @db = getLinesFromFile($dbfile);
    if (@db) {
        for my $i (0..$#db) {
            if ($db[$i]  =~ /(\[$catid\])/) {
                my $record = $db[$i];
                $record =~ s/[\r\n]//g;
                my @_col = split(/\|/,$record);
                for my $j ($DB_RECORD_DISP..$#_col) {
                    if ($_col[$j] =~ /\[([\d]+)\]/) {
                        my $_cid = $1;
                        if ($_cid == $catid) {
                            my $_val = getFilteredValue($shopid, $catid, $filter );
                            $header = $_val ne '' ? $_val : $_col[$j];
                            $header =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                            last;
                        } else {
                            my $_shopid = $_col[$j] !~ /\[[\*]+\]/ ? -1 : $shopid;

                            my $_val = getFilteredValue($_shopid, $_cid, $filter );
                            my $_crec = $_val ne '' ? $_val : $_col[$j];
                            $_crec =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                            my $linkurl;
                            if ($shopid > 0) {
                                if ($_col[$j] !~ /(\[[\*]\])/) {
                                    if ( (($_cid)) > 1) {
                                        $linkurl = get4PUrl($shopid, $_cid);
                                        push @navi_links,
                                            "<a href=\"$linkurl\" class=\"shopcat-textlink-navi\">$_crec</a>";
                                    }
                                } else {
                                    $linkurl = get4PUrl($shopid, $_cid);
                                    push @navi_links,
                                        "<a href=\"$linkurl\" class=\"shopcat-textlink-navi\">$_crec</a>";
                                }
                            } else {
                                if ( (checkUniqueCat($_cid)) > 1) {
                                    $linkurl = get4PUrl(0, $_cid);
                                    push @navi_links,
                                        "<a href=\"$linkurl\" class=\"shopcat-textlink-navi\">$_crec</a>";
                                }
                            }
                        }
                    }
                }
                last;
            }
        }
    } else {
        $navi_block = $MODULE_DEBUG == 1 ?
        "<div class=\"shopcat-fallback-mess-div\"><b>$MODULE_NAME:</b> getNaviHeader() - $dbfile not found!</div>"
        : '';
    }
    if (@navi_links) {
        $navi_block = join(" » ",@navi_links);
    } else {
        $navi_block = '&nbsp;'
    }
    return ($header,$navi_block);
}

#------------------------------------------------------------------------------
# getDescr ( )
#------------------------------------------------------------------------------
#
# Функция вывода данных из description.txt для конкретных уровня и категории.
# Прежнее наименование: _inner_shopcat2_browse_get_description
#
#------------------------------------------------------------------------------

sub getDescr {
    my $description;
    my $level_num = $_[0] || -1;
    my $cat_id = $_[1] || -1;
    my $filename = $MODULE_DATAPOOL."/levels/".$level_num."/".$cat_id.".description.txt";
    my $debug_info = "\n<!--Description file: $filename-->\n";
    if (-e $filename) {
        open my $fh,"<", $filename;
        $description = join q{}, <$fh>;
        $description =~ s/[\r\n]//g;
        close $fh;
    } else {
        $description = "Description file not found: $filename"
    }
    return $debug_info.$description;
}

#------------------------------------------------------------------------------
# getTabData()
#------------------------------------------------------------------------------
#
# Функция вывода данных на закладку уровня n.
#
#------------------------------------------------------------------------------

sub getTabData {
    my ($shopid, $_id, $_lvl, $datatype, $filter) = @_;
    my $tabdata;
    my $fname;
    if ($datatype) {
        $fname = "$MODULE_DATAPOOL/$shopid/levels/$_lvl/$_id/"
            .$datatype.".txt";
    } else {
        my $f_inx = getFilterIndex( $filter );
        my $f_tag = 'filter-'.(sprintf "%02d", $f_inx);
        my @keys = ( 'information',
                     'primenenie',
                     'files',
                     'video',
                     'analogs');
        for my $k (0..$#keys) {
            my $_k = $keys[$k];
            my $_f = getSingleSetting( $MODULE_NAME, 0, 'filterblocks/'.$_k);
            if ($_f eq $f_tag) {
                my $_kk = $_k;
                if ($_kk eq 'primenenie' && $_lvl eq 'n') {
                    $_kk = 'parameters';
                }
                $fname = "$MODULE_DATAPOOL/$shopid/levels/$_lvl/$_id/"
                    .$_kk.".txt";
            }
        }
        if ( !$fname || !(-e $fname)) {
            $fname = "$MODULE_DATAPOOL/$shopid/levels/$_lvl/$_id/"
                .$keys[0].".txt";
        }
    }
    my $debug_info = "\n<!-- tab file: $fname -->\n";
    if (-e $fname) {
        open my $fh, "<", $fname or die "unable to open $fname: ".$!;
        my $_data = join "", <$fh>;
        $_data = ParseV3Vars($_data);
        eval {
            $tabdata = Feedbck::Casual::parseFeedBackVars($_data);
        };
        if ($@) {
            $tabdata = $_data;
        }
        close $fh or die "unable to close $fname: ".$!;
    } else {
        $tabdata = "Tab file not found: $fname";
    }
    return $debug_info.$tabdata;
}

#------------------------------------------------------------------------------
# fillSlider ( )
#------------------------------------------------------------------------------
#
# Функция вывода html кода слайдера.
#
#------------------------------------------------------------------------------

sub fillSlider {
    my ($imgpth, @imgchck) = @_;
    my $_html;
    my $sliderdata;
    for my $j (0..$#imgchck) {
        if (-e "$imgpth/$imgchck[$j]") {
            $_html .= qq~
                <div class="slick-slider shopcat-slick-img-fixed" data-slick-index="$j" aria-hidden="true" tabindex="-1">
                   <a href="$imgpth/$imgchck[$j]" data-fancybox="gallery"><img src="images/admin/cms/spacer.gif"
                     width="150" height="150" alt=""
                     class="shopcat-variants-img"
                     style="background: url('$imgpth/$imgchck[$j]') no-repeat; background-size: cover;"></a>
                 </div>~;
        }
    }
    if ($_html ne '') {
        $sliderdata = qq~
            <div>
                <div class="slick-slider-pheix-embedded">
                    $_html
                </div>
            </div>
        ~;
    } else {
        $sliderdata = "Slider data is empty: check images in $imgpth";
    }
    return $sliderdata;
}

#------------------------------------------------------------------------------
# getSliderLvlN ( )
#------------------------------------------------------------------------------
#
# Функция вывода блока со слайдером для уровня n.
#
#------------------------------------------------------------------------------

sub getSliderLvl {
    my $sliderdata;
    my $_grpid;
    my @images;
    my $imgpth;
    my $lvlnam = 'n';
    my $shopid = $_[0] || -1;
    my $id     = $_[1] || -1;

    if (checkCatIsItem($shopid, $id) == 0) {
        $lvlnam = 'n-1';
        $imgpth = "images/shopcat/$shopid/levels/$lvlnam/$id";
    } else {
        my ($_catid, $_itemid) =
            getRecIdInPriceFile( getPriceFileFullPath($shopid), $id );
        $imgpth = "images/shopcat/$shopid/levels/$lvlnam/$_itemid";
    }
    eval {
        my $_sysgal = Gallery::System->new;
        $_grpid = $_sysgal->getSysGalGrpIdByPath($imgpth);
    };
    if (!$_grpid) {
        @images = getV3FilesArr($imgpth);
        @images = reverse(sort {$a cmp $b} @images);
    }
    if (@images) {
        my @imgchck;
        for my $j (0..$#images) {
                if ($images[$j] =~ /(\.jpg)|(\.png)|(\.gif)$/) {
                    push(@imgchck,$images[$j]);
                }
        }

        if (@imgchck) {
            my $sldrfname = "$MODULE_DATAPOOL/levels/$lvlnam/slider.html";
            if (-e $sldrfname) {
                open my $fh, "<", $sldrfname;
                $sliderdata = join "", <$fh>;
                close $fh;
            }
            $sliderdata = fillSlider($imgpth, @imgchck);
        } else {
            $sliderdata = "No JPG/PNG/GIF images in: $imgpth";
        }
    } else {
        $sliderdata = "No images in: $imgpth";
        if ($_grpid =~ /^[\d]+$/ && $_grpid > 0) {
            eval {
                my $_sysgal = Gallery::System->new;
                $sliderdata = $_sysgal->getGroupIndex($_grpid, $imgpth, $shopid ) || "No images in: $imgpth";
            };
        }
    }
    return $sliderdata;
}

#------------------------------------------------------------------------------
# browseN1Lvl ( )
#------------------------------------------------------------------------------
#
# Функция вывода данных для страницы n-1.
#
#------------------------------------------------------------------------------

sub browseN1Lvl {
    my $lvl_cnt;
    my $shopid         = $_[0] || -1;
    my $level_num      = $_[1] || -1;
    my $rows_n_columns = $_[2] || q{};
    my $catid          = $_[3];
    my $showall_flag   = $_[4];
    # my @variants       = @_[5..$#_];
    my $fname = "$MODULE_DATAPOOL/$shopid/levels/n-1/template.html";
    my $_dflt = "$MODULE_DATAPOOL/levels/n-1/template.html";
    if (-e $fname) {
        open my $fh, "<", $fname or die "unable to open $fname: ".$!;
        $lvl_cnt = join "", <$fh>;
        close $fh or die "unable to close $fname: ".$!;
    } elsif (-e $_dflt) {
        open my $fh, "<", $_dflt or die "unable to open $_dflt: ".$!;
        $lvl_cnt = join "", <$fh>;
        close $fh or die "unable to close $_dflt: ".$!;
    }else {
        $lvl_cnt = $MODULE_DEBUG == 1 ?
        qq~ <div class="shopcat-fallback-mess-div"><b>$MODULE_NAME:</b>
            in function browseN1Lvl() - $fname or $_dflt not found!
            Use \$DEFAULT_TEMPLATE_N_MINUS_1:</div>
        ~ : '';
        $lvl_cnt .= getN1LvlTempl();
        if ($FORCE_FALL_BACK != 1) {
            eval {
                $lvl_cnt = Shopcat::Templts::getNM1Template();
            };
        }
    }
    my @header_n_navi = getNaviHeader($catid);
    my @navi_arr = split(/ » /,$header_n_navi[1]);
    my $recid = getArtikulByCatId($catid);
    my $lastlink = ( $navi_arr[-1] ne '&nbsp;' ) ? $navi_arr[-1] : $BACK_LINK;
    $lvl_cnt =~ s/%navi_bottom%/$lastlink/g;

    if ($FORCE_FALL_BACK == 1) {
        $lvl_cnt =~ s/%rows_n_columns%/$rows_n_columns/g;
    }

    $lvl_cnt =~ s/%header%/$header_n_navi[0]/g;
    $lvl_cnt =~ s/%navi%/$header_n_navi[1]/g;
    my $sldr = getSliderLvl($shopid, $catid);

    if ($sldr =~ /No images/) {
        $sldr = '<div class="shopcat-fa-img"><i class="fa fa-image"></i></div>';
    }
    $lvl_cnt =~ s/%slider%/$sldr/g;
    if ($FILTERS_MODE != 1) {
        my $variants_block = getVarntsPage($shopid, 0, $showall_flag);
        my $information_block = getTabData($shopid, $catid, 'n-1', 'information');
        my $primenenie_block = getTabData($shopid, $catid, 'n-1', 'primenenie');
        my $video_block = getTabData($shopid, $catid, 'n-1', 'video');
        my $files_block = getTabData($shopid, $catid, 'n-1', 'files');
        $lvl_cnt =~ s/%information%/$information_block/g;
        $lvl_cnt =~ s/%primenenie%/$primenenie_block/g;
        $lvl_cnt =~ s/%video%/$video_block/g;
        $lvl_cnt =~ s/%variants%/$variants_block/g;
        $lvl_cnt =~ s/%files%/$files_block/g;
    } else {
        my $filteredtab = getTabData($shopid, $catid, 'n-1');
        $lvl_cnt =~ s/%filteredtab%/$filteredtab/g;
    }
    return $lvl_cnt;
}

#------------------------------------------------------------------------------
# browseNLvl ( )
#------------------------------------------------------------------------------
#
# Функция вывода данных для страницы n.
#
#------------------------------------------------------------------------------

sub browseNLvl {
    my $fallbackflag = 0;
    my $lvl_cnt;
    my $shop_id       = $_[0] || -1;
    my $level_id      = $_[1] || -1;
    my $level_num     = $_[2] || -1;
    my $prev_level_id = $_[3] || -1;
    my $filter        = $_[4] || 0;
    my $item_id       = $_[5] || -1;
    my $item_price    = $_[7] || 0;
    my $item_name     = $_[$#_] || 'undefined';
    $item_name =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
    $item_name =~ s/[\s]+/ /g;
    if ($item_price == 0) {
        $item_price = "под заказ";
    } else {
        $item_price = getCorrPrice($item_price);
        $item_price = "$item_price $GLOBAL_CURRENCY";
    }

    my $fname = "$MODULE_DATAPOOL/$shop_id/levels/n/template.html";
    my $_dflt = "$MODULE_DATAPOOL/levels/n/template.html";
    if (-e $fname) {
        open my $fh, "<", $fname or die "unable to open $fname: ".$!;
        $lvl_cnt = join "", <$fh>;
        close $fh or die "unable to close $fname: ".$!;
    } elsif (-e $_dflt) {
        open my $fh, "<", $_dflt or die "unable to open $_dflt: ".$!;
        $lvl_cnt = join "", <$fh>;
        close $fh or die "unable to close $_dflt: ".$!;
    } else {
        $lvl_cnt = $MODULE_DEBUG == 1 ? qq~
            <div class="shopcat-fallback-mess-div">
            <b>$MODULE_NAME:</b> in function browseNLvl() - $fname or $_dflt not found!<br>
            Use \$DEFAULT_TEMPLATE_N:</div>
        ~ : '';
        $lvl_cnt .= getNLvlTempl();
        if ($FORCE_FALL_BACK != 1) {
            eval {
                $lvl_cnt = Shopcat::Templts::getNTemplate($shop_id, $item_id, $level_id);
            };
        }
        $fallbackflag = 1;
    }

    my $navi_block = getNaviHeader( $level_id, $shop_id, $filter );
    my @navi_arr = split(/ » /,$navi_block);
    my $lastlink = ( $navi_arr[-1] ne '&nbsp;' ) ? $navi_arr[-1] : $BACK_LINK;
    $lvl_cnt =~ s/%artikul%/$item_id/g;
    $lvl_cnt =~ s/%navi%/$navi_block/g;
    $lvl_cnt =~ s/%navi_bottom%/$lastlink/g;

    my $_hdr = getFilteredValue( $shop_id,  $level_id, $filter );
    $_hdr =  $_hdr ne '' ? $_hdr : $item_name;

    $lvl_cnt =~ s/%header%/$_hdr/g;
    $lvl_cnt =~ s/%price%/$item_price/g;

    my $byuitnow;
    eval {
        $byuitnow = Lightshop::Sales::getBuyItLink($shop_id,$item_id,$item_price,$item_name);
    };
    if ( (!$byuitnow) ) {
        $byuitnow = 'недоступно';
    } else {
        if ($byuitnow =~ /href="(.{1,})"/) {
            if ($FORCE_FALL_BACK != 1) {
                $byuitnow = $1;
            }
        } else {
            $byuitnow = "#";
        }
    }

    $lvl_cnt =~ s/%byuitnow%/$byuitnow/g;

    my $sldr = getSliderLvl($shop_id, $level_id);
    if ($sldr =~ /No images/) {
        $sldr = '<div class="shopcat-fa-img"><i class="fa fa-image"></i></div>';
    }
    $lvl_cnt =~ s/%slider%/$sldr/g;

    if ($FILTERS_MODE != 1) {
        my $variants_block = getVarntsPage($shop_id, $item_id, 0);
        my $information_block = getTabData($shop_id, $item_id, 'n', 'information');
        my $parameters_block = getTabData($shop_id, $item_id, 'n', 'parameters');
        my $analogs_block = getTabData($shop_id, $item_id, 'n', 'analogs');
        my $files_block = getTabData($shop_id, $item_id, 'n', 'files');
        $lvl_cnt =~ s/%information%/$information_block/g;
        $lvl_cnt =~ s/%parameters%/$parameters_block/g;
        $lvl_cnt =~ s/%analogs%/$analogs_block/g;
        $lvl_cnt =~ s/%files%/$files_block/g;
        $lvl_cnt =~ s/%othermodels%/$variants_block/g;
    } else {
        my $filteredtab = getTabData($shop_id, $item_id, 'n', '', $filter);
        $lvl_cnt =~ s/%filteredtab%/$filteredtab/g;
    }
    return $lvl_cnt;
}

#------------------------------------------------------------------------------
# getDescrNM1 ( )
#------------------------------------------------------------------------------
#
# Функция получения описания descriptions.txt для уровня n-1.
#
#------------------------------------------------------------------------------

sub getDescrNM1 {
    my $description = '';
    my $shopid = $_[0] || -1;
    my $catid  = $_[1] || -1;
    my $itemid = $_[2] || -1;
    my $filename = "$MODULE_DATAPOOL/$catid/levels/n-1/$catid/descriptions.txt";
    my $debug_info = "\n<!--Descriptions from file: $filename-->\n";
    if (-e $filename) {
        open my $fh, "<", $filename;
        my @_db = <$fh>;
        close $fh;
        my $_fl;
        for my $i (0..$#_db) {
            my $_rec = $_db[$i];
            my @_col = split(/[\;\|\t]/,$_rec);
            $_col[0] =~ s/[\D]//g;
            if ($_col[0] == $itemid) {
                $description = $_col[1];
                $description =~ s/[\r\n]+//g;
                $_fl = 1;
            }
        }
        if (!$_fl) {
            $description = "No description record in $filename, record should be in single string:<br>$itemid|description for $itemid";
        }
    } else {
        $description = "Descriptions file not found: $filename";
    }
    return $debug_info.$description;
}

#------------------------------------------------------------------------------
# getRootCatsList ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Вывод списка корневых категорий каталога.
# Функция возвращает список корневых категорий каталога в формате HTML.
#
#------------------------------------------------------------------------------

sub getRootCatsList {
    my @root_cats_ids;
    my @root_cats_names;
    my $retcontent;
    my $ROOT_CATEGORY_INDEX = 1;
    my $db_fname            = $MODULE_DB_FILE;
    if ( -e $db_fname ) {
        open my $fh, "<", $db_fname or die "unable to open $db_fname: ".$!;
        my @maincat = <$fh>;
        close $fh or die "unable to close $db_fname: ".$!;
        for my $i (0..$#maincat) {
            my @arr    = split( /\|/, $maincat[$i] );
            my $shopid = ( $arr[0] =~ /^[\d]+$/ ) ? $arr[0] : -1;
            if ( $arr[$ROOT_CATEGORY_INDEX] =~ /\[([\d]+)\]/ ) {
                my $id      = $1;
                my $catname = $arr[$ROOT_CATEGORY_INDEX];
                $catname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                my $foundflag = 0;
                for ( my $j = 0 ; $j <= $#root_cats_ids ; $j++ ) {
                    if ( $root_cats_ids[$j] eq $id ) { $foundflag = 1; }
                }
                if ( $foundflag == 0 ) {
                    my $linkurl;
                    my $skiplevels = getSingleSetting( $MODULE_NAME, 1, 'skiplevels' );
                    if ( (checkUniqueCat($id)) > 1 || !$skiplevels) {
                        $linkurl = get4PUrl( 0, $id );
                    }
                    else {
                        if ( (countPriceFileRecords($shopid)) > 1 ) {
                            $linkurl = get4PUrl( $shopid, getCatIdByShopId($shopid) );
                        }
                        else {
                            my $itemid  = getFirstItemIdFromPriceFile($shopid);
                            my $itemtag =
                                getItemIdInPriceFile(getPriceFileFullPath($shopid), $itemid);
                            $linkurl = get4PUrl( $shopid, $itemtag );
                        }
                    }
                    push( @root_cats_ids, $id );
                    push @root_cats_names,
                        getTemplateBlock($id, -1, 0, $linkurl, $catname);
                }
            }
        }
        if ( $#root_cats_names > -1 && $#root_cats_ids > -1 ) {
            $retcontent = join( "", @root_cats_names );
        }
        else {
            $retcontent = qq~
            Can not find any root categories - $db_fname is empty or corrupted!
            ~;
        }
    }
    else {
        $retcontent = qq~
        Can not find root categories - $db_fname not found!
        ~;
    }
    return $retcontent;
}

#------------------------------------------------------------------------------
# getShopIdByCatId ( $catid )
#------------------------------------------------------------------------------
#
# $catid - идентификатор категории уровня N-1.
# Получить идентификатор прайс-листа по идентификатору категории уровня N-1.
# Функция возвращает идентификатор прайс-листа.
#
#------------------------------------------------------------------------------

sub getShopIdByCatId {
    my $cat_id   = $_[0] || -1;
    my $shopid   = -1;
    my @maincat = getModuleDb();
    if ( @maincat ) {
        for ( my $i = 0 ; $i <= $#maincat ; $i++ ) {
            if ( $maincat[$i] =~ /\[\*\*\]\[($cat_id)\]/ ) {
                my @db_columns = split( /\|/, $maincat[$i] );
                $shopid = $db_columns[0];
            }
        }
    }
    return $shopid;
}

#------------------------------------------------------------------------------
# getCatIdByShopId ( $shopid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# Получить идентификатор категории уровня N-1 по идентификатору прайс-листа.
# Функция возвращает идентификатор категории уровня N-1.
#
#------------------------------------------------------------------------------

sub getCatIdByShopId {
    my $shopid   = $_[0] || -1;
    my $cat_id   = 0;
    my @maincat = getModuleDb();
    if ( @maincat ) {
        for ( my $i = 0 ; $i <= $#maincat ; $i++ ) {
            my @record = split( /\|/, $maincat[$i] );
            if ( $record[0] == $shopid ) {
                if ($record[$#record-1] =~ /\[\*\*\]\[([\d]+)\]/) {
                    $cat_id = $1;
                }
            }
        }
    }
    return $cat_id;
}

#------------------------------------------------------------------------------
# getTitleImgFromDir ( $dir )
#------------------------------------------------------------------------------
#
# $dir - каталог в файловой системе.
# Получить имя файла индексной (*_01.*) картинки в заданном каталоге.
# Функция возвращает имя файла индексной картинки.
#
#------------------------------------------------------------------------------

sub getTitleImgFromDir {
    my @files = ();
    my $titleimage = 'undefined';
    my $dir        = $_[0];
    if (-e $dir) {
        @files = sort {$a cmp $b} getV3FilesArr($dir);
        if ($files[0]) {
            $titleimage = $files[0];
        }
    }
    return $titleimage;
}

#------------------------------------------------------------------------------
# showLightNaviTree ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Вывод навигационного меню, состоящего из root-категорий, соответсвующих
# заданным ID, а также входящийх в эти root-категории подкатегории.
# Функция возвращает навигационное меню в HTML формате.
#
#------------------------------------------------------------------------------

sub showLightNaviTree {
    my ($filter) = @_;
    my $leftnav;
    my $itemcontent;
    my $itemname;
    my $retcontent;
    my $shopid;
    my $db_fname  = $MODULE_DB_FILE;
    my $_savetofl = getSettingFromGroupByAttr(
        $MODULE_NAME, 1, 'lightnavitree', 'action', 'lightnavitree', 'lightnavitreetofile');
    if ($_savetofl != 0 && $_savetofl != 1) { $_savetofl = 0; }
    # Ускоряем работу - считываем HTML код меню из файла
    if (-e $MODULE_LEFT_NAV && $_savetofl == 1) {
        open my $fh, "<", $MODULE_LEFT_NAV or
            die "unable to open $MODULE_LEFT_NAV: ".$!;
        my $leftnav = join "", <$fh>;
        close $fh or die "unable to close $MODULE_LEFT_NAV: ".$!;
        chomp($leftnav);
        if ($leftnav) {
            return $leftnav;
        }
    } else {
        if (-e $MODULE_LEFT_NAV) {
            if ($_savetofl == 0) {
                unlink $MODULE_LEFT_NAV;
            }
        }
    }
    if (-e $db_fname) {
        my $incsubcats = getSettingFromGroupByAttr(
           $MODULE_NAME, 1, 'lightnavitree', 'action', 'lightnavitree', 'lightnavitreesubcats');
        if ($incsubcats != 0 && $incsubcats != 1) { $incsubcats = 0; }
        my @showIds = split /[\,\s]+/, getSettingFromGroupByAttr(
           $MODULE_NAME, 1, 'lightnavitree', 'action', 'lightnavitree', 'lightnavitreelist');
        if ($showIds[0] eq '*' && $#showIds == 0) { @showIds = (); }
        open my $fh, "<", $db_fname or die "unable to open $db_fname: ".$!;
        my @data = <$fh>;
        close $fh or die "unable to close $db_fname: ".$!;
        my $proc_root_id  = 0;
        my $proc_child_id = 0;
        my $target_id     = 0;
        my $curr_child_id = 0;
        my $curr_root_id  = -1;
        for ( my $i = 0 ; $i <= $#data ; $i++ ) {
            $data[$i] =~ s/[\r\n]+//g;
            my @record_arr = split( /\|/, $data[$i] );
            my $link_postfix = "_$record_arr[0]";
            if ($record_arr[3] !~ /\[\*\]/) {
                $link_postfix = '';
            }
            if ( $record_arr[1] =~ /\[([\d]+)\]/ ) { $curr_root_id = $1; }
            if ( ( $curr_root_id != $proc_root_id )) {
                if ( ($itemcontent ne '' || !$incsubcats) && $itemname ne '') {
                    my $linkurl = get4PUrl(0, $proc_root_id);
                    $retcontent .=
                      Shopcat::Templts::getLightNaviItem( $proc_root_id,
                       $itemname, $itemcontent, $linkurl);
                    $itemcontent = '';
                    $itemname    = '';
                }
                $target_id = 0;
                if (@showIds) {
                    for ( my $j = 0 ; $j <= $#showIds ; $j++ ) {
                        if ( $curr_root_id == $showIds[$j] ) {
                            $target_id = 1;
                            $record_arr[2] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                            my $id = $1;
                            $id =~ s/[\D]+//g;
                            $curr_child_id = $id;
                            if ($incsubcats) {
                                $itemcontent .= qq~
                                 <a href="shopcatalog\_$id$link_postfix.html">
                                 $record_arr[2]</a><br>\n
                                ~;
                            }
                        }
                    }
                } else {
                    $target_id = 1;
                    $record_arr[2] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                    my $id = $1;
                    $id =~ s/[\D]+//g;
                    $curr_child_id = $id;
                    if ($incsubcats) {
                        $itemcontent .= qq~
                          <a href="shopcatalog\_$id$link_postfix.html">
                          $record_arr[2]</a><br>\n
                        ~;
                    }
                }
                if ( $target_id == 1 ) {
                    $record_arr[1] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                    $itemname = $record_arr[1];
                    my $f_inx = getFilterIndex($filter);
                    if ( $f_inx > 1 && $f_inx < 13) {
                        my @f_vals = getFilterData(
                               -1, $curr_root_id, $f_inx );
                        if ( $f_vals[0] ) {
                            $itemname = $f_vals[0];
                        }
                    }
                }
                $proc_root_id = $curr_root_id;
                $shopid = $record_arr[0];
            }
            else {
                if ( $target_id == 1) {
                    $record_arr[2] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                    my $id = $1;
                    $id =~ s/[\D]+//g;
                    $proc_child_id = $id;
                    if ($proc_child_id != $curr_child_id) {
                        $curr_child_id = $proc_child_id;
                        if ($incsubcats) {
                            $itemcontent .= qq~
                             <a href="shopcatalog\_$id$link_postfix.html">
                             $record_arr[2]</a><br>\n
                            ~;
                        }
                    }
                }
            }
        }
        if ( ($itemcontent ne '' || !$incsubcats) && $itemname ne '') {
            my $linkurl = get4PUrl(0, $proc_root_id);
            $retcontent .=
             Shopcat::Templts::getLightNaviItem( $proc_root_id,
             $itemname, $itemcontent, $linkurl);
        }
        # Ускоряем работу - сохраняем HTML код меню из файла
        # В дальнейшем меню код меню берется из этого файла
        # Файл удаляется при загрузке нового каталога
        if ($retcontent && $_savetofl == 1) {
            open my $fh, ">", $MODULE_LEFT_NAV;
            print $fh $retcontent;
            close $fh;
        }
    }
    else {
        $retcontent = qq~
         Построение списка категорий в режиме
         встроенных шаблонов (fallback==$FORCE_FALL_BACK) невозможно!
        ~;
    }
    return $retcontent;
}

#------------------------------------------------------------------------------
# doCatsLoad ( $shopid, $catid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# Вывод таблицы категорий уровня == N-1.
# Функция возвращает таблицу категорий в HTML формате.
#
#------------------------------------------------------------------------------

sub doCatsLoad {
    my $shopid         = $_[0] || -1;
    my $catid          = $_[1] || -1;
    my $nm1_cats_table = '';
    my $trtdcontent    = '';
    my $DISP_IN_RECORD = 1;
    my $filepath       = $MODULE_DB_FILE;
    my $count           = 0;
    my $FOUND_CAT_INDEX = -1;
    my @data = getModuleDb();
    if ( @data ) {
        my @curr_ids_stamp = ();
        for ( my $i = 0 ; $i <= $#data ; $i++ ) {
            my $record = $data[$i];

            # строим дерево категорий

            my @arr = split( /\|/, $record );
            for ( my $j = $DISP_IN_RECORD ; $j <= $#arr - 1 ; $j++ ) {
                if ( $arr[$j] =~ /\[($catid)\]/ && $arr[0] =~ $shopid ) {
                    $FOUND_CAT_INDEX = $j;
                }
            }
            if ( $FOUND_CAT_INDEX > -1 ) {
                if (   $arr[$FOUND_CAT_INDEX] =~ /\[($catid)\]/
                    && $arr[ $FOUND_CAT_INDEX + 1 ] =~ /\[\*\*\]/ )
                {
                    my $catname = $arr[ $FOUND_CAT_INDEX + 1 ];
                    $catname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                    my $catid2 = $1;
                    $catid2 =~ s/[\[\]]+//g;
                    my $linkurl = get4PUrl($shopid, $catid2);
                    if ($linkurl =~ /shopcatalog/) {
                        $linkurl = "shopcatalog\_$catid2\_$arr[0].html";
                    }
                    $trtdcontent .= qq~
                        <div class="shopcatsubcatitem">
                        <a  href="$linkurl">
                            $catname</a>
                        </div>~;
                }
            }

            # строим дерево категорий

        }
    }
    else {
        $nm1_cats_table = qq~
        <p class=error>Ошибка: $filepath не найден!</p>\n~;
    }
    if ( $trtdcontent ne '' ) {
        $nm1_cats_table = $trtdcontent;
    }
    return $nm1_cats_table;
}

#------------------------------------------------------------------------------
# adjustStringLen ( $adjuststr )
#------------------------------------------------------------------------------
#
# $adjuststr - строка для обрезки.
# Обрезка строки (названия позиции).
# Функция возвращает обрезанную строку.
#
#------------------------------------------------------------------------------

sub adjustStringLen {
    my $adjuststr = $_[0] || 'undefined';

    if ($PROCESS_SORT_TITLES) {
        my $data1 = '';
        my $data2 = '';

        # Убираем {...}
        if ( $adjuststr =~ /(\{)/ ) {
            $adjuststr =~ s/(\{.{1,}\})|(\{.{1,})//;
        }

        if ( $adjuststr =~ /(\(.{1,}\))/ ) {
            $data1 = $1;
        }

        if ( $adjuststr =~ /(\[.{1,}\])/ ) {
            $data2 = $1;
        }

        # Обрезка по /
        my @obrezka = split( /\//, $adjuststr );
        $adjuststr = $obrezka[0];

        # Обрезка по пробелам (первые 6 слов)
        @obrezka = split( / /, $adjuststr );
        if (@obrezka) {
            if ( $#obrezka >= 5 ) {
                $adjuststr = join( " ", @obrezka[ 0 .. 5 ] );
            }
            else {
                $adjuststr = join( " ", @obrezka[ 0 .. $#obrezka ] );
            }
        }

        # Присоединяем данные из (...) или [...]
        if ( $data1 ne '' ) {
            $adjuststr =~ s/(\(.{1,}\))//ig;
            $adjuststr .= " " . $data1;
        }
        if ( $data2 ne '' ) {
            $adjuststr =~ s/(\[.{1,}\])//ig;
            $adjuststr .= " " . $data2;
        }
    }
    return $adjuststr;
}

#------------------------------------------------------------------------------
# getCorrPrice ( $native_price )
#------------------------------------------------------------------------------
#
# $native_price - значение цены.
# Рассчет наценки для заданной цены.
# Функция возвращает цену с рассчитанной наценкой.
#
#------------------------------------------------------------------------------

sub getCorrPrice {
    my ($native_price) = @_;
    my $trademargin    = getSingleSetting( $MODULE_NAME, 1, 'trademargin' );
    if ( $trademargin != 1 ) {
        return sprintf("%.2f",$native_price);
    }
    my @intervals   = ('0.001-10','10-30','30-50','50-100','100-500','500-1500','1500-4000','4000-10000','10000-9999999');
    my @nacenka     = (50,45,35,20,15,12,10,8,7);
    my $corrprice   = 0;
    for (my $i = 0; $i <= $#intervals; $i++) {
        my @interval_arr = split(/\-/,$intervals[$i]);
        if ($native_price > $interval_arr[0] && $native_price <= $interval_arr[1] ) {
            #$corrprice = "$interval_arr[0]\-$interval_arr[1]: $nacenka[$i]% - ".($native_price + ($native_price/100)*$nacenka[$i]);
            $corrprice = ($native_price + ($native_price/100)*$nacenka[$i]);
        }
    }
    if ($corrprice > 0) {$native_price = $corrprice;}

    if (-e $GLOBAL_CURRENCY_CONVERT_PATH) {
        open(CURRCONVFH, "<$GLOBAL_CURRENCY_CONVERT_PATH");
        my @convert = <CURRCONVFH>;
        close(CURRCONVFH);
        my $usd = $convert[0];
        $usd =~ s/[a-zA-Z\=\r\n]//g;
        $usd =~ s/\,/\./g;
        if ($usd > 1) {
            $native_price = $native_price * $usd;
        }
    } else {
        # $GLOBAL_CURRENCY = '$';
    }

    return sprintf("%.2f",$native_price);
}

#------------------------------------------------------------------------------
# getSearchResults ( $query )
#------------------------------------------------------------------------------
#
# $query - ключевая фраза для поиска.
# Поиск ключевой фразы в базе данных каталога.
#
#------------------------------------------------------------------------------

sub getSearchResults {
    my $query = Encode::decode("utf8", $_[0]);
    my $_qr = quotemeta($query);
    my $search_header;
    $search_header .= qq~<p class="pageheader">«$query»:</p>~;
    my $f_inx = getFilterIndex();
    my $search_content = doDbSearch($_qr, $f_inx, 1);

    if (!$search_content) {
        open my $fh, "<", $MODULE_PARAMETRIZED;
        my @_pdb = grep { $_ =~ /$_qr/gi } <$fh>;
        close $fh;
        if ( @_pdb ) {
            foreach my $_rec ( @_pdb ) {
                chomp( $_rec );
                my @_col = split( /\|/, $_rec );
                my $_shopid = $_col[0];
                my $_catid  = $_col[1];
                my $_itemid = $_col[2];
                if ($f_inx > 1) {
                    my $_id = $_itemid eq '-1' ? $_catid : getItemIdInPriceFile( getPriceFileFullPath($_shopid), $_itemid );
                    my @f_vals = getFilterData( $_shopid, $_id, $f_inx );
                    if ( defined $f_vals[0] && $f_vals[0] ne '' ) {
                        $f_vals[0] =~ s/($_qr)/\<span class=searchhl\>$1\<\/span>/ig;
                        my $_name   = getCatName( $_shopid, $_catid, $_itemid);
                        if ( $_name ) {
                            $search_content = doDbSearch(quotemeta($_name), $f_inx, 0);
                            $search_content =~ s/($_qr)/\<span class=searchhl\>$1\<\/span>/ig;
                        }
                    }

                } elsif ($f_inx == 1) {
                    my $_name   = getCatName( $_shopid, $_catid, $_itemid);
                    if ( $_name ) {
                        $search_content = doDbSearch(quotemeta($_name), $f_inx, 0);
                    }
                }
            }
        }
    }
    if (!$search_content) {
        $search_content = doDataFilesSearchNM1( $_qr, $f_inx, 0 );
    }
    if (!$search_content) {
        $search_content = doDataFilesSearchN( $_qr, $f_inx, 0 );
    }
    if (!$search_content) {
        $search_content = qq~<p class="standart_text"><b>Ничего не найдено!</b></p>~;
    }
    return $search_header.$search_content;
}

#------------------------------------------------------------------------------
# doDbSearch ( $_qr, $f_inx, $hl_fl )
#------------------------------------------------------------------------------
#
# $_qr - ключевая фраза для поиска.
# $f_inx - индекс установленного фильтра.
# $hl_fl - флаг подсветки найденной фразы.
# Поиск ключевой фразы в индексном файла БД и конечных файлах прайс-листов.
#
#------------------------------------------------------------------------------

sub doDbSearch {
    my ( $_qr, $f_inx, $hl_fl ) = @_;
    my $_srch;
    my $DISP_IN_RECORD = 1;
    if (-e $MODULE_DB_FILE) {
        open my $fh, "<", $MODULE_DB_FILE;
        my @_db = <$fh>;
        close $fh;
        for my $i (0..$#_db) {
            if ($_db[$i] !~ /$_qr/i) {
                my @record = split /\|/, $_db[$i];
                $record[-1] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                my $pricefile = $record[-1];
                chomp( $pricefile );

                if (-e $MODULE_PRICEPOOL.$pricefile) {
                    open my $fh, "<", $MODULE_PRICEPOOL.$pricefile;
                    my @_col = <$fh>;
                    close $fh;
                    for my $j (0..$#_col) {
                        chomp( $_col[$j] );
                        my @_rec = split /\|/, $_col[$j];
                        $_rec[-1] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                        my $id = $1;
                        $_rec[1] =~ s/(\[[\d]+\])//g;
                        $id =~ s/[\]\[]+//g;
                        if ($_rec[-1] =~ /$_qr/i) {
                            my $_nam = $_rec[-1];
                            if ( $f_inx > 1 ) {
                                my @f_vals = getFilterData( $record[0], $id, $f_inx );
                                if ($f_vals[0] ne '' && defined $f_vals[0]) {
                                    $_nam = $f_vals[0];
                                }
                            }
                            if ($hl_fl) {
                                $_nam =~ s/($_qr)/\<span class=searchhl\>$1\<\/span>/ig;
                            }
                            $_srch .= qq~
                                <p class="standart_text"><a href="shopcatalog_$id\_$_rec[1].html" class="shopcat-simplelink">$_nam</a></p>
                            ~;
                        }
                    }
                }
            } else {
                my @catsearchres = ();
                my @record = split /\|/, $_db[$i];
                for ( my $j = $DISP_IN_RECORD ; $j <= $#record - 1 ; $j++ ) {
                    $record[$j] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                    my $id = $1;
                    $id =~ s/[\]\[]+//g;
                    if ($hl_fl) {
                        $record[$j] =~ s/($_qr)/\<span class=searchhl\>$1\<\/span>/ig;
                    }
                    my $_nam = $record[$j];
                    if ($record[$j] !~ /\<span class=searchhl\>/) {
                        my $_shopid;
                        if ($j == $#record - 1 ) {
                            $_shopid = $record[0];
                        } else {
                            $_shopid = -1;
                        }
                        if ( $f_inx > 1 ) {
                            my @f_vals = getFilterData( $_shopid, $id, $f_inx );
                            if ($f_vals[0] ne '' && defined $f_vals[0]) {
                                $_nam = $f_vals[0];
                            }
                        }
                    }
                    if ($j == $#record - 1 ) { push(@catsearchres,"<a href=\"shopcatalog_$id\_$record[0].html\" class=\"shopcat-simplelink\">$_nam</a>"); }
                        else {push(@catsearchres,"<a href=\"shopcatalog_$id.html\" class=\"shopcat-simplelink\">$_nam</a>");}
                }
                my $navi = join(" / ",@catsearchres);
                $_srch .= qq~<p>$navi</p>~;
            }
        }
    }
    return $_srch;
}

#------------------------------------------------------------------------------
# doDataFilesSearchNM1 ( $_qr, $f_inx, $hl_fl )
#------------------------------------------------------------------------------
#
# $_qr - ключевая фраза для поиска.
# $f_inx - индекс установленного фильтра.
# $hl_fl - флаг подсветки найденной фразы.
# Поиск ключевой фразы в индексном файла БД.
#
#------------------------------------------------------------------------------

sub doDataFilesSearchNM1 {
    my ( $_qr, $f_inx, $hl_fl ) = @_;
    my $_srch;
    if (-e $MODULE_DB_FILE) {
        open my $fh, "<", $MODULE_DB_FILE;
        my @_db = <$fh>;
        close $fh;
        for my $i (0..$#_db) {
            my @_rec = split /\|/, $_db[$i];
            $_rec[-2] =~ s/\[\*\*\]\[([\d]+)\]//g;
            my $_id = $1;
            my $_cn = $_rec[-2];
            if ($_id) {
                my $path = $MODULE_DATAPOOL.q{/}.$_rec[0].'/levels/n-1/'.$_id;
                if (-e $path) {
                    my @files = glob $path.'/*.txt';
                    foreach my $file (@files) {
                        if (-e $file) {
                            open my $fh, "<", $file;
                            my @_srch = grep { $_ =~ /$_qr/gi } <$fh>;
                            close $fh;
                            if (@_srch) {
                                $_srch .= doDbSearch(quotemeta($_cn), $f_inx, 0);
                            }
                        }
                    }
                }
            }
        }
    }
    return $_srch;
}

#------------------------------------------------------------------------------
# doDataFilesSearchN ( $_qr, $f_inx, $hl_fl )
#------------------------------------------------------------------------------
#
# $_qr - ключевая фраза для поиска.
# $f_inx - индекс установленного фильтра.
# $hl_fl - флаг подсветки найденной фразы.
# Поиск ключевой фразы в конечных файлах прайс-листов.
#
#------------------------------------------------------------------------------

sub doDataFilesSearchN {
    my ( $_qr, $f_inx, $hl_fl ) = @_;
    my $_srch;
    if (-e $MODULE_DB_FILE) {
        open my $fh, "<", $MODULE_DB_FILE;
        my @_db = <$fh>;
        close $fh;
        for my $i (0..$#_db) {
            my @_rec = split /\|/, $_db[$i];
            $_rec[-1] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
            my $pricefile = $_rec[-1];
            chomp( $pricefile );
            if (-e $MODULE_PRICEPOOL.$pricefile) {
                open my $fh, "<", $MODULE_PRICEPOOL.$pricefile;
                my @_col = <$fh>;
                close $fh;
                for my $j (0..$#_col) {
                    chomp( $_col[$j] );
                    my @_rec_t = split /\|/, $_col[$j];
                    $_rec_t[-1] =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                    my $_cn = $_rec_t[-1];

                    my $path = $MODULE_DATAPOOL.q{/}.$_rec[0].'/levels/n/'.$_rec_t[0];
                    if (-e $path) {
                        my @files = glob $path.'/*.txt';
                        foreach my $file (@files) {
                            if (-e $file) {
                                open my $fh, "<", $file;
                                my @_srch = grep { $_ =~ /$_qr/gi } <$fh>;
                                close $fh;
                                if (@_srch) {
                                    $_srch .= doDbSearch(quotemeta($_cn), $f_inx, 0);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $_srch;
}

#------------------------------------------------------------------------------
# getRecIdInPriceFile ( $filepath, $itemId )
#------------------------------------------------------------------------------
#
# $filepath - путь до прайс-листа.
# $itemId - идентификатор позиции [*][xxxx].
# Получение идентификатора записи позиции [0] из БД модуля Shopcat::V3.
# Функция возвращает идентификатор записи позиции [0] из БД модуля Shopcat::V3.
#
#------------------------------------------------------------------------------

sub getRecIdInPriceFile {
    my @rc;
    my $filepath = $_[0];
    my $itemId   = $_[1];
    if (-e $filepath) {
        open(MFHDL, "<", $filepath);
        my @records = <MFHDL>;
        close(MFHDL);
        for(my $i=0; $i<=$#records; $i++) {
            if ($records[$i] =~ /\[\*\]\[$itemId\]/) {
                my $catid = -1;
                if ($records[$i] =~ /\[\*\*\]\[([\d]+)\]/) {$catid = $1;}
                my @rec_data = split(/\|/,$records[$i]);
                push(@rc, $catid, $rec_data[0]);
                last;
            }
        }
    }
    return @rc;
}

#------------------------------------------------------------------------------
# checkNN1Level ( $id )
#------------------------------------------------------------------------------
#
# $id - идентификатор категории.
# Проверка идентификатора категории на принадлежность уровню N-1 или N.
# Функция возвращает 0, если идентификатор категории не является идентификатором
# категории N-1 или N, в противном случае возращается $id без изменений.
#
#------------------------------------------------------------------------------

sub checkNN1Level {
    my $rc      = 0;
    my $shopid  = $_[0];
    my $catid   = $_[1];
    my @records = getModuleDb();
    if (@records) {
        my $found = 0;

        for(my $i=0; $i<=$#records; $i++) {
            if ($records[$i] =~ /\[\*\*\]\[$catid\]/) {
                $found = 1;
                last;
            }
        }

        if ($found == 1) {
            $rc = $shopid;
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------
# fillSeoTags ( $template_string, $shopid, $catid )
#------------------------------------------------------------------------------
#
# $template_string - строка с inline тегом для вставки seo-тега.
# $shopid - идентификатор прайс-листа.
# $cat_id - идентификатор текущей категории.
# Вставка seo-тега в строку шаблона.
# Функция возвращает строку шаблона с seo-тегом.
#
#------------------------------------------------------------------------------

sub fillSeoTags {
    my ($scheme, $catid, $shopid, $seoaction) = @_;
    my $found = 0;
    my $dhdr  = getSingleSetting( $MODULE_NAME, 1, 'defaulthdr' );
    my @details;
    if ( (
        $scheme =~ /\%page_title\%/ ||
        $scheme =~ /\%metadesc\%/ ||
        $scheme =~ /\%metakeys\%/ ||
        $scheme =~ /\%weblog-page-header\%/ ) ) {
        if (-e $MODULE_SEO_DB_FILE) {
            my $f_inx = getFilterIndex();
            if ( $f_inx == 1 ) {
                open my $fh, "<", $MODULE_SEO_DB_FILE;
                my @records = <$fh>;
                close $fh;
                for(my $i=0; $i<=$#records; $i++) {
                    my $record = $records[$i];
                    $record =~ s/[\r\n\t]+//g;
                    my @_rd = split(/\|/,$record);
                    if ($_rd[4] eq $seoaction) {
                        @details = ();
                        my $catname = getCatName(
                            $_rd[1],
                            $_rd[2],
                            $_rd[3]);
                        if ( $_rd[5] ne '' && defined $_rd[5] &&
                             $_rd[6] ne '' && defined $_rd[6] &&
                             $_rd[7] ne '' && defined $_rd[7] &&
                             $catname ne '' && defined $catname ) {
                            push(@details,
                                 ( $_rd[5] || $dhdr ), $_rd[6], $_rd[7], ( $catname || $dhdr ) );
                            $found = 1;
                        }
                        last;
                    }
                }
            }elsif ( $f_inx > 1 && $f_inx < 13) {
                my @f_vals = getFilterData(
                                $shopid > 0 ? $shopid : -1, $catid, $f_inx );
                if ( $f_vals[0] ne '' && defined $f_vals[0] &&
                     $f_vals[1] ne '' && defined $f_vals[1] &&
                     $f_vals[2] ne '' && defined $f_vals[2] &&
                     $f_vals[3] ne '' && defined $f_vals[3] ) {
                    @details = ();
                    push(@details,
                            ( $f_vals[1] || $dhdr ), $f_vals[2], $f_vals[3], ( $f_vals[0] || $dhdr ) );
                    $found = 1;
                }
            }
        }
        if ($found == 0) {
            my $catname;
            my $_titl = getSettingFromGroupByAttr(
                $MODULE_NAME, 1, 'pageseotags', 'action', 'shopcatalog', 'title');
            my $_met1 = getSettingFromGroupByAttr(
                $MODULE_NAME, 1, 'pageseotags', 'action', 'shopcatalog', 'metadescr');
            my $_met2 = getSettingFromGroupByAttr(
                $MODULE_NAME, 1, 'pageseotags', 'action', 'shopcatalog', 'metakeywords');
            if ($shopid == 0) {
                if ($catid != 0) {
                    $catname = getShopCatName($catid);
                    if ($catname == -1) {
                        $catname = 'Неопределенный раздел';
                    }
                }
                if ($catid == 0) {
                    $details[1] = $catname.' в каталоге';
                }
            } else {
                if (checkCatIsItem($shopid, $catid) == 0) {
                    $catname = getShopCatName($catid);
                    if ($catname == -1) {
                        $catname = 'Неопределенный раздел';
                    }
                    @details     = ( $_titl, $_met1, $_met2, $_titl );
                } else {
                    my $pricefile = getPriceFileFullPath( $shopid );
                    if (-e $pricefile) {
                        my @translatedIds = getRecIdInPriceFile($pricefile, $catid);
                        $catname = getCatName(
                            $shopid,
                            $translatedIds[0],
                            $translatedIds[1]);
                        if ($catname) {
                            my $description = getDescrNM1($shopid, $translatedIds[0], $catid);
                            if ($description =~ /No description record in/) {
                                $description = '';
                            } else {
                                $description =~ s/[\r\n]+//gi;
                                $description =~ s/\<\!\-\-[a-z0-9\/\:\.\-\s]+\-\-\>//gi;
                                $description =~ s/\<[a-z0-9\/\:\.\-\s]+\>//gi;
                                $description .= ": ";
                            }
                        } else {
                            @$catname = 'Неопределенная позиция';
                        }
                    }
                }
            }
            if ($catname) {
                s/%catname%/$catname/gi for( ($_titl, $_met1, $_met2) );
            } else {
                s/(\,\s?){0,}%catname%(\.\s?){0,}//gi for( ($_titl, $_met1, $_met2) );
            }
            s/%defaulthdr%/$dhdr/gi for( ($_titl, $_met1, $_met2) );
            @details = ( ( $_titl || $dhdr ), $_met1, $_met2, ( $_titl || $dhdr ) );
        }
    }
    $scheme =~ s/%page_title\%/$details[0]/;
    $scheme =~ s/%metadesc\%/$details[1]/;
    $scheme =~ s/%metakeys\%/$details[2]/;
    $scheme =~ s/%weblog-page-header\%/$details[3]/;
    return $scheme;
}

#------------------------------------------------------------------------------
# getCatName ( $shopid, $catid, $itemid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $cat_id - идентификатор категории N-1.
# $cat_id - идентификатор категории N.
# Получение наименования категории.
# Функция возвращает наименование категории.
#
#------------------------------------------------------------------------------

sub getCatName {
    my $shopid = $_[0];
    my $catid  = $_[1];
    my $itemid = $_[2];
    my $catname;
    if ( $itemid == -1 ) {
        my @shopcat = getModuleDb();
        for ( my $j = 0 ; $j <= $#shopcat ; $j++ ) {
            my @tmp = split( /\|/, $shopcat[$j] );
            for ( my $k = 0 ; $k <= $#tmp ; $k++ ) {
                if ( $tmp[$k] =~ /\[$catid\](.{1,})/ ) {
                    $catname = $1;
                    if ($catname) {
                        last;
                    }
                }
            }
        }
    }
    else {
        open( MFHDL, "<", getPriceFileFullPath( $shopid ) );
        my @pricefile = <MFHDL>;
        close(MFHDL);
        for ( my $j = 0 ; $j <= $#pricefile ; $j++ ) {
            my @tmp = split( /\|/, $pricefile[$j] );
            $pricefile[$j] =~ s/[\r\n\t]+//g;
            if ( $tmp[0] == $itemid ) {
                if ( $tmp[$#tmp] =~ /\[\*\]\[[\d]+\](.{1,})/ ) {
                    $catname = $1;
                    if ($catname) {
                        last;
                    }
                }
            }
        }
    }
    return $catname;
}

#------------------------------------------------------------------------------
# getVarntsPage ( $shopid, $catid, $page_num )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории N-1.
# $pagenum - номер страницы.
# Получение блока Варианты.
# Функция возвращает HTML код блока Варианты.
#
#------------------------------------------------------------------------------

sub getVarntsPage {
    my $variants_block;
    my $shopid        = $_[0] || 0;
    my $catid         = $_[1] || getCatIdByShopId($shopid) || 0;
    my $page_num      = $_[2] || 0;
    my @variants      = ();
    my $wrk_at_n_lev  = 0;
    if ($shopid) {
        my $pricefile = getPriceFileFullPath( $shopid );
        if (-e $pricefile) {
            open my $fh, "<", $pricefile;
            my @pricedat = <$fh>;
            close $fh;
            for my $i (0..$#pricedat) {
                my $rec = $pricedat[$i];
                $rec =~ s/[\r\n\t]//g;
                my @tmp = split(/\|/,$rec);
                if ( $tmp[0] != $catid && $tmp[2] ne '-1' ) {
                    push(@variants, $tmp[-1]);
                } else {
                    $wrk_at_n_lev = 1;
                }
            }
        }
    }
    if (@variants) {
    my $block          = '';
    my $template_fname = "$MODULE_DATAPOOL/levels/n-1/template.html";
    if ( -e $template_fname ) {
        open my $fh, "<", $template_fname;
        my @template_to_parse = <$fh>;
        close $fh;
        my $block_flag = 0;

        for my $i (0..$#template_to_parse) {
            if ( $template_to_parse[$i] =~ /\-\-\>/ ) { $block_flag = 0; last; }
            if ( $block_flag == 1 ) { $block .= $template_to_parse[$i]; }
            if ( $template_to_parse[$i] =~ /\<\!\-\-/ ) { $block_flag = 1; }
        }
    }
    else {
        $variants_block = $MODULE_DEBUG == 1 ?
            qq~ <div class="shopcat-fallback-mess-div"><b>$MODULE_NAME:</b> in function getVarntsPage() - $template_fname not found!<br>
                Use \$DEFAULT_VARIANTS_BLOCK:</div>
            ~ : '';
        $block = $DEFAULT_VARIANTS_BLOCK;
        if ( $FORCE_FALL_BACK != 1 ) {
            eval {
                $variants_block = '';
                $block          = Shopcat::Templts::getVariants();
            };
        }
    }

    if ( $block ne '' ) {
        my @vardata = getArrayPage($shopid, $catid, $page_num, @variants);
        @variants = @vardata[1..$#vardata];
        for ( my $i = 0 ; $i <= $#variants ; $i++ ) {
            my $work_block = $block;
            my $record     = $variants[$i];
            if ( $record =~ /\[([\d]+)\]/ ) {
                my $id = $1;
                my $artikul =
                  getArtikulByCatId( $shopid,
                    $id );
                my $itemprice = getItemCostByNLevID( $shopid, $id );
                if ( $itemprice == 0 ) { $itemprice = "под заказ" }
                else {
                    $itemprice  = getCorrPrice($itemprice);
                    $itemprice .=
                        " " . ( $GLOBAL_CURRENCY != 0 ? $GLOBAL_CURRENCY : q{} );
                }
                my $title_descr;

                # Chpu workout
                my $linkurl;
                my @pricepoolRecIds =
                  getRecIdInPriceFile( getPriceFileFullPath($shopid), $id );
                if (@pricepoolRecIds) {
                    $linkurl = get4PUrl(@pricepoolRecIds);
                }
                if ( $linkurl =~ /shopcatalog/ || !$linkurl ) {
                    $linkurl = get4PUrl( $id, $shopid );
                }

                # Chpu workout
                if ($wrk_at_n_lev) {
                    $catid = getCatIdByShopId($shopid);
                }
                $record =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                $work_block =~ s/%itemprice%/$itemprice/g;
                $work_block =~ s/%artikul%/$artikul/g;
                $work_block =~ s/%linkurl%/$linkurl /g;
                $work_block =~ s/%cat_id%/$id/g;
                $work_block =~ s/%cat_name%/$record/g;
                my $cat_descr = getDescrNM1( $shopid, $catid, $id );
                my $img_cont = "<i class=\"fa fa-image\" title=\"Image not found!\"></i>";
                my $img_path = q{};
                my $img_path_artikul =
                  "images/shopcat/$shopid/levels/n/$artikul";
                my $title_img = getTitleImgFromDir($img_path_artikul);

                if ( -e "$img_path_artikul/$title_img" ) {
                    $img_path = "$img_path_artikul/$title_img";
                    $img_cont = qq~
                    <img  src="images/admin/cms/spacer.gif"
                          width="150" height="150"
                          alt="%cat_name%"
                          style="background: url('$img_path') no-repeat; background-size: cover;">
                    ~;
                }
                else {
                    $title_descr = "image not found: $img_path_artikul/*_01.*";
                }
                $work_block =~ s/%img_content%/$img_cont/g;
                $work_block =~ s/%title_descr%/$title_descr/g;
                $work_block =~ s/%cat_descr%/$cat_descr/g;
                $variants_block .= $work_block;
            }
        }
        if ($variants_block ne '') {
             $variants_block = qq~
             <a name="variants-anchor" id="variants-anchor"></a>
             <div id="loadvariants" style="margin-left:0px;">
             $variants_block
             <p align=center>$vardata[0]</p>
             </div>~;
        }
    }
    if ( $variants_block eq '' ) {
        $variants_block = qq~Список товаров пуст!~;
        ;
    }
    } else {
        $variants_block = qq~Товары не найдены!~;
    }
    return $variants_block;
}

#------------------------------------------------------------------------------
# getArrayPage ( $shopid, $catid, $curr_page )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории N-1.
# $curr_page - номер страницы.
# @array - массив элементов для отображения.
# Получение нужного количества элементов для отображения на заданной странице,
# а также блок ссылок для постраничной навигации.
# Функция возвращает HTML код блока ссылок для постраничной навигации, а также
# массив для отображения на заданной странице.
#
#------------------------------------------------------------------------------

sub getArrayPage {
    my $shopid      = $_[0];
    my $catid       = $_[1];
    my $curr_page   = $_[2];
    my @array       = @_[3..$#_];

    # Формирование навигации по страницам

    my $page_linx = '';
    my $pages     = 0;

    $pages = floor( $#array  / $MODULE_VARIANTS_LIM );

    if (( $pages * $MODULE_VARIANTS_LIM <= $#array ) || ( $pages < 1 ) ) {
        $pages++;
    }

    if ( $curr_page >= $pages ) { $curr_page = 0; }
    $page_linx = '&nbsp;-&nbsp;';
    for ( my $i = 1 ; $i <= $pages ; $i++ ) {
        if ( ( $i - 1 ) != $curr_page ) {
            $page_linx .= "<a href=\"javascript:doDataLoad('"
                          . ( $i - 1 )
                          . "', '$shopid', '$catid')\" class=shopcat-pageslink>"
                          . $i
                          . '</a>&nbsp;-&nbsp;';
        }
        else {
            $page_linx .= '<span class="shopcat-disabled-link">'
                          . $i
                          . '</span>&nbsp;-&nbsp;';
        }
    }

    if ($pages > 1) {
        if ($curr_page >= 0) {
            my $get_page_action = getPageSeoAction($shopid, $catid) || 0;
            if ($get_page_action) {
                $page_linx .= qq~<a href="$get_page_action\-all.html#horizontalTab4" class=shopcat-pageslink>Все</a>&nbsp;-&nbsp;~;
            } else {
                my $tmpcatid = getCatIdByShopId($shopid);
                if ($tmpcatid) {
                    $page_linx .= qq~<a href="shopcatalog-all\_$tmpcatid\_$shopid.html#horizontalTab4" class=shopcat-pageslink>Все</a>&nbsp;-&nbsp;~;
                }
            }
        } else {
            $page_linx .= qq~<span class=shopcat-disabled-link>Все</span>&nbsp;-&nbsp;~;
        }
    }

    if ($curr_page >= 0) {
        if ( $curr_page >= $pages ) { $curr_page = $pages - 1; }
        my $end = ( ( $curr_page + 1 ) * $MODULE_VARIANTS_LIM ) - 1;
        if ( $end > $#array ) {
            $end = $#array;
        }
        @array = @array[ $curr_page * $MODULE_VARIANTS_LIM .. $end ];
    }
    # Формирование навигации по страницам

    return ($page_linx, @array);
}

#------------------------------------------------------------------------------
# getPageSeoAction ( $shopid, $catid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории N-1.
# Получение ЧПУ url по заданным входным аргументам.
# Функция возвращает ЧПУ url.
#
#------------------------------------------------------------------------------

sub getPageSeoAction {
    my $shopid  = $_[0];
    my $itemid  = $_[1];
    my $catid   = 0;
    my $rc      = '';
    if ($itemid > 0) {
        my $pricefile = getPriceFileFullPath( $shopid );
        if (-e $pricefile) {
            open my $fh, "<", $pricefile;
            my @prc_data = <$fh>;
            close $fh;
            for my $prc (@prc_data) {
                my @tmp = split (/\|/, $prc);
                if ($tmp[0] eq $itemid) {
                    if ($tmp[$#tmp-1] =~ /\[\*\*\]\[([\d]+)\]/) {
                        $catid = $1;
                    }
                }
            }
        }
    } else {
        my @cat_data = getModuleDb();
        if (@cat_data) {
            for(my $catinx = 0; $catinx <= $#cat_data; $catinx++) {
                my @tmp = split (/\|/,$cat_data[$catinx]);
                if ($tmp[0] eq $shopid) {
                    if ($tmp[$#tmp-1] =~ /\[\*\*\]\[([\d]+)\]/) {
                        $catid = $1;
                    }
                }
            }
        }
    }
    if ($catid > 0) {
        if (-e $MODULE_SEO_DB_FILE) {
            open my $fh ,"<", $MODULE_SEO_DB_FILE;
            my @seo_data = <$fh>;
            close $fh;
            foreach my $data (@seo_data) {
                my @tmp = split (/\|/, $data);
                if ($tmp[3] == -1) {
                    if (    $tmp[1] == $shopid &&
                            $tmp[2] == $catid ) {
                        $rc = $tmp[4];
                    }
                }
            }
        }
    }
    return $rc."";
}

#------------------------------------------------------------------------------
# getShopCatName ( $catid )
#------------------------------------------------------------------------------
#
# $catid - идентификатор категории 1 или 2 уровня.
# Получить имя категории 1 или 2 уровня.
# Функция возвращает имя категрии 1 или 2 уровня.
#
#------------------------------------------------------------------------------

sub getShopCatName {
    my $catename = -1;
    my $cat_id   = $_[0] || -1;
    if ( -e $MODULE_DB_FILE ) {
        open( CATFILEFH, "<", $MODULE_DB_FILE );
        my @maincat = <CATFILEFH>;
        close CATFILEFH;
        for ( my $i = 0 ; $i <= $#maincat ; $i++ ) {
            if ( $maincat[$i] =~ /\[$cat_id\](.{1,})/ ) {
                $catename = $1;
                my @tmp = split(/\|/,$catename);
                $catename = $tmp[0];
                last;
            }
        }
    }
    return $catename;
}

#------------------------------------------------------------------------------
# getModJsCssCode ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить дополнительный код js/css для данного модуля.
# Функция возвращает дополнительный код js/css для данного модуля.
#
#------------------------------------------------------------------------------

sub getModJsCssCode {
    my $modulecode = qq~
    <!-- TABS -->
    <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/easytabs/easyResponsiveTabs.js" type="text/javascript"></script>
    <!-- FANCYBOX -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script type="text/javascript">
        jQuery(function(){
            jQuery('[data-fancybox="gallery"]').fancybox({ infobar: false, buttons: ["zoom","slideShow","close"] });
        });
    </script>
    ~;
    return $modulecode
}

#------------------------------------------------------------------------------
# setFilterCookie ( $co, $name, $value, $expire )
#------------------------------------------------------------------------------
#
# $name - имя cookie.
# $value - значение cookie.
# $expire - время жизни cookie.
#
# Установить значение cookie.
#
#------------------------------------------------------------------------------

sub setFilterCookie {
    my ($name, $value, $expire) = @_;
    my $result  = CGI::Cookie->new(
        -domain   => q{.}.$ENV{SERVER_NAME},
        -name     => $name,
        -value    => $value,
        -expires  => $expire, # exmpl: "+365d"
        -path     => "/",
        -httponly => 0
    );
    return $result;
}

#------------------------------------------------------------------------------
# getFilterCookie ( $co, $name )
#------------------------------------------------------------------------------
#
# $co - объект CGI.
# $name - имя cookie.
#
# Прочитать значение cookie.
#
#------------------------------------------------------------------------------

sub getFilterCookie {
    my ($co, $name) = @_;
    my $result  = $co->cookie($name);
    return $result;
}

#------------------------------------------------------------------------------
# getFilterCookieName ( )
#------------------------------------------------------------------------------
#
# Прочитать название cookie.
#
#------------------------------------------------------------------------------

sub getFilterCookieName {
    return '_pheix_shopcat_filters';
}

#------------------------------------------------------------------------------
# getFiltersArray ( )
#------------------------------------------------------------------------------
#
# Получить массив фильтров.
#
#------------------------------------------------------------------------------

sub getFiltersArray {
    my $co = CGI->new;
    my $ck = getFilterCookie($co, getFilterCookieName());
    my @filters = split( /\|/, $ck );
    return @filters;
}

#------------------------------------------------------------------------------
# modifyFilter ( )
#------------------------------------------------------------------------------
#
# Модифицировать массив фильтров.
#
#------------------------------------------------------------------------------

sub modifyFilter {
    my $co = $_[0];
    my $flrt = $_[1];
    my $ck = getFilterCookie($co, getFilterCookieName());
    my @filters = split( /\|/, $ck );
    for my $i (0..11) {
        $filters[$i] = !$filters[$i] ||
                       $FILTERS_MODE == 1 ||
                       $FILTERS_MODE == 0 ? 0 : $filters[$i];
    }
    $filters[$flrt] = $filters[$flrt] == 0 ? 1 : 0;
    my $rc = setFilterCookie(getFilterCookieName(), join("|", @filters), '+365d');
    return $rc;
}

#------------------------------------------------------------------------------
# getFiltersMask ( )
#------------------------------------------------------------------------------
#
# Получить маску фильтров.
#
#------------------------------------------------------------------------------

sub getFiltersMask {
    my @filters = @_;
    my $mask;
    for my $i (0..$#filters) {
        if ($filters[$i] == 1) {
            $mask |= 1 << $i;
        }
    }
    return $mask;
}

#------------------------------------------------------------------------------
# doShopCatFiltering ( )
#------------------------------------------------------------------------------
#
# Отфильтровать входные данные по маске.
#
#------------------------------------------------------------------------------

sub doShopCatFiltering {
    my ($shopid, @f_all) = @_;
    my @filtered;
    my $mask = getFiltersMask(getFiltersArray());
    if (!$mask) { return @f_all; }
    for my $i (0..$#f_all) {
        if ($f_all[$i] =~ m/\[([\d]+)\]/gsi) {
            my $id        = $1;
            my $record_id = getArtikulByCatId($shopid, $id);
            my @filters = decodeFiltersMaskById($shopid, $record_id);
            my $itemmask = getFiltersMask(decodeFiltersMaskById($shopid, $record_id));
            if (($itemmask & $mask)) {
                push(@filtered, $f_all[$i]);
            }
        }
    }
    return @filtered
}

#------------------------------------------------------------------------------
# decodeFiltersMaskById ( )
#------------------------------------------------------------------------------
#
# Декодирование маски фильтров по идентификатору.
#
#------------------------------------------------------------------------------

sub decodeFiltersMaskById {
    my ($shopid, $itemtid) = @_;
    my @decodedfilters = split ( /\|/, getValueFromRedis('filters-'.$shopid.'-'.$itemtid) );
    if (@decodedfilters) {
        return @decodedfilters;
    }
    my $catfile = getPriceFileFullPath($shopid);
    if (-e $catfile) {
        open my $ifh, "<", $catfile or die "unable to open $catfile: $!";
        my @data = <$ifh>;
        close $ifh or die "unable to close: $!";
        for my $i (0..$#data) {
            $data[$i]  =~ s/[\r\n\t]//gi;
            my @dbcol  = split /\|/, $data[$i];
            if ($dbcol[0] eq $itemtid) {
                my $itemlastid;
                if ($dbcol[-1] =~ /\[\*\]\[([\d]+)\]/) {
                    $itemlastid = $1;
                    if ($itemlastid > 9999) {
                        my $decodesmask = ($itemlastid - ($itemlastid % 10000)) / 10000;
                        my $f_num = getSettingChildNum('Shopcat', 0, 'filters');
                        for my $i (0..$f_num) {
                            push(@decodedfilters, $decodesmask & 1 || 0);
                            $decodesmask = $decodesmask >> 1;
                        }
                        if (@decodedfilters) {
                            setValueToRedis('filters-'.$shopid.'-'.$itemtid, join("|", @decodedfilters));
                        }
                    }
                }
            }
        }
    }
    return @decodedfilters;
}

#------------------------------------------------------------------------------
# getFilterIndex ( )
#------------------------------------------------------------------------------
#
# Получить индекс фильтра.
#
#------------------------------------------------------------------------------

sub getFilterIndex {
    my ($forcefilter) = @_;
    my $f_inx = 1;
    if ($FILTERS_MODE == 1) {
        my @_fltrs = getFiltersArray();
        for my $i (0..$#_fltrs) {
            if ($_fltrs[$i] == 1) {
                $f_inx = $i+1;
                last;
            }
        }
    }
    return $forcefilter ? $forcefilter : $f_inx;
}

#------------------------------------------------------------------------------
# getFilterData ( )
#------------------------------------------------------------------------------
#
# Получить массив значений по фильтру.
#
#------------------------------------------------------------------------------

sub getFilterData {
    my ($shopid, $_id, $fltr) = @_;
    my @vals;
    my @_v;
    if ($shopid == -1) {
        $shopid = getArtikulByCatId(-1, $_id);
    }
    if (checkCatIsItem($shopid, $_id) == 0) {
        @_v = getParametrizedVals($shopid, $_id, -1);
    } else {
        my $_catid = getCatIdByShopId($shopid);
        my $_itemid = getArtikulByCatId($shopid, $_id);
        @_v = getParametrizedVals($shopid, $_catid, $_itemid);
    }
        for my $i (0..$#_v) {
            if ($_v[$i] =~ /\[$fltr\]/) {
                my $_rec = $_v[$i];
                $_rec =~ s/\[$fltr\]//gi;
                push @vals, $_rec;
            }
        }
    return @vals;
}

#------------------------------------------------------------------------------
# getParametrizedVals ( $sid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# $catid - идентификатор категории.
# $itemid - идентификатор позиции.
# Получение параметризованных значений.
#
#------------------------------------------------------------------------------

sub getParametrizedVals {
    my ($shopid, $catid, $itemid) = @_;
    my @db;
    my @params;
    my $path = 'conf/system/shopcat2.parametrized.tnk';
    if (-e $path) {
        open my $fh, "<", $path or
            die 'unable to open for read ' . $path . ': ' . $!;
        my @db = <$fh>;
        close $fh or die 'unable to close ' . $path. ': ' . $!;
        for my $i (0..$#db) {
            $db[$i] =~ s/[\r\n]+//gix;
            my @cols = split(/\|/, $db[$i]);
            if ($cols[0] == $shopid &&
                $cols[1] == $catid && $cols[2] == $itemid ) {
                @params = @cols[3..$#cols];
                last;
            }
        }
    }
    return @params;
}

#------------------------------------------------------------------------------
# getFilteredValue ( )
#------------------------------------------------------------------------------
#
# Получить значение из массива значений по фильтру.
#
#------------------------------------------------------------------------------

sub getFilteredValue {
    my ( $shopid, $recid, $val_n ) = @_;
    my $_val;
    my $f_inx = getFilterIndex( $val_n );
    my @f_vals = getFilterData($shopid, $recid, $f_inx );
    if ($f_inx > 1) {
        $_val = $f_vals[0] ? $f_vals[0] : '';
    }
    return $_val;
}

#------------------------------------------------------------------------------
# getRandomItems ( )
#------------------------------------------------------------------------------
#
# Выбрать случайные позиции из каталога.
#
#------------------------------------------------------------------------------

sub getRandomItems {
    my ($shopid, $numi, $filter) = @_;
    my $cntnt;
    my $_block = qq~
    <div class="shopcat-rnd-items-block">
     <p class="shopcat-rnd-items-header"><a href="%link%">%header%</a></p>
     <p class="shopcat-rnd-items-body">%body%</p>
    </div>
    ~;
    my $_fn = getPriceFileFullPath($shopid);
    if (-e $_fn) {
        open my $fh, "<", $_fn or die "unable to open $_fn: ".$!;
        my @data = <$fh>;
        close $fh or die "unable to close $_fn: ".$!;
        my $_numi = $numi > $#data ? $#data : $numi <= 0 ? 1 : $numi;
        my @_data = @data[(shuffle(0..$#data))[0..$_numi]];
        if (@_data) {
            foreach my $_rec (@_data) {
                chomp($_rec);
                my @_col = split( /\|/, $_rec );
                my $f_inx = getFilterIndex($filter);
                my $_bcp;
                if ($f_inx == 1) {
                    my $_cid = -1;
                    my $_id  = -1;
                    if ($_col[-1] =~ s/(\[[\*]\])|(\[[\d]+\])//g) {
                        $_id = $2;
                        if ($_id) {
                            $_id =~ s/\[\]//g;
                        }
                    }
                    if ($_rec =~ /\[\*\*\]\[([\d]+)\]/) {
                        $_cid = $1;
                    }
                    $_bcp = $_block;
                    my @_fvals = getParametrizedVals($shopid, $_cid, $_col[0]);
                    if ($_fvals[0]) {
                        $_fvals[0] =~ s/^\[\d+\]//;
                        $_fvals[0] = parseMarkUp($_fvals[0]);
                    }
                    my $_lnk = get4PUrl($shopid, $_id);
                    $_bcp =~ s/%header%/$_col[-1]/g;
                    $_bcp =~ s/%link%/$_lnk/g;
                    $_bcp =~ s/%body%/$_fvals[0]/g;
                } else {
                    my $_id = -1;
                    if ($_col[-1] =~ /\[[\*]\]\[([\d]+)\]/) {
                        $_id = $1;
                    }
                    my @_fvals = getFilterData($shopid, $_id, $f_inx);
                    if ($_fvals[0]) {
                        $_bcp = $_block;
                        my $_lnk = get4PUrl($shopid, $_id);
                        $_bcp =~ s/%header%/$_fvals[0]/g;
                        $_bcp =~ s/%link%/$_lnk/g;
                        if ($_fvals[4]) {
                            $_fvals[4] = parseMarkUp($_fvals[4]);
                            $_bcp =~ s/%body%/$_fvals[4]/g;
                        }
                    }
                }
                if ($_bcp) {
                    $cntnt .= $_bcp;
                }
            }
        }
    }
    return $cntnt;
}

#------------------------------------------------------------------------------
# checkUniqueCat ( $level_id )
#------------------------------------------------------------------------------
#
# $level_id - идентификатор категории.
# Проверить заданный $level_id на уникальность
#
#------------------------------------------------------------------------------

sub checkUniqueCat {
    my $rc = 0;
    my ($level_id) = @_;
    my @maincat = getModuleDb();
    for my $i (0..$#maincat)  {
        if ($maincat[$i] =~ /(\[$level_id\])/ ) {
            $rc++;
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------
# countPriceFileRecords ( $shopid )
#------------------------------------------------------------------------------
#
# $shopid - $shopid - идентификатор прайс-листа.
# Подсчитать количество уникальных позиций в заданном прайс-листе.
#
#------------------------------------------------------------------------------

sub countPriceFileRecords {
    my ($shopid) = @_;
    my $rc = 0;
    if ($shopid > 0 && $shopid =~ /^[\d]+$/) {
        open my $fh, "<", getPriceFileFullPath( $shopid );
        my @rows = <$fh>;
        close $fh;
        my @uniqueitems = 0;
        foreach my $row (@rows) {
            my @cols = split( /\|/, $row );
            if ($cols[0] =~ /^[\d]+$/ && $#cols == $PRICEFILE_DB_COLUMS) {
                if ( !(map { $cols[0] eq $_ ? ($_) : () } @uniqueitems ) ) {
                    push @uniqueitems, $cols[0];
                    $rc++;
                }
            }
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------
# getFirstItemIdFromPriceFile ( $shopid )
#------------------------------------------------------------------------------
#
# $shopid - идентификатор прайс-листа.
# Получить идентификатор первой позиции в заданном прайс-листе.
#
#------------------------------------------------------------------------------

sub getFirstItemIdFromPriceFile {
    my ($shopid) = @_;
    my $id = 0;
    if ($shopid > 0 && $shopid =~ /^[\d]+$/) {
        open my $fh, "<", getPriceFileFullPath( $shopid );
        my @rows = <$fh>;
        close $fh;
        my @cols = split /\|/, $rows[0];
        if ($cols[0] =~ /^[\d]+$/ && $#cols == $PRICEFILE_DB_COLUMS) {
            $id = $cols[0];
        }
    }
    return $id;
}

#------------------------------------------------------------------------------
# getCatShopid ( $catid, $getrand )
#------------------------------------------------------------------------------
#
# $catid - идентификатор категории.
# $getrand - флаг рандомизации.
# Получить идентификатор прайс-листа по идентификатору категории.
# Если флаг рандомизации = 0, то будет получен идентифкатор первого вхождения.
# Если флаг рандомизации = 1, то будет получен случайный идентификатор,
# при условии, что их несколько.
#
#------------------------------------------------------------------------------

sub getCatShopid {
    my ($catid, $getrand) = @_;
    my @rows = getModuleDb();
    my @shopids;
    my $shopid;
    foreach my $row (@rows) {
        if ($row =~ /\[$catid\]/) {
            my @r = split( /\|/, $row );
            push(@shopids, $r[0]);
        }
    }
    if ($getrand == 1) {
        $shopid = $shopids[rand @shopids];
    }
    else {
        $shopid = @shopids[0];
    }
    return $shopid;
}

#------------------------------------------------------------------------------
# getImgForTmplsBlock ( $lvl, $shopid, $recid )
#------------------------------------------------------------------------------
#
# $lvl - номер уровня вложенности.
# $shopid - идентификатор прайс-листа.
# $recid - идентификатор категории.
# Получить путь к файлу изображения для заданного уровня вложенности,
# идентификатора прайс-листа и идентификатора категории.
# Поиск файла изображения выполняется в галереи уровня N.
#
#------------------------------------------------------------------------------

sub getImgForTmplsBlock {
    my ($lvl, $shopid, $recid) = @_;
    my $img_full_path;
    my $pricefile;
    my @data;
    if ($lvl eq '0') {
        $shopid = getCatShopid($recid, 1);
        @data = ($recid, getFirstItemIdFromPriceFile($shopid));
    }
    else {
        if ($shopid > -1) {
            $pricefile = getPriceFileFullPath($shopid);
            @data = getRecIdInPriceFile($pricefile, $recid);
        }
        else {
            $shopid = getShopIdByCatId($recid);
            @data = ($recid, getFirstItemIdFromPriceFile($shopid));
        }
    }
    my $img_dir = "images/shopcat/$shopid/levels/n/" . $data[1];
    $img_full_path = $img_dir . q{/} . getTitleImgFromDir($img_dir);
    if ( !(-e $img_full_path) ) {
        $img_full_path = "images/no_photo.png";
    }
    return $img_full_path;
}

#------------------------------------------------------------------------------
# getShopCatV3Sitemap ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов
# Получить массив страниц каталога для публикации в карте сайта.
#
#------------------------------------------------------------------------------

sub getShopCatV3Sitemap {
    my @pages;
    my $_off  = getV3PowerStatus();
    my $_set  = getSingleSetting($MODULE_NAME, 1, 'exportsitemap');
    if ( -e $MODULE_DB_FILE  && ( $_off == 0 ) && ( $_set == 1) )  {
        my @db    = getModuleDb();
        my $proto = getProtoSrvName();
        my $dbfts = strftime(
            '%Y-%m-%d',
            localtime(${stat($MODULE_DB_FILE)}[9])
        );
        foreach my $rec (@db) {
            chomp($rec);
            my @cols = split( /\|/, $rec );
            my $shopid = $cols[0];
            for my $col (@cols)  {
                if ( $col =~ /\[([\d]+)\]/ ) {
                    my $id = $1;
                    if ( $col =~ /\[\*\]/ ) {
                        my $fname     = getPriceFileFullPath($shopid);
                        if (-e $fname) {
                            my $ts = strftime(
                                '%Y-%m-%d',
                                localtime(${stat($fname)}[9])
                            );
                            open my $fh, "<", $fname;
                            my @rows = <$fh>;
                            close $fh;
                            for my $row (@rows)  {
                                my @c = split( /\|/, $row );
                                if (
                                    $c[-1] =~ /\[\*\]\[([\d]+)\]/ &&
                                    $c[2] ne '-1' &&
                                    $#c == $PRICEFILE_DB_COLUMS
                                ) {
                                    my $cid  = $1;
                                    #my $actn =
                                    #    'shopcatalog_'. $cid . '_' . $shopid;
                                    my $actn = get4PUrl($shopid, $cid);
                                    my $url  = $proto.'/'.$actn;
                                    if (
                                        !(map {
                                            $url eq $_->{loc} ? ($_) : ()
                                        } @pages)
                                    ) {
                                        my %rec  = (
                                            'loc'     => $url,
                                            'lastmod' => $ts
                                        );
                                        push @pages, \%rec;
                                    }
                                }
                            }
                        }
                    }
                    elsif ( $col =~ /\[\*\*\]/ && $id > 1) {
                        #my $actn =
                        #    'shopcatalog_' . $id . q{_} . $shopid . '.html';
                        my $actn = get4PUrl(0, $id);
                        my $url  = $proto.'/'.$actn;
                        if ( !(map { $url eq $_->{loc} ? ($_) : () } @pages) ) {
                            my %rec  = (
                                'loc'     => $url,
                                'lastmod' => $dbfts
                            );
                            push @pages, \%rec;
                        }
                    }
                    else {
                        # my $actn = 'shopcatalog_' . $id . '.html';
                        my $actn = get4PUrl(0, $id);
                        my $url  = $proto.'/'.$actn;
                        if ( !(map { $url eq $_->{loc} ? ($_) : () } @pages) ) {
                            my %rec  = (
                                'loc'     => $url,
                                'lastmod' => $dbfts
                            );
                            push @pages, \%rec;
                        }
                    }
                }
            }
        }
    }
    return @pages;
}

#------------------------------------------------------------------------------
# getTemplateBlock ( )
#------------------------------------------------------------------------------
#
# Функция вывода блока для заданного уровня вложенности.
#
#------------------------------------------------------------------------------

sub getTemplateBlock {
    my ($recid, $shopid, $lvl, $uri, $title, $parent_id) = @_;
    my $tmpl_blk;
    if ($lvl !~ /^[\d]+$/ && $lvl ne 'n-1') {
        $lvl = 0;
    }
    if (1) {
    #if (!$FORCE_FALL_BACK) {
        if ($lvl >= 0 || $lvl eq 'n-1')  {
            my $path;
            if ($shopid > -1) {
                if ($lvl eq 'n-1') {
                    $path = "$MODULE_DATAPOOL/$shopid/levels/$lvl/template.html";
                } else {
                    $path = "$MODULE_DATAPOOL/$shopid/levels/$lvl/template\_$parent_id.html";
                }
            }  else {
                if ($lvl > 0) {
                    $path = "$MODULE_DATAPOOL/levels/$lvl/template\_$parent_id.html";
                }
                else {
                    $path = "$MODULE_DATAPOOL/levels/$lvl/template.html";
                }
            }
            if (-e $path) {
                open my $fh, "<", $path or die "unable to open $path: ".$!;
                my @tmpl = <$fh>;
                close $fh or die "unable to close $path: ".$!;
                my $blk_f = 0;
                my $blk;
                for my $i (0..$#tmpl) {
                    if ($tmpl[$i] =~ /\-\-\>/) { $blk_f = 0; last; }
                    if ($blk_f == 1) { $blk .= $tmpl[$i];}
                    if ($tmpl[$i] =~ /\<\!\-\-/) { $blk_f = 1; }
                }
                if ($blk ne '') {
                    $tmpl_blk = $blk;
                }
            }
        }
    }
    if (!$tmpl_blk) {
        $tmpl_blk = $DEFAULT_TMPL_BLOCK;
    }
    if ($tmpl_blk) {
        $tmpl_blk = parseTmplBlk(
            $tmpl_blk, $recid, $shopid, $lvl, $uri, $title, $parent_id );
    }
    return $tmpl_blk;
}

#------------------------------------------------------------------------------
# parseTmplBlk ( )
#------------------------------------------------------------------------------
#
# Парсинг блока шаблона.
#
#------------------------------------------------------------------------------

sub parseTmplBlk {
    my ($tmpl_blk, $recid, $shopid, $lvl, $uri, $title, $descr) = @_;
    my $_dscr;
    my $_imgp = getImgForTmplsBlock($lvl, $shopid, $recid);
    $tmpl_blk =~ s/\%blk_uri\%/$uri/gi;
    $tmpl_blk =~ s/\%img_path\%/$_imgp/gi;
    if ($FILTERS_MODE == 1) {
        my $f_tag;
        my $f_inx = getFilterIndex();
        my @f_vals = getFilterData($shopid, $recid, $f_inx );
        my $_title = $f_vals[0] && $f_inx > 1 ? $f_vals[0] : $title;
        my $_descr;
        if ($f_inx > 1) {
            $_descr = $f_vals[4] ? $f_vals[4] : '';
        } else {
            $_descr = $f_vals[0] ? $f_vals[0] : '';
        }
        $tmpl_blk =~ s/\%blk_t\%/$_title/gi;
        if ($_descr) {
            $_descr = parseMarkUp($_descr);
            $_dscr = "<div class=\"shopcat-margined-li-i-d\">$_descr</div>";
        }
    } else {
        $tmpl_blk =~ s/\%blk_t\%/$title/gi;
    }
    $tmpl_blk =~ s/\%blk_p\%/$_dscr/gi;
    return $tmpl_blk;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# ParseV3Vars ( )
#------------------------------------------------------------------------------
#
# Парсинг встроенных переменных.
#
#------------------------------------------------------------------------------

sub ParseV3Vars {
    my ($data) = @_;
    my $imported_content = q{};
    while ($data =~ /%V3_IMPORT_CONTENT\(([\d]+)\,([\d]+),([a-z]+|[\d]{1})\)%/s) {
        my $shop_id = $1;
        my $item_id = $2;
        my $option  = $3;

        if ($FILTERS_MODE != 1) {
            $imported_content = getTabData($shop_id, $item_id, 'n', $option);
        }
        elsif ($option =~ /^[\d]+$/) {
            my $nlev_itemid = getItemIdInPriceFile(getPriceFileFullPath($shop_id), $item_id);
            my $itemprice   = getItemCostByNLevID($shop_id, $nlev_itemid);
            my $js_module   = getSingleSetting($MODULE_NAME, 1, 'defaultnlevhidejs');

            $imported_content .= $itemprice > 0 ?
                getTabData($shop_id, $item_id, 'n', '', $option) :
                    (-e './'.$js_module ? sprintf("<script type=\"text/javascript\" src=\"%s\"></script>", $js_module) : q{});
        }

        $data =~ s/%V3_IMPORT_CONTENT\(([\d]+)\,([\d]+),([a-z]+|[\d]{1,2})\)%/$imported_content/s;
    }

    return $data;
}

END { }

1;
