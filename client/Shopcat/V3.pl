my $shopcat_is_switchedoff  = getV3PowerStatus();
my $shopcat_userarea_status = getV3UserareaStatus();
if ( $shopcat_is_switchedoff == 0 && $shopcat_userarea_status == 1) {
    my $showallflag   = 0;
    my $privateAction = -1;
    my @privActPars   = ();
    if ( defined( $env_params[0] ) ) {
        if ( $env_params[0] =~ /(-all)$/ ) {
            $showallflag = -1;
            $env_params[0] =~ s/(-all)$//g;
        }
        $privateAction = isThisShopCatAction( $env_params[0] );
    }
    if (   ( $privateAction == 1 )
        || ( defined( $env_params[0] ) && $env_params[0] eq 'shopcatalog' ) )
    {
        use Shopcat::4PUrl;

        my $show_page = 1;
        my $shopid    = 0;
        my $id        = 0;

        if ( $privateAction == 1 ) {
            @privActPars = getActionParams( $env_params[0] );
        }
        else {
            @privActPars = ( $env_params[2], $env_params[1] );
        }

        if ( defined( $privActPars[0] ) && $privActPars[0] =~ /^[\d]+$/ ) {
            $shopid = $privActPars[0];
        }
        if ( defined( $privActPars[1] ) && $privActPars[1] =~ /^[\d]+$/ ) {
            $id = $privActPars[1];
        }

        if ( checkCatIsItem($shopid, $id) ) {
            my $itemprice = getItemCostByNLevID( $shopid, $id );
            if ( $itemprice == -1 ) {
                $show_page = 0;
            }
        }
        if ( $show_page == 1 ) {
            print $co->header(
                -type            => 'text/html; charset=UTF-8',
                -Last_Modified   => $global_last_modified,
                '-Expires'       => $global_last_modified,
                '-Cache-Control' => 'max-age=0, must-revalidate'
            );
            showV3HomePage( $id, $shopid, $env_params[0], $showallflag );
            exit;
        }
    }

    if ( defined( $env_params[0] ) && $env_params[0] eq 'shopcatloaditems' ) {
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        my $shopid  = 0;
        my $catid   = 0;
        my $pagenum = 0;
        if ( defined( $env_params[1] ) && $env_params[1] =~ /^[\d]+$/ ) {
            $shopid = $env_params[1];
        }
        if ( defined( $env_params[2] ) && $env_params[2] =~ /^[\d]+$/ ) {
            $catid = $env_params[2];
        }
        if ( defined( $env_params[3] ) && $env_params[3] =~ /^[\d]+$/ ) {
            $pagenum = $env_params[3];
        }
        print getVarntsPage( $shopid, $catid, $pagenum );
        exit;
    }

    if ( defined( $env_params[0] ) && $env_params[0] eq 'shopcatloadcats' ) {
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        my $shopid = 0;
        my $catid  = 0;
        if ( $env_params[1] =~ /^[\d]+$/ ) { $shopid = $env_params[1]; }
        if ( $env_params[2] =~ /^[\d]+$/ ) { $catid  = $env_params[2]; }
        print doCatsLoad( $shopid, $catid );
        exit;
    }

    if ( defined( $env_params[0] ) && $env_params[0] eq 'shopcatsearch' ) {
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        my $search_query = $co->param("searchquery");
        $search_query =~ s/[\$\%\t\r\n\^]+/\_/g;
        print getSearchResults($search_query);
        exit;
    }

    if ( defined( $env_params[0] ) && $env_params[0] eq 'shopcatfilter' ) {
        my $filter = $env_params[1];
        my $cookie;
        if ( $filter =~ m/^[\d]+$/ && ( $filter >= 0 && $filter <= 11 ) ) {
            $cookie = modifyFilter( $co, $filter );
        }
        print $co->redirect( -uri => $ENV{HTTP_REFERER}, -cookie => $cookie );
        exit;
    }

    if ( defined( $env_params[0] ) && $env_params[0] eq 'shopcatsetfilter' ) {
        my $cookie;
        my $filter      = $env_params[1];
        my $showallflag = 0;
        if ( $filter =~ m/^[\d]+$/ && ( $filter >= 0 && $filter <= 11 ) ) {
            $cookie = modifyFilter( $co, $filter );
        }
        print $co->header(
            -type            => 'text/html; charset=UTF-8',
            -cookie          => $cookie,
            -Last_Modified   => $global_last_modified,
            '-Expires'       => $global_last_modified,
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );
        showV3HomePage(
            reverse( getActionParams( 'index' ) ),
            'index',
            $showallflag,
            ( $filter + 1 )
        );
        exit;
    }
}
