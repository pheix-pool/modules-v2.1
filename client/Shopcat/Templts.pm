package Shopcat::Templts;

use strict;
use warnings;

##########################################################################
#
# File   :  Templts.pm
#
##########################################################################
#
# Щаблоны для вывода данных в пользовательской части для Shopcat::V3
#
##########################################################################

our ( @ISA, @EXPORT );

BEGIN {
    use lib '../';
    use lib 'libs/modules';
    use lib '../.sys_libs';

    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
      getLightNaviItem
    );
}

#------------------------------------------------------------------------------

use Encode;
use utf8;
use open qw(:std :utf8);
use Pheix::Tools;

#------------------------------------------------------------------------------
# getLightNaviItem ( $itemid, $itemname, $subitemsHTML )
#------------------------------------------------------------------------------
#
# $itemid - идентификатор корневой категории (level=0).
# $itemname - название корневой категории (level=0).
# $subitemsHTML - список подкатегорий первого уровня в HTML (level=1).
# Шаблон вывода блока 'категория-подкатегории' для навигационного меню слева
# Функция возвращает шаблон в формате HTML
#
#------------------------------------------------------------------------------

sub getLightNaviItem {
    my $templatecotent;
    my ($itemid, $itemname, $subitemsHTML, $lu) = @_;
    if ($subitemsHTML) {
        $subitemsHTML =~ s/\<br\>/\<\/li\>\<li\>/gi;
        $templatecotent = qq~
        <li class="level nav-5  no-level-thumbnail"><a name="id$itemid"></a>
            <div class="spoiler-wrapper">
                <div class="spoiler folded"><a href="javascript:void(0);">$itemname</a></div>
                <div class="spoiler-text" id="$itemid">
                    <div class="spoiler-text-block">
                        <ul class="spoilerlist"><li>$subitemsHTML</li></ul>
                    </div>
                </div>
            </div>
        </li>
    ~;
    } else {
        my $_href = $lu || "shopcatalog\_$itemid.html";
        $templatecotent = qq~<li class="first"><a href="$_href" title="$itemname">$itemname</a></li>~;
    }
    return $templatecotent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getCatTreeTable ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Шаблон дерева каталога в виде таблицы
# Функция возвращает шаблон в формате HTML
#
#------------------------------------------------------------------------------

sub getCatTreeTable {
    my $templatecotent = '';
    my $DISP_IN_RECORD = 1;
    my $filepath       = $_[0];
    my $count          = 0;

    if ( -e $filepath ) {
        open( CATFILEDESCRFH, "<$filepath" );
        my @data = <CATFILEDESCRFH>;
        close CATFILEDESCRFH;

        my @curr_ids_stamp = ();
        for ( my $i = 0 ; $i <= $#data ; $i++ ) {
            my $record = $data[$i];

            # строим дерево категорий

            my @arr = split( /\|/, $record );
            for ( my $j = $DISP_IN_RECORD ; $j <= $#arr - 1 ; $j++ ) {
                my $id = 0;
                if ( $arr[$j] =~ /\[([\d]+)\]/ ) {
                    $id = $1;
                    my $index = $j - $DISP_IN_RECORD;
                    push( @curr_ids_stamp, -1 );
                    if ( $curr_ids_stamp[$index] != $id ) {

                        my $catname = $arr[$j];
                        if ( $catname =~ /\[\*\*\]/ ) { next; }
                        $curr_ids_stamp[$index] = $id;
                        $catname =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                        my $otstup = 50 * ($index) . "px";
                        my $catHTML = qq~
                            $catname
                        ~;

                        if ( $index > 0 ) {
                            $catHTML = qq~
                             <a href="javascript:doSubCatsLoad('$id', '$arr[0]')">$catname</a>
                            ~;
                        }
                        $templatecotent .= qq~
                        <div class="shopcatfullcatitem"
                             style="margin-left:$otstup">
                             <a name="anchor$id"></a>$catHTML
                             <div id="loaddiv$id"></div></div>
                        ~;
                        $count++;
                    }
                }
            }

            # строим дерево категорий

        }
    }
    else {
        $templatecotent = qq~
            <tr><td><p class=error>$filepath не найден!</p></td></tr>\n~;
    }
    $templatecotent = qq~
    <script>
    function doSubCatsLoad(into, catid) {
    if (jQuery("#loaddiv"+into).contents().length == 0)
      {
        jQuery("#loaddiv"+into).load("$ENV{SCRIPT_NAME}?action=shopcatloadcats&amp;shopid="+catid+"&amp;catid="+into+"&amp;rand="+Math.random(), function(responseTxt, statusTxt, xhr){
            if(statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
        });
      } else {
        jQuery("#loaddiv"+into).empty();
      }
    }
    </script>
    <div class="pageheader"><h3>Каталог</h3></div>\n
    <div class="shopcatcontainer">
        $templatecotent
    </div>
    ~;
    return $templatecotent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getNM1Template ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Шаблон для уровня каталога N-1
# Функция возвращает шаблон в формате HTML
#
#------------------------------------------------------------------------------

sub getNM1Template {
    my $templatecotent = qq~
      <div class="shopcattopchainnavitempl">%navi%</div>\n
      <div class="shopcatheader"><h3>%header%</h3></div>\n
      <div class="shopcatcatmodellist"><ol class="products-list" id="products-list">%variants%</ol></div>\n
      <div class="shopcatbotchainnavitempl">Обратно в: %navi_bottom%</div>\n
    ~;
    return $templatecotent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDynoTemplate ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Динамический шаблон для уровня каталога < N-1
# Функция возвращает шаблон в формате HTML
#
#------------------------------------------------------------------------------

sub getDynoTemplate {
    my $templatecotent = qq~
     <div class="shopcattopchainnavitempl">%navi%</div>\n
     <div class="shopcatheader"><h3>%header%</h3></div>\n
     <div class="shopcatlist"><ul>%rows_n_columns%</ul></div>\n
     <div class="shopcatbotchainnavitempl">%navi%</div>\n
    ~;
    return $templatecotent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getVariants ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Шаблон для вкладки Варианты (Модели в линейке)
# Функция возвращает шаблон в формате HTML
#
#------------------------------------------------------------------------------

sub getVariants {
    my $templatecotent = qq~
    <li class="item" itemscope itemtype="http://schema.org/product">
    <div class="product-wrapper">
        <div class="product-image-container">
        <a href="%linkurl%" title="" class="product-image" style="border:none;">
            <img src="%img_path%"
                 alt="%title_descr%"
                 width="150"
                 height="150"
                 title="%title_descr%"/></a>
        </div>
        <div class="product-shop">
          <div class="f-fix">
            <div class="list-left">
            <p class="product-name">
            <a href="%linkurl%" title="%cat_name%" itemprop="name">%cat_name%</a></p>
            <div class="desc std">
                <div itemprop="description">%cat_descr%</div>
            </div>
          </div>
          <div class="list-right">
            <div class="price-box" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <span class="regular-price"><span class="price">%itemprice%</span></span>
                <div class="clear"></div>
            </div>
            <div class="ratings"><p class="rating-links"><a href="#">Артикул: %artikul%</a></p></div>
            <div class="moreinfocont" style="padding:0 7px 7px 0;">
                <button type="button" title="Подробнее..."
                        class="button btn-more"
                        onclick="javascript:top.location.href='%linkurl%'">
                <span><span class="moreinfospan">Подробнее...</span></span></button>
            </div>
          </div>
          <div class="clear"></div>
        </div>
    </div></div>
    </li>
    ~;
    return $templatecotent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getNTemplate ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Шаблон для уровня каталога N-1
# Функция возвращает шаблон в формате HTML
#
#------------------------------------------------------------------------------

sub getNTemplate {
    my $templatecontent = qq~
      <script type="text/javascript">
        jQuery(document).ready(function () {
         jQuery('#horizontalTab').easyResponsiveTabs({
            type: 'default',
            width: 'auto',
            fit: true,
            closed: 'accordion',
            activate: function(event) {
                var \$tab = jQuery(this);
                var \$info = jQuery('#tabInfo');
                var \$name = jQuery('span', \$info);
                \$name.text(\$tab.text());
                \$info.show();
            }
         });

         jQuery('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
         });
       });
      </script>
    <div class="shopcattopchainnavitempl">%navi%</div>\n
    <div class="product-view" itemscope itemtype="http://schema.org/Product">
    <div class="product-essential">
      <div class="product-img-box">
       <div class="product-box-customs">
        <div class="product-image">
            <img class="big" src="%slider%"
                 width="250" height="250" alt='' title="" />
        </div>
       </div>
      </div>
      <div class="product-shop">
       <div class="product-name">
        <div class="shopcatposition">%header%</div>
       </div>
       <p class="availability in-stock">Наличие: <span>есть в наличии!</span></p>
       <div class="add-to-cart">
        <table border=0 width="100%" >
        <tr>
         <td><span class="regular-price" itemprop="price" id="product-price-14"> <span class="price">%price%</span></td>
         <td align="right"><button type="button" title="Купить!" class="button btn-cart" onclick="top.location.href='%byuitnow%';">
         <span><span class="buyittabspan">Купить сейчас!</span></span>
         </button></td></tr></table>
       </div>
       <div class="clear"></div>
       <div class="short-description"><h2>Информация</h2>
        <div class="std" itemprop="description">%information%</div>
       </div>
      </div>
     </div>
    </div>
    <div id="horizontalTab" style="padding: 0 0 0 0">
       <ul class="resp-tabs-list">
        <li>Технические характеристики</li>
        <li>Информация</li>
        <li>Аналогичные модели</li>
        <li>Файлы и руководства</li>
       </ul>
       <div class="resp-tabs-container" >
        <div>%parameters%</div>
        <div>%information%</div>
        <div><ol class="products-list" id="products-list">%othermodels%</ol></div>
        <div>%files%</div>
       </div>
      </div>
    <div class="shopcatbotchainnavitempl">Обратно в: %navi_bottom%</div>\n
    ~;
    return $templatecontent;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getFiltersBlock ( @f_rs )
#------------------------------------------------------------------------------
#
# @f_rs - список фильтров
# Функция возвращает блок фильтров для шаблона в формате HTML
#
#------------------------------------------------------------------------------

sub getFiltersBlock {
    my @f_rs = @_;
    my $filters;
    my $fb_mode = Pheix::Tools::getSingleSetting( 'Shopcat', 1, 'fallbackmode' );
    my $f_limit = Pheix::Tools::getSettingChildNum('Shopcat', 0, 'filters');
    for my $i (0..$f_limit) {
        my $cnt   = $i+1;
        my $f_num = sprintf("%02d", $i);
        my $f_name  = 'filter-'.(sprintf "%02d", $cnt);
        my $f_label = 'filters/'.$f_name;
        my $f_descr = Pheix::Tools::getSingleSetting('Shopcat', 0, $f_label);
        if ( $f_descr eq '0' ) {
            next;
        }
        if (!$fb_mode) {
            if ($f_rs[$i]) {
                $filters .= qq~
                <div class="col-xs-4 col-sm-3 col-md-1 factories-filters-cell fc-r">
                  <div class="factories-filters-cell-ob-selected">
                    <a href="shopcatfilter_$i.html"><img src="images/artihome/themesgale/icons/filters/filter\_$f_num\.png" alt="$f_descr" title="$f_descr" class="grayscale"></a></div></div>
                ~;
            } else {
                $filters .= qq~
                <div class="col-xs-4 col-sm-3 col-md-1 factories-filters-cell fc-r">
                  <div class="factories-filters-cell-ob" onmouseover="showFilterDescr($cnt)" onmouseout="showFilterDescr(0)">
                    <a href="shopcatfilter_$i.html"><img src="images/artihome/themesgale/icons/filters/filter\_$f_num.png\" alt="$f_descr" title="$f_descr" class="grayscale"></a></div></div>
                ~;
            }
        } else {
            if ($f_rs[$i]) {
                my $selected = $fb_mode == 1 ? 'selected' : '';
                $filters .= qq~
                    <option class="shopcat-sel-filters-selected" $selected onclick="top.location.href='/shopcatfilter_$i.html';">$f_descr</option>
                ~;
            } else {
                $filters .= qq~
                    <option onclick="top.location.href='/shopcatfilter_$i.html';">$f_descr</option>
                ~;
            }
        }
    }
    if (!$fb_mode) {
        return qq~
            <div class="row">$filters</div>
        ~;
    } else {
        return qq~
            <select class="shopcat-sel-filters">$filters</select>
        ~;
    }
}

#------------------------------------------------------------------------------

END { }

1;
