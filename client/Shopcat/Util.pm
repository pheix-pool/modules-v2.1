package Shopcat::Util;

use strict;
use warnings;

##########################################################################
#
# File   :  Templts.pm
#
##########################################################################

our ( @ISA, @EXPORT );

BEGIN {
    use lib '../';
    use lib 'libs/modules';

    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
        getValueFromRedis
        setValueToRedis
        getModuleDb
        getPageTemplate
        getPriceFileFullPath
        getV3FilesArr
        parseMarkUp
    );
}

#------------------------------------------------------------------------------

use Redis;
use Encode;
use utf8;
use open qw(:std :utf8);
use Pheix::Tools;

#------------------------------------------------------------------------------

my $REDIS_EXPIRE         = 7776000;
my $MODULE_DB_FILE       = 'conf/system/shopcat2.tnk';
my $MODULE_USER_TEMPLATE = 'conf/config/template.html';
my $MODULE_PRICEPOOL     = 'conf/system/pricepool/';     # WARNING!!! / is needed at the end


#------------------------------------------------------------------------------
# getValueFromRedis($key)
#------------------------------------------------------------------------------
#
# Получить значение из Redis.
#
#------------------------------------------------------------------------------

sub getValueFromRedis {
    my ($key) = @_;
    my $rc;
    my $redis;
    eval {
        $redis =
            Redis->new( server => '127.0.0.1' . ':' . 6379 );
    };
    if ($redis) {
        my $value = $redis->get( $ENV{SERVER_NAME} . ':'.$key );
        if ($value) {
            $rc = decode( "utf8", $value );
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------
# setValueToRedis($key, $value)
#------------------------------------------------------------------------------
#
# Записать значение в Redis.
#
#------------------------------------------------------------------------------

sub setValueToRedis {
    my ($key, $value) = @_;
    my $rc;
    my $redis;
    eval {
        $redis =
            Redis->new( server => '127.0.0.1' . ':' . 6379 );
    };
    if ($redis && $value) {
        $redis->set(
            $ENV{SERVER_NAME} . ':'.$key  => encode( "utf8", join( "", $value ) ),
            'EX' => $REDIS_EXPIRE
            );
    }
}

#------------------------------------------------------------------------------
# getModuleDb()
#------------------------------------------------------------------------------
#
# Получить данные из файла БД модуля.
#
#------------------------------------------------------------------------------

sub getModuleDb {
    my @rc;
    my $redis;
    eval {
        $redis =
            Redis->new( server => '127.0.0.1' . ':' . 6379 );
    };
    if ($redis) {
        my $tmpl = $redis->get( $ENV{SERVER_NAME} . ':moduledb:shopcat2' );
        if ($tmpl) {
            @rc = split( /[\r\n]+/, decode( "utf8", $tmpl) );
        }
    }
    if ( !@rc ) {
        if ( -e $MODULE_DB_FILE ) {
            open my $ifh, "<:encoding(UTF-8)", $MODULE_DB_FILE;
            @rc = <$ifh>;
            close $ifh;
            if ($redis) {
                $redis->set(
                    $ENV{SERVER_NAME} . ':moduledb:shopcat2' => encode( "utf8", join( "", @rc ) ),
                    'EX' => $REDIS_EXPIRE
                );
            }
        }
        else {
            print __FILE__. ":"
              . __LINE__ . " - "
              . $MODULE_DB_FILE
              . " not found";
        }
    }
    return @rc;
}

#------------------------------------------------------------------------------
# getPageTemplate()
#------------------------------------------------------------------------------
#
# Получить шаблон страницы в виде массива.
#
#------------------------------------------------------------------------------

sub getPageTemplate {
    my @rc;
    my $redis;
    eval {
        $redis =
            Redis->new( server => '127.0.0.1' . ':' . 6379 );
    };
    if ($redis) {
        my $tmpl = $redis->get( $ENV{SERVER_NAME} . ':templ:admin' );
        if ($tmpl) {
            @rc = map { $_."\n" } split( /[\r\n]+/, decode( "utf8", $tmpl) );
        }
    }
    if ( !@rc ) {
        my $t = $MODULE_USER_TEMPLATE;
        #if ($ENV{SERVER_NAME} !~ /tarqvara\.apopheoz\.ru/) {
        #    $t = 'conf/config/template-2.html';
        #}
        if ( -e $t ) {
            open my $ifh, "<:encoding(UTF-8)", $t;
            @rc = <$ifh>;
            close $ifh;
            if ($redis) {
                $redis->set(
                    $ENV{SERVER_NAME} . ':templ:admin' => encode( "utf8", join( "", @rc ) ),
                    'EX' => $REDIS_EXPIRE
                );
            }
        }
        else {
            print __FILE__. ":"
              . __LINE__ . " - "
              . $MODULE_USER_TEMPLATE
              . " not found";
        }
    }
    return @rc;
}

#------------------------------------------------------------------------------
# getPriceFileFullPath ( $shopid )
#------------------------------------------------------------------------------
#
# Получить полный путь до файла прай-листа (pricepool/*) по идентификатору
# прайс-листа ($shopid).
#
#------------------------------------------------------------------------------

sub getPriceFileFullPath {
    my $shopid  = $_[0] || return -1;
    my @rc;
    my $errmes;
    my $redis;
    eval {
        $redis =
            Redis->new( server => '127.0.0.1' . ':' . 6379 );
    };
    if ($redis) {
        my $pf = $redis->get( $ENV{SERVER_NAME} . ':pricefilepath:' . $shopid );
        if ($pf) {
            @rc = split( /[\r\n]+/, decode( "utf8", $pf) );
        }
    }
    if ( !@rc ) {
        if ( -e $MODULE_DB_FILE ) {
            open( my $ifh, "<:encoding(UTF-8)", $MODULE_DB_FILE );
            @rc = <$ifh>;
            close($ifh);
            if ($redis) {
                $redis->set(
                    $ENV{SERVER_NAME} . ':pricefilepath:' . $shopid => encode( "utf8", join( "", @rc ) ),
                    'EX' => $REDIS_EXPIRE
                );
            }
        }
        else {
            $errmes .= __FILE__. ":"
              . __LINE__ . " - "
              . $MODULE_DB_FILE
              . " not found";
        }
    }
    if (@rc) {
        for my $record (@rc) {
            $record =~ s/[\r\n]//g;
            my @record_arr = split( /\|/, $record );
            if ( $record_arr[0] eq $shopid ) {
                my $pricefilepath = $record_arr[-1];
                $pricefilepath =~ s/(\[[\d]+\])|(\[[\*]+\])//g;
                return $MODULE_PRICEPOOL . $pricefilepath;
            }
        }
        $errmes .= "\$#rc=$#rc, no $shopid record";
    } else {
        $errmes .= "\@rc is empty";
    }
    return $errmes
}

#------------------------------------------------------------------------------
# getV3FilesArr($dir)
#------------------------------------------------------------------------------
#
# Функция получения списка файлов в некотором каталоге.
#
#------------------------------------------------------------------------------

sub getV3FilesArr {
    my ($dir) = @_;
    my @files;
    $dir =~ s/\\/\//g;
    if (-e $dir) {
        my $dh;
        opendir $dh, $dir;
        @files = grep {(!/^\.+$/) && !(-d "$dir/$_")} readdir $dh;
        closedir $dh;
    }
    return @files;
}

#------------------------------------------------------------------------------
# parseMarkUp($str)
#------------------------------------------------------------------------------
#
# Парсинг markup разметки.
#
#------------------------------------------------------------------------------

sub parseMarkUp {
    my ($str) = @_;
    $str =~ s/\*\s{0,}\*\s{0,}\*\s{0,}([^\*]+)\s{0,}\*\s{0,}\*\s{0,}\*/\<i\>\<b\>$1\<\/b\>\<\/i\>/g;
    $str =~ s/\*\s{0,}\*\s{0,}([^\*]+)\s{0,}\*\s{0,}\*/\<b\>$1\<\/b\>/g;
    $str =~ s/\*\s{0,}([^\*]+)\s{0,}\*/\<i\>$1\<\/i\>/g;
    return $str;
}

#------------------------------------------------------------------------------

END { }

1;
