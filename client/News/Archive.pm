package News::Archive;

use Moo;
use strictures 2;
use namespace::clean;

use JSON::PP;
use File::Slurp;
use Encode;

has name         => (is => 'ro', default => 'News');
has description  => (is => 'ro', default => 'News feed');
has keywords     => (is => 'ro', default => 'news, feed, announcement, rss');
has databasepath => (is => 'ro', default => 'conf/system/news.tnk');
has installpath  => (is => 'ro', default => 'conf/system/install.tnk');
has template     => (is => 'ro', default => 'conf/config/template.html');

sub get_power_status {
    my $self = shift;

    my $status = -1;

    return $status unless -e $self->installpath;

    my @installed_modules = read_file($self->installpath);

    foreach my $module (@installed_modules) {
        my @install_details = split(/\|/, $module);

        $status = $install_details[4] if @install_details && $install_details[2] eq $self->name;

        last if defined $status && $status > -1;
    }

    return $status;
}

sub show_start_page {
    my $self = shift;

    my @seo_data_strings = (1, $self->name, $self->description, $self->name, $self->keywords);

    eval { @seo_data_strings = Seo::Tags::getSeoTags($ENV{REQUEST_URI}); 1 }
      or do {
          my $error = $@;

          warn $error;
      };

    return sprintf("%s render failure: %s file not found!", $self->name, $self->template) unless -e $self->template;

    my @template = read_file($self->template);

    my ($seoflag, $name, $title, $metadesc, $metakeys) = @seo_data_strings;

    my $content = $self->get_news_list;

    foreach my $string (@template) {
        $string =~ s/\%page_title\%/$title/;
        $string =~ s/\%metadesc\%/$metadesc/;
        $string =~ s/\%metakeys\%/$metakeys/;

        $string = Pheix::Pages::fillCommonTags($string, $ENV{REQUEST_URI});

        $string =~ s/\%content\%/$content/;
    }

    return join "\n", @template;
}

sub get_news_list {
    my $self = shift;

	return sprintf("<h4>%s</h4><p class=\"standart_text\">%s</p>", $self->name, $self->description);
}

sub get_json {
    my $self = shift;

    my @news;

    return '[]' unless -e $self->databasepath;

	my @database = read_file($self->databasepath);

    foreach my $row (@database) {
        my @details = split(/\|/, $row);

        push @news, {
            header      => decode('utf8', $details[7]),
            description => decode('utf8', $details[8]),
            link        => decode('utf8', $details[10]),
        };
    }

    return JSON::PP->new->encode(\@news);
}

1;
