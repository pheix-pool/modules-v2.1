my $news = News::Archive->new;
my $switchedoff = $news->get_power_status;

if ($switchedoff == 0) {
    if (@env_params && $env_params[0] eq 'news') {
        die 'CGI object is missing' unless defined $co;

        print $co->header(
            -type            => 'application/json; charset=UTF-8',
            -Last_Modified   => $global_last_modified // q{},
            '-Expires'       => $global_last_modified // q{},
            '-Cache-Control' => 'max-age=0, must-revalidate'
        );

        print $news->get_json;

        exit;
    }
}
